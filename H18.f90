program HED18                                 !Jarl Giske

!FORTRAN Program code for decisions by emotion.
!Developed at Dept of Biology, University of Bergen, Norway
!The program code was developed for this paper:
!  Giske, J., M. Mangel, P. Jakobsen, G. Huse, C. Wilcox & E. Strand 2003.
!  Explicit trade-off rules in proximate adaptive agents. Evol Ecol Res 5:835-864.
!and then further developed to the present version, which was first published here:
!  Giske J, S Eliassen, � Fiksen, PJ Jakobsen, DL Aksnes, C J�rgensen & M Mangel 2013.
!  Effects of the emotion system on adaptive behavior. Am. Nat. 182:689-703.
!  The online supplementary for that paper includes a description of the program according to the ODD protocol:
!  http://www.jstor.org/stable/full/10.1086/673533#apa.

!The same code is used in:
!  Giske J, S Eliassen, � Fiksen, PJ Jakobsen, DL Aksnes, M Mangel & C J�rgensen 2014.
!  The emotion system promotes diversity and evolvability. Proc R Soc B

!-----------------------------------------------------------------------------------------------
!Explanation to file "Hed18.f90"
!
!The program HED18 consists of these files:
!  "Hed18.f90"               is the main FORTRAN program.
!  "Commonfish.txt"          is a commonblock that specifies array dimensions for arrays
!                            in use in several subroutines
!  "i101-parameters.txt"     specifies most of the variables in the program.
!
!The FORTRAN code in HED18 is organised by a short main program that accesses several subroutines.
!The program is organised in three main loops.
!first (outer) loop: experiments
!initiates the program and opens the parameter file "i101-parameters.txt" through the
!        subroutine SRinitrun. It can start by reading one or more saved population sets
!        or by generating a randomly initiated population. The saved descriptors must be
!        found in the files
!  "readgenes-1.txt" (and higher numbers if several populations are to be read simulatneously)
!  "readperceptiondata.txt"
!        The readgenes file(s) contain data on each individal to start the first genration, while
!        readperceptiondata.txt contains average perception data for the stored population.
!        Then it loops through one or more experiments (simulations) with each of the equation sets.
!        Output files are named "Ex-tag-OnHED18-???.txt". Here
!        x    means experiment (expmt) number in this loop.
!        tag  is a descriptor of the current (series of) experiment(s) (6 characters)
!        O    means output file. These have a number n
!        n    is the FORTRAN file number in Hed18.f90
!        ???  is a descriptive text for the contents of the file.
!
!first loop: generations
!is the Genetic Algorithm simulation of the evolution of the population gene pool. It cycles
!        over a number of generations specified by the variable "generations". The two main
!        subroutines in this loop is SRlifecycle that follows the entire population through the
!        time steps of a generation, and SRneweggs that produces the gene pool for next generation.
!        The subroutine SRopenclose is doing the file handling at the end of each generation (and
!        sometimes also during a generation).
!        Most of the biology occurs within the subroutine SRlifecycle. In this subroutine we
!        find the last major loop.
!second loop: ages in a life
!is the Individual-based model which cycles over the "flifespan" number of ages (time steps)
!        in a generation. First, SRinitage gives conditions that do not change during the age.
!        Then SRhedonic interpretes the environment and determines the dominant feeling
!        and the next habitat selection decision for each organism alive. This decision allows
!        calculation of the spatial distribution of the population in the current time step
!        in SRhabitat. Growth is determined for each individual in SRgrowth on basis of stomach
!        capacity and the density-dependent functions in SRhabitat.

!third loop: individuals
!many places the program loops through all individuals in the population
!
!After 2nd loop, i.e. at the end of the final age in a generation, the subroutine SRfecundity
!determines the number of eggs to be produced by each survivor. The actual formation of eggs is done
!in SRneweggs at the end of a generation in 1st loop.

!There has been a gradual shift in concepts throughout the code development,
!and many old names and explanatory texts include these old names.
!Every time the code says            you should read
!    feeling or affect                  emotion
!     dominant affect        Global Organismic State (GOS)

!!
!Jarl Giske, September 2001/March 2008/Autumn 2010/May 2014
!Jarl.Giske@bio.uib.no
!!The core program was developed during my sabbatical research period with Marc Mangel, UCSC, spring 2001
!-----------------------------------------------------------------------------------------------

!Description:
!A population of fish eats zooplankton, which is renewed each time step.
!The zooplankton prey is forced to follow a vertical migration pattern, where 1/2 of the depths are occupied in a time step.
!The fish individuals may choose to move to new habitats FLIFESPAN times per generation.
!The individuals relocate one by one in the time step, and habitat quality is re-calculated after each individual relocation.
!The environmental information impacts the two emotions (in the code often called feelings) fear and hunger, and the organism will
! try to reduced the strength of the strongest of these two.
!The depth choice that most likely will (strongly reduce) the strongest emotion (also called affect some place in the code) is
!  selected, new choices are made FLIFESPAN times.
!The GA determines the genes for the affects, which lasts a life.
!The environment goes through a diel light cycle DIELCYCLES times during FLIFESPAN, with fluctuations in light intensity
!  and prey distribution. (7 cycles is standard.)
!The habitat profitability depends on density of fish, both in feeding rate and mortality risk.
!Feeding rate is also influenced by density of prey, light intensity, and stomach capacity.
!Individual fecundity may also be limited by body size capacity, making the optimal movement rule also state-dependent.
!Reproduction occurs at the end of the life, i.e. in the last time step.
!Fecundity is density-dependent to keep population below FISH.
!Mortality and growth may be set to be more generous in the first few generations, to reduce risk of extinction before adaptation

!fstatus: meaning of the signal
!  1: the organism is alive
!  0: this organism number is not in use in the generation (fpop < n <= FISH)
! -1: organism died of predation
! -2: organism died of starving down to structural body mass
! -3: organism died of starving down below half egg mass
! -5: organism given new number. Replaced lower-numbered dead ind in SRgenecreate



implicit none             !only declared variables are allowed
include 'Commonfish.txt'  !include commonblock of arrays
!variables in main program
character(6) runtag       !part of file name of a series if files, to describe a characteristic of this (series of) simulation(s)
character(8) deviation    !state of generation off normal
character(10) code1        !tag combining runtag and experiment
character(15) code2       !tag combining runtag, experiment, and generation
character(37) string10    !file name of file 10 during errorsearch
character(46) string29    !file name of file 29
character(42) F17         !file name of file 17
integer alleleconv        !1 for 1:1, 2 for F = (10*a)^2/10, interpretation of allele value
                          !   in affect and hedonic functions
integer attackduration    !number of time steps a school of predators remain, depending on flifespan
integer behavstep,behavmax !step and max for files for individual behaviour-data
integer brain             ! 1 for mental states and attention, otherwise hedonic tones
integer cop2egg           !number of copepods needed to make 1 fish egg, weight of an egg and a newborn fish
integer corrplots         !switch for making correlation plots in final generations
integer curve             !1 for sigmoid, 2 for linear, 3 for gamma function relationship
                          !   between perception and affect, 4 for predefined gamma functions
integer densdep           !1 if growth and survival are density dependent, else 0 (reversed 9June08)
integer dep,a,g,p,class1 !dummies for depth, age and generation
integer depth             !number of depths to choose among
integer descendants(0:10) !if file = 1; number of descendants from each starting population (max 10)
integer dev               !numeric flag for current environment (normal or deviant)
integer dielcycles        !number of days and nights in a life cycle
integer diellight         !switch for using natural light regime (=1) or constant surface light
integer dummy,begin,ind,i !dummies
integer during            !on/off switch for rich prints DURING the evolution
integer env               !signal that end of generations are coming and environment data shall be printed for each age
integer expmt,l           !current serial run
integer fearplot          !1 if files for fearplots are to be made, 0 if not
integer file,readfiles    !file = 1 means read start data from file, readfiles is number of files to read
integer fish,fpop,fnewpop !max and actual number of fish individuals at the start of a generation
integer flifespan         !current number of ages in a life (see maxlife)
integer maxlife           !number of ages in a life near end of simulations
integer fmaxweight        !maximum body size of a fish
integer fsurvivors        !number of individuals surviving predation in a generation
integer gABM              !number of genes in an affect multiplication factor of age or body mass
integer gainfunction      !functional relationship of density-dependency in feeding, 1:linear, 2: sqrt, 3:N^2
integer gen               !loop variables
integer gen0              !the generation before the start of the simulation
integer generations,gg    !last generation in GA
!integer immigration       !number of external individuals that enter the population each generation
integer maxpartners       !maximum number of males a female will check before mating with the largest
integer Mboundary,HFboundary,FHtest,statetest  !switch for experiments which can be done at the end of last gener.
integer memory            !0 if no memory of past feelings, 1 if dynamic and individual-based memory ability
integer motherlength      !length of motherstring: number of mothers to be remembered
integer nextfile          !alternating file label (file 10A or 10B) during errorseach
integer nomut             !number of generations at end of simul without mutations
integer partnerchoice     !1 for random selection, 2 for the largest and 3 for the genetically most similar
integer zrange             !max number of depth cells above or below that an agent can move to in a time step
integer realgen           !the generation number including a previous simulation
integer restart           !1 if genes are recently read from file, 0 otherwise
integer riskfunction      !functional relationship of density-dependency in predation risk,  1:linear, 2: sqrt, 3:N^2
integer screenplot        !1 if vertical distribution shall be printed at screen each age
integer soft              !1 if soft selection first generations, 0 if brutal evol from start
integer softM,softG,softE !2 if soft selection still continues, 0 when finished
integer srerr             !dummy that identifies the number of a SR that reports error
integer swFsize,swFlight,swFothers,swFmort !on off switches of affect functions for testing of their importance
integer swHsize,swHstomach,swHfood !on off switches of affect functions for testing of their importance
integer tempregime        !0 if constant in all depths, 1 if depth-dependent
integer test99,test99now  !If test99 = 1, a SR-identifier is printed each time a subroutine is called
integer teststart         !nmber of generations BEFORE errorsearch starts if test99 = 1
integer totalsenses       !number of input information senses in a fish
integer nzlast,nzero,cpop,copdvm
integer zavgstart,zavgkids,zavgsurv
real nzloss,daylight
real allmax,allmin        !max and min allele value of any affect and hedonic gene
real attackrate,attackrate0,attackfactor !parameters describing the occasional entrance and effect of a school of predators
real auto                 !proportional scaling factor for environmental autocorrelation over short time scales
real AVGstdev,sdgABM,sdAfear,sdAhunger,sdHfear,sdHhunger,sdAHunused  !average standard deviation
                          !of affect and hedonic genes in generation
real beamatt,contrast,preyarea,viscap,eyesat  !vision variables
real BMscale              !exponential scaling factor for size-dependent mortality risk
real density              !scaling factor in feeding and mortality in absence of density-dependent environment
real development          !fraction of fbodymax which is non-reusable (structural, developmental growth)
real errorP              !inherent uncertainty in measurement of strength of perception stimulus
real flivingcost          !metabolic cost, to be scaled with body size and number of time steps
real fmultgain            !factor in determination of environmental feeding rate
real fmultrisk            !factor in determination of environmental mortality risk
real fmultrisk0,fmultgain0,eyesat0 !desired levels of these variables, under soft natural selection
real fmultriskBAS         !factor in determination of environmental mortality risk, unchanged
real fpopfec              !total egg production in a generation
real GenMaxPercept(7,100) !maximum of sensory information encountered in each of 100 past generations
real hungersensitivity    !minimum hunger for the individual to detect it and respond to it. Below this, the animal is afraid
real interval,stepABM     !step between allele values (0.05 - 0.1 - 0.2 - 0.5 - 1.0)
real lightdecay           !vertical attenuation of light, per depth
real max01,max02,max03,max04,max05 !part of MaxPercept, covering 20 of 100 past generations
real maxABM,minABM        !max and min allele value of any ABM gene
real MaxPercept(7)        !maximum of sensory information encountered during 500 past generations
real maxstomcap           !maximum fraction of body mass available for undigested food
                          !  (max feeding per time step at empty stomach)
real mutrate              !current mutation rate
real mut1,mut2,mut3       !mutation rates in 1st, 2nd and 3rd part of simulation
real normal               !probability of an average generation, with normal risk and prey field
real NowMaxPercept(7)     !maximum of sensory information encountered in current generations
real p1,p2,p3,p4,p5,p6,p7 !start up "memory" of environmental conditions in past generations
real prey                 !number of prey realtive to max number of agents
real preymax              !maximum prey density in all depths at age 1 in each generation
real r                    !real dummy
real random,rand2         !random numbers
real reprocap             !generation-dependent realisation of reproinvest
real reproinvest          !fraction of final body mass of mother that is converted to gonads
real restattention        !fraction of full attention capacity left for the unused affect
                          !  (feeding attention in fear, avoidance under hunger)
real softscale            !multiplication factor for initial change in feeding rate and mort risk under soft selection
real zavgEgg,zavgR0,zsdR0
!---------------------------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------
gen0 = 0

!---------------------------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------

!begin loop over model repeats: how many "parallells" shall be run (in serial)
!do expmt = runmin,runmax
!deviantgener = 0

call SRinitrun(expmt,srerr,test99,teststart,depth,fish,totalsenses,nomut,        &
    softG,softM,generations,densdep,maxlife,fpop,gen,realgen,during,restart,         &
    cop2egg,fmaxweight,gen0,lightdecay,brain,mut1,mut2,mut3,fmultrisk,fmultgain,     &
    allmax,allmin,alleleconv,soft,softscale,zrange,BMscale,behavstep,behavmax,       &
    motherlength,auto,fmultriskBAS,interval,fsurvivors,dielcycles,development,       &
    diellight,prey,normal,density,tempregime,memory,maxpartners,curve,errorP,      &
    partnerchoice,gABM,restattention,maxstomcap,fearplot,flivingcost,gainfunction,   &
    riskfunction,beamatt,contrast,preyarea,viscap,eyesat,attackrate,attackfactor,    &
    swHsize,swHstomach,swHfood,swFsize,swFlight,swFothers,swFmort,reproinvest,       &
    corrplots,file,readfiles,runtag,screenplot,maxABM,minABM,stepABM,nzlast,nzero,   &
    copdvm,cpop,nzloss,daylight,hungersensitivity,Mboundary,HFboundary,FHtest,statetest)

!wait until last part of simulation to test for errors
test99now = test99
test99 = 0
reprocap = reproinvest !initial value of reprocap, to be changed in SRfecundity
attackrate0 = attackrate !store initial value of attackrate

!prepare tag for output files
 if (expmt > 9) then
      write(code1,1704)runtag,"-E",expmt
    else
      write(code1,1703)runtag,"-E0",expmt
 end if
!prepare one-line file of simulation summary
 write(F17, 1715)"HED18-",code1,"-o017-simul_comparison.txt"
 1715 format(A6,A10,A26)
 !1714 format(2A6,A2,I2,A26)

        !make printout of comparison of the equation sets
        open(17, file = F17)
        write(17,*) "Average result of 500 last generations:"
        write(17,1702) "code","sim","mut","gain","risk","start","end","%mort","avgR0","sdR0","kids",   &
               "nevAF","nevAM","%fearF","%fearM","ABM#a","gAF#a","PAF#a","gAH#a","PAH#a",        &
               "gHFL#a","PHFL#a","gHFO#a","PHFO#a","gHHF#a","PHHF#a","gHHO#a","PHHO#a",          &
               "avgSD","sdgABM","sdAfear","sdAhunger","sdHfear","sdHhunger","sdAHUnused",        &
               "code","BM1mDAYf","BM1mNIGf","BM50%f","BMLmDAYf","BMLmNIGf","BMfinalf",           &
               "code","z-Eggf","z1mDAYf","z1mNIGf","z50%f","zLmDAYf","zLmNIGf","zfinalf",        &
               "code","N1mDAYf","N1mNIGf","N50%f","NLmDAYf","NLmNIGf","Nfinalf",                 &
               "code","BM1mDAYm","BM1mNIGm","BM50%m","BMLmDAYm","BMLmNIGm","BMfinalm",           &
               "code","z-Eggm","z1mDAYm","z1mNIGm","z50%m","zLmDAYm","zLmNIGm","zfinalm",        &
               "code","N1mDAYm","N1mNIGm","N50%m","NLmDAYm","NLmNIGm","Nfinalm"
        1702 format(A12,A4,26A7,6A10,A12,2(A12,6A9,A12,7A9,A12,6A9))

    !------------------------------------------------------------------------------
    !                              evolution starts here
    !------------------------------------------------------------------------------

!   create subdirectories for output files near end of simulation
 !   call system('mkdir Normal')
    call system('mkdir behaviour')
    call system('mkdir experiment')
    if (fearplot == 1) then
       call system('mkdir fearplot10')
       call system('mkdir fearplot25')
       call system('mkdir fearplot50')
       call system('mkdir fearplot75')
       call system('mkdir fearplt100')
    end if

 !begin generation loop
 if (screenplot == 1) then
    write(6,*) "     "
    write(6,*) "     "
    write(6,*) "total number of generations = ", generations
    write(6,*) "current generation :          ", gen
    write(6,*) "     "
    write(6,*) "     "
 end if
   env = 0 !we are not near the end of the simulation, and environment data shall not be printed for each time step

!startup conditions
    !run through all generations in a single experiment of one equation set (The Genetic Algorithm)
    gg = generations
    !teststart = 70
    do gen = gen0+1, generations
      fsurvivors = fpop !before the first time step, no one has died
      realgen = realgen + 1 !generation number in printouts. Equals GEN if simulation not started from previous simulation
      if (gen == generations - teststart) then
          test99 = test99now
          nextfile = 1
      end if
      if (gen > generations - (teststart+1) .and. test99 == 1) then
          close (10) !close current warning file and prepare for print of errorsearch for this generation
          nextfile = 1
         !open output file
         if (nextfile == 1) then !keep memory of prevoius and current generation
             write(string10, 5001)"HED18-",code1,"-o010A-errorsearch.txt"
            else
             write(string10, 5001)"HED18-",code1,"-o010B-errorsearch.txt"
         end if
         nextfile = nextfile * (-1)
         open(10, file = string10)
         write(10,*)"current generation is ",gen
         write(10,*)" "
      end if !errorsearch is on
5001 format(A6,A10,A22)

!!set current mutation rate
mutrate = mut1

!set maximum longevity of the organisms, i.e. number of time steps
! and decision events through a life
flifespan = maxlife
attackrate = attackrate0/flifespan
attackduration = 4 !full life and full attack durations
call SRlifespan(depth,flifespan,dielcycles,copdvm,    &
     nzlast,nzero,cpop,nzloss,daylight,prey,autocop, &
     numcop,numcopBAS,surlig,autosurlig,diellight)

!     initialise memory of maximum perception values
      if(gen == gen0+1) then
         if (file == 0) then
            p1 = 0.8           !stomach fullness
            p2 = 90.           !light at depth
            p3 = 6000.         !others at depth
            p4 = 1.*fmaxweight !body mass
            p5 = 300000.       !prey at depth
            p6 = 1.*flifespan  !age
            p7 = fmultrisk     !predation risk
          else !read stored data from previous simulation
           open(103, file = "readperceptiondata.txt")
           read(103,*) p1
           read(103,*) p2
           read(103,*) p3
           read(103,*) p4
           read(103,*) p5
           read(103,*) p6
           read(103,*) p7
           close(103)
         end if

         do g = 1,100
            GenMaxPercept(1,g) = p1  !stomach fullness
            GenMaxPercept(2,g) = p2  !light at depth
            GenMaxPercept(3,g) = p3  !others at depth
            GenMaxPercept(4,g) = p4  !body mass
            GenMaxPercept(5,g) = p5  !prey at depth
            GenMaxPercept(6,g) = p6  !max age
            GenMaxPercept(7,g) = p7  !predation risk
         end do
         do i = 1,7
          MaxPercept(i) = GenMaxPercept(i,1)
         end do
       end if

     !initialise at milder environment if starting from random genes
     if (soft == 1) then
        dev = 0
        deviation = "Soft sel" !deviation = soft selection
        !initiate in mild conditions
        if (gen == 1) then
          softM = 2
          softG = 2
          softE = 2  !eye quality of agents (and their predators evolve also)
          fmultrisk0 = fmultrisk
          fmultgain0 = fmultgain
          eyesat0 = eyesat
          fmultrisk = fmultrisk0/softscale
          fmultgain = fmultgain0*softscale
          eyesat = 3.
        endif
        !sharpen conditions gradually ...
        !if (softM == 2 .and. fmultrisk < fmultrisk0) fmultrisk = fmultrisk*1.1
        if (softM > 1) fmultrisk = fmultrisk*1.07  !was 1.05 intil 1 Aug 2010
        !if (softG == 2 .and. fmultgain > fmultgain0) fmultgain = fmultgain/1.1
        if (softG > 1) fmultgain = fmultgain/1.07  !was 1.05 intil 1 Aug 010
        if (softE > 1) eyesat = 1.5*eyesat

        !... until desired level is reached
        if (fmultrisk > fmultrisk0) then
           fmultrisk = fmultrisk0
           softM = 0
          else
           if (screenplot == 1) write(6,*)"  soft mortality (abs & relative):",fmultrisk, fmultrisk/fmultrisk0
        end if
        if (fmultgain < fmultgain0) then
           fmultgain = fmultgain0
           softG = 0
          else
           if (screenplot == 1) write(6,*)"soft growth (absolute & relative):",fmultgain, fmultgain/fmultgain0
        end if
        if (eyesat > eyesat0) then
           eyesat = eyesat0
           softE = 0
          else
           if (screenplot == 1) write(6,*)"soft eyesaturation (absolute & relative):",eyesat, eyesat/eyesat0
        end if
        !stop this procedure if all variables are at right level
        if (softM + softG + softE < 1) then
            soft = 0
            if (screenplot == 1) then
                write(6,*) "  "
                write(6,*) "  "
                write(6,*) "  "
                write(6,*)" soft selection conditions ended before generation number ", realgen
                write(10,*)" soft selection conditions ended before generation number ", realgen
                write(6,*) "  "
                write(6,*) "  "
                write(6,*) "  "
            end if
        end if
     end if !Soft = 1

     !environment can vary stochastically between generations and ages
     !print information of environment in each time step near the end of the simulation
     if (generations-gen < 200) then    !to ensure not too big dataset
        if (env == 0) then   !first time this happens: open output file
          if (expmt < 10) then
               write(string29,2901)"HED18-",runtag,"-E0",expmt,"-o029-environment-each-age.txt"
            else
              write(string29,2910)"HED18-",runtag,"-E",expmt,"-o029-environment-each-age.txt"
          end if
          open(29, file = string29)
          write(29,2902)"Generation","deviation","age","time","surlight","time","fmultrisk","autorisk",  &
                        "time","foodmax","time","prey@f-max","time","temp@f-max","# alive","avg depth",  &
                        "neighbors","mortality","feeding","growth","% fear"
          env = 1
        end if
     end if
     2901 format(2A6,A3,I1,A30)
     2902 format(2A12,9A10,A12,A10,A12,A10,6A12)
     2910 format(2A6,A2,I2,A30)

     !Allow for quite deviant generations, to break close "Pavlovian responses"
     !   between such stimuli as light and food and risk.
     deviation = "Normal" !First assume the generation
     dev = 1              !not to deviate in food or risk
     if (soft == 0 .and. normal < 1.) then  !deviant generations are allowed to occur
        fmultrisk = fmultriskBAS

        do a = 1, flifespan
         do dep = 1,depth
            numcop(dep,a) = numcopBAS(dep,a)
         end do
        end do
 !      else
        if (gen < generations) then !allow for deviation among generations, but let the last be "normal"

          if (gen > generations - 9) then  !this is going to be a deviant generation
!                deviantgener = 1
                if (gen == generations - 1) then  !lower mortality risk (fewer predators)
                     dev = 2
                  else if (gen == generations - 2) then  !higher mortality risk
                     dev = 3
                  else if (gen == generations - 3) then  !deviation food: all at great depth
                     dev = 4
                  else if (gen == generations - 4) then  !deviation food: all at shallow depth
                     dev = 5
                  else if (gen == generations - 5) then  !deviation food: less food at all depths
                     dev = 6
                  else if (gen == generations - 6) then  !deviation food: more food at all depths
                      dev = 7
                   else if (gen == generations - 7) then !deviation: both more food and higher risk at all depths
                     dev = 8
                   else if (gen == generations - 8) then !deviation: short-term fluctuations in environment
                     dev = 9
                endif
            !find the deviation this generation
            else
              call random_number (random)
              if (random > normal) then !this is a deviant generation
                 call random_number (rand2)
                r = 0.125 !1/8: each deviation's share of appearance
                if (rand2 > 1.-r) then  !deviation in mortality risk
                     dev = 2
                  else if (rand2 > 1.-2*r) then  !deviation in mortality risk
                     dev = 3
                  else if (rand2 > 1.-3*r) then  !deviation food: all at great depth
                     dev = 4
                  else if (rand2 > 1.-4*r) then  !deviation food: all at shallow depth
                     dev = 5
                  else if (rand2 > 1.-5*r) then  !deviation food: less food at all depths
                     dev = 6
                  else if (rand2 > 1.-6*r) then  !deviation food: more food at all depths
                      dev = 7
                  else if (rand2 > 1.-7*r) then  !deviation: both more food and higher risk at all depths
                      dev = 8
                  else !deviation: short-term fluctuations in environment
                     dev = 9
                 end if ! rand2
              end if !random > normal
          end if !gen > generations - 8

!        make sure 1st generation is "Normal"
         if(gen == 1) dev = 1

!              d = depth*1/3  ! number of depth cells containing food
!               call random_number (random)
              call random_number (rand2)

              !specify the condition for each class of deviation
              if (dev == 2) then  !deviation in mortality risk
                       if (screenplot == 1) write(6,*) "DEVIANT GENERATION:  mortality low"
                     deviation = "Low_Mort"
                     if (file == 0 .and. gen < 51) then  !milder deviations in early stages of evolution
                           fmultrisk = 0.02*(gen*(1.-0.5*rand2)*fmultriskBAS + (50-gen)*fmultriskBAS)
                          else
                           fmultrisk = (1.-0.5*rand2)*fmultriskBAS
                     end if
                 else if (dev == 3) then  !deviation in mortality risk
                       if (screenplot == 1) write(6,*) "DEVIANT GENERATION: mortality high"
                     deviation = "HighMort"
                     if (file == 0 .and. gen < 51) then  !milder deviations in early stages of evolution
                           fmultrisk = 0.02*(gen*(1.+0.5*rand2)*fmultriskBAS + (50-gen)*fmultriskBAS)
                          else
                           fmultrisk = (1.+0.5*rand2)*fmultriskBAS
                     end if
                     !(multiplication factor reduced from 2 to 1.5 25Oct10 to avoid restarts)
                 else if (dev == 4) then  !deviation food: all at great depth
                         if (screenplot == 1) write(6,*) "DEVIANT GENERATION:  food deep"
                     deviation = "DeepFood"
                     !gradually increase void area
                     begin = 5
                     if (file == 0) then
                       if (gen < 21) then
                         begin = 2
                        else if (gen < 41) then
                         begin = 3
                        else if (gen < 61) then
                         begin = 4
                        else
                         begin = 5
                       end if
                     end if
                     do a = 1, flifespan
                        !no prey in 1-5 m any time
                        do dep = 1,begin
                           numcop(dep,a) = 0
                        end do
                     end do
                 else if (dev == 5) then  !deviation food: all at shallow depth
                         if (screenplot ==1) write(6,*) "DEVIANT GENERATION:  food shallow"
                     deviation = "ShalFood"
                     !gradually increase void area if not starting from previous evolution
                     begin = 24
                     if (file == 0) then
                       if (gen < 11) then
                         begin = 29
                        else if (gen < 21) then
                         begin = 28
                        else if (gen < 41) then
                         begin = 27
                        else if (gen < 61) then
                         begin = 26
                        else if (gen < 81) then
                         begin = 25
                        else
                         begin = 24
                       end if !gen
                     end if !file


                     do a = 1, flifespan
                        !no prey in 24-30 m any time (changed from 22-30 at 25Feb10 to avoid restarts)
                        do dep = begin,depth
                           numcop(dep,a) = 0
                        end do
                     end do
                 else if (dev == 6) then  !deviation food: less food at all depths
                         if (screenplot == 1) write(6,*) "DEVIANT GENERATION:  less food"
                     deviation = "LessFood"


                     do a = 1, flifespan
                        do dep = 1,30
                           if (file == 0 .and. gen < 51) then !milder deviations in early stages of evolution
                               numcop(dep,a) = int(0.02*(gen*0.85*numcopBAS(dep,a) + (50-gen)*numcopBAS(dep,a)))
                              else
                               numcop(dep,a) = int(0.85*numcopBAS(dep,a))
                           end if
                        end do
                     end do
                 else if (dev == 7) then  !deviation food: more food at all depths
                         if (screenplot ==1) write(6,*) "DEVIANT GENERATION:  more food"
                     deviation = "MoreFood"


                     do a = 1, flifespan
                        do dep = 1,30
                           if (file == 0 .and. gen < 51) then  !milder deviations in early stages of evolution
                               numcop(dep,a) = int(0.02*(gen*1.25*numcopBAS(dep,a) + (50-gen)*numcopBAS(dep,a)))
                              else
                               numcop(dep,a) = int(1.25*numcopBAS(dep,a))
                           end if
                        end do
                     end do
                 else if (dev == 8) then  !deviation: both more food and higher risk at all depths
                         if (screenplot ==1) write(6,*) "DEVIANT GENERATION:  high food and risk"
                     deviation = "High_FaM"
                     if (file == 0 .and. gen < 51) then  !milder deviations in early stages of evolution
                           fmultrisk = 0.02*(gen*1.25*fmultriskBAS + (50-gen)*fmultriskBAS)
                          else
                           fmultrisk = 1.25*fmultriskBAS
                     end if


                     do a = 1, flifespan
                        do dep = 1,30
                           if (file == 0 .and. gen < 51) then  !milder deviations in early stages of evolution
                               numcop(dep,a) = int(0.02*(gen*1.25*numcopBAS(dep,a) + (50-gen)*numcopBAS(dep,a)))
                              else
                               numcop(dep,a) = int(1.25*numcopBAS(dep,a))
                           end if
                        end do
                     end do
                 else if (dev == 9) then  !deviation: short-term fluctuations in environment
                         if (screenplot ==1) write(6,*) "DEVIANT GENERATION:  short-term fluctuations"
                     deviation = "Autocorr"
                end if ! dev
         end if ! gen < generations
       end if ! soft = 0 or normal = 1

      if (gen == 51) then
        !stop here to make some checking
        dep = gen
      end if

      !find preymax: max prey concentration at any depth at age 1
      preymax = 0.
      do dep = 1,depth
        if (numcop(dep,1) > preymax) preymax = numcop(dep,1)*1.
      end do

      !add new element in motherstring (individual-number of selected fore-mothers)
      dummy = generations/motherlength
      if (gen == dummy .or. gen == 2*dummy .or. gen == 3*dummy .or. gen == 4*dummy .or. gen == 5*dummy .or.   &
              gen == 6*dummy .or. gen == 7*dummy .or. gen == 8*dummy .or. gen == 9*dummy) then
          dummy = 1 + gen/dummy

          do ind = 1,fpop
             motherstring(ind,dummy) = ind
          end do
      end if

      !find population frequency distribution of feelings
      if (fearplot == 1) then
        if (gen == generations/100) then
           call srfeelings(runtag,1,fpop,curve,gABM,expmt,swHsize,swHstomach,swHfood,swFsize,swFlight, &
                           swFothers,swFmort,alleleconv,hungersensitivity,errorP,test99)
          else if (gen == generations/4) then
           call srfeelings(runtag,2,fpop,curve,gABM,expmt,swHsize,swHstomach,swHfood,swFsize,swFlight, &
                           swFothers,swFmort,alleleconv,hungersensitivity,errorP,test99)
          else if (gen == generations/2) then
           call srfeelings(runtag,3,fpop,curve,gABM,expmt,swHsize,swHstomach,swHfood,swFsize,swFlight, &
                           swFothers,swFmort,alleleconv,hungersensitivity,errorP,test99)
          else if (gen == 3*generations/4) then
           call srfeelings(runtag,4,fpop,curve,gABM,expmt,swHsize,swHstomach,swHfood,swFsize,swFlight, &
                           swFothers,swFmort,alleleconv,hungersensitivity,errorP,test99)
          else if (gen == generations) then
           call srfeelings(runtag,5,fpop,curve,gABM,expmt,swHsize,swHstomach,swHfood,swFsize,swFlight, &
                           swFothers,swFmort,alleleconv,hungersensitivity,errorP,test99)
        end if
      end if

!start this generation

      !call subroutine for life cycle, including habitat, hedonic decisions, growth and egg production
      call SRlifecycle(expmt,srerr,test99,generations,gen,depth,dielcycles,totalsenses,    &
        cop2egg,densdep,density,restart,realgen,fpop,fish,flifespan,fmaxweight,fsurvivors,  &
        fpopfec,lightdecay,fmultrisk,fmultgain,BMscale,behavstep,behavmax,motherlength,auto,           &
        allmax,allmin,interval,development,env,deviation,zrange,preymax,dev,memory,     &
        curve,NowmaxPercept,MaxPercept,gABM,brain,restattention,maxstomcap,flivingcost,        &
        gainfunction,riskfunction,beamatt,contrast,preyarea,viscap,eyesat,attackrate,attackfactor,     &
        swHsize,swHstomach,swHfood,swFsize,swFlight,swFothers,swFmort,reproinvest,sdgABM,sdAfear,      &
        sdAhunger,sdHfear,sdHhunger,sdAHunused,runtag,alleleconv,screenplot,stepABM,minABM,maxABM,     &
        hungersensitivity,errorP,during,reprocap,attackduration,zavgkids,zavgEgg,zavgR0,zsdR0,zavgsurv,zavgstart)


!     update of maximum perception values
         if (gen == 1) then
            write(14,1401)"Gener","Stomach","Light","Agents","Bodymass","Prey","Age","Risk",               &
                          "Stomach","Light","Agents","Bodymass","Prey","Age","Risk"
            write(14,1401)"-ation","GenMax1","GenMax2","GenMax3","GenMax4","GenMax5","GenMax6","GenMax7",  &
                            "MaxPercept1","MaxPercept2","MaxPercept3","MaxPercept4","MaxPercept5",           &
                            "MaxPercept6","MaxPercept7"
         end if

          do i = 1,7      !seven kinds of perceptual information
            do g = 1,99  !averaging over 100 generations, in groups of 20
               GenMaxPercept(i,g) = GenMaxPercept(i,g+1)
            end do
            GenMaxPercept(i,100) = NowMaxPercept(i)
            MaxPercept(i) = 0.
            max01 = 0.
            max02 = 0.
            max03 = 0.
            max04 = 0.
            max05 = 0.

!           Find max in the last 5 groups of 20 generations
            do g = 1,20
               max01 = max(GenMaxPercept(i,g),max01)
            end do
            do g = 21,40
               max02 = max(GenMaxPercept(i,g),max02)
            end do
            do g = 41,60
               max03 = max(GenMaxPercept(i,g),max03)
            end do
            do g = 61,80
               max04 = max(GenMaxPercept(i,g),max04)
            end do
            do g = 81,100
               max05 = max(GenMaxPercept(i,g),max05)
            end do
!           Make the maximum the average of thes 5 maxima,to smooth
!                   the change in maximum level between generations
            MaxPercept(i) = 0.2*(max01+max02+max03+max04+max05)

         end do !i
         write(14,1402)realgen,(GenMaxPercept(i,100),i = 1,7),(MaxPercept(i), i = 1,7)
      1401 format(A8,14A12)
      1402 format(I8,14F12.2)

      !write success of descendant from each founder population, if file = 1
      if (file == 1) then
          do class1 = 0, readfiles
             descendants(class1) = 0
          end do

          do ind = 1,fpop
             class1 = max(0,motherpop(ind))
             if (class1 > readfiles) write(10,*)ind," motherpop(class1) =", class1
             descendants(class1) = descendants(class1) + 1
          end do
          write(16,1616)realgen,(descendants(class1), class1 = 0,readfiles)
      end if
      1616 format(12I8)

      !call subroutine for making the eggs for next generation
      call SRneweggs(srerr,test99,expmt,gen,fish,fpop,fnewpop,depth,generations,realgen,   &
                 flifespan,allmax,allmin,mutrate,motherlength,    &
                 interval,memory,maxpartners,curve,partnerchoice,gABM,fmaxweight, &
                 corrplots,deviation,runtag,stepABM,minABM,maxABM,dev)

      !update population size for next generation
      fpop = fnewpop
      !terminate program if fpop larger than array. Then many other arrays also are exceeded
      if (fpop > fish) then
        write(10,665) "fpop = ",fpop, " after SRneweggs in generation ",realgen
        write(6,665) "fpop = ",fpop, " after SRneweggs in generation ",realgen
        goto 999
      end if
      665 format(A8,I6,A31,I5)

      !call subroutine for control of output files in the middle of an experiment
      if ((gen == gg/10) .or. (gen == 2*gg/10) .or. (gen == 3*gg/10) .or. (gen == 4*gg/10)        &
           .or. (gen == 5*gg/10) .or. (gen == 6*gg/10).or. (gen == 7*gg/10)                        &
           .or. (gen == 8*gg/10) .or. (gen == 9*gg/10) .or. (gen == gg))                          &
           call SRopenclose(runtag,srerr,test99,expmt,gen,generations,during)

      if (gen > 1 .and. gen < gg .and. test99 == 1 .and. srerr == 4) write(10,*) "after SRopenclose # 1"


    !experiments can be done at the end of each simulation
    if(gen == generations/4 .or. gen == generations/2 .or. gen == 3*generations/4 .or. gen == generations) then
        if(gen == generations/4) then
           if (expmt > 9) then
              write(code1,1704)runtag,"-E",expmt
             else
                 write(code1,1703)runtag,"-E0",expmt
           end if
           write(code2,1705)code1,"-1qrt"
          else if(gen == generations/2) then
           write(code2,1705)code1,"-half"
          else if(gen == 3*generations/4) then
           write(code2,1705)code1,"-3qrt"
          else
           write(code2,1705)code1,"-full"
        end if
      if(FHtest == 1) call SR_FHtest(fpop,code2,curve,gABM,errorP,test99)
      if(statetest == 1) call SRstatetest(fpop,code2,curve,gABM,errorP,test99)
      if(HFboundary == 1) call SRHFboundary(fpop,code2,curve,gABM,errorP,test99)
      if(Mboundary == 1) call SRMboundary(fpop,code2,curve,gABM,errorP,test99)
    end if

      if (gen > 1 .and. gen < gg .and. test99 == 1 .and. srerr == 4) write(10,*) "after SRopenclose # 2"

      if (gen == gg) then
        close(10)
        close(12)
        close(13)
        close(14)
        close(15)
        if (file == 1) close(16)
        close(18)
        close(19)
        close(22)
        close(23)
        close(24)
        close(25)
        close(26)
        close(27)
        close(28)
        close(29)
        close(31)
        close(32)
        close(33)
        close(34)
        close(35)
        close(40)
        close(41)
        close(42)
        close(52)
        close(53)
        close(54)
        close(60)
        close(61)
        close(62)
        close(63)
        close(64)
        close(65)
        close(66)
        close(67)
      end if
      if (gen > 1 .and. gen < gg .and. test99 == 1 .and. srerr == 4) write(10,*) "after SRopenclose # 3, end of gen", gen

    !end of generation loop
    end do !gen , the genetic algorithm

    l = expmt
    r = (zavgstart-zavgsurv)/(0.01*zavgstart)
    AVGstdev = (sdgABM+sdAfear+sdAhunger+sdHfear+sdHhunger)/5.
    if (l > 9) then
        write(code1,1704)runtag,"-E",l
      else
        write(code1,1703)runtag,"-E0",l
    end if
    write(17,1701) code1,l,mut1,fmultgain,fmultriskBAS,zavgstart,zavgsurv,r,      &
                   zavgR0,zsdR0,zavgkids,zavgNeverA(1),                &
                   zavgNeverA(2),zavgpctA(1),zavgpctA(2),zeAalleles(1),     &
                   zeAalleles(2),zeAalleles(3),zeAalleles(5),zeAalleles(6),       &
                   zeHalleles(1),zeHalleles(2),zeHalleles(4),zeHalleles(5),       &
                   zeHalleles(7),zeHalleles(8),zeHalleles(10),zeHalleles(11),     &
                   AVGstdev,sdgABM,sdAfear,sdAhunger,sdHfear,sdHhunger,           &
                   sdAHunused,code1,(zavgBM(p,1), p = 1,6),code1,zavgEgg,      &
                   (zavgDepth(p,1), p = 1,6),code1,(zavgLocal(p,1), p = 1,6),  &
                   code1,(zavgBM(p,2), p = 1,6),code1,zavgEgg,                 &
                   (zavgDepth(p,2), p = 1,6),code1,(zavgLocal(p,2), p = 1,6)

    1701 format(A12,I4,F7.3,2F7.2,2I7,F7.1,2F7.3,3I7,2F7.3,13F7.2,6F10.4,F12.4, &
                2(A12,6F9.1,A12,7F9.2,A12,6F9.1))
    1703 format(A6,A3,I1)
    1704 format(A6,A2,I2)
    1705 format(A10,A5)

close(17)

999 continue

!end of program
end

!------------------------------------------------------------------------------
!              END OF MAIN PROGRAM --- START LIBRARY OF SUBROUTINES
!------------------------------------------------------------------------------


!------------------------------------------------------------------------------
subroutine SRinitrun(expmt,srerr,test99,teststart,depth,fish,totalsenses,nomut,      &
          softG,softM,generations,densdep,maxlife,fpop,gen,realgen,during,restart,       &
          cop2egg,fmaxweight,gen0,lightdecay,brain,mut1,mut2,mut3,fmultrisk,fmultgain,   &
          allmax,allmin,alleleconv,soft,softscale,zrange,BMscale,behavstep,behavmax,      &
          motherlength,auto,fmultriskBAS,interval,fsurvivors,dielcycles,development,     &
          diellight,prey,normal,density,tempregime,memory,maxpartners,curve,errorP,    &
          partnerchoice,gABM,restattention,maxstomcap,fearplot,flivingcost,gainfunction, &
          riskfunction,beamatt,contrast,preyarea,viscap,eyesat,attackrate,attackfactor,  &
          swHsize,swHstomach,swHfood,swFsize,swFlight,swFothers,swFmort,reproinvest,     &
          corrplot,file,readfiles,runtag,screenplot,maxABM,minABM,stepABM,nzlast,nzero,  &
          copdvm,cpop,nzloss,daylight,hungersensitivity,Mboundary,HFboundary,FHtest,statetest)

!------------------------------------------------------------------------------
implicit none
!variables from main program
INTEGER, DIMENSION(:), ALLOCATABLE :: seed
integer expmt,depth,fish,totalsenses,generations
integer densdep,maxlife,dielcycles,fpop,copdvm,restart,soft,gen,gen0,realgen
integer cop2egg,fmaxweight,freerandom,srerr,test99,teststart,nomut,diellight
integer behavstep,behavmax,motherlength,softG,softM,fsurvivors,fearplot,during
integer tempregime,memory,zrange,maxpartners,curve,partnerchoice,gABM,screenplot
integer gainfunction,riskfunction,gainplot,corrplot,file,readfiles,alleleconv
integer swHsize,swHstomach,swHfood,swFsize,swFlight,swFothers,swFmort
integer cpop,nzlast,nzero,Mboundary,HFboundary,FHtest,statetest,brain

!integer tmpyear,tmpsec,tmpmonth,tmpmin,tmphour,tmpday,tmphund
real    allmax,allmin,fmultrisk,fmultgain,mut1,mut2,mut3,BMscale,softscale,auto
real    fmultriskBAS,interval,development,prey,normal,density,attackrate,attackfactor
real    maxstomcap,restattention,flivingcost,beamatt,contrast,preyarea,viscap,eyesat
real    nzloss,daylight,reproinvest,maxABM,minABM,stepABM,hungersensitivity,errorP
character(6) runtag

!include commonblock of arrays
include 'Commonfish.txt'

!local variables
integer age,a,i,g,ind,dep,gn,error99,copies,new
real    random,temp0,tempz,tempc,delta,lightdecay
integer clock,n
character(41) string
character(8) deviation
character(8) itag(15000)
character (9) gen1,gen2
character (19) gen3
character (54) string104


error99 = 0
if (srerr == 1) error99 = 1

!open input file
open(101, file = 'i101-parameters.txt')
!initialize variables of this simulation
read(101,*)
read(101,*) expmt    !(1-5) or a higher number
read(101,*) runtag
read(101,*) fish
read(101,*) generations
read(101,*) maxlife
read(101,*) depth
read(101,*) gABM
read(101,*) totalsenses
read(101,*) motherlength
read(101,*) behavstep
read(101,*) behavmax
read(101,*) test99    !0 or 1, errorsearch (1) or not
read(101,*) srerr     !0 or or SR-number where test99 reported error
read(101,*) teststart !number of gererations to pass before errorseach shall begin
read(101,*)
read(101,*) file
read(101,*) soft
read(101,*) densdep
read(101,*) copdvm
read(101,*) diellight
read(101,*) freerandom
read(101,*) dielcycles
read(101,*) tempregime
read(101,*) memory
read(101,*) curve
read(101,*) alleleconv
read(101,*) during
read(101,*) screenplot
read(101,*) fearplot
read(101,*) gainplot
read(101,*) corrplot
read(101,*) FHtest
read(101,*) statetest
read(101,*) HFboundary
read(101,*) Mboundary
read(101,*) gainfunction
read(101,*) riskfunction
read(101,*) brain
read(101,*) swFsize
read(101,*) swFlight
read(101,*) swFothers
read(101,*) swFmort
read(101,*) swHsize
read(101,*) swHstomach
read(101,*) swHfood
read(101,*)
read(101,*) readfiles
!read(101,*) immigration
read(101,*) allmax
read(101,*) allmin
read(101,*) interval
read(101,*) maxABM
read(101,*) minABM
read(101,*) stepABM
read(101,*) mut1
read(101,*) mut2
read(101,*) mut3
read(101,*) errorP
read(101,*) partnerchoice
read(101,*) maxpartners
read(101,*) nomut
read(101,*) normal
read(101,*) auto
read(101,*) hungersensitivity
read(101,*) flivingcost
read(101,*) cop2egg
read(101,*) fmaxweight
read(101,*) development
read(101,*) lightdecay
read(101,*) daylight
read(101,*) fmultrisk
read(101,*) attackrate
!attackrate = attackrate/(1.*maxlife) !converted from number of events to probability
read(101,*) attackfactor
read(101,*) restattention
read(101,*) BMscale
read(101,*) maxstomcap
read(101,*) fmultgain
read(101,*) prey
read(101,*) reproinvest
read(101,*) softscale
read(101,*) density
read(101,*) nzero
read(101,*) nzlast
read(101,*) nzloss
read(101,*) zrange
read(101,*) beamatt
read(101,*) preyarea
read(101,*) viscap
read(101,*) eyesat
read(101,*) contrast
read(101,*) temp0
read(101,*) tempz
read(101,*) tempc
close(101)

!allow gentler environment for fish with hedonic tone-brains (brain = 0)
if (brain == 0) then
   prey = prey * 3.
   fmultrisk = fmultrisk * 3.
end if

!experiment with variable prey densities
!prey = prey - expmt*.01

!fmultrisk = fmultrisk*4.
!fmultgain = fmultgain - 10*0.14

!assume normal levels of growth and survival
softM = 0
softG = 0
if (soft == 1) then
    softG = 2
    softM = 2
end if

!open files for input and result output
call SRfileopen(expmt,file,runtag,gABM,test99,during)
if (test99 == 1) write(10,*) "SRinitrun"

!open output file
if (expmt < 10) then
   write(string, 5001)"HED18-",runtag,"-E0",expmt,"-o103-parameter_out.txt"
  else
   write(string, 5002)"HED18-",runtag,"-E",expmt,"-o103-parameter_out.txt"
end if
open(103, file = string)
5001 format(2A6,A3,I1,A23)
5002 format(2A6,A2,I2,A23)
if (freerandom == 0) then
    seed = 251160
  else
    CALL RANDOM_SEED(size = n)
    ALLOCATE(seed(n))
    CALL SYSTEM_CLOCK(COUNT=clock)
    seed = clock + 1000*expmt + 37 * (/ (i - 1, i = 1, n) /)
end if
CALL RANDOM_SEED(PUT = seed)
write(103,*) "random seed = ", seed
DEALLOCATE(seed)

gen = 0
age = 0

!Mortality risk is scaled to number of depth cells. Risk factor in input file is made for 30 cells.
!If there are fewer cells, the mortality risk must be reduced to allow survival during daylight.
fmultrisk = fmultrisk*depth/30.
fmultriskBAS = fmultrisk

!Initialise current population sizes
fpop = int(0.999*fish) !modelled, dynamic population
cpop = int(fish*prey)       !prey field, changed from 500 to 125 from HED10 to HED14, as FISH increased from 10 to 40000

!initiate temperature field
if (tempregime == 0) then !no temperature variation
   do dep = 1, depth
     temperature(dep) = tempc
     autotemp(dep) = tempc
   end do
  else !Temperature is constant in the upper 1/3 of the water column, then deceasing linearly towards bottom
   do dep = 1, depth/3
     temperature(dep) = temp0
     !initiate autocorrelated temp field as the average
     autotemp(dep) = temp0
   end do
   delta = depth - depth/3
   delta = (temp0-tempz)/delta
   do dep = 1 + depth/3, depth
     temperature(dep) = temp0 -(dep - depth/3)*delta
     !initiate autocorrelated temp field as the average
     autotemp(dep) = temperature(dep)
   end do
end if

!INITIATE motherstring (individual-number of a small subset of all fore-mothers)
do ind = 1, fish
   motherstring(ind,1) = ind
   do gn = 2,motherlength
      motherstring(ind,gn) = 0
   end do
   mcreation(ind) = 0
   gcreation(ind) = 1
   acreation(ind) = 1
   icreation(ind) = ind

   !INITIATE memory (level of chemicals in brain associated with fear and hunger)
   do a = 1,2
      fmemory(ind,a) = 0.
   end do
end do

!INITIATE genes
!a) either by random variation (if file = 0) or from file
if (file == 0) then
   !define all inds as not yet alive
   do ind = 1,fpop
      fstatus(ind) = 0
   end do
   fsurvivors = 0
   write(6,*) "calling SRgenecreate, fpop =",fpop
   call SRgenecreate(0,fpop,allmax,allmin,interval,depth,gen,age,maxlife/4,cop2egg,   &
                     fsurvivors,motherlength,memory,curve,gABM,stepABM,minABM,maxABM, &
                     fish,test99,copies,new,itag,brain)
   write(6, *) " "
   write(6, *) "number of individuals created from living:", copies
   write(6, *) "number of individuals created from random:", new
   write(6, *) " "

   !send info to warnings-file
   write(10, *) " "
   write(10, 1001) "generation, age and survivors:", gen, age,fsurvivors-copies-new
   write(10, *) "number of individuals created from living and random:", copies, new
   write(10, *) " "
1001 format(A30,3I9)


   !print new gene pool. First make file name
   if (gen < 10) then
            write(gen1,2902) "gen-0000",gen
          else if (gen < 100) then
            write(gen1,2903) "gen-000",gen
          else if (gen < 1000) then
            write(gen1,2904) "gen-00",gen
          else if (gen < 10000) then
            write(gen1,2905) "gen-0",gen
          else
            write(gen1,2906) "gen-",gen
   end if

   if (age < 10) then
            write(gen2,2902) "age-0000",age
          else if (age < 100) then
            write(gen2,2903) "age-000",age
          else if (age < 1000) then
            write(gen2,2904) "age-00",age
          else if (age < 10000) then
            write(gen2,2905) "age-0",age
          else
            write(gen2,2906) "age-",age
   end if

   write(gen3,2908)gen1,"-",gen2

   if (expmt < 10) then
            write(string104,2901)"HED18-",runtag,"-E0",expmt,"-o104-genepool-",gen3,".txt"
          else
            write(string104,2910)"HED18-",runtag,"-E",expmt,"-o104-genepool-",gen3,".txt"
   end if

2901 format(2A6,A3,I1,A15,A19,A4)
2902 format(A8,I1)
2903 format(A7,I2)
2904 format(A6,I3)
2905 format(A5,I4)
2906 format(A4,I5)
2908 format(A9,A1,A9)
2909 format(2I8,A8,I8,37F8.2,2F12.5,I8)
2910 format(2A6,A2,I2,A15,A19,A4)
2911 format(2I8,A10,I8,F8.0,I8,F8.3)
2912 format(2A8,A10,4A8)

   open(104, file = string104)
   write(104, 1001) "generation, age and survivors:", gen, age,fsurvivors-copies-new
   write(104, *) "number of individuals created from living and random:", copies, new
   write(104, *) " "

   do i = 1,fish
           write(104,2909) i,fstatus(i),itag(i),motherpop(i),(geneAHsize(i,g),g = 1,10),   &
                 (geneAFlight(i,g),g = 1,3),(geneAFother(i,g),g = 1,3),            &
                 (geneAFmort(i,g),g = 1,3),(geneAHstom(i,g),g = 1,3),              &
                 (geneAHfood(i,g),g = 1,3),(geneHFlight(i,g),g = 1,3),             &
                 (geneHFother(i,g),g = 1,3),(geneHHfood(i,g),g = 1,3),             &
                 (geneHHother(i,g),g = 1,3),(geneMemo(i,g),g = 1,2),fbirthdepth(i)
   end do
   close(104)

   fsurvivors = 0
   restart = 0
   realgen = 0
  else
!b) or by reading gene data from a file of a previous run
   gen0 = 0
   call SRgeneread(readfiles,fish,fpop,gen,age,motherlength,cop2egg,realgen,test99)
   restart = 1
end if

!print the initial allele frequency at the start of creation, before natural selection
call SRallelefrequency(runtag,srerr,test99,expmt,0,generations,fpop,realgen,allmin,  &
                          allmax,interval,curve,gABM,stepABM,minABM,maxABM)

!organisms are born at a given depth and will later decide whether to move.
!initialization of birth depth distribution.
!The birth depth is the habitat choice of its parent at the last age.
!Here is the random birth depth of the first generation, i.e. the assumed random
!distribution of its imaginary parent generation (generation zero)
!(This routine is not in use if gene data are read from file, as the birth depths then also are given.)
if (file == 0) then
   do ind = 1,fpop
      call random_number (random)
      dep = NINT(1+random*depth)
      fbirthdepth(ind) = min(dep,depth)
   end do
end if

!find vertical distribution at birth
do dep = 1, depth
   numfish(dep,0) = 0
end do
do ind = 1,fpop
   dep = fbirthdepth(ind)
   numfish(dep,0) = numfish(dep,0) + 1
end do

!initiate environmental information
call SRinitage(srerr,test99,gen,age,depth,fpop,maxlife/4,fmultrisk,fmultgain,   &
               0,0,attackfactor,beamatt,contrast,preyarea,viscap,eyesat,lightdecay)
!deviation = "Init Run"
call SRhabitat(srerr,test99,expmt,generations,densdep,density,       &
                   gen,age,depth,fpop,fpop,maxlife/4,gainfunction,     &
                   riskfunction,restattention,BMscale,flivingcost,maxstomcap,     &
                   0,0,lightdecay,1.,1.,fmultrisk,deviation,screenplot,realgen,1,during)

!make plots of fgain and frisk: (density-dependent but not individual-based) feeding and mortality risk
if (gainplot == 1) call SRgainplot(runtag,attackfactor,beamatt,BMscale,contrast,     &
                        densdep,density,depth,deviation,expmt,eyesat,maxlife,   &
                        flivingcost,fmultgain,fmultrisk,fpop,realgen, &
                        gen,generations,lightdecay,maxstomcap,preyarea,restattention, &
                        srerr,viscap,screenplot,test99,during)

!print interpretation of run conditions
write(103,*) "simulation # ", expmt
write(103,*) "fish =        ", fish
write(103,*) "generations = ", generations
write(103,*) "maxlife =     ", maxlife
write(103,*) "depth =       ", depth
write(103,*) "gABM =        ", gABM
write(103,*) "totalsenses = ", totalsenses
write(103,*) "motherlength =", motherlength
write(103,*) "behavstep =   ", behavstep
write(103,*) "behavmax =    ", behavmax
write(103,*) "test99 =      ", test99
write(103,*) "srerr =       ", srerr
write(103,*) "teststart =   ", teststart
if (file == 1) then
   write(103,*) "file =                    1 (starting genes from earlier run)"
  else
   write(103,*) "file =                    0 (starting genes from random)"
end if
write(103,*) "soft =        ", soft ,"(hard=0 or soft=1 start of selection)"
write(103,*) "densdep =     ", densdep ,"(density-dependent if 1, not if 0)"
write(103,*) "copdvm =      ", copdvm, "(1 if classical dvm, 2 if all in upper quarter)"
write(103,*) "diellight =   ", diellight, "(1 if natural light regime, else constant light)"
write(103,*) "freerandom =  ", freerandom
write(103,*) "dielcycles =  ", dielcycles
write(103,*) "tempregime =  ", tempregime
write(103,*) "readfiles =   ", readfiles
write(103,*) "memory =      ", memory
write(103,*) "curve =       ", curve,  "(1 for sigmoid, 2 for linear, 3 for gamma affect functions, 4 for predefined)"
write(103,*) "alleleconv =  ", alleleconv
write(103,*) "during =      ", during
write(103,*) "screenplot =  ", screenplot
write(103,*) "fearplot =    ", fearplot
write(103,*) "gainplot =    ", gainplot
write(103,*) "corrplot =    ", corrplot
write(103,*) "FHtest =      ", FHtest
write(103,*) "statetest =   ", statetest
write(103,*) "HFboundary =  ", HFboundary
write(103,*) "Mboundary =   ", Mboundary
write(103,*) "gainfunction =", gainfunction
write(103,*) "riskfunction =", riskfunction
write(103,*) "brain =       ", brain
write(103,*) "swFsize =     ", swFsize
write(103,*) "swFlight =    ", swFlight
write(103,*) "swFothers =   ", swFothers
write(103,*) "swFmort =     ", swFmort
write(103,*) "swHsize =     ", swHsize
write(103,*) "swHstomach =  ", swHstomach
write(103,*) "swHfood =     ", swHfood
!write(103,*) "immigration = ", immigration
write(103,*) "allmax =      ", allmax
write(103,*) "allmin =      ", allmin
write(103,*) "interval =    ", interval
write(103,*) "maxABM =      ", maxABM
write(103,*) "minABM =      ", minABM
write(103,*) "stepABM =     ", stepABM
write(103,*) "mut1 =        ", mut1
write(103,*) "mut2 =        ", mut2
write(103,*) "mut3 =        ", mut3
write(103,*) "errorP =      ", errorP
write(103,*) "partnerchoice=", partnerchoice
write(103,*) "maxpartners = ", maxpartners
write(103,*) "nomut =       ", nomut
write(103,*) "normal =      ", normal
write(103,*) "auto =        ", auto
write(103,*) "hungersensit.=", hungersensitivity
write(103,*) "flivingcost = ", flivingcost
write(103,*) "cop2egg =     ", cop2egg
write(103,*) "fmaxweight =  ", fmaxweight
write(103,*) "development = ", development
write(103,*) "lightdecay =  ", lightdecay
write(103,*) "daylight =    ", daylight
write(103,*) "fmultrisk =   ", fmultrisk*30./(1.*depth), "scaled fmultrisk = ", fmultrisk
write(103,*) "attackrate =  ", attackrate
write(103,*) "attackfactor =", attackfactor
write(103,*) "restattention=", restattention
write(103,*) "fmultgain =   ", fmultgain
write(103,*) "prey =        ", prey
write(103,*) "BMscale   =   ", BMscale
write(103,*) "maxstomcap =  ", maxstomcap
write(103,*) "reproinvest = ", reproinvest
write(103,*) "softscale =   ", softscale
write(103,*) "density   =   ", density
write(103,*) "nzero     =   ", nzero
write(103,*) "nzlast    =   ", nzlast
write(103,*) "nzloss    =   ", nzloss
write(103,*) "zrange =       ", zrange
write(103,*) "beamatt =     ", beamatt
write(103,*) "prey area =   ", preyarea
write(103,*) "visual capacity =        ", viscap
write(103,*) "eye saturation =         ", eyesat
write(103,*) "inherent contrast =      ", contrast
write(103,*) "temperature in surface = ", temp0
write(103,*) "temperature at bottom =  ", tempz
write(103,*) "constant temperature =   ", tempc
close(103)


write(12,1202) "gen","environ","help","m-risk","repcap","att","food","start","end","% mort","R0sur",  &
               "fec","kids","Mut","NotAfF","NotAfM","% fearF","% fearM","ABM#a","gAF#a","PAF#a","gAH#a","PAH#a",  &
               "gHFL#a","gHFLa1","gHFLf1","PHFL#a","PHFLa1","PHFLf1",             &
               "gHFO#a","gHFOa1","gHFOf1","PHFO#a","PHFOa1","PHFOf1",             &
               "gHHF#a","gHHFa1","gHHFf1","PHHF#a","PHHFa1","PHHFf1",             &
               "gHHO#a","gHHOa1","gHHOf1","PHHO#a","PHHOa1","PHHOf1",             &
               "sdgABM","sdAFear","sdAHunger","sdHFear","sdHHunger","sdUnused",   &
               "BM1mDAYf","BM1mNIGf","BM50%f","BMLmDAYf","BMLmNIGf","BMfinalf",   &
               "z1mDAYf","z1mNIGf","z50%f","zLmDAYf","zLmNIGf","zfinalf","z-Egg", &
               "N1mDAYf","N1mNIGf","N50%f","NLmDAYf","NLmNIGf","Nfinalf",         &
               "BM1mDAYm","BM1mNIGm","BM50%m","BMLmDAYm","BMLmNIGm","BMfinalm",   &
               "z1mDAYm","z1mNIGm","z50%m","zLmDAYm","zLmNIGm","zfinalm","z-Egg", &
               "N1mDAYm","N1mNIGm","N50%m","NLmDAYm","NLmNIGm","Nfinalm"
call SRfileheadings(18,curve,9,test99)
call SRfileheadings(19,curve,10,test99)


1202 format(A6,2A9,2A8,A4,10A8,2A9,5A7,8(3A7),6A10,38A9)
!1801 format(48A9)
!1802 format(52A9)

end
!end of subroutine SRinitrun


!------------------------------------------------------------------------------
subroutine SRlifespan(depth,flifespan,dielcycles,copdvm,nzlast,nzero,cpop,nzloss, &
                     daylight,prey,autocop,numcop,numcopBAS,surlig,autosurlig,diellight)
!------------------------------------------------------------------------------
implicit none

!variables from main program
integer depth,flifespan,dielcycles,copdvm,diellight
integer nzlast,nzero,cpop,numcop(30,0:5000),numcopBAS(30,0:5000),autocop(30,0:5000)
real nzloss,daylight,prey,surlig(0:5000),autosurlig(0:5000)
!local variables
integer a,dep,count,count2,b,copcenterdepth,least,increase
integer backgroundcop(30,5000)
real copsindepth


!As light regime and prey fields repeat exactly for each generation,
!these are only calculated once.
if (copdvm == 1) then  !copepods (food) migrate diurnally, with animals in 1/2 of the water column any time
   least = 100
   increase = 100

   do a = 1, flifespan
     !find center of copepod distribution
     copsindepth = sin(3.14*a*2.*dielcycles/(flifespan+1.))
     copcenterdepth = int(depth/2 + 0.33*depth*copsindepth)

     !first make a small number of prey in each depth, increasing towards COPCENTERDEPTH
     do dep = 1,copcenterdepth
!        numcop(dep,a) = 0
        if (dep == 1) then
           backgroundcop(dep,a) = least
          else
           backgroundcop(dep,a) = backgroundcop(dep-1,a) + increase
        end if
!        backgroundcop(dep,a) = most - scala*max(5,abs(copcenterdepth-dep)) !added 30 July 2010 to help the lost find food
        numcop(dep,a) = backgroundcop(dep,a)
        autocop(dep,a) = backgroundcop(dep,a)
     end do
     do dep = depth,copcenterdepth+1,-1
!        numcop(dep,a) = 0
        if (dep == depth) then
           backgroundcop(dep,a) = least
          else
           backgroundcop(dep,a) = backgroundcop(dep+1,a) + increase
        end if
!        backgroundcop(dep,a) = most - scala*max(5,abs(copcenterdepth-dep)) !added 30 July 2010 to help the lost find food
        numcop(dep,a) = backgroundcop(dep,a)
        autocop(dep,a) = backgroundcop(dep,a)
     end do

     count = 0
     do dep = copcenterdepth-7, copcenterdepth
        count = count + 1
        if (dep > 0) then
           numcop(dep,a) = backgroundcop(dep,a) + count*cpop/64
           !initiate autocorrelated prey field as the average
           autocop(dep,a) = numcop(dep,a)
        end if
     end do
     do dep = copcenterdepth, copcenterdepth+6
        count = count - 1
        if (dep < depth+1) numcop(dep,a) = backgroundcop(dep,a) + count*cpop/64
        !initiate autocorrelated prey field as the average
        autocop(dep,a) = numcop(dep,a)
     end do
   end do

  else if (copdvm == 2) then     !food / copepods are always found in the top layer
   do a = 1, flifespan
     do dep = 1, depth/4
        numcop(dep,a) = cpop*4/depth
        !initiate autocorrelated prey field as the average
        autocop(dep,a) = numcop(dep,a)
     end do
     do dep = (depth/4)+1,depth
        numcop(dep,a) = 0
        !initiate autocorrelated prey field as the average
        autocop(dep,a) = numcop(dep,a)
     end do
   end do

  else if (copdvm == 3) then     !food evenly distributed in the top layer, gradually declining below mixing depth
   do a = 1, flifespan
     do dep = 1, nzlast
        numcop(dep,a) = nzero*prey  !prey multiplication added 22 April 2010
        !initiate autocorrelated prey field as the average
        autocop(dep,a) = numcop(dep,a)
     end do
     do dep = nzlast+1,depth
        numcop(dep,a) = int(numcop(dep-1,a)*nzloss)
        !initiate autocorrelated prey field as the average
        autocop(dep,a) = numcop(dep,a)
     end do
   end do

  else if (copdvm == 4) then     !food / copepods have dvm in spring to autumn, but overwinter deep
   do a = 1, flifespan*8/10
     !find center of copepod distribution
     copsindepth = sin(3.14*a*2.*dielcycles/(flifespan+1.))
     copcenterdepth = int(depth/2 + 0.33*depth*copsindepth)
     do dep = 1,depth
        numcop(dep,a) = 0
     end do
     count = 0
     do dep = copcenterdepth-7, copcenterdepth
        count = count + 1
        if (dep > 0) then
           numcop(dep,a) = count*cpop/64
           !initiate autocorrelated prey field as the average
           autocop(dep,a) = numcop(dep,a)
        end if
     end do
     do dep = copcenterdepth, copcenterdepth+6
        count = count - 1
        if (dep < depth+1) numcop(dep,a) = count*cpop/64
        !initiate autocorrelated prey field as the average
        autocop(dep,a) = numcop(dep,a)
     end do
   end do

   !here comes the winter
   a = 1 + 8*flifespan/10
   count2 = 0
   count = 0
   copsindepth = sin(3.14*a*2.*dielcycles/(flifespan+1.))
   copcenterdepth = int(depth/2 + 0.33*depth*copsindepth)
   b = copcenterdepth
   if (b < depth-5) then
      do a = b, depth-3
        count2 = count2 + 1
        copcenterdepth = copcenterdepth + 1
        do dep = max (1,copcenterdepth-7), copcenterdepth
          count = count + 1
          if (dep > 0) then
           numcop(dep,a) = count*cpop/64
           !initiate autocorrelated prey field as the average
           autocop(dep,a) = numcop(dep,a)
          end if
        end do
        do dep = copcenterdepth, min(copcenterdepth+6, depth)
          count = count - 1
          if (dep < depth+1) numcop(dep,a) = count*cpop/64
          !initiate autocorrelated prey field as the average
          autocop(dep,a) = numcop(dep,a)
        end do
      end do
   end if

   do a = count2 + 1 + 8*flifespan/10,flifespan
      do dep = 1,depth -7
           numcop(dep,a) = cpop/3000
           !initiate autocorrelated prey field as the average
           autocop(dep,a) = numcop(dep,a)
      end do
      count = 0
      do dep = depth-6,depth
         count = count + 1
         numcop(dep,a) = count*cpop/28
         !initiate autocorrelated prey field as the average
         autocop(dep,a) = numcop(dep,a)
      end do
   end do

  else if (copdvm == 5) then  !Shallow DVM of copepods
   do a = 1, flifespan
     !find center of copepod distribution
     copsindepth = sin(3.14*a*2.*dielcycles/(flifespan+1.))
     copcenterdepth = int(depth/2 + 0.33*depth*copsindepth) - 2 !here is the shallower depth with increased risk and gain
     do dep = 1,depth
        numcop(dep,a) = 0
     end do
     count = 0
     do dep = max(1,copcenterdepth-7), copcenterdepth
        count = count + 1
        if (dep > 0) then
           numcop(dep,a) = count*cpop/64
           !initiate autocorrelated prey field as the average
           autocop(dep,a) = numcop(dep,a)
        end if
     end do
     do dep = copcenterdepth, min(copcenterdepth+6,depth)
        count = count - 1
        if (dep < depth+1) numcop(dep,a) = count*cpop/64
        !initiate autocorrelated prey field as the average
        autocop(dep,a) = numcop(dep,a)
     end do
   end do

  else if (copdvm == 6) then     !food evenly distributed
   do a = 1, flifespan
     do dep = 1, depth
        numcop(dep,a) = nzero*prey  !prey multiplication added 22 April 2010
        !initiate autocorrelated prey field as the average
        autocop(dep,a) = numcop(dep,a)
     end do
   end do

end if  !DVM of prey

!create the backup prey field to return to after changes
do a = 1, flifespan
   do dep = 1,depth
      numcopBAS(dep,a) = numcop(dep,a)
   end do
end do

!make printout of copepod (food) distribution
!call SRcopepod(srerr,test99,flifespan,realgen,numcop)

!Diel light
!Normally, surface light goes through a number of diel cycles over FLIFESPAN
if (diellight == 1) then
   do a = 0, flifespan
      surlig(a) = daylight*0.5*(1.01+sin(3.14*2.*dielcycles*a/(1.*flifespan)))
      !intiate autocorrelated light as the average
      autosurlig(a) = surlig(a)
   end do
  else
   do a = 0, flifespan
      surlig(a) = daylight*0.1  !always same intermediate light intensity
      !intiate autocorrelated light as the average
      autosurlig(a) = surlig(a)
   end do
 end if !diellight
end
!end of subroutine SRlifespan


!-------------------------------------------------------------------
subroutine SRlifecycle(expmt,srerr,test99,generations,gen,depth,dielcycles,   &
       totalsenses,cop2egg,densdep,density,restart,realgen,fpop,fish,flifespan,fmaxweight, &
       fsurvivors,fpopfec,lightdecay,fmultrisk,fmultgain,BMscale,behavstep,behavmax,      &
       motherlength,auto,allmax,allmin,interval,development,env,deviation,zrange,     &
       preymax,dev,memory,curve,NowMaxPercept,MaxPercept,gABM,brain,   &
       restattention,maxstomcap,flivingcost,gainfunction,riskfunction,beamatt,contrast,   &
       preyarea,viscap,eyesat,attackrate,attackfactor,swHsize,swHstomach,swHfood,swFsize, &
       swFlight,swFothers,swFmort,reproinvest,sdgABM,sdAfear,sdAhunger,sdHfear,sdHhunger, &
       sdAHunused,runtag,alleleconv,screenplot,stepABM,minABM,maxABM,hungersensitivity,   &
       errorP,during,reprocap,attackduration,zavgkids,zavgEgg,zavgR0,zsdR0,zavgsurv,zavgstart)
!-------------------------------------------------------------------
!This subroutine follows each individual in a population from birth until egg production,
!through FLIFESPAN age/time steps
!The SR calls upon other subroutines for habitat, hedonic decisions, growth and egg production

implicit none
!include commonblock of array variables
include 'Commonfish.txt'
!variables from main program
integer expmt,generations,gen,realgen,depth,cop2egg,fpop,fish,flifespan,densdep
integer fmaxweight,alleleconv,totalsenses,fsurvivors,fnewpop,srerr,test99,dev
integer restart,behavstep,behavmax,motherlength,dielcycles,env,screenplot
integer memory,zrange,curve,gABM,gainfunction,riskfunction,attackduration
integer swHsize,swHstomach,swHfood,swFsize,swFlight,swFothers,swFmort,during
integer zavgkids,zavgsurv,zavgstart,brain
character(8) deviation
character(6) runtag
real NowMaxPercept(7),MaxPercept(7),hungersensitivity,errorP
real fpopfec,BMscale,allmax,allmin,interval,density,attackrate,attackfactor
real lightdecay,fmultrisk,fmultgain,auto,development,reproinvest,reprocap
real maxstomcap,restattention,preymax,flivingcost,beamatt,contrast,preyarea,viscap,eyesat
real sdgABM,sdAfear,sdAhunger,sdHfear,sdHhunger,sdAHunused,stepABM,minABM,maxABM
real zavgR0,zsdR0,zavgEgg
!local variables
integer gg,hist,copies,new,crashtag
integer age,a,i,g,dep,ind,error99,inp,autocorr,autoend,autotime,foodmax,alive,wasalive
integer predatorattack,attackdepth,attackstep,attack,attacks,duration,danger,avgattacks
real attrisk,oldrisk,feednow,growthnow
real rand,autorisk,fmultrisk1,prev,Rage,mult,copmult,avgdepth,neighbors,inds,mortnow,fearlevel
real STdata(0:101),STkilled(0:101),STstarved(0:101),STfeeding(0:101),STrisk(0:101)
real STaffect(0:101),STage(0:101),STagents(0:101),STdepth(0:101),STmove(0:101)
real STAFlight(0:101,3),STAFother(0:101,3),STAFmort(0:101,3),STAHstom(0:101,3),STAHfood(0:101,3)
real STHFlight(0:101,3),STHFother(0:101,3),STHHfood(0:101,3),STHHother(0:101,3),STAHsize(0:101,10)
character (7) help              !code for rescue-operation
character (8) itag(15000)
character (9) gen1,gen2
character (19) gen3
character (54) string104
character (1) lastgenaffect(15000,5000)  !dominant affect of each ind each age in final generation
integer lastgenzchoice(15000,5000)  !vertical position of each ind each age in final generation
integer lastgenweight(15000,5000)  !body mass of each ind each age in final generation
real lastgenstocap(15000,5000) !available stomach capacity at start of each age in final generation


help = "  "

if (test99 == 1) write(10,*) "SRlifecycle"
error99 = 0
if (srerr == 2) error99 = 1
!if (gen == 2) write(6,*) "SRlifecycle"
!declare all organisms of the new generation alive
do ind = 1,fpop
   findfeel(ind) = -1
   fgonad(ind) = 0.
   fstomach(ind,0) = 0.
   fstomach(ind,1) = 0.
   foffspring(ind) = 0
   father(ind) = 0
   fstatus(ind) = 1
   fbodymax(ind) = cop2egg
   fbodymass(ind) = cop2egg
   fdeathBM(ind) = 0
   fdeathage(ind) = 0
   fdeathaffect(ind) = -1
   fdeathdepth(ind) = 0
   fnumberafraid(ind) = 0
   fneverafraid(ind) = 1  !will be set at 0 as soon as ind becomes afraid
   do a = 1,2
      fmemory(ind,a) = 0.
   end do
end do
!and make sure all others are non-existing
do ind = fpop+1,fish
   findfeel(ind) = -2
   fgonad(ind) = 0.
   foffspring(ind) = 0
   father(ind) = 0
   fstatus(ind) = 0
   fbodymax(ind) = 0.
   fbodymass(ind) = 0.
   fdeathage(ind) = 0
   fbirthdepth(ind) = 0
   fdeathBM(ind) = -10
   fdeathage(ind) = -10
   fdeathaffect(ind) = -10
   fdeathdepth(ind) = -10
   do a = 1,2
      fmemory(ind,a) = 0.
   end do
   do a = 1,motherlength
      motherstring(ind,a) = 0
   end do
   mcreation(ind) = -1 !code for error
   gcreation(ind) = 0
   acreation(ind) = 0
   icreation(ind) = 0
end do

!reset number of eggs produced in a generation
fpopfec = 0.

!reset count of number of predator attacks in generations
attacks = 0

!empty array of fish in depth and time
do dep = 1,depth
   do age = 0,flifespan
      numfish(dep,age) = 0
   end do
end do

do ind = 1,fpop
   dep = fbirthdepth(ind)
   fdepthchoice(ind,0) = dep
   fdepthchoice(ind,1) = dep
   numfish(dep,0) = numfish(dep,0) + 1
end do
do dep = 1,depth
   numfish(dep,1) = numfish(dep,0)
end do

!reset calculations of input neuron statistics
do inp = 1,totalsenses
   finmin(inp) = 1000.
   finmax(inp) = - 1000.
   do age = 1, flifespan
      finavg(inp,age) = 0.
      finsum(inp,age) = 0
   end do
end do
do age = 1, flifespan
   finobs(age) = 0
end do
do i = 1,7
   NowMaxPercept(i) = 0.
end do

!reset calculations of average hedonic affect in each depth
gg = generations
hist = 11
if ((gen > 0 .and. age > 0).and. ((gen > gg-hist) .or. (gen < 0.9*gg .and. gen > 0.9*gg-hist)  &
  .or. (gen < 0.8*gg .and. gen > 0.8*gg-hist) .or. (gen < 0.7*gg .and. gen > 0.7*gg-hist)  &
  .or. (gen < 0.6*gg .and. gen > 0.6*gg-hist) .or. (gen < 0.5*gg .and. gen > 0.5*gg-hist)  &
  .or. (gen < 0.4*gg .and. gen > 0.4*gg-hist) .or. (gen < 0.3*gg .and. gen > 0.3*gg-hist)  &
  .or. (gen < 0.2*gg .and. gen > 0.2*gg-hist) .or. (gen < 0.1*gg .and. gen > 0.1*gg-hist))) then
    do age = 1, flifespan
       do dep = 1, depth
          ffeeling(dep,age) = 0.
       end do
    end do
end if

!reset statistics or incidences of hunger and fear
do i = 1,2
  do g = 1,2
     numHF(i,g) = 0  !counting incidences of hunger and fear
  end do
end do

!open file for output of individual-data in final 10 generations
if (gen > generations-11) then
   call SRendopenclose(expmt,realgen,runtag,test99)
end if

!open files for individual variation and behaviour for the last generation
if (gen == generations) then
   call SRbehavfiles(runtag,expmt,realgen,1,behavstep,behavmax,curve,test99)
end if


fmultrisk1 = fmultrisk
!reset environmental autocorrelation timer
autocorr = 0  !time step within cycle
autorisk = fmultrisk1
autotime = flifespan/20  !autocorrelation cycle length
do a = 1, flifespan
   do dep = 1,depth
      autocop(dep,a) = numcop(dep,a)
   end do
end do

!currently no predator school attack
predatorattack = 0

!make a tag for the event that the population crashed
crashtag = 0
copmult = 1.
mult = 1.

avgattacks = 5
attrisk = avgattacks/(1.*flifespan)
danger = 1 !background risk in absence of school attack
attack = 0 ! no attacks right now

do age = 1, flifespan  !start IBM loop
   !set time according to small-scale environmental variation (covering 5% of lifespan when autotime = flifespan/20)
   if (autocorr == flifespan/20) autocorr = 0
   autocorr = autocorr + 1
   autoend = min(flifespan, age+autotime-1)

   if (dev > 1) then ! predator schools may attack at any random time step
     if (attack == 0) then !no current predator attack
      call random_number (rand)
      if (rand < attrisk) then ! attack initiated just now
         if (screenplot == 2) write(6,*)"attack now"
         oldrisk = autorisk !memory of risk before attack (and after)
         attack = 1  !current status
         attacks = attacks + 1 !sum of attacks in generation
         duration = 1 !counter for duration of attack
         danger = 2   !present elevation
         autorisk = oldrisk * danger
       end if
      else !ongoing attack
       duration = duration + 1
       if (duration == 2) then
          danger = 4
          autorisk = oldrisk * danger
        else if (duration < 8) then
          danger = 10
          autorisk = oldrisk * danger
         else    !attack terminated at time step 8
          attack = 0
          duration = 0
          danger = 1
          autorisk = oldrisk  !background risk reestablished
       end if !duration
     end if ! attack = 0
  else !dev = 1, i.e. "Normal"
    !5 fixed time steps of initiation of attack
      if (age == flifespan/7 .or. age == flifespan/3 .or. age == int(0.6*flifespan) .or. age == int(0.85*flifespan)) then
         oldrisk = autorisk  !memory of risk before attack (and after)
         attack = 1  !current status
         attacks = attacks + 1 !sum of attacks in generation
         duration = 1 !counter for duration of attack
         danger = 2   !present elevation
         autorisk = oldrisk * danger
        else if (attack == 1) then !attack is already going on
          duration = duration + 1
          if (duration == 2) then
            danger = 4
            autorisk = oldrisk * danger
           else if (duration < 8) then
            danger = 10
            autorisk = oldrisk * danger
           else    !attack terminated at time step 8
            attack = 0
             duration = 0
            danger = 1
            autorisk = oldrisk  !background risk reestablished
           end if !duration
       end if !age
   end if !dev

   if (age == 1) then
      if (dev > 1) then  !no small-scale changes in NORMAL generations, except in mortality risk
        !start risk-variation
         call random_number (rand)
         mult = 1. + 2.*auto*(rand-0.5) ! mult is now a random value in the range [1-auto,1+auto]  new from 3 May 10
         autorisk = fmultrisk1*mult*danger
         !intiate autocorrelated prey field deviant from the average
         call random_number (rand)
         mult = 1. + 2.*auto*(rand-0.5) ! mult is now a random value in the range [1-auto,1+auto]  new from 3 May 10
         copmult = mult !ongoing variation in copepod density
         do a = age, autoend
          do dep = 1,depth
            autocop(dep,a) = autocop(dep,a)*mult
          end do
         end do
       else   !non-random short-term changes in mortality risk in NORMAL generations
          autorisk = fmultrisk1 !always same high risk early in life in normal generations
      end if
   end if


   !adjust the environmental autocorrelations
   if (autocorr == 1) then ! adjust surface light
     if (dev > 1) then
        !intiate autocorrelated light deviant from the average
         call random_number (rand)
         mult = 1. + 2.*auto*(rand-0.5) ! mult is now a random value in the range [1-auto,1+auto]  new from 3 May 10
       else
         mult = 1.
     end if
     do a = age, autoend
       autosurlig(a) = surlig(a)*mult
     end do
   end if

   if (autocorr == autotime/3) then ! adjust overall mortality risk (e.g. horizontal movement of predators in or out)
      if (dev > 1) then
         !intiate autocorrelated field deviant from the average
         call random_number (rand)
         mult = 1. + 2.*auto*(rand-0.5) ! mult is now a random value in the range [1-auto,1+auto]  new from 3 May 10
         autorisk = fmultrisk1*mult*danger
        else !normal generations
         autorisk = fmultrisk1
      end if
   end if

   if (dev > 1 .and. autocorr == 2*autotime/3) then ! adjust prey field
         !intiate autocorrelated prey field deviant from the average
         call random_number (rand)
         mult = 1. + 2.*auto*(rand-0.5) ! mult is now a random value in the range [1-auto,1+auto]  new from 3 May 10
         copmult = mult !ongoing variation in copepod density
         do a = age, autoend
           do dep = 1,depth
             autocop(dep,a) = autocop(dep,a)*mult
           end do
         end do
   end if


!find depth of maximum food in current age
   foodmax = 0
   prev = -1.
   do dep = 1, depth
      if (autocop(dep,age) > prev) then
         foodmax = dep
          prev = autocop(dep,age)
      end if
   end do

  !open file for output of individual-data in final 10 generations
  if (gen > generations-11 .and. age == flifespan) then
!    call SRcopepod(srerr,test99,flifespan,realgen,autocop)
     do a = 1, flifespan
       write(13,1301) realgen,a,(autocop(dep,a), dep = 30,1,-1)
     end do
 end if
 1301 format(2I8,30I8)

  !give environmental and other information that lasts the whole time step
  !- is there an attack from a school of predators?
  if (predatorattack == 0) then
      call random_number (rand)
      if (rand < attackrate) then !a predator school attacks
          predatorattack = 1
          attackstep = attackduration !duration of attack
      end if
    else  !ongoing predator school attack
      attackstep = attackstep - 1
      if (attackstep == 0) predatorattack = 0 !the attack lasts a few timesteps
  end if
  call SRinitage(srerr,test99,gen,age,depth,fpop,flifespan,autorisk,    &
                 fmultgain,predatorattack,attackdepth,attackfactor,beamatt, &
                 contrast,preyarea,viscap,eyesat,lightdecay)
  !find the current feeling for all individuals, make spatial decisions
  if (brain == 1) then
         call SRhedonic(srerr,test99,flifespan,dielcycles,age,gen,generations,   &
               alleleconv,behavstep,depth,fpop,totalsenses,restart,lightdecay,   &
               autorisk,foodmax,zrange,curve,NowMaxPercept,MaxPercept,gABM,      &
               maxstomcap,swHsize,swHstomach,swHfood,swFsize,swFlight,swFothers, &
               swFmort,hungersensitivity,realgen,errorP)
      else !use the formulations of hedonic tone from Giske & al. (2003)
         call SR2003hed(srerr,test99,flifespan,age,gen,depth,fpop,totalsenses,   &
               restart,lightdecay,NowMaxPercept,MaxPercept,autorisk,maxstomcap)
  end if
  !store depth for all individuals in all ages in final generation
  if (gen > generations-11) then
    do ind = 1,fpop
      if (fstatus(ind) == 1) then
         lastgenzchoice(ind,age) = fdepthchoice(ind,1)
         lastgenstocap(ind,age) = 1.- fstomach(ind,1)/(0.25*fbodymass(ind))
         if (findfeel(ind) == 1) then
            lastgenaffect(ind,age) = "F"
           else
            lastgenaffect(ind,age) = "H"
         end if
        else
         lastgenaffect(ind,age) = "D"
         lastgenzchoice(ind,age) = 0
         lastgenstocap(ind,age) = -1.
      end if
    end do
  end if

  !evaluate consequences of hedonic decisions, number of individuals in each depth
  call SRhabitat(srerr,test99,expmt,generations,densdep,density,gen,  &
                   age,depth,fpop,fsurvivors,flifespan,gainfunction,   &
                   riskfunction,restattention,BMscale,flivingcost,maxstomcap,     &
                   predatorattack,attackdepth,lightdecay,copmult,autorisk,        &
                   fmultrisk1,deviation,screenplot,realgen,danger,during)

  !Find individual feeding, metabolic costs, survival and growth,
  !i.e. the consequence of the behaviour of the survivors
  feednow = 0.
  growthnow = 0.
  call SRgrowth(runtag,srerr,test99,expmt,generations,flifespan,gen,age,fpop,depth,    &
                fsurvivors,fmaxweight,cop2egg,BMscale,dielcycles,development,   &
                deviation,flivingcost,gABM,restattention,maxstomcap,   &
                realgen,STdata,STkilled,STstarved,STfeeding,STrisk,STaffect,STage,      &
                STagents,STdepth,STmove,STAFlight,STAFother,STAFmort,STAHstom,  &
                STAHfood,STHFlight,STHFother,STHHfood,STHHother,STAHsize,feednow,growthnow)

  !store info on all individuals in all ages in final generations
  if (gen > generations-11) then
    do ind = 1,fpop
      if (fstatus(ind) == 1) then
         lastgenweight(ind,age) = int(fbodymass(ind))
        else
         lastgenweight(ind,age) = 0
      end if
    end do
  end if

  !print info on all individuals in all ages in final generation
  if (gen > generations-11 .and. age == flifespan) then
     write(60,6159)"depth choices of all inds in all ages in generation", gen
     write(60,6101)"age:",(a, a=1,flifespan)
     write(61,6153)"body mass of all inds in all ages in generation", gen
     write(61,6105)"age:",(a, a=1,flifespan)
     write(62,6159)"dominant affect of all inds in all ages in generation", gen
     write(62,6101)"age:",(a, a=1,flifespan)
     write(63,6159)"available stomach capacity in all ages in generation", gen
     write(63,6105)"age:",(a, a=1,flifespan)

    do ind = 1,fpop
         write(60,6102)ind,(lastgenzchoice(ind,a), a=1,flifespan)
     end do
     do ind = 1,fpop
         write(61,6103)ind,(lastgenweight(ind,a), a=1,flifespan)
     end do
     do ind = 1,fpop
         write(62,6104)ind,(lastgenaffect(ind,a), a=1,flifespan)
     end do
     do ind = 1,fpop
         write(63,6106)ind,(lastgenstocap(ind,a), a=1,flifespan)
     end do
  end if
  6101 format(A6,5000I4) !prepared for up to 5000 ages in each life span
  6102 format(I6,5000I4)
  6103 format(I6,5000I6)
  6104 format(I6,5000A4)
  6105 format(A6,5000I6)
  6106 format(I6,5000F6.2)
  6153 format(A53,I6)
  6159 format(A59,I6)

!print environment stocasticity if near the end of the simulation
if (env == 1) then
   if (age == 1) wasalive = fpop  !establish pop.size. at beginning of age

   !make age-statistics and some generation statistics
   avgdepth = 0.
   inds = 0.
   neighbors = 0.
   fearlevel = 0.

   alive = fpop

   do ind = 1,fpop
      if (fstatus(ind) == 1) then
            fearlevel = fearlevel + findfeel(ind) !findfeel is 0 when hungry and 1 when afraid
        else
         alive = alive - 1
      end if
   end do
   mortnow = 100.*(wasalive-alive)/(1.*wasalive) !mortality rate this age
   feednow = feednow/(1.*alive)
   growthnow = growthnow/(1.*alive)
   wasalive = alive !prepare for next age
   fearlevel = 100.*fearlevel/(1.*alive)


   do dep = 1, depth
      inds = inds + numfish(dep,age)
      avgdepth = avgdepth + numfish(dep,age)*dep
      neighbors = neighbors + numfish(dep,age)*max(0,(numfish(dep,age) - 1)) !self not included among neighbours
   end do
   avgdepth = avgdepth/inds
   neighbors = neighbors/inds

   Rage = 1.*gen + (age-1)/(1.*flifespan)
   write(29,2929) realgen,deviation,age,Rage,autosurlig(age),Rage,fmultrisk,autorisk,   &
                    Rage,foodmax,Rage,autocop(foodmax,age),Rage,autotemp(foodmax),      &
                    alive,avgdepth,neighbors,mortnow,feednow,growthnow,fearlevel
end if
2929 format(I12,A12,I10,F10.3,F10.2,F10.3,2F10.5,F10.3,I10,F10.3,I12,F10.3,F12.2,I10,F12.2,F12.0,2F12.4,2F12.3)


fmultrisk1 = fmultrisk


!  prepare array of depth choice and stomach contents for next time step

   do ind = 1,fpop
      fdepthchoice(ind,0) = fdepthchoice(ind,1)
      fstomach(ind,0) = fstomach(ind,1)
   end do

  !boost fecundity in first few generations
  if (gen < 15) help = "boost9"  !make tag


  !create new individuals if population comes close to extinction
   if ((gen < 3 .and. fsurvivors < 3000) .or. (fsurvivors < 1500)) then
        help = "restart"  !make tag
        crashtag = 1 !make tag for printout of fpop after SRfecundity in last age
        write(6,*) "calling SRgenecreate near extinction, fsurvivors =",fsurvivors
        call SRgenecreate(1,fpop,allmax,allmin,interval,depth,gen,age,flifespan,     &
                          cop2egg,fsurvivors,motherlength,memory,curve,gABM,stepABM, &
                          minABM,maxABM,fish,test99,copies,new,itag,brain)

        write(6, *) " "
        write(6, *) "number of individuals created from living:", copies
        write(6, *) "number of individuals created from random:", new
        write(6, *) " "

        !send info to warnings-file
        write(10, *) " "
        write(10, 1001) "generation, age and survivors:", gen, age,fsurvivors-copies-new
        write(10, *) "number of individuals created from living and random:", copies, new
        write(10, *) " "
1001 format(A30,3I9)

        !print new gene pool. First make file name
        if (gen < 10) then
            write(gen1,2902) "gen-0000",gen
          else if (gen < 100) then
            write(gen1,2903) "gen-000",gen
          else if (gen < 1000) then
            write(gen1,2904) "gen-00",gen
          else if (gen < 10000) then
            write(gen1,2905) "gen-0",gen
          else
            write(gen1,2906) "gen-",gen
        end if

        if (age < 10) then
            write(gen2,2902) "age-0000",age
          else if (age < 100) then
            write(gen2,2903) "age-000",age
          else if (age < 1000) then
            write(gen2,2904) "age-00",age
          else if (age < 10000) then
            write(gen2,2905) "age-0",age
          else
            write(gen2,2906) "age-",age
        end if

        write(gen3,2908)gen1,"-",gen2

        if (expmt < 10) then
            write(string104,2901)"HED18-",runtag,"-E0",expmt,"-o104-genepool-",gen3,".txt"
          else
            write(string104,2910)"HED18-",runtag,"-E",expmt,"-o104-genepool-",gen3,".txt"
        end if

2901 format(2A6,A3,I1,A15,A19,A4)
2902 format(A8,I1)
2903 format(A7,I2)
2904 format(A6,I3)
2905 format(A5,I4)
2906 format(A4,I5)
2908 format(A9,A1,A9)
2909 format(2I8,A8,I8,37F8.2,2F12.5,I8)
2910 format(2A6,A2,I2,A15,A19,A4)
2911 format(2I8,A10,I8,F8.0,I8,F8.3)
2912 format(2A8,A10,4A8)

        open(104, file = string104)
        write(104, 1001) "generation, age and survivors:", gen, age,fsurvivors-copies-new
        write(104, *) "number of individuals created from living and random:", copies, new
        write(104, *) " "

        do i = 1,fish
           write(104,2909) i,fstatus(i),itag(i),motherpop(i),(geneAHsize(i,g),g = 1,10),   &
                 (geneAFlight(i,g),g = 1,3),(geneAFother(i,g),g = 1,3),            &
                 (geneAFmort(i,g),g = 1,3),(geneAHstom(i,g),g = 1,3),              &
                 (geneAHfood(i,g),g = 1,3),(geneHFlight(i,g),g = 1,3),             &
                 (geneHFother(i,g),g = 1,3),(geneHHfood(i,g),g = 1,3),             &
                 (geneHHother(i,g),g = 1,3),(geneMemo(i,g),g = 1,2),fbirthdepth(i)
       end do
       close(104)

        !find new fpop, defined by highest individual-number of living ind
      fpop = 0
      do i = 1,fish
         if(fstatus(i) == 1) fpop = fpop + 1
      end do

   endif !(gen < 3 .and. fsurvivors < 3000) .or. (fsurvivors < 1500)

end do !IBM loop

!boost fecundity if population size at end of generation is very low
if (fsurvivors < 1500) help = "boost2"  !make tag

!close files for individual variation and behaviour for the last generation
if (gen == generations) then
    call SRbehavfiles(runtag,expmt,realgen,2,behavstep,behavmax,curve,test99)
!    call SRexpbehavfiles(runtag,expmt,realgen,2,fpop,ind,expmntfish,curve)
end if

!preparation for reproduction, mutations, recombinations and next generation
!call subroutine for producing the eggs of the new generation, depending on individual size
 call SRfecundity(srerr,test99,expmt,generations,gen,fish,fpop,brain,flifespan,      &
        cop2egg,fnewpop,fpopfec,motherlength,totalsenses,deviation,help,fmultrisk,    &
        preymax,curve,gABM,development,reproinvest,sdgABM,sdAfear,sdAhunger,sdHfear,  &
        sdHhunger,sdAHunused,realgen,during,memory,attacks,reprocap,dev,zavgkids,     &
        zavgEgg,zavgR0,zsdR0,zavgsurv,zavgstart)

if (crashtag == 1) then !population has crashed at some age in this generation,
                        !now print the final outcome of the resurrection
        if (expmt < 10) then
            write(string104,2901)"HED18-",runtag,"-E0",expmt,"-o105-finalpop-",gen3,".txt"
          else
            write(string104,2910)"HED18-",runtag,"-E",expmt,"-o105-finalpop-",gen3,".txt"
        end if

        open(105, file = string104)
        write(105, *) "final state of population at the end of generation", gen
        write(105, *) " "
        write(105, 2912) "ind","status","itag","dAge","bmass","sex","gonad"
        do i = 1,fish
           write(105,2911) i,fstatus(i),itag(i),fdeathage(i),fbodymass(i),fgender(i),fgonad(i)
        end do
        close(105)
end if !crashtag


end !end subroutine SRlifecycle


!---------------------------------------------------------------------------------
subroutine SRinitage(srerr,test99,gen,age,depth,fpop,flifespan,autorisk,  &
                     fmultgain,predatorattack,attackdepth,attackfactor,   &
                     beamatt,contrast,preyarea,viscap,eyesat,lightdecay)
!---------------------------------------------------------------------------------
!The environment is a depth gradient where copepod distribution and
!light intensity have major influence on growth and mortality.
!Environment also changes over a diel cycle, due to light intensity.
!Temperature does not vary over time, and was set in the subroutine SRINTRUN

implicit none
!include commonblock of array variables
include 'Commonfish.txt'
!variables from main program
integer gen,age,depth,fpop,srerr,test99
integer flifespan,predatorattack,attackdepth
real autorisk,fmultgain,beamatt,contrast,preyarea,viscap,eyesat,lightdecay,attackfactor
!local variables
integer dep,ind,error99,z,dens
real parta,digestion,span,visr,Eb


if (test99 == 1) write(10,*) "SRinitage"
error99 = 0
if (srerr == 6) error99 = 1
if (error99 == 1) then
   write(6,*) "gen = ",gen
   write(6,*) "age = ",age
   write(6,*) "depth = ",depth
   write(6,*) "fpop = ",fpop
   write(6,*) "flifespan = ",flifespan
   write(6,*) "autorisk = ",autorisk
   write(6,*) "fmultgain = ",fmultgain
end if

span = 1.*flifespan !real number value of flifespan

!if (gen == 2) write(6,*) "SRinitage"
!a fish has not yet made its depth choices
if (error99 == 1) write(6,*) "reset fdepthchoice"

!no individual- based calculation if coming from SRgainplot
!(in SRgainplot the local variable test4 = 100)
if (test99 < 99) then

   do ind = 1,fpop
      fdepthchoice(ind,1) = 0
   end do
end if

if (error99 == 1) write(6,*) "reset numfish(dep)"

do dep = 1, depth
   numfish(dep,age) = 0
end do


!before feeding, the stomach content is evacuated by digestion
!(stomach contents is already part of body size, but limits feeding in current time step)
if (error99 == 1) write(6,*) "reset stomach(ind)"
!no individual- based calculation if coming from Sgainplot
!(in SRgainplot the local variable test4 = 100)
if (test99 < 99) then

   do ind = 1,fpop
     if(age < 2) then  ! i.e. age is 0 (at start of new experiment) or 1 (first time step, no history)
        fstomach(ind,1) = 0.
       else
        !Digestion is scaled so that stomach contents is reduced by 1/3 for a lifespan of 150 time steps.
        digestion = fstomach(ind,0)*100./span
!        digestion = fstomach(ind,0)*0.3333  !new 4 Aug 2010
        !cannot digest more than everything
        fstomach(ind,1) = max(0., fstomach(ind,0) - digestion)
     end if
   end do
end if

!find density-independent habitat values at each depth for whole time step
if (error99 == 1) write(6,*) "cycle depth to find didgain and didrisk"
!Eb is background irradiance at a depth
Eb = autosurlig(age)

do dep = 1, depth
   !Find light at depth and visual range
   Eb = Eb * exp(-lightdecay)
!   irradiance(dep,age) = Eb
   call SRgetR(visr,beamatt,contrast,preyarea,viscap,eyesat,Eb,test99)
   !visual range in current depth in previous time step
   if (age == 1) then
       visran(dep,0) = visr
      else
       visran(dep,0) = visran(dep,1)
   end if
   !visual range in current depth
   visran(dep,1) = visr

   !feeding rate is temperature-dependent (swimming speed etc)
   !fmultgain is assumed for 0C, increasing by a Q10 = 2 for 10 degrees
   !q10 = ln(Q10)/10 = 0.069
!!   parta = fmultgain*exp(autotemp(dep)*0.069315)  !at 15C: 2.83*fmultgain
   !light- and food-dependent feeding gain
   !e = pi*r^2*v*N, so assume the v-component found above (included in parta)
   !(in encounter-rate models, v is swimming speed, which is not accounted for here.
   ! The present model is very generous with growth,
   ! as the life cycle is fulfilled in two or a few diel cycles.)
!!   parta = parta*3.14*visran(dep,age)*visran(dep,age)*autocop(dep,age)
   parta = 2.83*fmultgain*3.14*visr*visr*autocop(dep,age)
   !increasing feeding gain at earliest generations to prevent extinction
!!!!!   if (gen < 3) parta = 1.5*parta + (4-gen)
   !scale feeding gain against number of ages in life cycle (i.e. duration of time step)
   didgain(dep) = parta/span
   !density-independent predation risk component
   didrisk(dep) = autorisk*visr/span
end do

!no calculation if coming from SRgainplot
!(in SRgainplot the local variable test4 = 100)
if (test99 < 99) then
   !is there a predator school attacking?
   if (predatorattack == 1) then !a predator attach is going on, attacking the depth of max agents
!      find depth of attack,i.e. the depth with max number of agents AT THE BEGINNING of the current time step
       z = 1
       dens = numfish(1,age-1)
       do dep = 2,depth
           if (numfish(dep,age-1) > dens) then
              z = dep
              dens = numfish(dep,age-1)
           end if
       end do
       !increase risk where the attack is
       didrisk(z) = attackfactor*didrisk(z)
       attackdepth = z
       if (z > 1) didrisk(z-1) = 0.1*attackfactor*didrisk(z-1)
       if (z < depth) didrisk(z+1) = 0.1*attackfactor*didrisk(z+1)
   end if
end if

end
!end of subroutine SRinitage

!-----------------------------------------------------------------------
subroutine SR2003hed(srerr,test99,flifespan,age,gen,depth,fpop,totalsenses,  &
                     restart,lightdecay,NowMaxPercept,MaxPercept,autorisk,maxstomcap)
!-----------------------------------------------------------------------
!Start state-dependent hedonic decision process as used in Giske & al (2003)
!  J. Giske, M. Mangel, P. Jakobsen, G. Huse, C. Wilcox, and E. Strand. 2003.
!  Explicit trade-off rules in proximate adaptive agents
!  Evolutionary Ecology Research 5: 835-865.
!  web: bio.uib.no/te/papers/Giske_2003_Explicit_trade-off_rules.pdf
!The fish will perceive its environment and state and then make the decision which gives the best overall feelings.
!No mental state, no attention restriction

!include commonblock of arrays
include 'Commonfish.txt'
!variables from main program
integer flifespan,totalsenses,fpop,restart
integer depth,age,gen,srerr,test99,doneit
real lightdecay,autorisk,maxstomcap
real g1,g2,g3,g4,g5,g6,g7,g8,g9  !hedonic genes of this individual
real NowMaxPercept(7),MaxPercept(7)


!local variables
integer ind,choice,dep,lastage,inp,d,error99,first,ind2,old,new,z,class2
real val,comp1,comp2,comp3,habval(3),random  !,comp4
real state1,state2 !,state3,state4,state5,state6,state7,state8
!real rest


if (test99 == 1) write(6,*) "SR2003hed"
error99 = 0
if (srerr == 8) error99 = 1


if (error99 == 1) write(6,*) "start cycle over individuals in do1indHED"
   !remember that age = 0 is the same as age = flifespan, as conditions repeat for each generation
   if (age == 1) then
      if (gen == 1 .or. restart == 1) then
         lastage = 1
          else
         lastage = flifespan
      end if
     else
     lastage = age - 1
 end if

doneit = 0
!do ind = 1,fpop  !indHED-loop
!  !skip the dead
!  if(fstatus(ind) == 0) goto 975


!update vertical distribution at start of desicion process
 do dep = 1,depth
   numfish(dep,age) = numfish(dep,age-1)
end do


!find a random starting point to move through the population
call random_number (random)
first = INT(random*(fpop-1))
do ind2 = 1,fpop  !indHED loop
   if (ind2 + first .le. fpop) then
      ind = ind2 + first
     else
      ind = ind2 + first - fpop
   end if
   !skip the dead
   if(fstatus(ind) < 1) goto 975
   if (ind > fpop) then
      write(6,*)"ind in ind2 in SRhedonic > fpop"
      write(10,*)"ind in ind2 in SRhedonic > fpop:",ind,fpop
      goto 975
   end if

   !!!    !!!!  !!!   !!!!!!
   !!!    !!!!! !!!   !!!
   !!!   !!! !!!!!   !!!!!!      PROCESSES IN TRANSFORMATION OF INFORMATION INPUT TO PERCEPTION VALUES
   !!!    !!!  !!!!   !!!
   !!!    !!!  !!!!   !!!

   !receive information
   !scale information to make each sensory information less than 10
   !   (to allow direct comparison of synaptic strengths)
   if (restart == 1) then
      dep = fbirthdepth(ind)
     else
      dep = fdepthchoice(ind,0)
   end if
   dep = min(dep,depth)
   dep = max(dep,1)

    !  1 stomach fullness
!   fineuron 1 is degree of stomach fullness, before the meal of the time step, but after the digestion in current time step
!    finfo(1) = log(1.+ 2.*fstomach(ind,1)/(0.25*fbodymass(ind)))
!   New thinking: up to HED 13, finfo(1) has measured stomach fullness.
!   Now turning round, measuring stomach free capacity
!   full capacity is MAXSTOMCAP * body mass
    comp1 = 1.- fstomach(ind,1)/(maxstomcap*fbodymass(ind)) !fraction of allowed capacity remaining
!   finfo(1) = comp1/par(1)
    NowMaxPercept(1) = max(NowMaxPercept(1),comp1)
    finfo(1) = comp1/MaxPercept(1)


    !Input neuron 2-6 is light.
   do d = -2,2
      z = dep+d
      z = max(z,1)
      z = min(z,30)
      comp1 = autosurlig(age)*exp(-lightdecay*(z))
!        finfo(d+4) = comp1/par(2)
      NowMaxPercept(2) = max(NowMaxPercept(2),comp1)
         finfo(d+4) = comp1/MaxPercept(2)
   end do

    !7-11 concentration of others at depth
   do d = -2,2
      z = dep+d
      z = max(z,1)
      z = min(z,30)
!      finfo(d+9) = numfish(z,age)/par(3)
      comp1 = numfish(z,age)
      NowMaxPercept(3) = max(NowMaxPercept(3),comp1)
         finfo(d+9) = comp1/MaxPercept(3)
   end do

    !12   body mass
    comp1 = fbodymass(ind)
    NowMaxPercept(4) = max(NowMaxPercept(4),comp1)
       finfo(12) = comp1/MaxPercept(4)

    !13-17 prey encounter rate at depth
    !next 5 informations estimate prey encounter rates.
    !e=pi*r^2*v*N, so that r^2*N is a scaled measure of the same.
    do d = -2,2
      z = dep+d
      z = max(z,1)
      z = min(z,30)
!      finfo(d+15) = visran(z,lastage)*visran(z,lastage)*autocop(z,lastage)/par(5)
      comp1 = visran(z,0)*visran(z,0)*autocop(z,lastage)
      NowMaxPercept(5) = max(NowMaxPercept(5),comp1)
         finfo(d+15) = comp1/MaxPercept(5)
    end do

    !18  age
   NowMaxPercept(6) = max(NowMaxPercept(6),age*1.)
   finfo(18) = 1.*age/MaxPercept(6)

    !19  predation risk
   NowMaxPercept(7) = max(NowMaxPercept(7),autorisk)
   finfo(19) =  autorisk/MaxPercept(7)


!    !  13-15 temperature at depth
!
!      if (error99 == 1 .and. ind < 5) write(6,*) "information number 13-15 for ind", ind
!      if (error99 == 1 .and. ind/10 == ind/10.) write(6,*) "information number 13-15 for ind", ind
!    !Three input neurons remember temperatures
!    if (dep .le. 1) then
!       finfo(13) = 0.
!      else
!       finfo(13)=temperature(dep-1)
!    end if
!    finfo(14) = temperature(dep)
!    if (dep .ge. depth) then
!       finfo(15) = 0.
!      else
!       finfo(15)=temperature(dep+1)
!    end if


   if (error99 == 1 .and. ind < 5) then
      write(6,*) "information rescaling for ind", ind
      do inp = 1,totalsenses
         write(6,*) "finfo", inp,finfo(inp), " for ind ",ind
      end do
   end if
   if (error99 == 1 .and. ind/10 == ind/10.) write(6,*) "information rescaling for ind", ind

     !find frequency, average, min and max value of each neuron each generation
   finobs(age) = finobs(age) + 1
   do inp = 1, totalsenses
      finsum(inp,age) = finfo(inp) + finsum(inp,age)
      if (finmax(inp) < finfo(inp)) finmax(inp) = finfo(inp)
      if (finmin(inp) > finfo(inp)) finmin(inp) = finfo(inp)
      class2 = int(10*finfo(inp))+1
      if(class2 > 20) then
         itrans(inp,21) = itrans(inp,21) + 1  ! finfo larger than range searched
        else
         itrans(inp,class2) = itrans(inp,class2) + 1
      endif
   end do



if (error99 == 1 .and. ind < 5) write(6,*) "hedonic equations for ind", ind
if (error99 == 1 .and. ind/10 == ind/10.) write(6,*) "hedonic equations for ind", ind

   !!!    !!!   !!!!!   !!!!!
   !!!    !!!   !!!     !!! !!!
   !!!!!!!!!   !!!!!   !!!  !!!     Hedonic equations, convert perceptions to feelings
   !!!    !!!   !!!     !!! !!!
   !!!    !!!   !!!!!   !!!!!
   state1= finfo(1)   !food1: stomach fullness
   state2= finfo(12)  !food2: body mass


!convert genes
g1 = -5. + 10.*geneAHfood(ind,1) !convert from (0,1) to (-5,5)
g2 = -5. + 10.*geneAHfood(ind,2) !convert from (0,1) to (-5,5)
g3 = -5. + 10.*geneAHfood(ind,3) !convert from (0,1) to (-5,5)
g4 = -5. +  5.*geneAFlight(ind,1) !convert from (0,1) to (-5,0)   light must yield fear
g5 = -5. + 10.*geneAFlight(ind,2) !convert from (0,1) to (-5,5)
g6 = -5. + 10.*geneAFlight(ind,3) !convert from (0,1) to (-5,5)
g7 = -5. + 10.*geneAFother(ind,1) !convert from (0,1) to (-5,5)
g8 = -5. + 10.*geneAFother(ind,2) !convert from (0,1) to (-5,5)
g9 = -5. + 10.*geneAFother(ind,3) !convert from (0,1) to (-5,5)

!Find hedonic value of current depth (d = 0), and depths above (d = -1) and below (d = 1)
do d = -1, 1
    !the feeling for perceived food
    comp1 = g1 * state1**g2 * state2**g3 * finfo(15+d)
    !the feeling for perceived light
    comp2 = g4 * state1**g5 * state2**g6 * finfo(4+d)
    !the feeling for perceived presence of other conspecifics
    comp3 = g7 * state1**g8 * state2**g9 * finfo(9+d)
    !the feeling for perceived temperature
 !!   comp4 = fgene(ind,10) * state7**fgene(ind,11) * state8**fgene(ind,12)* finfo(14+d)
 !   comp4 = 0. !no temperature any more
    !the sum of arousal from the four feelings in the depth considered
    habval(d+2) = comp1+comp2+comp3 !+comp4
end do

if (error99 == 1) write(6,*) "start comparing choices"
!COMPARE THE THREE POSSIBLE MOVEMENT CHOICES
choice = 1  !(move 1 upwards)
val = habval(3)
do d = 3,2,-1
  if (habval(d) .gt. val) then
    val = habval(d)
    choice = d - 3
  end if
end do

!... AND CHOOSE DEPTH ... while keeping each individual inside the depth range
old = fdepthchoice(ind,0)
new = max(1,old+choice)
new = min(depth,new)
fdepthchoice(ind,1) = new

!then update vertical distribution after each individual's move
!subtract one from previous depth
numfish(old,age) = numfish(old,age) - 1
!add one at new depth
numfish(new,age) = numfish(new,age) + 1

!calculate average hedonic affect in each depth
!(this array is later in SRhabitat divided by actual number of fish in depth)
if (gen == 1 .or. gen == 2 .or. gen == 10 .or. gen == 20 .or. gen == 100 .or. gen == 200    &
   .or. gen == 500 .or. gen == 1000 .or. gen == 2000) then
   dep = fdepthchoice(ind,1)
   ffeeling(dep,age) = ffeeling(dep,age) + 1.   !NB var + val ikke +1
end if


975 continue !next individual, if this one was dead
end do !indHED loop

!if genes were read from file (file = 1),then the restart value shall be switched to 0 after first tour through this subroutine
if (restart == 1) restart = 0

if (error99 == 1) write(6,*) "find finsum(inp,age)"
!final estimate of average value of each input neuron during an age interval
do inp = 1, totalsenses
   if(finobs(age) > 0) then
        finavg(inp,age) = finsum(inp,age)/(1.*finobs(age))
      else
        finavg(inp,age) = 0.
    end if
end do

end
!end SR2003hed (hedonic state dependent decisions)


!-----------------------------------------------------------------------
subroutine SRhedonic(srerr,test99,flifespan,dielcycles,age,gen,     &
                 generations,alleleconv,behavstep,depth,fpop,totalsenses, &
                 restart,lightdecay,autorisk,foodmax,zrange,curve,    &
                 NowMaxPercept,MaxPercept,gABM,         &
                 maxstomcap,swHsize,swHstomach,       &
                 swHfood,swFsize,swFlight,swFothers,swFmort,       &
                 hungersensitivity,realgen,errorP)
!-----------------------------------------------------------------------
!start state-dependent hedonic decision process (Giske et al 2013, 2014)
!The fish will perceive its environment and state and then make the decision giving best feelings

implicit none
!include commonblock of arrays
include 'Commonfish.txt'
!variables from main program
!character(6) runtag
integer flifespan,dielcycles,totalsenses,fpop,restart,foodmax,zrange,curve
integer depth,age,gen,generations,srerr,test99,behavstep,gABM,alleleconv
integer realgen
integer swHsize,swHstomach,swHfood,swFsize,swFlight,swFothers,swFmort
real lightdecay,autorisk,maxstomcap,hungersensitivity,errorP
real NowMaxPercept(7),MaxPercept(7)

!local variables
integer ind,choice,dep,lastage,inp,d,error99,z,g
integer ind2,first,midday,midnight,prevnight,old,new,hist,gg,class2
integer numhungry(30),numafraid(30),numsex(30,2),numafr(30,2)
real value,comp1,random,zero
real a1,a2,a3,a4,a5,a6,a7,a8  !dummies for alleles and finfo in hedonic functions
!real par(6)                !transformation parameters from metrics to 0-1 perception scale
!real g45(200)              !allele value of memory gene for every 50th individual among 10.000 first
real habval(5)
real HdomF(5)              !component of feeling in depth due to perception of food for each ind
real FdomL(5)              !component of feeling in depth due to perception of light for each ind
real FdomO(5)              !component of feeling of fear in depth due to perception of others for each ind
real HdomO(5)              !component of feeling of hunger in depth due to perception of others for each ind
!real ABMgenes(10)          !up to 10 genes deciding how fear or hunger are controlled by age or body mass
real zdominant(30)         !the dominant hedonic tone at this depth (dep)
integer affect             !the dominant hedonic tone for the individual at the start of the time step
real hunger,Hstomach,Hsize,Hfood              !hunger affect and its components
real fear,Fsize,Flight,Fothers,Fmort         !fear affect and its components
!general functions that gives genetically modified affect from perception of stimulus
real sigmoid,linear,gamma2gene,gamma1gene
character (10) comingfrom  !name of this SR to be sent to SRaffect to know where call came from
character (1) tag          !letter for dominant affect (F or H)

comingfrom = "SRhedonics"

if (test99 == 1) write(10,*) "SRhedonic"
error99 = 0
if (srerr == 8) error99 = 1
!if (gen == 2) write(6,*) "SRhedonic"


!to be used only in (files to be made in) last generation
if (gen == generations) then
   midday = flifespan-int(0.75*flifespan/(1.*dielcycles))
   prevnight = flifespan-int(1.25*flifespan/(1.*dielcycles))
   midnight = flifespan-int(0.25*flifespan/(1.*dielcycles))
end if

if (error99 == 1) write(6,*) "start cycle over individuals in do1indHED"
   !remember that age = 0 is the same as age = flifespan, as conditions repeat for each generation
   if (age == 1) then
      if (gen == 1 .or. restart == 1) then
          lastage = 1
        else
          lastage = flifespan
      end if
     else
        lastage = age - 1
   end if

!reset array of dominant hedonic tones
if (gen > generations-10) then

   do dep = 1, depth
     zdominant(dep) = 0.
   end do
end if


!update vertical distribution at start of desicion process

do dep = 1,depth
   numfish(dep,age) = numfish(dep,age-1)
   numafraid(dep) = 0
   numhungry(dep) = 0
end do

!find a random starting point to move through the population
call random_number (random)
first = NINT(random*(fpop-1))


do ind2 = 1,fpop  !indHED loop
   if (ind2 + first .le. fpop) then
      ind = ind2 + first
     else
      ind = ind2 + first - fpop
   end if
   !skip the dead
   if(fstatus(ind) < 1) goto 975
   if (ind > fpop) then
      write(6,*)"ind in ind2 in SRhedonic > fpop"
      write(10,*)"ind in ind2 in SRhedonic > fpop:",ind,fpop
      goto 975
   end if


   !!!!!!  !!!!!!   !!!!!!    !!!!!
   !!  !!  !!!      !!  !!   !!!
   !!!!!!  !!!!!!   !!!!!!   !!      PERCEPTION OF INFORMATION
   !!!     !!!      !! !!    !!!
   !!!     !!!!!!   !!  !!    !!!!!


   !receive information
   !scale information to make each sensory information less than 10
   !   (to allow direct comparison of synaptic strengths)
   if (restart == 1) then
      dep = fbirthdepth(ind)
     else
      dep = fdepthchoice(ind,0)
   end if
   dep = min(dep,depth)
   dep = max(dep,1)

   !All senses are scaled to ensure that each signal is in the range 0-1
   if (error99 == 1 .and. ind < 5) write(6,*) "information number 1 for ind", ind
   if (error99 == 1 .and. ind/10 == ind/10.) write(6,*) "information number 1 for ind", ind


    !  1 stomach fullness

!   fineuron 1 is degree of stomach fullness, before the meal of the time step, but after the digestion in current time step
!    finfo(1) = log(1.+ 2.*fstomach(ind,1)/(0.25*fbodymass(ind)))
!   New thinking: up to HED 13, finfo(1) has measured stomach fullness.
!   Now turning round, measuring stomach free capacity
!   full capacity is MAXSTOMCAP * body mass
    comp1 = 1.- fstomach(ind,1)/(maxstomcap*fbodymass(ind)) !fraction of allowed capacity remaining
!   finfo(1) = comp1/par(1)
    NowMaxPercept(1) = max(NowMaxPercept(1),comp1)
    finfo(1) = comp1/MaxPercept(1)

    !Input neuron 2-6 is light.
   do d = -2,2
      z = dep+d
      z = max(z,1)
      z = min(z,30)
      comp1 = autosurlig(age)*exp(-lightdecay*(z))
!        finfo(d+4) = comp1/par(2)
      NowMaxPercept(2) = max(NowMaxPercept(2),comp1)
         finfo(d+4) = comp1/MaxPercept(2)
   end do


    !7-11 concentration of others at depth
   do d = -2,2
      z = dep+d
      z = max(z,1)
      z = min(z,30)
!      finfo(d+9) = numfish(z,age)/par(3)
      comp1 = numfish(z,age)
      NowMaxPercept(3) = max(NowMaxPercept(3),comp1)
         finfo(d+9) = comp1/MaxPercept(3)
   end do


    !12   body mass
!   finfo(12) = fbodymass(ind)/par(4)  !0.37 until 21.09.09, but this gave transformed body sizes of 2.4
    comp1 = fbodymass(ind)
    NowMaxPercept(4) = max(NowMaxPercept(4),comp1)
       finfo(12) = comp1/MaxPercept(4)


    !13-17 prey encounter rate at depth
    !next 5 informations estimate prey encounter rates.
    !e=pi*r^2*v*N, so that r^2*N is a scaled measure of the same.
    do d = -2,2
      z = dep+d
      z = max(z,1)
      z = min(z,30)
!      finfo(d+15) = visran(z,lastage)*visran(z,lastage)*autocop(z,lastage)/par(5)
      comp1 = visran(z,0)*visran(z,0)*autocop(z,lastage)
      NowMaxPercept(5) = max(NowMaxPercept(5),comp1)
         finfo(d+15) = comp1/MaxPercept(5)
    end do

    !18  age
   NowMaxPercept(6) = max(NowMaxPercept(6),age*1.)
   finfo(18) = 1.*age/MaxPercept(6)

    !19  predation risk
   NowMaxPercept(7) = max(NowMaxPercept(7),autorisk)
   finfo(19) =  autorisk/MaxPercept(7)


   if (error99 == 1 .and. ind < 5) then
      write(6,*) "information rescaling for ind", ind
      do inp = 1,totalsenses
         write(6,*) "finfo", inp,finfo(inp), " for ind ",ind
      end do
   end if
   if (error99 == 1 .and. ind/10 == ind/10.) write(6,*) "information rescaling for ind", ind


   !find frequency, average, min and max value of each neuron each generation
   finobs(age) = finobs(age) + 1
   do inp = 1, totalsenses
      finsum(inp,age) = finfo(inp) + finsum(inp,age)
      if (finmax(inp) < finfo(inp)) finmax(inp) = finfo(inp)
      if (finmin(inp) > finfo(inp)) finmin(inp) = finfo(inp)
      class2 = int(10*finfo(inp))+1
      if(class2 > 20) then
         itrans(inp,21) = itrans(inp,21) + 1  ! finfo larger than range searched
        else
         itrans(inp,class2) = itrans(inp,class2) + 1
      endif
   end do


if (error99 == 1 .and. ind < 5) write(6,*) "hedonic equations for ind", ind
if (error99 == 1 .and. ind/10 == ind/10.) write(6,*) "hedonic equations for ind", ind

       !       !!!!!    !!!!!
      ! !      !!!      !!!              Affect equations, convert perceptions
     !!!!!     !!!!!    !!!!!            into feelings and affective state
    !!   !!    !!!      !!!
   !!     !!   !!!      !!!


call SRaffect(ind,curve,alleleconv,gABM,affect,tag,hungersensitivity,errorP,  &
             swHsize,swHstomach,swHfood,swFsize,swFlight,swFothers,swFmort,  &
             hunger,Hstomach,Hsize,Hfood,fear,Fsize,Flight,Fothers,Fmort,    &
             comingfrom,test99)



   !!!   !!!   !!!!!   !!!!!
   !!!   !!!   !!!     !!! !!!         Hedonic equations, using dominant
   !!!!!!!!!   !!!!!   !!!  !!!        affect to make desisions
   !!!   !!!   !!!     !!! !!!
   !!!   !!!   !!!!!   !!!!!

do d = 1,5
  FdomL(d) = 0.
  FdomO(d) = 0.
  HdomF(d) = 0.
  HdomO(d) = 0.
end do


!sample environment to find the least painful place
if (affect == 1) then !fear is dominant

!Change from 2 Feb 2010. The program can allow an agent with visual range > 1 m to search 2 cells
!above and below, not just 1, and make choice among 5 rather than 3 depths. This happens if RANGE = 2.


!   IF FEAR IS DOMINANT AFFECT
!   Find fear for current depth (d = 0), and 2 depths above (d = -2) and below (d = 2)
    a1 = geneHFlight(ind,1)
    a2 = geneHFlight(ind,2)
    a3 = geneHFlight(ind,3)
    a4 = geneHFother(ind,1)
    a5 = geneHFother(ind,2)
    a6 = geneHFother(ind,3)


    if (alleleconv == 2) then
          a1 = 10*a1*abs(a1)
          a2 = 10*a2*abs(a2)
          a3 = 10*a3*abs(a3)
          a4 = 10*a4*abs(a4)
          a5 = 10*a5*abs(a5)
          a6 = 10*a6*abs(a6)

      else if (alleleconv == 3) then
         a1 = 10*a1
         a2 = 10*a2
         a3 = 10*a3
         a4 = 10*a4
         a5 = 10*a5
         a6 = 10*a6
    end if

    do d = -2, 2
            a7 = finfo(4+d)
            a8 = finfo(9+d)
          if (curve == 1) then ! sigmoid relationship
             FdomL(d+3) = sigmoid(a1,a2,a3,a7)
             zero = sigmoid(a1,a2,a3,0.)
             FdomL(d+3) = FdomL(d+3)-zero
           else if (curve == 2) then !linear relationship
             FdomL(d+3) = linear(a1,a2,a3,a7)
           else if (curve == 3) then !gamma distribution relationship
             FdomL(d+3) = gamma2gene(a1,a2,a7,errorP)
           else if (curve == 4) then !predefined gamma distribution relationship
             FdomL(d+3) = gamma1gene(a1,a2,a7,errorP)
          end if

       !The frightened animal has a desire to hide in a crowd
       !the strength of the attraction to others
          if (curve == 1) then ! sigmoid relationship
             FdomO(d+3) = sigmoid(a4,a5,a6,a8)
             zero = sigmoid(a4,a5,a6,0.)
             FdomO(d+3) = FdomO(d+3)-zero
           else if (curve == 2) then !linear relationship
             FdomO(d+3) = linear(a4,a5,a6,a8)
           else if (curve == 3) then !gamma distribution relationship
             FdomO(d+3) = gamma2gene(a4,a5,a8,errorP)
           else if (curve == 4) then !predefined gamma distribution relationship
             FdomO(d+3) = gamma1gene(a4,a5,a8,errorP)
          end if

      !the sum of arousal in the depth considered: attraction to others and avoidance of light
      !positive value means habitat is good
      habval(d+3) = FdomO(d+3) - FdomL(d+3)
    end do

  else

!   IF HUNGER IS DOMINANT AFFECT
!   Find hedonic value of current depth (d = 0), and 2 depths above (d = -2) and below (d = 2)
    a1 = geneHHfood(ind,1)
    a2 = geneHHfood(ind,2)
    a3 = geneHHfood(ind,3)
    a4 = geneHHother(ind,1)
    a5 = geneHHother(ind,2)
    a6 = geneHHother(ind,3)
    if (alleleconv == 2) then
          a1 = 10*a1*abs(a1)
          a2 = 10*a2*abs(a2)
          a3 = 10*a3*abs(a3)
          a4 = 10*a4*abs(a4)
          a5 = 10*a5*abs(a5)
          a6 = 10*a6*abs(a6)
      else if (alleleconv == 3) then
         a1 = 10*a1
         a2 = 10*a2
         a3 = 10*a3
         a4 = 10*a4
         a5 = 10*a5
         a6 = 10*a6
    end if
    do d = -2, 2
            a7 = finfo(15+d)
            a8 = finfo(9+d)
       !foood attraction: seeing food increases appetite
          if (curve == 1) then ! sigmoid relationship
             HdomF(d+3) = sigmoid(a1,a2,a3,a7)
             zero = sigmoid(a1,a2,a3,0.)
             HdomF(d+3) = HdomF(d+3)-zero
           else if (curve == 2) then !linear relationship
             HdomF(d+3) = linear(a1,a2,a3,a7)
           else if (curve == 3) then !gamma distribution relationship
             HdomF(d+3) = gamma2gene(a1,a2,a7,errorP)
           else if (curve == 4) then !predefined gamma distribution relationship
             HdomF(d+3) = gamma1gene(a1,a2,a7,errorP)
          end if

       !competitor avoidance: food competitors increase stress
          if (curve == 1) then ! sigmoid relationship
             HdomO(d+3) = sigmoid(a4,a5,a6,a8)
             zero = sigmoid(a4,a5,a6,0.)
             HdomO(d+3) = HdomO(d+3)-zero
           else if (curve == 2) then !linear relationship
             HdomO(d+3) = linear(a4,a5,a6,a8)
           else if (curve == 3) then !gamma distribution relationship
             HdomO(d+3) = gamma2gene(a4,a5,a8,errorP)
           else if (curve == 4) then !predefined gamma distribution relationship
             HdomO(d+3) = gamma1gene(a4,a5,a8,errorP)
          end if

      !the sum of arousal in the depth considered: appetite from food and stress in crowds
      !positive value means habitat is good
       habval(d+3) = HdomF(d+3) - HdomO(d+3)
    end do
end if


if (error99 == 1) write(6,*) "start comparing choices"
!COMPARE THE 3-5 POSSIBLE MOVEMENT CHOICES
!The organism is trying to reduce the value of its dominant affect, either hunger or fear
!both affects are impacted by a positive and a negative hedonic feeling.
!Choose the depth (down, stay, up) that gives the strongest (most positive or least negative) feeling.
!habval 1: conditions 2 depths up
!habval 2: conditions 1 depth up
!habval 3: conditions in present depth
!habval 4: conditions 1 depth down
!habval 5: conditions 2 depths down

if (zrange == 2 .and. visran(dep,0) > 1.0) then     !explore 5 depth cells
  choice = 2  !(move 2 upwards)
  value = habval(5)
  do d = 4,1,-1
    if (habval(d) .gt. value) then
     value = habval(d)
     choice = d - 3
    end if
  end do
 else                    !explore 3 depth cells (zrange = 1 or visual range short)
  choice = 1  !(move 1 upwards)
  value = habval(3)
  do d = 3,2,-1
    if (habval(d) .gt. value) then
     value = habval(d)
     choice = d - 3
    end if
  end do
end if

!... AND CHOOSE DEPTH ... while keeping each individual inside the depth range
old = fdepthchoice(ind,0)
new = max(1,old+choice)
new = min(depth,new)
fdepthchoice(ind,1) = new

!then update vertical distribution after each individual's move
!subtract one from previous depth
numfish(old,age) = numfish(old,age) - 1
!add one at new depth
numfish(new,age) = numfish(new,age) + 1


!Count individuals in depth according to dominant affect
if (affect == 1) then !animal afraid
     numafraid(new) = numafraid(new) + 1
   else
     numhungry(new) = numhungry(new) + 1
endif

!print individual state and decisions for selected individuals
if (gen == generations .and. ind < 10001 .and. ind/100 == ind/100.)   &
   call SRbehaviour(tag,ind,age,behavstep,habval,fear,hunger,Fsize, &
                    Flight,Fothers,Fmort,Hstomach,Hsize,Hfood,HdomF,   &
                    FdomL,FdomO,HdomO,foodmax,test99)


!calculate average hedonic affect in each depth
!(this array is later in SRhabitat divided by actual number of fish in depth)

gg = generations
hist = 11
if ((gen > 0 .and. age > 0).and. ((gen > gg-hist) .or. (gen < 0.9*gg .and. gen > 0.9*gg-hist)                             &
  .or. (gen < 0.8*gg .and. gen > 0.8*gg-hist) .or. (gen < 0.7*gg .and. gen > 0.7*gg-hist)  &
  .or. (gen < 0.6*gg .and. gen > 0.6*gg-hist) .or. (gen < 0.5*gg .and. gen > 0.5*gg-hist)  &
  .or. (gen < 0.4*gg .and. gen > 0.4*gg-hist) .or. (gen < 0.3*gg .and. gen > 0.3*gg-hist)  &
  .or. (gen < 0.2*gg .and. gen > 0.2*gg-hist) .or. (gen < 0.1*gg .and. gen > 0.1*gg-hist))) then
   dep = fdepthchoice(ind,1)
   ffeeling(dep,age) = ffeeling(dep,age) + affect
end if

!find dominant affect at depth. If no fish zdominant remains at 0, if many hungry average will be close to 2.
if (gen > generations-11) then
   dep = fdepthchoice(ind,1)
   zdominant(dep) = zdominant(dep) + affect
end if


975 continue !next individual, if this one was dead
end do !indHED loop
!Here ends the IND2 loop

!make print near end for 3D plot
if (gen > generations-11) then
  !find depth and affect for each gender

  do dep = 1,depth
     do g = 1,2
        numsex(dep,g) = 0
        numafr(dep,g) = 0
     end do
  end do

  do ind = 1,fpop
     if (fstatus(ind) == 1) then
        dep = fdepthchoice(ind,1)
        g = fgender(ind)
        numsex(dep,g) = numsex(dep,g) + 1
        numafr(dep,g) = numafr(dep,g) + findfeel(ind) !findfeel = 0 if ind is hungry
     end if
  end do

  !find dominant feeling in depth

  do dep = 1,depth
   if (numfish(dep,age) < 1) then
      zdominant(dep) = 0.
     else
      zdominant(dep) = zdominant(dep)/(1.*numfish(dep,age))
   endif
  end do
  do dep = 1,depth
       write(35,3571)age,dep,autocop(dep,age),numfish(dep,age),numhungry(dep),   &
       numafraid(dep),zdominant(dep),numsex(dep,1),numafr(dep,1),numsex(dep,2),numafr(dep,2)
  end do
end if

3571 format(2I7,4I9,F7.3,4I9)

!if genes were read from file (file = 1), then the restart value shall be switched to 0 after first tour through this subroutine
if (restart == 1) restart = 0

if (error99 == 1) write(6,*) "find finsum(inp,age)"
!final estimate of average value of each input neuron during an age interval
   do inp = 1, totalsenses
      if(finobs(age) > 0) then
        finavg(inp,age) = finsum(inp,age)/(1.*finobs(age))
       else
          finavg(inp,age) = 0.
      end if
   end do

  !print age-dependent average values of input neurons
    if (gen == generations/100 .or. gen == generations/50 .or. gen == generations/25        &
     .or. gen == generations/20 .or. gen == 2*generations/20 .or. gen == 3*generations/20    &
     .or. gen == 4*generations/20 .or. gen == 5*generations/20 .or.                            &
     gen == 6*generations/20 .or. gen == 7*generations/20 .or.gen == 8*generations/20        &
     .or. gen == 9*generations/20 .or. gen == 10*generations/20 .or.                        &
     gen == 11*generations/20 .or. gen == 12*generations/20 .or. gen == 13*generations/20    &
     .or. gen == 14*generations/20 .or. gen == 15*generations/20 .or.                        &
     gen == 16*generations/20 .or. gen == 17*generations/20 .or.gen == 18*generations/20    &
     .or. gen == 19*generations/20 .or. gen == generations .or. (gen > 1 .and. gen < 5) )   &
     call SRageneurons(srerr,test99,realgen,age,totalsenses,fpop,MaxPercept)

end
!end SRhedonic (hedonic state- and personality-dependent decisions)


!-------------------------------------------------------------------
subroutine SRhabitat(srerr,test99,expmt,generations,densdep,density,gen,   &
                     age,depth,fpop,fsurvivors,flifespan,gainfunction,      &
                     riskfunction,restattention,BMscale,flivingcost,maxstomcap,        &
                     predatorattack,attackdepth,lightdecay,copmult,autorisk,           &
                     fmultrisk1,deviation,screenplot,realgen,danger,during)

!-------------------------------------------------------------------
!The environment is a depth gradient varying in contribution to
!fecundity and in mortality risk.
!0 and DEPTH+1 are fake depths for simpler calculation of neurons

implicit none
!include commonblock of array variables
include 'Commonfish.txt'
!variables from main program
integer expmt,generations,gen,age,depth,fpop,fsurvivors,densdep
integer srerr,test99,flifespan,gainfunction,riskfunction
integer predatorattack,attackdepth,screenplot,realgen,danger,during
real density,restattention,BMscale,flivingcost,maxstomcap,lightdecay
real copmult,autorisk,fmultrisk1
!local variables
integer dep,ind,i,gg,error99,finalGG,hist,Hnum(30),Anum(30),verticalfile
integer z(200)
real HavgBM(30),HavgPstAvail(30),AavgBM(30),AavgPstAvail(30),divisor
real Hrisk,Arisk,Havgcost,Aavgcost,PstAvail,AvailStomCap,maxstom,Eb
character(2) PredFlag(30)
character(8) deviation


if (test99 == 1) write(10,*) "SRhabitat"
error99 = 0
if (srerr == 7) error99 = 1

!initiate print-control variables
gg = generations
finalGG = 0
if (gen > 0.81*gg) then
   hist = 11
  else
   hist = 5
end if
!last = gg-hist
finalGG = 0
if (gen > 0 .and. age > 0) then
  if (during == 1) then !many prints during evolution
    if  ((gen > gg-hist) .or. (gen < 0.9*gg .and. gen > 0.9*gg-hist)                          &
     .or. (gen < 0.8*gg .and. gen > 0.8*gg-hist) .or. (gen < 0.7*gg .and. gen > 0.7*gg-hist)  &
     .or. (gen < 0.6*gg .and. gen > 0.6*gg-hist) .or. (gen < 0.5*gg .and. gen > 0.5*gg-hist)  &
     .or. (gen < 0.4*gg .and. gen > 0.4*gg-hist) .or. (gen < 0.3*gg .and. gen > 0.3*gg-hist)  &
     .or. (gen < 0.2*gg .and. gen > 0.2*gg-hist) .or. (gen < 0.1*gg .and. gen > 0.1*gg-hist)) then
        finalGG = 1
    end if
   else if  (gen > gg-hist) then
      finalGG = 1
  end if !during
end if !gen > 0

!Find number, body mass and stomach capacity of hungry and afraid agents
if (finalGG == 1) then

   do dep = 1,depth
      HavgBM(dep) = 0.
      AavgBM(dep) = 0.
      HavgPstAvail(dep) = 0.
      AavgPstAvail(dep) = 0.
      Hnum(dep) = 0
      Anum(dep) = 0
   end do

   do ind = 1,fpop
      if (fstatus(ind) == 1) then
         if (age == 0) then
            dep = fbirthdepth(ind)
           else
            dep = fdepthchoice(ind,1)
         end if
         if (findfeel(ind) == 0) then ! hungry individual
            Hnum(dep) = Hnum(dep) + 1
            HavgBM(dep) = HavgBM(dep) + fbodymass(ind)
            !stomach capacity is given as maximum MAXSTOMCAP of body mass
               maxstom = maxstomcap*fbodymass(ind)
               availstomcap = max(0., maxstom - fstomach(ind,0))
            PstAvail = 100.*availstomcap/maxstom
            HavgPstAvail(dep) = HavgPstAvail(dep) + PstAvail
           else  !ind afraid
            Anum(dep) = Anum(dep) + 1
            AavgBM(dep) = AavgBM(dep) + fbodymass(ind)
            !stomach capacity is given as maximum MAXSTOMCAP of body mass
               maxstom = maxstomcap*fbodymass(ind)
               availstomcap = max(0., maxstom - fstomach(ind,0))
            PstAvail = 100.*availstomcap/maxstom
            AavgPstAvail(dep) = AavgPstAvail(dep) + PstAvail
         end if
      end if
   end do


   do dep = 1, depth
      if (Hnum(dep) > 0) then
         HavgBM(dep) = HavgBM(dep)/(1.*Hnum(dep))
         HavgPstAvail(dep) = HavgPstAvail(dep)/(1.*Hnum(dep))
      end if
      if (Anum(dep) > 0) then
         AavgBM(dep) = AavgBM(dep)/(1.*Anum(dep))
         AavgPstAvail(dep) = AavgPstAvail(dep)/(1.*Anum(dep))
      end if
      if (numfish(dep,age) > 0) then
         ffeeling(dep,age) = ffeeling(dep,age)/(1.*numfish(dep,age))
        else
         ffeeling(dep,age) = -0.01
      end if
   end do

  !print average hedonic affect in each depth
  !(this statistic comes from SRhedonic and is here divided by actual number of fish in depth)
   if (gen == 1 .or. gen == 2 .or. gen == 100 .or. gen == 200 .or. gen == 1000 .or. gen == 2000    &
      .or. gen == 5000 .or. gen == 10000 .or. gen == 20000 .or. gen == gg/2 .or. gen == gg) then
      write(15,1501)expmt,realgen,age,(ffeeling(dep,age),dep = depth, 1, -1)
   end if
end if
1501 format(I3,I6,I5,30F11.3)

!current surface light
Eb = autosurlig(age)

!find density-dependent gain and risk in each depth
fgain(0) = 0.
frisk(0) = 1.
do dep = 1, depth
   !(in SRgainplot the local variable test4 = 100)
   if (test99 < 99 .and. numfish(dep,age) < 1) then
     fgain(dep) = 0.
     frisk(dep) = 0.
    else
     !density-dependent gain and risk - or not
     !(in SRgainplot the local variable test4 = 100)
     if (test99 == 100) then  !plotting density-dependent gain and risk
         divisor = 1.*fpop
       else if (densdep == 1) then
        divisor = 1.*numfish(dep,age)
       else
        divisor = density
     end if
     !density-dependent component added to feeding gain
     if (gainfunction == 1) then
         fgain(dep) = didgain(dep)/divisor      !linear
       else if (gainfunction == 2) then
         fgain(dep) = didgain(dep)/sqrt(divisor) !sqare root
      else
         fgain(dep) = didgain(dep)/(divisor)**2. !squared
     end if
   !density-dependent component added to predation risk
     if (riskfunction == 1) then
         frisk(dep) = didrisk(dep)/divisor      !linear
       else if (riskfunction == 2) then
         frisk(dep) = didrisk(dep)/sqrt(divisor) !sqare root
      else
         frisk(dep) = didrisk(dep)/(divisor)**2. !squared
     end if

     !keep mortality rate within outer limits
     frisk(dep) = min (frisk(dep), 0.99)
     frisk(dep) = max (frisk(dep), 0.)
   end if


   if (finalGG == 1) then
      !light at depth
      Eb = Eb *    exp(-lightdecay)
      !approximate risk level for hungry and afraid agents
      Hrisk = frisk(dep)*(0.001*HavgBM(dep))**BMscale
      Arisk = restattention*frisk(dep)*(0.001*AavgBM(dep))**BMscale
        Havgcost = HavgBM(dep)*flivingcost/(1.*flifespan)
        Aavgcost = AavgBM(dep)*flivingcost/(1.*flifespan)
      if (predatorattack == 1 .and. attackdepth == dep) then
         PredFlag(dep) = "A"
        else
         PredFlag(dep) = " "
      end if
      if (dep == 1) then
         write(11,*) " "
         write(11,1101) "gen","age","z", "light", "visran","Attack","cop","agents","Hungry",  &
                  "Afraid","H-BM","A-BM","H-cost","A-cost","H%StAv","A%StAv",  &
                  "DI-gain"," gainHung"," gainAfr","DI-risk","f-risk",         &
                  " riskHung"," riskAfr","% Afraid"
         write(11,1103)"Dev:",autosurlig(age)/surlig(age),deviation,danger,copmult,autorisk/fmultrisk1
      end if
      write(11,1102) realgen,age,dep,Eb,visran(dep,1),PredFlag(dep),autocop(dep,age),  &
           numfish(dep,age),Hnum(dep),Anum(dep),HavgBM(dep),AavgBM(dep),Havgcost, &
           Aavgcost,HavgPstAvail(dep),AavgPstAvail(dep),didgain(dep),    &
           fgain(dep),restattention*fgain(dep),didrisk(dep),frisk(dep),     &
           Hrisk,Arisk,100.*ffeeling(dep,age)
   end if
end do
fgain(depth+1) = 0.
frisk(depth+1) = 1.

if (gen == generations) then
  do i = 1,200
   ind = i*50
   if(ind .le. fpop) then
     z(i) = fdepthchoice(ind,1)
    else
     z(i) = 0
   end if
  end do
  write(28,2801)age,(z(i), i = 1,200)
end if
2801 format(201I7)

gg = generations
verticalfile = 0
if(during == 1) then
  if ((gen < 21 .and. gen > 0) .or. (gen > 0 .and. (gen > gg - 21) .or. (gen < 1+0.9*gg .and. gen > 0.9*gg-10)      &
     .or. (gen < 1+0.8*gg .and. gen > 0.8*gg-10).or. (gen < 1+0.7*gg .and. gen > 0.7*gg-10)  &
     .or. (gen < 1+0.6*gg .and. gen > 0.6*gg-10).or. (gen < 1+0.5*gg .and. gen > 0.5*gg-10)  &
     .or. (gen < 1+0.4*gg .and. gen > 0.4*gg-10).or. (gen < 1+0.3*gg .and. gen > 0.3*gg-10)  &
     .or. (gen < 1+0.2*gg .and. gen > 0.2*gg-10).or. (gen < 1+0.1*gg .and. gen > 0.1*gg-10))) then
     verticalfile = 1
  end if
end if
if (during == 0 .and. gen > gg-10) verticalfile = 1


if (verticalfile + screenplot > 0) call SRvertical(srerr,test99,expmt,screenplot,  &
        verticalfile,39,realgen,age,fsurvivors,flifespan,depth,autorisk)
1101 format (A5,2A5,A10,  A9,  A7,A8,3A7,2A8,  4A7,  4A10,        3A13,  A10)
1102 format (I5,2I5,F10.4,F9.4,A7,I8,3I7,2F8.1,4F7.1,F10.1,3F10.3,3F13.7,F10.1)
1103 format (A5,    F20.4,A9,  I6,F9.4,                      F105.4)

end
!end subroutine SRhabitat


!-------------------------------------------------------------------------
subroutine SRgrowth(runtag,srerr,test99,expmt,generations,flifespan,gen,        &
                    age,fpop,depth,fsurvivors,fmaxweight,cop2egg,BMscale,      &
                    dielcycles,development,deviation,flivingcost,     &
                    gABM,restattention,maxstomcap,realgen,STdata,STkilled,   &
                    STstarved,STfeeding,STrisk,STaffect,STage,STagents,STdepth,      &
                    STmove,STAFlight,STAFother,STAFmort,STAHstom,STAHfood,STHFlight,  &
                    STHFother,STHHfood,STHHother,STAHsize,feednow,growthnow)
!-------------------------------------------------------------------------
!evaluate growth and mortality consequences of habitat choice under
!density-dependent or density-independent growth

implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer expmt,generations,realgen,flifespan,gen,age,fpop,depth
integer fmaxweight,srerr,test99,cop2egg,dielcycles,gABM
character(6)runtag
real BMscale,development,restattention,maxstomcap,flivingcost,feednow,growthnow
real STdata(0:101),STkilled(0:101),STstarved(0:101),STfeeding(0:101),STrisk(0:101)
real STaffect(0:101),STage(0:101),STagents(0:101),STdepth(0:101),STmove(0:101)
real STAFlight(0:101,3),STAFother(0:101,3),STAFmort(0:101,3),STAHstom(0:101,3),STAHfood(0:101,3)
real STHFlight(0:101,3),STHFother(0:101,3),STHHfood(0:101,3),STHHother(0:101,3),STAHsize(0:101,10)
!local variables
integer ind,fsurvivors,dep,g,c,error99
!midday,midnight,prevnight,field
!integer thousand,var1(200),nr,gn,totalgenes,i
integer weight,step,class1
integer countinds(10),counthungry(10),countafraid(10)
integer nowalive(2),numgender(30,2)
integer firstmidday,firstmidnight,lastmidday,lastmidnight,checkpoint,point,year
real weightatdep(30)       !average weight of fish at each depth
!real var2(200),var3(200),var4(200),igene(200,49)
real survival,periodcost,availstomcap,fishrisk,feeding,factor,comp1,span
!real hundredind(200,9) !characteristics of 200 individuals midday and midnight last day in last gen
real quarter,d
real nowlocal(2),nowdep(2),nowBM(2)
!character(6) stringgen
character(8) deviation
!character(16) string1
character(18) stringEX
character(57) stringST

if (test99 == 1) write(10,*) "SRgrowth"
error99 = 0
if (srerr == 9) error99 = 1
!if (gen == 2) write(6,*) "SRgrowth"

!reset stomach content classes for each generation
if (gen > generations - 100 .and. age == 1) then

   do c = 0,101
      STdata(c) = 0.
      STaffect(c) = 0.
      STage(c) = 0.
      STagents(c) = 0.
      STdepth(c) = 0.
      STmove(c) = 0.
      STstarved(c) = 0.
      STkilled(c) = 0.
      STrisk(c) = 0.
      STfeeding(c) = 0.
      do g = 1,gABM
        STAHsize(c,g) = 0.
      end do
      do g = 1,3
        STAFlight(c,g) = 0.
        STAFother(c,g) = 0.
        STAFmort(c,g) = 0.
        STAHstom(c,g) = 0.
        STAHfood(c,g) = 0.
        STHFlight(c,g) = 0.
        STHFother(c,g) = 0.
        STHHfood(c,g) = 0.
        STHHother(c,g) = 0.
      end do
   end do
end if

span = 1.*flifespan
!STEP 1: the contribution to the life time fitness of an ind is its energy saved for fecundity, ...

do ind = 1,fpop
   !skip dead fish
   if (fstatus(ind) == 1) then
     !energetic costs is a fraction of body weight and scales to number of time steps
!     periodcost = fbodymass(ind)*flivingcost/span
     periodcost = fbodymass(ind)*flivingcost/span  !redefined 4 Aug 2010
     !stomach capacity is given as maximum MAXSTOMCAP of body mass
     availstomcap = max(0., maxstomcap*fbodymass(ind) - fstomach(ind,1))

     !assign the stomach fullness class of this individual
     !(classified by unused fraction of max stomach capacity at start of this time step,
     !i.e. after affect obtained and movement done, but no feeding yet)
5510 format(4I6,4F12.5)

     if (gen > generations - 100) then !only do this in latter part of simulation
        comp1 = 1.- fstomach(ind,1)/(maxstomcap*fbodymass(ind))
        class1 = nint(100.*comp1)
        if (class1 > 100) then
           write(10,5510) class1,ind,age,realgen,comp1,fstomach(ind,1),maxstomcap,fbodymass(ind)
           class1 = 100
          else if (class1 < 1) then
           write(10,5510) class1,ind,age,realgen,comp1,fstomach(ind,1),maxstomcap,fbodymass(ind)
           class1 = 1
        end if
        STdata(class1) = STdata(class1) + 1. !another observation to the data set
        STaffect(class1) = STaffect(class1) + findfeel(ind) !0 when hungry, 1 when afraid
        STage(class1) = STage(class1) + age
        STagents(class1) = STagents(class1) + numfish(fdepthchoice(ind,1),age)
        STdepth(class1) = STdepth(class1) + fdepthchoice(ind,1)
        !upwards is a negative number, downwards a positive
        STmove(class1) = STmove(class1) + fdepthchoice(ind,1) - fdepthchoice(ind,0)
      do g = 1,gABM
        STAHsize(class1,g) = STAHsize(class1,g) + geneAHsize(ind,g)
      end do
      do g = 1,3
        STAFlight(class1,g) = STAFlight(class1,g) + geneAFlight(ind,g)
        STAFother(class1,g) = STAFother(class1,g) + geneAFother(ind,g)
        STAFmort(class1,g) = STAFmort(class1,g) + geneAFmort(ind,g)
        STAHstom(class1,g) = STAHstom(class1,g) + geneAHstom(ind,g)
        STAHfood(class1,g) = STAHfood(class1,g) + geneAHfood(ind,g)
        STHFlight(class1,g) = STHFlight(class1,g) + geneHFlight(ind,g)
        STHFother(class1,g) = STHFother(class1,g) + geneHFother(ind,g)
        STHHfood(class1,g) = STHHfood(class1,g) + geneHHfood(ind,g)
        STHHother(class1,g) = STHHother(class1,g) + geneHHother(ind,g)
      end do

     end if

!    frightended animals eat less efficiently than hungry animals
     if (findfeel(ind) == 0) then !hungry animal, full attention on food
         factor = 1.
        else  !animal afraid and eat less
         factor = restattention
     end if
     !feeding can be limited by food encounter or stomach capacity
     feeding = min(availstomcap, factor*fgain(fdepthchoice(ind,1)))
     feednow = feednow + feeding ! population average feeding rate, to be printed in file 29
     growthnow = growthnow + (feeding-periodcost)/fbodymass(ind) ! population average growth rate, to be printed in file 29
     if (gen > generations - 100) then !only do this in latter part of simulation
        STfeeding(class1) = STfeeding(class1) + feeding
     end if

     !stomach is filled with last meal (evacuation occurred at start of time step)
     fstomach(ind,1) = fstomach(ind,1) + feeding

     !final body mass is limited by maximum body size
     fbodymass(ind) = min(1.*fmaxweight, fbodymass(ind) + feeding - periodcost)
     !compare with previous body mass to find new maximum
     fbodymax(ind) = max(fbodymass(ind), fbodymax(ind))


     ! death by starvation: either below birth weight or below structural weight
     if (fbodymass(ind) .le. cop2egg*0.5) then
        if (fstatus(ind) == 1) then
          fstatus(ind) = -3 ! **DEATH EVENT: starved below half egg size
          fdeathage(ind) = age
          fdeathdepth(ind) = fdepthchoice(ind,1)
          fdeathaffect(ind) = findfeel(ind)
          fdeathBM(ind) = int(fbodymass(ind))
          fbodymass(ind) = 0.
          numfish(fdepthchoice(ind,1),age) = numfish(fdepthchoice(ind,1),age) - 1
          if (gen > generations - 100) then !only do this in latter part of simulation
             STstarved(class1) = STstarved(class1) + 1
          end if
        end if
      else if (fbodymass(ind)-fstomach(ind,1) < development*fbodymax(ind)) then
           !Due to individual development, only the fraction (1-development) of bodymax
         ! (except the fraction of BM which may be stomach contents)
         ! can be used as energy source during starvation.
         ! If less, the animal dies
         fstatus(ind) = -2 ! **DEATH EVENT: death due to starvation down to structural mass
         fdeathage(ind) = age
         fdeathaffect(ind) = findfeel(ind)
         fdeathBM(ind) = int(fbodymass(ind))
         fdeathdepth(ind) = fdepthchoice(ind,1)
         fbodymass(ind) = 0.

         numfish(fdepthchoice(ind,1),age) = numfish(fdepthchoice(ind,1),age) - 1
          if (gen > generations - 100) then !only do this in latter part of simulation
             STstarved(class1) = STstarved(class1) + 1
          end if
     end if ! death by starvation

!    else !fstatus = 0, i.e. already dead
!       fstomach(ind,1) = 0.
   end if !fstatus
end do !ind-loop over fpop in growth and metabolic costs

!STEP 2: fitness is set to zero if organism dies of predation, ...
fsurvivors = 0

do ind = 1,fpop
  !skip already dead fish
  if (fstatus(ind) == 1) then

! frightended animals eat less efficiently than hungry animals
  if (findfeel(ind) == 0) then !hungry animal, less vigilant
     factor = 1.
    else  !animal afraid and eat less
     factor = restattention
  end if

!  Individual risk is size-dependent. We assume frisk() applies to an ind of body mass 1000,
!    and that risk scales with BodyMass^BMscale.
!     For a single predator, M is proportional to r^2 prop to Area prop til BM^2/3,
!     but we may assume other predators for smaller individuals and higher escape for the larger,
!     hence M may also be prop to BM^0.5). A value of BMscale > 0.67 indicates that the predator
!     have additional preferences for large prey. The following relative relationships applies:
! size            250    500    1000    2000    3000   5000   10000   20000    30000
! BMscale = 0.5   0.5    0.7       1     1.4     1.7    2.2     3.2     4.5      5.5
! BMscale = 0.67  0.4    0.6       1     1.6     2.0    2.9     4.7     7.4      9.8
! BMscale = 1.0   0.25   0.5       1     2       3      5      10      20       30
! BMscale = 1.5   0.125  0.35      1     2.8     5.2   11      32      89      164

! If fbodymass() is divided by a larger number than 1000, then the overall risk will decrease for all
! body sizes, but the slope will not be changed.
   fishrisk = frisk(fdepthchoice(ind,1)) * (fbodymass(ind)*0.001)**BMscale
   !hungry individuals less vigilant than fearful inds
   fishrisk = fishrisk * factor
   if (gen > generations - 100) then !only do this in latter part of simulation
        STrisk(class1) = STrisk(class1) + fishrisk
   end if

   !check if it survives the age
   call random_number (survival)
   if (fishrisk > survival) then
      !organism dies now from predation and leaves no offspring
      fstatus(ind) = -1 ! **DEATH EVENT: death from predation
      fdeathaffect(ind) = findfeel(ind)
      fdeathBM(ind) = int(fbodymass(ind))
      fbodymass(ind) = 0.
      fdeathage(ind) = age
      fdeathdepth(ind) = fdepthchoice(ind,1)
      numfish(fdepthchoice(ind,1),age) = numfish(fdepthchoice(ind,1),age) - 1
      if (gen > generations - 100) then !only do this in latter part of simulation
         STkilled(class1) = STkilled(class1) + 1
      end if
     else
      fsurvivors = fsurvivors + 1
   end if
  end if
end do !ind-loop over fpop in predation


!find average value of each time an individual has been in one of the stomach fullness classes
5500 format(41A12)
5520 format(2A6,A3,I1,A2)
5521 format(2A6,A2,I2,A2)
5522 format(A18,I3,A34)
if (gen == 1 .and. age == 1) then
   if (expmt < 10) then
       write(stringEX,5520)"HED18-",runtag,"-E0",expmt,"-o"
     else
       write(stringEX,5521)"HED18-",runtag,"-E",expmt,"-o"
   end if
   write(stringST,5522)stringEX,109,"-correlations-stomach-capacity.txt"
   open(109, file = stringST)
end if
if (gen > generations - 100 .and. age == flifespan) then
   write(109,*) " "
   write(109,5500)"Generation","Environment","RemainCap","Observ","Affect","Age",   &
                    "Neighbours","Depth","Move","Feeding","Risk","Starved","Killed", &
                    "gAFLight","PAFLight","gAFOth","P0AFOth","gAFPred",    &
                       "PAFPred","gAHStom","PAHStom","gAHFood","PAHFood",     &
                    "gFearLight","PFearLight","gAttrOth","P0AttrOth","gHungFood",    &
                    "PHungFood","gAvoidOth","P0AvoidOth","ABM-1","ABM-2","ABM-3",    &
                    "ABM-4","ABM-5","ABM-6","ABM-7","ABM-8","ABM-9","ABM-10"

   do c = 0,101
      d = max(1.,STdata(c))
      STdata(c) =  STdata(c)/span
      STaffect(c) = STaffect(c)/d
      STage(c) = STage(c)/d
      STagents(c) = STagents(c)/d
      STdepth(c) = STdepth(c)/d
      STmove(c) = STmove(c)/d
      STstarved(c) = 100.*STstarved(c)/d
      STkilled(c) = 100.*STkilled(c)/d
      STrisk(c) = 100.*STrisk(c)/d
      STfeeding(c) = STfeeding(c)/d
      do g = 1,gABM
        STAHsize(c,g) = STAHsize(c,g)/d
      end do
      do g = 1,3
        STAFlight(c,g) = STAFlight(c,g)/d
        STAFother(c,g) = STAFother(c,g)/d
        STAFmort(c,g) = STAFmort(c,g)/d
        STAHstom(c,g) = STAHstom(c,g)/d
        STAHfood(c,g) = STAHfood(c,g)/d
        STHFlight(c,g) = STHFlight(c,g)/d
        STHFother(c,g) = STHFother(c,g)/d
        STHHfood(c,g) = STHHfood(c,g)/d
        STHHother(c,g) = STHHother(c,g)/d
      end do
      write(109,5501)realgen,deviation,c*0.01,STdata(c),STaffect(c),STage(c),STagents(c),  &
                    STdepth(c),STmove(c),STfeeding(c),STrisk(c),STstarved(c),STkilled(c),  &
                    (STAFlight(c,g),g=1,2),(STAFother(c,g),g=1,2),(STAFmort(c,g),g=1,2),   &
                    (STAHstom(c,g),g=1,2),(STAHfood(c,g),g=1,2),(STHFlight(c,g),g=1,2),    &
                    (STHFother(c,g),g=1,2),(STHHfood(c,g),g=1,2),(STHHother(c,g),g=1,2),   &
                    (STAHsize(c,g),g=1,gABM)
   end do
end if
if (gen == generations .and. age == flifespan) then
   close(109)
end if
5501 format(I12,A12,F12.3,38F12.5)


!find average body weight at each depth in the latest generations
if (gen > generations -11) then
  do step = 1,10
      countinds(step) = 0
      counthungry(step) = 0
      countafraid(step) = 0
  end do
  do dep = 1, depth
      weightatdep(dep) = 0.

      do ind = 1,fpop
         if (fdepthchoice(ind,1) == dep) then
            weightatdep(dep) = weightatdep(dep) + fbodymass(ind)
         end if
      end do
  end do

  do dep = 1, depth
     weightatdep(dep)=weightatdep(dep)/(max(1.,1.*numfish(dep,age)))
     write(34,3401)age,dep,weightatdep(dep)
  end do

  do ind = 1,fpop
     if (fstatus(ind) < 1) goto 3444
     do step = 1,10
        weight = step*fmaxweight/10
        if (fbodymass(ind) < weight) then
           countinds(step) = countinds(step) + 1
           if (findfeel(ind) == 0) then !hungry animal
               counthungry(step) = counthungry(step) + 1
                else  !animal afraid
               countafraid(step) = countafraid(step) + 1
           end if
           goto 3444
         end if
     end do
3444 continue
   end do
   do step = 1,10
      write(36,3601)age,step*fmaxweight/10,countinds(step),counthungry(step),countafraid(step)
   end do
end if
3401 format(2I5,F8.0)
3601 format(2I8,3I9)

!!!! removed a section with prints of many files in last 10 genrations to subdirectories
!!!! as the same data are saved in other files (29 April 2011)
!removed from here:
!Save population subset in last generation of each environment type
! (This requires saving lots of generations as we don't know whether
! a later generation will be of the same environment type. Later
! generations will overwrite earlier of same sort.)
!to here:
!end if !gen > generations - 50

!sum up characteristic of this generation and this simulation
quarter = flifespan/(4.*dielcycles)
firstmidday = int(quarter)
firstmidnight =  int(3.*quarter)
lastmidday = flifespan - int(3.*quarter)
lastmidnight = flifespan - int(quarter)
checkpoint = 0
if (age == firstmidday) then
   checkpoint = 1
   point = 1
  else if (age == firstmidnight) then
   checkpoint = 1
   point = 2
  else if (age == flifespan/2) then
   checkpoint = 1
   point = 3
  else if (age == lastmidday) then
   checkpoint = 1
   point = 4
  else if (age == lastmidnight) then
   checkpoint = 1
   point = 5
  else if (age == flifespan) then
   checkpoint = 1
   point = 6
end if
if (checkpoint == 1) then
!  make statistics of average values of depth choice and body mass
   do g = 1,2 !for each gender
      nowBM(g) = 0.
      nowdep(g) = 0.
      nowlocal(g) = 0.
      nowalive(g) = 0
      do dep = 1,depth
         numgender(dep,g) = 0
      end do
   end do

   do ind = 1,fpop
     if (fstatus(ind) == 1) then !only count those still alive
        g = fgender(ind)
        nowBM(g) = nowBM(g) + fbodymass(ind)
        nowalive(g) = nowalive(g) + 1
        dep = fdepthchoice(ind,1)
        numgender(dep,g) = numgender(dep,g) + 1
     end if
   end do

   do dep = 1,depth
!      nowalive = nowalive + numfish(dep,age)
      do g = 1,2
         nowdep(g) = nowdep(g) + dep*numgender(dep,g)
         !self not included among neighbours of both sexes:
         nowlocal(g) = nowlocal(g) + numgender(dep,g)*max(0,(numfish(dep,age) - 1))
      end do
   end do
   do g = 1,2
      if (nowalive(g) > 0) then
        nowBM(g) = nowBM(g)/(1.*nowalive(g))       !average body mass of each alive
        nowdep(g) = nowdep(g)/(1.*nowalive(g))     !average depth of each alive
        nowlocal(g) = nowlocal(g)/(1.*nowalive(g)) !average number of neighbours in same depth as the agent
       else
        nowBM(g) = 0.
        nowdep(g) = 0.
        nowlocal(g) = 0.
      end if !nowalive
   end do

  if (gen < generations - 499) then !calculations only to be used in "...-o12-popdyn.txt"
     year = 1
    else
     year = 500 - (generations - gen)
  end if
  do g = 1,2
     fzAVGBM(point,year,g) = nowBM(g)
     fzAVGdepth(point,year,g) = nowdep(g)
     fzAVGlocal(point,year,g) = nowlocal(g)
  end do
end if  ! checkpoint = 1

end
!end of subroutine SRgrowth



!-----------------------------------------------------------------------
subroutine SRfecundity(srerr,test99,expmt,generations,gen,fish,fpop,     &
              brain,flifespan,cop2egg,fnewpop,fpopfec,motherlength, &
              totalsenses,deviation,help,fmultrisk,preymax,curve,gABM,   &
              development,reproinvest,sdgABM,sdAfear,sdAhunger,sdHfear,  &
              sdHhunger,sdAHunused,realgen,during,memory,attacks,        &
              reprocap,dev,zavgkids,zavgEgg,zavgR0,zsdR0,zavgsurv,zavgstart)
!-----------------------------------------------------------------------
!reproduction and formation of new genes
!This SR is called at the end of a generation, after the last time step

implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
!character(6) runtag
character(8) deviation
integer    expmt,gen,generations,fish,fpop,fnewpop,motherlength,totalsenses,dev
integer srerr,test99,flifespan,cop2egg,curve,gABM,realgen,during,memory,attacks
integer zavgkids,zavgsurv,zavgstart,brain
real fpopfec,fmultrisk,preymax,development,reproinvest,reprocap
real sdgABM,sdAfear,sdAhunger,sdHfear,sdHhunger,sdAHunused,zavgR0,zsdR0,zavgEgg

!LOCAL ARRAYS
character (7) help             !message for a rare recovery of the population
integer ind,alive,kids,last,now,fbd,frd,fmove,gg,g10,i,g,p,y,LNG
integer finalGG,least,error99,el,strBM,neverA(2),egg,eggs,killed,toomany,nummut,genes
real diff,percentA(2),random,eggmort,favgR0,fsurvR0,nowBD,AvgMut,rest,rlast

if (test99 == 1) write(10,*) "SRfecundity"
error99 = 0
if (srerr == 10) error99 = 1
!if (gen == 2) write(6,*) "SRfecundity"


!find number of survivors and estimate the number of offspring at end of generation
fpopfec = 0.
alive = fpop

!gradual increase in realism in biomass price of egg, but only if population size of newborn near FISH
if (gen < 50) then
    reprocap = 0.35
  else if (gen/50 == gen/50. .and. reproinvest < reprocap .and. fpop > fish-1000) then
    reprocap = max(reproinvest,reprocap - 0.01)
end if


do ind = 1,fpop
   if (fstatus(ind) < 1) then
      alive = alive - 1
      fgonad(ind) = 0.
     else
      !only females may make eggs
      if (fgender(ind) == 1) then
         fgonad(ind) = max(0., fbodymass(ind) * reprocap/(cop2egg*1.))

!        only females with at least a half egg can participate in the reproductive lottery
         if (fgonad(ind) > 0.5) fpopfec = fpopfec + fgonad(ind)
        else ! male
         fgonad(ind) = 0.
      end if
      !individuals that survive until reproduction die afterwards
      fdeathage(ind) = flifespan+1
   end if
end do

!boost reproduction in survivors in first few generations
if (help == "boost9") then !first few generations
   do i = 1,3
      if (fpopfec < fish/10) then
       fpopfec = 0.

       do ind = 1,fpop
          fgonad(ind) = fgonad(ind) * 9.
          if (fgonad(ind) > 0.5) fpopfec = fpopfec + fgonad(ind)
       end do
      end if
   end do
end if

!boost reproduction of survivors if final population size is very low
if (help == "boost2") then
   fpopfec = 0.

   do ind = 1,fpop
      fgonad(ind) = fgonad(ind) * 2.
      if (fgonad(ind) > 0.5) fpopfec = fpopfec + fgonad(ind)
   end do
end if

!Conversion of gonads to eggs
!Two cases: there are gonad material for more than FISH eggs, or not
!In first case a lottery will determine surivival of each egg,
!In second case, only the unfinished egg fractions in each gonad will participate in lottery
if (fpopfec > fish-1) then !lottery for all eggs
    eggmort = (1+fpopfec-fish)/fpopfec ! if too many eggs produced, an egg mortality will occur
    fnewpop = 0

    do ind = 1,fpop
      if (fgonad(ind) > 0.5) then !gonad was included in fpopfec and in this lottery
       eggs = 0 !number of offspring from this mother
       do egg = 1,int(fgonad(ind))
          call random_number(random)
          if (random > eggmort) eggs = eggs+1 ! this egg survived the egg mortality
       end do ! egg

       !add the last fraction of an unfinished egg into the lottery
       rest = fgonad(ind)-int(fgonad(ind))
       call random_number(random)
       if (random*rest > eggmort) eggs = eggs+1 ! this last eggfraction also survived the egg mortality
       foffspring(ind) = eggs
       fnewpop = fnewpop + eggs
      else
       foffspring(ind) = 0
     end if
   end do !ind
  else ! all whole eggs survive, but death risk for egg fractions
   fnewpop = 0

   do ind = 1,fpop
     if (fstatus(ind) == 1) then
        eggs = int(fgonad(ind))
        !add the last fraction of an unfinished egg into the lottery
        rest = fgonad(ind)-int(fgonad(ind))
        call random_number(random)
        if (random < rest) eggs = eggs+1 ! this last eggfraction also survived the egg mortality
        foffspring(ind) = eggs
        fnewpop = fnewpop + eggs
       else
        foffspring(ind) = 0
     end if
   end do
end if

!if too many eggs still are produced, then kill some randomly
if (fnewpop > fish-1) then
  toomany = fnewpop+1-fish
  killed = 0
  do i = 1,100000
    call random_number(random)
    ind = max(1,int(fpop*random))
    if (foffspring(ind) > 0) then
        foffspring(ind) = foffspring(ind) - 1
        killed = killed + 1
         fnewpop = fnewpop - 1
    end if
    if (killed == toomany) goto 1111
  end do  !i
end if !fnewpop
1111 continue


!Find average birth depth of the eggs. Since each egg is randomly given
! the gender of the father or the mother, egg depth is not sex-specific
nowBD = 0.

do ind = 1,fpop
   if (fstatus(ind) == 1) nowBD = nowBD + fdepthchoice(ind,1)*foffspring(ind)
end do
nowBD = nowBD/(1.*fnewpop)

if (fnewpop > fish) then
   write(10,663) "fnewpop = ",fnewpop, " after SRfecundity in generation ",realgen
   write(6,663) "fnewpop = ",fnewpop, " after SRfecundity in generation ",realgen
end if
663 format(A11,I6,A31,I5)

!initiate print-control variables
gg = generations
g10 = gg/10
finalGG = 0
least = min(10, g10-1)
if (during == 1) then  ! rich printing during evolution
     if ((gen > gg-least) .or. (gen < 0.9*gg .and. gen > 0.9*gg-least)                             &
       .or. (gen < 0.8*gg .and. gen > 0.8*gg-least) .or. (gen < 0.7*gg .and. gen > 0.7*gg-least)  &
       .or. (gen < 0.6*gg .and. gen > 0.6*gg-least) .or. (gen < 0.5*gg .and. gen > 0.5*gg-least)  &
       .or. (gen < 0.4*gg .and. gen > 0.4*gg-least) .or. (gen < 0.3*gg .and. gen > 0.3*gg-least)  &
       .or. (gen < 0.2*gg .and. gen > 0.2*gg-least) .or. (gen < 0.1*gg .and. gen > 0.1*gg-least)) finalGG = 1
end if
if (during == 0 .and. gen > gg - least) finalGG = 1

!make prints of genome and individual-data for all "normal" generations near end of simulation
if (generations < 200) then
   if (gen == max(1,generations-100)) then
      call SRfileheadings(27,curve,12,test99)
      write(26,3026) "gen","ind","sex","father"
   end if
 else
   if (gen == generations-1000) then
      call SRfileheadings(27,curve,12,test99)
      write(26,3026) "gen","ind","sex","father"
   end if
end if
3026 format(4A8)

if (((generations < 1000) .and. (gen > max(1,generations-100))) .or. (gen > generations - 1000)) then
   LNG = 1
  else
   LNG = 0 !we are not among Late Normal Generations
end if

!Print gene-file in normal generations at the end of a generation
if (LNG == 1 .and. dev == 1) then !a late NORMAL generation
   do ind = 1,fpop
     fbd = fbirthdepth(ind)
     if (fstatus(ind) == 1) then
       frd = fdepthchoice(ind,1)
       fmove = fbd-frd
      else
       frd = -9999
       fmove = -9999
     end if
     i = ind
     strBM = int(fbodymax(ind)*development + fstomach(ind,1))
     write(27,3007) gen,i,(geneAHsize(i,g),g = 1,10),(geneAFlight(i,g),g = 1,3), &
         (geneAFother(i,g),g = 1,3),(geneAFmort(i,g),g = 1,3),(geneAHstom(i,g),g = 1,3),    &
            (geneAHfood(i,g),g = 1,3),(geneHFlight(i,g),g = 1,3),   &
         (geneHFother(i,g),g = 1,3),(geneHHfood(i,g),g = 1,3),   &
         (geneHHother(i,g),g = 1,3),(geneMemo(i,g),g = 1,2),fgender(i),fbd,frd,fmove,fdeathdepth(i),fdeathage(i), &
         fdeathaffect(i),fdeathBM(i),fstatus(i),fnumberafraid(i),fbodymax(i),strBM,fbodymass(i), &
         fgonad(i),foffspring(i)," x ",motherpop(i),mcreation(i),gcreation(i),acreation(i),icreation(i), &
         (motherstring(i,el),el = 1,motherlength)
         !"x": space for fatherly offspring
  end do !ind
end if !LNG

!Print gene-file at the end of a generation a few times during an experiment
if (finalGG == 1) then
   write(30,3001) expmt, " !       experiment number"
   write(30,3001) realgen, " !       generation number"
   write(30,3001) alive, " ! survivors at end of gen"
   write(30,3001) fpop,  " !popul. size (dead&alive)"
   call SRfileheadings(30,curve,4,test99)

   write(20,3001) expmt, " !       experiment number"
   write(20,3001) realgen, " !       generation number"
   write(20,3001) alive, " ! survivors at end of gen"
   write(20,3001) fpop,  " !popul. size (dead&alive)"
   call SRfileheadings(20,curve,11,test99)

   do ind = 1,fpop
     fbd = fbirthdepth(ind)
     if (fstatus(ind) == 1) then
       frd = fdepthchoice(ind,1)
       fmove = fbd-frd
      else
       frd = -9999
       fmove = -9999
     end if
     i = ind
     strBM = int(fbodymax(ind)*development + fstomach(ind,1))
     write(30,3006) i,(geneAHsize(i,g),g = 1,10),(geneAFlight(i,g),g = 1,3), &
         (geneAFother(i,g),g = 1,3),(geneAFmort(i,g),g = 1,3),(geneAHstom(i,g),g = 1,3),    &
            (geneAHfood(i,g),g = 1,3),(geneHFlight(i,g),g = 1,3),   &
         (geneHFother(i,g),g = 1,3),(geneHHfood(i,g),g = 1,3),   &
         (geneHHother(i,g),g = 1,3),(geneMemo(i,g),g = 1,2),fgender(i),fbd,frd,fmove,fdeathdepth(i),fdeathage(i), &
         fdeathaffect(i),fdeathBM(i),fstatus(i),fnumberafraid(i),fbodymax(i),strBM,fbodymass(i), &
         fgonad(i),foffspring(i)," x ",motherpop(i),mcreation(i),gcreation(i),acreation(i),icreation(i), &
         (motherstring(i,el),el = 1,motherlength)
         !"x": space for fatherly offspring
     write(20,3005) i,(mutAHsize(i,g),g = 1,10),(mutAFlight(i,g),g = 1,3), &
         (mutAFother(i,g),g = 1,3),(mutAFmort(i,g),g = 1,3),(mutAHstom(i,g),g = 1,3),    &
            (mutAHfood(i,g),g = 1,3),(mutHFlight(i,g),g = 1,3),   &
         (mutHFother(i,g),g = 1,3),(mutHHfood(i,g),g = 1,3),   &
         (mutHHother(i,g),g = 1,3),(mutMemo(i,g),g = 1,2)
  end do !ind
end if
3005 format(40I8)
3006 format(I8,10F8.2,9(3F8.2), & !i,(geneAHsize(i,g),g = 1,10),(geneAFlight(i,g),g = 1,3), etc
            2F8.5,6I8,          & !(geneMemo(i,g),g = 1,2),fbd,frd,fmove,fdeathdepth(i),fdeathage(i),
            4I8,F8.0,I8,F8.0,   & !fdeathaffect(ind),fdeathBM(ind),fstatus(i),fnumberafraid(i),fbodymax(ind),strBM,fbodymass(i),
            F8.3,I8,A8,15I8) !fgonad(i),foffspring(i),"x",motherpop(i),,mcreation(i),gcreation(i),acreation(i),icreation(i),
                             !(motherstring(i,el),el = 1,motherlength)
3007 format(2I8,10F8.2,9(3F8.2), & !gen,i,(geneAHsize(i,g),g = 1,10),(geneAFlight(i,g),g = 1,3), etc
            2F8.5,6I8,          & !(geneMemo(i,g),g = 1,2),fbd,frd,fmove,fdeathdepth(i),fdeathage(i),
            4I8,F8.0,I8,F8.0,   & !fdeathaffect(ind),fdeathBM(ind),fstatus(i),fnumberafraid(i),fbodymax(ind),strBM,fbodymass(i),
            F8.3,I8,A8,15I8) !fgonad(i),foffspring(i),"x",motherpop(i),,mcreation(i),gcreation(i),acreation(i),icreation(i),
                             !(motherstring(i,el),el = 1,motherlength)
!Find average number of mutations of a gene
numMut = 0
genes = gABM+9*2 + memory*2  !assuming only 2 parameters in the affect equations

do ind = 1,fpop
   do g = 1,gABM
      numMut = mutAHsize(ind,g) + nummut
   end do
   do g = 1,2
      numMut = mutAFlight(ind,g) + mutAFlight(ind,g) + mutAFother(ind,g)  &
               + mutAFmort(ind,g) + mutAHstom(ind,g) + mutAHfood(ind,g)   &
               + mutHFlight(ind,g) + mutHFother(ind,g) + mutHHfood(ind,g) &
               + mutHHother(ind,g) + nummut
    end do
    if (memory > 0) then
      do g = 1,2
        numMut = numMut + mutMemo(ind,g)
      end do
    end if
end do
AvgMut =numMut/(1.* genes * max(1,fpop))

!print gene statistics during GENERATIONS
call SRgenestat(srerr,test99,expmt,gen,generations,realgen,fpop,flifespan,totalsenses,  &
                deviation,gABM,sdgABM,sdAfear,sdAhunger,sdHfear,sdHhunger,sdAHunused)

!find average R0 for the whole pop and print population dynamics
favgR0 = fpopfec
fsurvR0 = fpopfec
if (alive > 0) fsurvR0 = fpopfec/(1.*alive)
if (fpop > 0) favgR0 = favgR0/(1.*fpop)
kids = int(alive*fsurvR0)

!make statistics of incidences of fear
neverA(1) = 0
neverA(2) = 0

do ind = 1,fpop
   neverA(fgender(ind)) = neverA(fgender(ind)) + fneverafraid(ind)
end do
do g = 1,2 !percent fear in each sex
   percentA(g) = 100.*numHF(2,g)/(0.+ numHF(1,g) + numHF(2,g))
end do

if (gen < generations - 499) then !calculations only to be used in "...-o12-popdyn.txt"
    y = 1
   else
    y = 500 - (generations - gen)
end if

!%mortality = 100*(start-end)/start = 100 - end/(0.01*start)
if (brain == 0) then !no mental states, no fear or hunger
   do i = 1,2
      neverA(i) = -100
      percentA(i) = -100.
   end do
end if
write(12,1201) realgen,deviation,help,fmultrisk,reprocap,attacks,int(preymax),fpop,alive,            &
               100.-alive/(0.01*fpop),fsurvR0,int(fpopfec),fnewpop,AvgMut,neverA(1),neverA(2),       &
               percentA(1),percentA(2),Aalleles(1),Aalleles(2),Aalleles(3),Aalleles(5),Aalleles(6),  &
               Halleles(1),Hdominant(1),Hdomfreq(1),Halleles(2),Hdominant(2),Hdomfreq(2),            &
               Halleles(4),Hdominant(4),Hdomfreq(4),Halleles(5),Hdominant(5),Hdomfreq(5),            &
               Halleles(7),Hdominant(7),Hdomfreq(7),Halleles(8),Hdominant(8),Hdomfreq(8),            &
               Halleles(10),Hdominant(10),Hdomfreq(10),Halleles(11),Hdominant(11),Hdomfreq(11),      &
               sdgABM,sdAfear,sdAhunger,sdHfear,sdHhunger,sdAHunused,                                &
               (fzAVGBM(p,y,1),p=1,6),(fzAVGdepth(p,y,1),p=1,6),nowBD,(fzAVGlocal(p,y,1),p=1,6),     &
               (fzAVGBM(p,y,2),p=1,6),(fzAVGdepth(p,y,2),p=1,6),nowBD,(fzAVGlocal(p,y,2),p=1,6)
1201 format(I6,2A9,2F8.2,I4,3I8,2F8.2,2I8,F8.2,2I8,2F9.4,5I7,8(I7,F7.2,F7.4),6F10.4,2(13F9.1,6F9.0))


!make variables for comparing habitat descriptors at the end of each experiment
last = min(500, generations)
if (gen > generations - last) then
   now = gen - generations + last
   favgstart(now) = fpop
   favgsurv(now) = alive
   favgkids(now) = kids
   fzEggDepth(now) = nowBD
   do g = 1,2 !for females and males
      favgNeverA(now,g) = NeverA(g)
      favgpctA(now,g) = percentA(g)
   end do
   fzavgR0(now) = favgR0
   do g = 1,7
      zgAalleles(now,g) = Aalleles(g)
   end do
   do g = 1,12
      zgHalleles(now,g) = Halleles(g)
   end do
end if
if (gen == generations) then
   do g = 1,7
      zeAalleles(g) = 0.
   end do
   do g = 1,12
       zeHalleles(g) = 0.
   end do
   rlast = 1./(1.*last)
   do now = 1,last
      zavgstart = zavgstart + favgstart(now)
      zavgkids = zavgkids + favgkids(now)
      zavgR0 = zavgR0 + fzavgR0(now)
      zavgsurv = zavgsurv + favgsurv(now)
      do g = 1,2 !both sexes
         zavgNeverA(g) = zavgNeverA(g) + favgNeverA(now,g)
         zavgpctA(g) = zavgpctA(g) + favgpctA(now,g)
      end do
      zavgEgg = zavgEgg + fzEggDepth(now)
      do p = 1,6
         do g = 1,2
            zavgDepth(p,g) = zavgDepth(p,g) + fzAVGdepth(p,now,g)
            zavgBM(p,g) = zavgBM(p,g) + fzAVGBM(p,now,g)
            zavgLocal(p,g) = zavgLocal(p,g) + fzAVGlocal(p,now,g)
         end do
      end do
      do g = 1,7
        zeAalleles(g) = zeAalleles(g) + zgAalleles(now,g)
      end do
      do g = 1,12
        zeHalleles(g) = zeHalleles(g) + zgHalleles(now,g)
      end do
   end do
   zavgstart = zavgstart*rlast
   zavgkids = zavgkids*rlast
   zavgR0 = zavgR0*rlast
   zavgsurv = zavgsurv*rlast
   zavgEgg = zavgEgg*rlast
   do g = 1,2
      zavgNeverA(g) = zavgNeverA(g)*rlast
      zavgpctA(g) = zavgpctA(g)*rlast
   end do
   do now = 1,last
      diff = fzavgR0(now) - zavgR0
      zsdR0 = zsdR0 + diff*diff
   end do
   zsdR0 = sqrt(zsdR0*rlast)
   do p = 1,6
      do g = 1,2
         zavgDepth(p,g) = zavgDepth(p,g)*rlast
         zavgBM(p,g) = zavgBM(p,g)*rlast
         zavgLocal(p,g) = zavgLocal(p,g)*rlast
      end do
   end do
   do g = 1,7
      zeAalleles(g) = zeAalleles(g)*rlast
   end do
   do g = 1,12
      zeHalleles(g) = zeHalleles(g)*rlast
   end do
end if

!1601 format(2A7,4A10)
!1602 format(2I7,I10,2F10.0,I10)
3001 format(I6,A30)
!3002 format(58A8)
!3003 format(I8,33F8.2,2F8.5,4I8,F8.0,F8.3,5I8,10I8)
!3004 format(I6,A40)

end
!end subroutine SRfecundity


!-----------------------------------------------------------------------
subroutine SRneweggs(srerr,test99,expmt,gen,fish,fpop,fnewpop,depth,     &
                     generations,realgen,flifespan,allmax,allmin,    &
                     mutrate,motherlength,interval,      &
                     memory,maxpartners,curve,               &
                     partnerchoice,gABM,fmaxweight,corrplot,deviation,   &
                     runtag,stepABM,minABM,maxABM,dev)
!-----------------------------------------------------------------------
!Formation, mutation and recombination of new genes; the haploid individuals may have two parents
!Finally, a low number of IMMIGRANTS enter the population with slightly deviant alleles

!procedure:
!1 for all mothers
! - find and choose the male to become father of eggs
! - make the eggs
!   for each egg
!   - recombine: exchange part of the egg's genome with the father genome
!2 for each egg, after recombination of all eggs
! - test for mutation and perform mutation for each triplet of genes

implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer    fpop,fish,fnewpop,depth,generations,realgen,flifespan,motherlength
integer srerr,test99,gen,expmt,dev
integer memory,maxpartners,curve,partnerchoice,gABM,fmaxweight
integer corrplot
real allmax,allmin,mutrate,interval,stepABM,minABM,maxABM
character(8) deviation
character(6) runtag

!local variables
integer egg,gn,half,ind1,indN
integer ind,baby,eggs,new,start,dirsign,error99,LNG
integer partner,guy,daddy,dep,j,g
real mass,dummy,distance,dist,BMdiff
real mthresh,allele,random,rand2
character(65) stringABM
character(60) stringBD,stringBM,stringPHFL,stringgHFO,stringPHHO
character(60) stringPHFO,stringgHHO,stringgHFL,stringPHHF,stringgHHF
character(60) stringgAFP,stringPAFP,stringgAHF,stringPAHF,stringgAFL
character(60) stringPAFL,stringgAFO,stringPAFO,stringgAHS,stringPAHS
character(18) stringEX

!LOCAL ARRAYS
real EGGAHsize(15000,10),EGGAFlight(15000,3),EGGAFother(15000,3),EGGAFmort(15000,3)
real EGGAHstom(15000,3),EGGAHfood(15000,3),EGGHFlight(15000,3)
real EGGHFother(15000,3),EGGHHfood(15000,3),EGGHHother(15000,3),EGGmemo(15000,2)
real igene(10),iEGG(10)
integer mEGGAHsize(15000,10),mEGGAFlight(15000,3),mEGGAFother(15000,3),mEGGAFmort(15000,3)
integer mEGGAHstom(15000,3),mEGGAHfood(15000,3),mEGGHFlight(15000,3)
integer mEGGHFother(15000,3),mEGGHHfood(15000,3),mEGGHHother(15000,3),mEGGmemo(15000,2)
integer meggcreation(15000),geggcreation(15000),aeggcreation(15000),ieggcreation(15000)
integer eggmotherpop(15000),EGGgender(15000),MUT(10)

if (test99 == 1) write(10,*) "SRneweggs"
error99 = 0
if (srerr == 3) error99 = 1
!if (gen == 2) write(6,*) "SRneweggs"

!local variable for mutation risk
mthresh = mutrate

!reset memory of fatherhood
do ind = 1,fpop
  father(ind) = 0
end do

!count eggs
baby = 0

!start formation of eggs at random mother individual number, to eliminate effect of inheritance of birth number
!this do-loop HALF is very long!

call random_number (random)
start = int((fpop-1)*random) + 1
do half = 1,2 ! eggloop Make the lower part of next generation
  if (half == 1) then    ! from the upper part of the previous
     ind1 = start+1
     indN =  fpop
    else
     ind1 = 1
     indN =  start
  end if
  do ind = ind1,indN  !indeggs loop

  !find egg number of mother
   eggs = foffspring(ind)
   if (eggs < 1) goto 99


  !determine who is to become father
   dep = fdepthchoice(ind,1) !mate search is always local
   !partner choice method is determined by parameter "partnerchoice"

   if (partnerchoice == 6) then  !accept any mate, irrespective of depth
      !find  mate
      mass = 0.
      partner = 0
      do j = 1,fpop !search the population, but stop at first living male
          call random_number (random)
          guy = 1 + int((fpop-1) * random)
          if (fgender(guy) == 2 .and. fstatus(guy) == 1) then  !alive and male
                 daddy = guy                           !remember the array number for later
                 goto 9090                             !mission accoplished
         end if
      end do

     else if (partnerchoice == 5) then  !search MAXPARTNERS for the largest of mate, irrespective of depth
      !find  mate
      mass = 0.
      partner = 0
      do j = 1,fpop !search a high number of mates, but stop when having compared MAXPARTNER inds
          call random_number (random)
        guy = 1 + int((fpop-1) * random)
        if (fgender(guy) == 2 .and. fstatus(guy) == 1) then  !alive
              partner = partner + 1                    !register the guy as a candidate
              if (fbodymass(guy) > mass) then          !this is the largest guy found alive so far
                 daddy = guy                           !remember the array number for later
                 mass = fbodymass(daddy)               !remember his weight for later comparison
              end if
              if (partner == maxpartners) goto 9090    !no time to search for more than "maxpartner" candidates
         end if
      end do

     else if (partnerchoice == 4) then  !mate choice depending on genetic similarity
      !find  mate: find where to start
      distance = 100000.
      partner = 0
      9088 continue ! if not enough individuals found in depth, go one depth down or up.
      do j = 1,fpop !search a high number of mates, but stop when having compared MAXPARTNER inds
          call random_number (random)
        guy = 1 + int((fpop-1) * random)
        if (fgender(guy) == 2 .and. fdepthchoice(guy,1) == dep) then !allow only males in current depth
            partner = partner + 1                   !register the guy as a candidate
            if (partner > maxpartners) goto 9090    !no time to search for more than "maxpartner" candidates
            dist = 0.
            do g = 1,gABM                           !find genetic distance between mother and potential partner
                dist = dist + (geneAHsize(ind,g) - geneAHsize(guy,g)) **2.
            end do
            do g = 1,3
                dist = dist + (geneAFlight(ind,g) - geneAFlight(guy,g)) **2.
                dist = dist + (geneAFother(ind,g) - geneAFother(guy,g)) **2.
                dist = dist + (geneAFmort(ind,g) - geneAFmort(guy,g)) **2.
                dist = dist + (geneAHstom(ind,g) - geneAHstom(guy,g)) **2.
                dist = dist + (geneAHfood(ind,g) - geneAHfood(guy,g)) **2.
                dist = dist + (geneHFlight(ind,g) - geneHFlight(guy,g)) **2.
                dist = dist + (geneHFother(ind,g) - geneHFother(guy,g)) **2.
                dist = dist + (geneHHfood(ind,g) - geneHHfood(guy,g)) **2.
                dist = dist + (geneHHother(ind,g) - geneHHother(guy,g)) **2.
             end do
                do g = 1,2
                dist = dist + (geneMemo(ind,g) - geneMemo(guy,g)) **2.
             end do
             if (dist < distance) then               !this is the most similar guy found alive so far
                 daddy = guy                         !remember the array number for later
                 distance = dist                     !remember distance for later comparison
             end if
         end if
      end do
      if (partner == 0) then                          !need to search for candidates in other depths
          if (dep == 1 .or. dep == depth) then
              dep = 15                                !sorry, no local partner found for you, try somewhere else
              goto 9088
          end if
          if (dep > depth/2) then                     !if deep, search deeper
             dep = dep + 1
            else
             dep = dep - 1                            !if shallow, search shallower
          end if
          goto 9088
      end if

     else if (partnerchoice == 3) then !mate choice depending on male body size most similar to mother's
      !set first (far too high) difference in BM between mother and father
      BMdiff = 50000.
      !find  mate
      mass = 0.
      partner = 0
      9086 continue ! if not enough individuals found in depth, go one depth down or up.
      do j = 1,fpop !search a high number of mates, but stop when having compared MAXPARTNER inds
          call random_number (random)
        guy = 1 + int((fpop-1) * random)
        if (fgender(guy) == 2 .and. fdepthchoice(guy,1) == dep) then !allow only males in current depth
            partner = partner + 1                   !register the guy as a candidate
            if (partner > maxpartners) goto 9090    !no time to search for more than "maxpartner" candidates
            dummy = abs(fbodymass(guy)-fbodymass(ind))
            if (dummy < BMdiff) then                !this is the most similar-sized guy found alive so far
               daddy = guy                           !remember the array number for later
               BMdiff = dummy                        !remember his weight for later comparison
             end if
          end if
       end do
      if (partner == 0) then                          !need to search for candidates in other depths
          if (dep == 1 .or. dep == depth) then
              dep = 15                                !sorry, no local partner found for you, try somewhere else
              goto 9086
          end if
          if (dep > depth/2) then                     !if deep, search deeper
             dep = dep + 1
            else
             dep = dep - 1                            !if shallow, search shallower
          end if
          goto 9086
      end if

     else if (partnerchoice == 2) then !mate choice depending on largest possible male body size
      !find  mate
      mass = 0.
      partner = 0
      9089 continue ! if not enough individuals found in depth, go one depth down or up.
      do j = 1,fpop !search a high number of mates, but stop when having compared MAXPARTNER inds
          call random_number (random)
        guy = 1 + int((fpop-1) * random)
        if (fstatus(guy) == 1) then  !alive
           if (fgender(guy) == 2 .and. fdepthchoice(guy,1) == dep) then !allow only males in current depth
              partner = partner + 1                   !register the guy as a candidate
              if (partner > maxpartners) goto 9090    !no time to search for more than "maxpartner" candidates
              if (fbodymass(guy) > mass) then         !this is the largest guy found alive so far
                 daddy = guy                           !remember the array number for later
                 mass = fbodymass(daddy)               !remember his weight for later comparison
               end if
           end if
         end if
      end do
      if (partner == 0) then                          !need to search for candidates in other depths
          if (dep == 1 .or. dep == depth) then
              dep = 15                                !sorry, no local partner found for you, try somewhere else
              goto 9089
          end if
          if (dep > depth/2) then                     !if deep, search deeper
             dep = dep + 1
            else
             dep = dep - 1                            !if shallow, search shallower
          end if
          goto 9089
      end if

   else  !mate choice random, but still local
      !find mate: find where to start
      partner = 0
      9087 continue ! if not enough individuals found in depth, go one depth down or up.
      do j = 1,fpop !search a high number of mates, but stop when having compared MAXPARTNER inds
          call random_number (random)
        guy = 1 + int((fpop-1) * random)
        if (fstatus(guy) == 1) then
           if (fgender(guy) == 2 .and. fdepthchoice(guy,1) == dep) then !allow only males in current depth
              partner = partner + 1                   !register the guy as a candidate
              if (partner > maxpartners) goto 9090    !no time to search for more than "maxpartner" candidates
              daddy = guy                             !remember the array number for later
           end if
         end if
       end do
      if (partner == 0) then                          !need to search for candidates in other depths
          if (dep == 1 .or. dep == depth) then
              dep = 15                                !sorry, no local partner found for you, try somewhere else
              goto 9087
          end if
          if (dep > depth/2) then                     !if deep, search deeper
             dep = dep + 1
            else
             dep = dep - 1                            !if shallow, search shallower
          end if
          goto 9087
      end if
    end if !partner choice mechanism

    !continue here when partner is found
    9090 continue

    !count fatherly offspring
    father(daddy) = father(daddy) + eggs


   !now: make the new haploid eggs from mother and father genomes
   do 111 new = 1, eggs
      baby = baby+1     !individual-number in the next generation

      !the family tree of the baby is preserved:
      !INITIATE motherstring (individual-number of a small group of fore-mothers)
      do gn = 1,motherlength
         eggstring(baby,gn) = motherstring(ind,gn)
      end do
      meggcreation(baby) = mcreation(ind)
      geggcreation(baby) = gcreation(ind)
      aeggcreation(baby) = acreation(ind)
      ieggcreation(baby) = icreation(ind)
      eggmotherpop(baby) = motherpop(ind)

        !the new egg is given a birth depth, which is its mother's habitat choice
      !in the final time step of her life.
      fbirthdepth(baby) = fdepthchoice(ind,1)
      !prevent out-of-range birth depths
      if (fbirthdepth(baby) < 1) fbirthdepth(baby) = 1
      if (fbirthdepth(baby) > depth) fbirthdepth(baby) = depth


    !Recombination only of full local train of genes (one and one affect function)
    !Equal chance of inheriting motherly and fatherly genes
    call random_number (random)
    if (random > 0.5) then
       EGGgender(baby) = 2  !gender inherited from father
       !ABM-genes are sex-specific: boys inherit from father and girls from mother
       do g = 1,gABM
          EGGAHsize(baby,g) = geneAHsize(daddy,g)
          mEGGAHsize(baby,g) = mutAHsize(daddy,g)
       end do
      else
       EGGgender(baby) = 1  !gender and ABM-genes inherited from mother
       do g = 1,gABM
          EGGAHsize(baby,g) = geneAHsize(ind,g)
          mEGGAHsize(baby,g) = mutAHsize(ind,g)
       end do
    end if

    call random_number (random)
    if (random > 0.5) then
       do g = 1,3
          EGGAFlight(baby,g) = geneAFlight(daddy,g)
          mEGGAFlight(baby,g) = mutAFlight(daddy,g)
       end do
      else
       do g = 1,3
          EGGAFlight(baby,g) = geneAFlight(ind,g)
          mEGGAFlight(baby,g) = mutAFlight(ind,g)
       end do
    end if

    call random_number (random)
    if (random > 0.5) then
       do g = 1,3
          EGGAFother(baby,g) = geneAFother(daddy,g)
          mEGGAFother(baby,g) = mutAFother(daddy,g)
       end do
      else
       do g = 1,3
          EGGAFother(baby,g) = geneAFother(ind,g)
          mEGGAFother(baby,g) = mutAFother(ind,g)
       end do
    end if

    call random_number (random)
    if (random > 0.5) then
       do g = 1,3
          EGGAFmort(baby,g) = geneAFmort(daddy,g)
          mEGGAFmort(baby,g) = mutAFmort(daddy,g)
       end do
      else
       do g = 1,3
          EGGAFmort(baby,g) = geneAFmort(ind,g)
          mEGGAFmort(baby,g) = mutAFmort(ind,g)
       end do
    end if

    call random_number (random)
    if (random > 0.5) then
       do g = 1,3
          EGGAHstom(baby,g) = geneAHstom(daddy,g)
          mEGGAHstom(baby,g) = mutAHstom(daddy,g)
       end do
      else
       do g = 1,3
          EGGAHstom(baby,g) = geneAHstom(ind,g)
          mEGGAHstom(baby,g) = mutAHstom(ind,g)
       end do
    end if

    call random_number (random)
    if (random > 0.5) then
       do g = 1,3
          EGGAHfood(baby,g) = geneAHfood(daddy,g)
          mEGGAHfood(baby,g) = mutAHfood(daddy,g)
       end do
      else
       do g = 1,3
          EGGAHfood(baby,g) = geneAHfood(ind,g)
          mEGGAHfood(baby,g) = mutAHfood(ind,g)
       end do
    end if

    call random_number (random)
    if (random > 0.5) then
       do g = 1,3
          EGGHFlight(baby,g) = geneHFlight(daddy,g)
          mEGGHFlight(baby,g) = mutHFlight(daddy,g)
       end do
      else
       do g = 1,3
          EGGHFlight(baby,g) = geneHFlight(ind,g)
          mEGGHFlight(baby,g) = mutHFlight(ind,g)
       end do
    end if

    call random_number (random)
    if (random > 0.5) then
       do g = 1,3
          EGGHFother(baby,g) = geneHFother(daddy,g)
          mEGGHFother(baby,g) = mutHFother(daddy,g)
       end do
      else
       do g = 1,3
          EGGHFother(baby,g) = geneHFother(ind,g)
          mEGGHFother(baby,g) = mutHFother(ind,g)
       end do
    end if

    call random_number (random)
    if (random > 0.5) then
       do g = 1,3
          EGGHHfood(baby,g) = geneHHfood(daddy,g)
          mEGGHHfood(baby,g) = mutHHfood(daddy,g)
       end do
      else
       do g = 1,3
          EGGHHfood(baby,g) = geneHHfood(ind,g)
          mEGGHHfood(baby,g) = mutHHfood(ind,g)
       end do
    end if

    call random_number (random)
    if (random > 0.5) then
       do g = 1,3
          EGGHHother(baby,g) = geneHHother(daddy,g)
          mEGGHHother(baby,g) = mutHHother(daddy,g)
       end do
      else
       do g = 1,3
          EGGHHother(baby,g) = geneHHother(ind,g)
          mEGGHHother(baby,g) = mutHHother(ind,g)
       end do
    end if

    call random_number (random)
    if (random > 0.5) then
       do g = 1,2
          EGGmemo(baby,g) = geneMemo(daddy,g)
          mEGGmemo(baby,g) = mutMemo(daddy,g)
       end do
      else
       do g = 1,2
          EGGmemo(baby,g) = geneMemo(ind,g)
          mEGGmemo(baby,g) = mutMemo(ind,g)
       end do
    end if
    111 continue  ! all eggs of this mother are given genes and recombined with a father
    99 continue
  end do !ind: indeggs loop
!continue with the lowest numbered individuals
end do !half: eggloop
!do HALF ends here


!the new population of next generation consists of these newly produced eggs
fnewpop = baby
!send alarm signal if new fpop exceeeds array
if (fnewpop > fish) then
   write(10,662) "fnewpop = ",fnewpop, " after egg formation in SRneweggs in generation ",realgen
   write(6,662) "fnewpop = ",fnewpop, " after egg formation in SRneweggs in generation ",realgen
end if
662 format(A11,I6,A49,I5)

!now all new eggs are produced, but then mutations may occur

!Also very long..
do baby = 1, fnewpop
    !the genes for decay of short-term memory of past affects
    do 124 gn = 1,2  !1 = fear, 2 = hunger
       if (memory > 0) then !individuals carry memory of past feelings
          !allow point mutations
          call random_number (random)
          if (random < mthresh) then  !mutation occurs
                allele = EGGmemo(baby,gn)
            !find direction of mutation (incresasing or decreasing allele value)
            call random_number (rand2)
            if (rand2 > 0.5) then
              dirsign = 1
             else
                 dirsign = -1
             end if
            call random_number (rand2)
            !any value from 0.00001 to 0.99999 allowed in step of 0.00001
            if (gen < 0.8*generations) then
               EGGmemo(baby,gn) = allele + dirsign*int(1+rand2*3000)*1.e-5
              else
               EGGmemo(baby,gn) = allele + dirsign*int(1+rand2*300)*1.e-5
            endif
            EGGmemo(baby,gn) = max(EGGmemo(baby,gn), 1.e-5)
            EGGmemo(baby,gn) = min(EGGmemo(baby,gn), 0.99999)
            mEGGmemo(baby,gn) = mEGGmemo(baby,gn) + 1
          end if !mutation
        end if !memory on/off
     124 continue


       !alleles of the affective and hedonic genes are distinct 1 or 2-digit decimal
       ! numbers in steps determined by parameter INTERVAL
        do g = 1,max(3,gABM)
           igene(g) = EGGAHsize(baby,g)
        end do
        if (curve == 1) then !sigmoid functions
            call SR_sig_mutFH(igene,iEGG,stepABM,minABM,maxABM,mthresh,MUT,test99)
            do g = 1,3
                 EGGAHsize(baby,g) = iEGG(g)
            end do
           else   !linear or gamma functions --> no functional relationship in gABM
            call SR_mutABM(igene,iEGG,stepABM,minABM,maxABM,mthresh,gABM,MUT,test99)
            do g = 1,gABM
              EGGAHsize(baby,g) = iEGG(g)
              mEGGAHsize(baby,g) = mEGGAHsize(baby,g) + MUT(g)
            end do
        endif

        do g = 1,3
           igene(g) = EGGAFlight(baby,g)
        end do
        if (curve == 1) then !sigmoid functions
            call SR_sig_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 2) then !linear functions
            call SR_lin_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 3 .or. curve == 4) then !gamma functions
            call SR_gam_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
         endif
        do g = 1,3
           EGGAFlight(baby,g) = iEGG(g)
           mEGGAFlight(baby,g) = mEGGAFlight(baby,g) + MUT(g)
        end do

        do g = 1,3
           igene(g) = EGGAFother(baby,g)
        end do
        if (curve == 1) then !sigmoid functions
            call SR_sig_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 2) then !linear functions
            call SR_lin_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 3 .or. curve == 4) then !gamma functions
            call SR_gam_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
        endif
        do g = 1,3
           EGGAFother(baby,g) = iEGG(g)
           mEGGAFother(baby,g) = mEGGAFother(baby,g) + MUT(g)
        end do

        do g = 1,3
           igene(g) = EGGAFmort(baby,g)
        end do
        if (curve == 1) then !sigmoid functions
            call SR_sig_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 2) then !linear functions
            call SR_lin_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 3 .or. curve == 4) then !gamma functions
            call SR_gam_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
        endif
        do g = 1,3
           EGGAFmort(baby,g) = iEGG(g)
           mEGGAFmort(baby,g) = mEGGAFmort(baby,g) + MUT(g)
        end do

        do g = 1,3
           igene(g) = EGGAHstom(baby,g)
        end do
        if (curve == 1) then !sigmoid functions
            call SR_sig_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 2) then !linear functions
            call SR_lin_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 3 .or. curve == 4) then !gamma functions
            call SR_gam_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
        endif
        do g = 1,3
           EGGAHstom(baby,g) = iEGG(g)
            mEGGAHstom(baby,g) = mEGGAHstom(baby,g) + MUT(g)
       end do

        do g = 1,3
           igene(g) = EGGAHfood(baby,g)
        end do
        if (curve == 1) then !sigmoid functions
            call SR_sig_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 2) then !linear functions
            call SR_lin_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 3 .or. curve == 4) then !gamma functions
            call SR_gam_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
        endif
        do g = 1,3
           EGGAHfood(baby,g) = iEGG(g)
            mEGGAHfood(baby,g) = mEGGAHfood(baby,g) + MUT(g)
        end do

        do g = 1,3
           igene(g) = EGGHFlight(baby,g)
        end do
        if (curve == 1) then !sigmoid functions
            call SR_sig_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 2) then !linear functions
            call SR_lin_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 3 .or. curve == 4) then !gamma functions
            call SR_gam_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
        endif
        do g = 1,3
           EGGHFlight(baby,g) = iEGG(g)
            mEGGHFlight(baby,g) = mEGGHFlight(baby,g) + MUT(g)
        end do

        do g = 1,3
           igene(g) = EGGHFother(baby,g)
        end do
        if (curve == 1) then !sigmoid functions
            call SR_sig_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 2) then !linear functions
            call SR_lin_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 3 .or. curve == 4) then !gamma functions
            call SR_gam_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
        endif
        do g = 1,3
           EGGHFother(baby,g) = iEGG(g)
            mEGGHFother(baby,g) = mEGGHFother(baby,g) + MUT(g)
        end do

        do g = 1,3
           igene(g) = EGGHHfood(baby,g)
        end do
        if (curve == 1) then !sigmoid functions
            call SR_sig_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 2) then !linear functions
            call SR_lin_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 3 .or. curve == 4) then !gamma functions
            call SR_gam_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
        endif
        do g = 1,3
           EGGHHfood(baby,g) = iEGG(g)
            mEGGHHfood(baby,g) = mEGGHHfood(baby,g) + MUT(g)
        end do

        do g = 1,3
           igene(g) = EGGHHother(baby,g)
        end do
        if (curve == 1) then !sigmoid functions
            call SR_sig_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 2) then !linear functions
            call SR_lin_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
           else  if (curve == 3 .or. curve == 4) then !gamma functions
            call SR_gam_mutFH(igene,iEGG,interval,allmin,allmax,mthresh,MUT,test99)
        endif
        do g = 1,3
           EGGHHother(baby,g) = iEGG(g)
            mEGGHHother(baby,g) = mEGGHHother(baby,g) + MUT(g)
        end do
end do !baby mutations



!Print fatherhood data at the end of a generation
if (gen > generations - 11)  then
   write(31,3001) expmt, " !       experiment number"
   write(31,3001) realgen,   " !       generation number"
   write(31,3002) "ind","sex","deathage","bmass","gonad","mother","father"

   do ind = 1,fpop
     write(31,3003) ind,fgender(ind),fdeathage(ind),fbodymass(ind),fgonad(ind),foffspring(ind),father(ind)
   end do
end if

!make prints of genome and individual-data for all "normal" generations near end of simulation
if (((generations < 1000) .and. (gen > max(1,generations-100)) .or. (gen > generations - 1000))) then
   LNG = 1
  else
   LNG = 0 !we are not among Late Normal Generations
end if
if (LNG == 1 .and. dev == 1) then
   do ind = 1,fpop
     write(26,3027) gen,ind,fgender(ind),father(ind)
   end do
end if
3027 format(4I8)

!make and write plots of correlation between genes and traits
if (gen > generations - 101 .and. corrplot == 1) then
   if (gen == generations - 100 .or. gen == 1) then !open files
      if (expmt < 10) then
         write(stringBM,  6000)"HED18-",runtag,"-E0",expmt,"-o",110,"-BodyMass-correlations",".txt"
         write(stringBD,  6007)"HED18-",runtag,"-E0",expmt,"-o",111,"-birth-depth-correlations",".txt"

         write(stringgHHF,6013)"HED18-",runtag,"-E0",expmt,"-o",116,"-g-HH-food-correlations",".txt"
         write(stringPHHF,6009)"HED18-",runtag,"-E0",expmt,"-o",117,"-P0-HH-food-correlations",".txt"
         write(stringgHFL,6009)"HED18-",runtag,"-E0",expmt,"-o",112,"-g-HF-light-correlations",".txt"
         write(stringPHFL,6007)"HED18-",runtag,"-E0",expmt,"-o",113,"-P0-HF-light-correlations",".txt"
         write(stringgHHO,6007)"HED18-",runtag,"-E0",expmt,"-o",118,"-g-HH-others-correlations",".txt"
         write(stringgHFO,6007)"HED18-",runtag,"-E0",expmt,"-o",114,"-g-HF-others-correlations",".txt"
         write(stringPHFO,6011)"HED18-",runtag,"-E0",expmt,"-o",115,"-P0-HF-others-correlations",".txt"
         write(stringPHHO,6011)"HED18-",runtag,"-E0",expmt,"-o",119,"-P0-HH-others-correlations",".txt"

         write(stringgAFP,6013)"HED18-",runtag,"-E0",expmt,"-o",124,"-g-AF-pred-correlations",".txt"
         write(stringPAFP,6009)"HED18-",runtag,"-E0",expmt,"-o",125,"-P0-AF-pred-correlations",".txt"
         write(stringgAHF,6013)"HED18-",runtag,"-E0",expmt,"-o",128,"-g-AH-food-correlations",".txt"
         write(stringPAHF,6009)"HED18-",runtag,"-E0",expmt,"-o",129,"-P0-AH-food-correlations",".txt"
         write(stringgAFL,6009)"HED18-",runtag,"-E0",expmt,"-o",120,"-g-AF-light-correlations",".txt"
         write(stringPAFL,6007)"HED18-",runtag,"-E0",expmt,"-o",121,"-P0-AF-light-correlations",".txt"
         write(stringgAFO,6007)"HED18-",runtag,"-E0",expmt,"-o",122,"-g-AF-others-correlations",".txt"
         write(stringPAFO,6011)"HED18-",runtag,"-E0",expmt,"-o",123,"-P0-AF-others-correlations",".txt"
         write(stringgAHS,6011)"HED18-",runtag,"-E0",expmt,"-o",126,"-g-AH-stomach-correlations",".txt"
         write(stringPAHS,6015)"HED18-",runtag,"-E0",expmt,"-o",127,"-P0-AH-stomach-correlations",".txt"


        else
         write(stringBM,  6001)"HED18-",runtag,"-E",expmt,"-o",110,"-BodyMass-correlations",".txt"
         write(stringBD,  6008)"HED18-",runtag,"-E",expmt,"-o",111,"-birth-depth-correlations",".txt"

         write(stringgHHF,6014)"HED18-",runtag,"-E",expmt,"-o",116,"-g-HH-food-correlations",".txt"
         write(stringgHFL,6010)"HED18-",runtag,"-E",expmt,"-o",112,"-g-HF-light-correlations",".txt"
         write(stringPHHF,6010)"HED18-",runtag,"-E",expmt,"-o",117,"-P0-HH-food-correlations",".txt"
         write(stringgHHO,6008)"HED18-",runtag,"-E",expmt,"-o",118,"-g-HH-others-correlations",".txt"
         write(stringPHFL,6008)"HED18-",runtag,"-E",expmt,"-o",113,"-P0-HF-light-correlations",".txt"
         write(stringgHFO,6008)"HED18-",runtag,"-E",expmt,"-o",114,"-g-HF-others-correlations",".txt"
         write(stringPHHO,6012)"HED18-",runtag,"-E",expmt,"-o",119,"-P0-HH-others-correlations",".txt"
         write(stringPHFO,6012)"HED18-",runtag,"-E",expmt,"-o",115,"-P0-HF-others-correlations",".txt"

         write(stringgAFP,6014)"HED18-",runtag,"-E",expmt,"-o",124,"-g-AF-pred-correlations",".txt"
         write(stringPAFP,6010)"HED18-",runtag,"-E",expmt,"-o",125,"-P0-AF-pred-correlations",".txt"
         write(stringgAHF,6014)"HED18-",runtag,"-E",expmt,"-o",128,"-g-AH-food-correlations",".txt"
         write(stringPAHF,6010)"HED18-",runtag,"-E",expmt,"-o",129,"-P0-AH-food-correlations",".txt"
         write(stringgAFL,6010)"HED18-",runtag,"-E",expmt,"-o",120,"-g-AF-light-correlations",".txt"
         write(stringPAFL,6008)"HED18-",runtag,"-E",expmt,"-o",121,"-P0-AF-light-correlations",".txt"
         write(stringgAFO,6008)"HED18-",runtag,"-E",expmt,"-o",122,"-g-AF-others-correlations",".txt"
         write(stringPAFO,6012)"HED18-",runtag,"-E",expmt,"-o",123,"-P0-AF-others-correlations",".txt"
         write(stringgAHS,6012)"HED18-",runtag,"-E",expmt,"-o",126,"-g-AH-stomach-correlations",".txt"
         write(stringPAHS,6016)"HED18-",runtag,"-E",expmt,"-o",127,"-P0-AH-stomach-correlations",".txt"
      end if !expmnt

      open(110, file = stringBM)
      open(111, file = stringBD)

      open(112, file = stringgHFL)
      open(113, file = stringPHFL)
      open(114, file = stringgHFO)
      open(115, file = stringPHFO)
      open(116, file = stringgHHF)
      open(117, file = stringPHHF)
      open(118, file = stringgHHO)
      open(119, file = stringPHHO)

      open(120, file = stringgAFL)
      open(121, file = stringPAFL)
      open(122, file = stringgAFO)
      open(123, file = stringPAFO)
      open(124, file = stringgAFP)
      open(125, file = stringPAFP)
      open(126, file = stringgAHS)
      open(127, file = stringPAHS)
      open(128, file = stringgAHF)
      open(129, file = stringPAHF)


     !open ABM-files
     if (expmt < 10) then
         write(stringEX,6020)"HED18-",runtag,"-E0",expmt,"-o"
        else
         write(stringEX,6021)"HED18-",runtag,"-E",expmt,"-o"
     end if
     do g = 140,139+gABM  !file number to be opened
        j = g-139         !number of current ABM-gene
        if (j < 10) then
           write(stringABM,6022)stringEX,g,"Hsize-correlations-for-ABM-gene-0",j,".txt"
          else
           write(stringABM,6023)stringEX,g,"Hsize-correlations-for-ABM-gene-",j,".txt"
        end if
        open(g, file = stringABM)
     end do

   end if !gen == generations - 100 .or. gen == 1)

   write(110,*) "  "
   write(110,6004)"Generation","Environment","Max value","Individuals",   &
                  "Birth depth","Gonad mass","Fatherkids","Num afraid", &
                    "gFearLight","PFearLight","gAttrOth","P0AttrOth",        &
                  "gHungFood","PHungFood","gAvoidOth","P0AvoidOth"
   do g = 111,119
      write(g,*) "  "
      write(g,6003)"Generation","Environment","Max value","Individuals","Body mass",   &
                   "Birth depth","Gonad mass","Fatherkids","Num afraid","Survival", &
                   "Lifespan","gFearLight","PFearLight","gAttrOth","P0AttrOth",        &
                      "gHungFood","PHungFood","gAvoidOth","P0AvoidOth"
   end do
   do g = 120,129
      write(g,*) "  "
      write(g,6005)"Generation","Environment","Max value","Individuals","Body mass",   &
                   "Birth depth","Gonad mass","Fatherkids","Num afraid","Survival", &
                   "Lifespan","gFearLight","PFearLight","gAttrOth","P0AttrOth",        &
                      "gFearPred","PFearPred","gHungStom","PHungStom","gHungFood","PHungFood"
   end do
   do g = 140,139+gABM  !file number ABM-files
      write(g,*) "  "
      write(g,6006)"Generation","Environment","Max value","Individuals","Body mass",        &
                   "Birth depth","Gonad mass","Fatherkids","Num afraid","Survival",         &
                   "Lifespan","gFearLight","PFearLight","gAttrOth","P0AttrOth",             &
                      "gFearPred","PFearPred","gHungStom","PHungStom","gHungFood","PHungFood", &
                   "ABM-1","ABM-2","ABM-3","ABM-4","ABM-5","ABM-6","ABM-7","ABM-8","ABM-9", &
                   "ABM-10"
   end do

   !call SR for correlation between phenoptye and ABM-genes
   call SRcorrplotsABM(140,fpop,realgen,deviation,flifespan,gABM,test99)
   !call SR for correlation between phenoptye and affect genes
   call SRcorrplotsAFF(120,fpop,realgen,deviation,flifespan,allmin,test99)
   !call SR for correlation between phenoptye and hedonic genes
   call SRcorrplotsHED(110,fpop,fmaxweight,realgen,deviation,flifespan,allmin,test99)

   if (gen == generations) then !close files
      do g = 110,129
         close(g)
      end do
   end if
end if !make and write correlation plots


6000 format(2A6,A3,I1,A2,I3,A22,A4)
6001 format(2A6,A2,I2,A2,I3,A22,A4)
6007 format(2A6,A3,I1,A2,I3,A25,A4)
6008 format(2A6,A2,I2,A2,I3,A25,A4)
6009 format(2A6,A3,I1,A2,I3,A24,A4)
6010 format(2A6,A2,I2,A2,I3,A24,A4)
6011 format(2A6,A3,I1,A2,I3,A26,A4)
6012 format(2A6,A2,I2,A2,I3,A26,A4)
6013 format(2A6,A3,I1,A2,I3,A23,A4)
6014 format(2A6,A2,I2,A2,I3,A23,A4)
6015 format(2A6,A3,I1,A2,I3,A27,A4)
6016 format(2A6,A2,I2,A2,I3,A27,A4)


6020 format(2A6,A3,I1,A2)
6021 format(2A6,A2,I2,A2)
6022 format(A18,I3,A33,I1,A4)
6023 format(A18,I3,A32,I2,A4)


6003 format(19A12)
6004 format(16A12)
6005 format(21A12)
6006 format(31A12)

3001 format(I6,A30)
3002 format(7A9)
3003 format(3I9,F9.0,F9.2,2I9)


!convert the egg genes to become genes of the parents of next generation

do egg = 1, fnewpop
   !the family tree of the baby is preserved:
   do gn = 1,motherlength
      motherstring(egg,gn)= eggstring(egg,gn)
   end do
   mcreation(egg) = meggcreation(egg)
   gcreation(egg) = geggcreation(egg)
   acreation(egg) = aeggcreation(egg)
   icreation(egg) = ieggcreation(egg)
   motherpop(egg) = eggmotherpop(egg)

   fgender(egg) = EGGgender(egg)
   do gn = 1,10
      geneAHsize(egg,gn) = min(max(EGGAHsize(egg,gn),minABM),maxABM)
      mutAHsize(egg,gn) = mEGGAHsize(egg,gn)
   end do
   do gn = 1,3
      geneAFlight(egg,gn) = min(max(EGGAFlight(egg,gn),allmin),allmax)
      geneAFother(egg,gn) = min(max(EGGAFother(egg,gn),allmin),allmax)
      geneAFmort(egg,gn) = min(max(EGGAFmort(egg,gn),allmin),allmax)
      geneAHstom(egg,gn) = min(max(EGGAHstom(egg,gn),allmin),allmax)
      geneAHfood(egg,gn) = min(max(EGGAHfood(egg,gn),allmin),allmax)
      geneHFlight(egg,gn) = min(max(EGGHFlight(egg,gn),allmin),allmax)
      geneHFother(egg,gn) = min(max(EGGHFother(egg,gn),allmin),allmax)
      geneHHfood(egg,gn) = min(max(EGGHHfood(egg,gn),allmin),allmax)
      geneHHother(egg,gn) = min(max(EGGHHother(egg,gn),allmin),allmax)

      mutAFlight(egg,gn) = mEGGAFlight(egg,gn)
      mutAFother(egg,gn) = mEGGAFother(egg,gn)
      mutAFmort(egg,gn) = mEGGAFmort(egg,gn)
      mutAHstom(egg,gn) = mEGGAHstom(egg,gn)
      mutAHfood(egg,gn) = mEGGAHfood(egg,gn)
      mutHFlight(egg,gn) = mEGGHFlight(egg,gn)
      mutHFother(egg,gn) = mEGGHFother(egg,gn)
      mutHHfood(egg,gn) = mEGGHHfood(egg,gn)
      mutHHother(egg,gn) = mEGGHHother(egg,gn)
  end do
  do gn = 1,2
        geneMemo(egg,gn) = EGGmemo(egg,gn)
        mutMemo(egg,gn) = mEGGmemo(egg,gn)
  end do
end do


!new from 7 Sept 2010
!renumber some new individuals to get them away from their "sisters"
!(swap two and two babies named "egg" and "ind")
do j = 1,min(500,fnewpop/10)
    call random_number (random)
    egg = int((0.5*fnewpop-1)*random) + 1             !from the first half of fnewpop
    call random_number (random)
    ind = int((0.5*fnewpop-1)*random) + fnewpop/2 - 1 !from the second half of fnewpop

    !store all data on "egg" in EGG-arrays
   do gn = 1,motherlength
      eggstring(1,gn)= motherstring(egg,gn)
   end do
   meggcreation(1) = mcreation(egg)
   geggcreation(1) = gcreation(egg)
   aeggcreation(1) = acreation(egg)
   ieggcreation(1) = icreation(egg)
   eggmotherpop(1) = motherpop(egg)
   start = fbirthdepth(egg)
   do gn = 1,10
      EGGAHsize(1,gn) = geneAHsize(egg,gn)
   end do
   do gn = 1,3
      EGGAFlight(1,gn) = geneAFlight(egg,gn)
      EGGAFother(1,gn) = geneAFother(egg,gn)
      EGGAFmort(1,gn) = geneAFmort(egg,gn)
      EGGAHstom(1,gn) = geneAHstom(egg,gn)
      EGGAHfood(1,gn) = geneAHfood(egg,gn)
      EGGHFlight(1,gn) = geneHFlight(egg,gn)
      EGGHFother(1,gn) = geneHFother(egg,gn)
      EGGHHfood(1,gn) = geneHHfood(egg,gn)
      EGGHHother(1,gn) = geneHHother(egg,gn)
  end do
  do gn = 1,2
        EGGMemo(1,gn) = genememo(egg,gn)
  end do
  EGGgender(1) = fgender(egg)

   !move all IND-data into the "egg" number
   do gn = 1,motherlength
      motherstring(egg,gn)= motherstring(ind,gn)
   end do
   mcreation(egg) = mcreation(ind)
   gcreation(egg) = gcreation(ind)
   acreation(egg) = acreation(ind)
   icreation(egg) = icreation(ind)
   motherpop(egg) = motherpop(ind)
   fbirthdepth(egg) = fbirthdepth(ind)

   do gn = 1,10
      geneAHsize(egg,gn) = geneAHsize(ind,gn)
   end do
   do gn = 1,3
      geneAFlight(egg,gn) = geneAFlight(ind,gn)
      geneAFother(egg,gn) = geneAFother(ind,gn)
      geneAFmort(egg,gn) = geneAFmort(ind,gn)
      geneAHstom(egg,gn) = geneAHstom(ind,gn)
      geneAHfood(egg,gn) = geneAHfood(ind,gn)
      geneHFlight(egg,gn) = geneHFlight(ind,gn)
      geneHFother(egg,gn) = geneHFother(ind,gn)
      geneHHfood(egg,gn) = geneHHfood(ind,gn)
      geneHHother(egg,gn) = geneHHother(ind,gn)
  end do
  do gn = 1,2
        geneMemo(egg,gn) = geneMemo(ind,gn)
  end do
  fgender(egg) = fgender(ind)

    !retrieve the stored data on "egg" in the IND-number
   !the family tree of the baby is preserved:
   do gn = 1,motherlength
      motherstring(ind,gn)= eggstring(1,gn)
   end do
   mcreation(ind) = meggcreation(1)
   gcreation(ind) = geggcreation(1)
   acreation(ind) = aeggcreation(1)
   icreation(ind) = ieggcreation(1)
   motherpop(ind) = eggmotherpop(1)
   fbirthdepth(ind) = start

   do gn = 1,10
      geneAHsize(ind,gn) = EGGAHsize(1,gn)
   end do
   do gn = 1,3
      geneAFlight(ind,gn) = EGGAFlight(1,gn)
      geneAFother(ind,gn) = EGGAFother(1,gn)
      geneAFmort(ind,gn) = EGGAFmort(1,gn)
      geneAHstom(ind,gn) = EGGAHstom(1,gn)
      geneAHfood(ind,gn) = EGGAHfood(1,gn)
      geneHFlight(ind,gn) = EGGHFlight(1,gn)
      geneHFother(ind,gn) = EGGHFother(1,gn)
      geneHHfood(ind,gn) = EGGHHfood(1,gn)
      geneHHother(ind,gn) = EGGHHother(1,gn)
  end do
  do gn = 1,2
        geneMemo(ind,gn) = EGGmemo(1,gn)
  end do
  fgender(ind) = EGGgender(1)

end do !j

!reset genes of non-existant individuals

do egg = fnewpop+1, fish
   !the family tree of the baby
   do gn = 1,motherlength
      motherstring(egg,gn)= 0
   end do
   mcreation(egg) = -1
   gcreation(egg) = -1
   acreation(egg) = -1
   icreation(egg) = -1
   motherpop(egg) = -1


  do g = 1,gABM
      geneAHsize(egg,g) = -10.
  end do
   do g = 1,3
      geneAFlight(egg,g) = -10.
      geneAFother(egg,g) = -10.
      geneAFmort(egg,g) = -10.
      geneAHstom(egg,g) = -10.
      geneAHfood(egg,g) = -10.
      geneHFlight(egg,g) = -10.
      geneHFother(egg,g) = -10.
      geneHHfood(egg,g) = -10.
      geneHHother(egg,g) = -10.
  end do
  do g = 1,2
        geneMemo(egg,g) = -10.
  end do
   fstatus(egg) = 0
   !fgender(egg) = -10
end do

!send alarm signal if new fpop exceeeds array
!if (fnewpop > fish) then
!   write(10,661) "fnewpop = ",fnewpop, " after immigration in SRneweggs in generation ",realgen
!   write(6,661)  "fnewpop = ",fnewpop, " after immigration in SRneweggs in generation ",realgen
!end if
!661 format(A11,I6,A49,I5)

if (fnewpop > 0) call SRallelefrequency(runtag,srerr,test99,expmt,gen,generations,fnewpop,   &
                      realgen,allmin,allmax,interval,curve,gABM,stepABM,minABM,maxABM)

end
!end subroutine SRneweggs


!-------------------------------------------------------------------------------
subroutine SR_mutABM(gene,EGG,stepABM,minABM,maxABM,mthresh,gABM,MUT,test99)
!-------------------------------------------------------------------------------
!mutations in genes in affect and hedonic functions, if linear or gamma function
implicit none

!variables from main program
real gene(10),EGG(10)
integer MUT(10),test99
real stepABM,minABM,maxABM,mthresh
integer gABM
!local variables
integer dirsign,g,step
real random,rand2
if (test99 == 1) write(10,*) "SR_mutABM"

! the gABM next genes are controlling the affects in real function linearABM
do g = 1,gABM
   MUT(g) = 0
   call random_number (random)
   if (random < mthresh) then !mutation occurs
       MUT(g) = 1
       !find direction of mutation (incresasing or decreasing allele value)
       call random_number (rand2)
       if (rand2 > 0.5) then
          dirsign = 1
         else
          dirsign = -1
       end if
       if(stepABM < 0.1) then
          call random_number (random)
          step = NINT(1.+random*9.)
         else
          step = 1
       end if
       EGG(g) = gene(g) + stepABM*dirsign*step
       if (EGG(g) < minABM) EGG(g) = minABM
       if (EGG(g) > maxABM) EGG(g) = maxABM

     else
      !no mutation, allele value preserved
       EGG(g) = gene(g)
   end if
end do

end
!end subroutine SR_mutABM


!-------------------------------------------------------------------------------
subroutine SR_gam_mutFH(gene,EGG,interval,allmin,allmax,mthresh,MUT,test99)
!-------------------------------------------------------------------------------
!mutations in genes in affect and hedonic functions, if gamma distribution function
implicit none

!variables from main program
integer MUT(10),test99
real gene(10),EGG(10),interval,allmin,allmax,mthresh
!local variables
integer dirsign,g,steps
real random,rand2,step

if (test99 == 1) write(10,*) "SR-gam_mutFH"

! the 3 next genes are controlling the affects in real function gamma2gene,
!but the third has no function. It is kept since LINEAR and SIGMOID require 3 genes
do g = 1,2  !the third gene is not in use
   MUT(g) = 0
   call random_number (random)
   if (random < mthresh) then !mutation occurs
      MUT(g) = 1
      if (random > mthresh*0.1) then  !90% of mutations are small and local
         !find direction of mutation (incresasing or decreasing allele value)
         call random_number (rand2)
         if (rand2 > 0.5) then
           dirsign = 1
          else
           dirsign = -1
         end if
         !Now as dirsign is found, find allele value of the three genes
         EGG(g) = gene(g) + interval*dirsign
         if (EGG(g) < allmin) EGG(g) = allmin
         if (EGG(g) > allmax) EGG(g) = allmax
       else ! random mutation within (allmin,allmax)
         steps = (allmax-allmin)/interval
         call random_number (rand2)
         step = nint(steps*rand2)
         EGG(g) = interval*step
      end if
    else
    !no mutation, allele value preserved
     EGG(g) = gene(g)
  end if
end do

!if allmin < 0, this should only appply to gamma, not P0
if (allmin < 0.) EGG(2) = abs(EGG(2))


end
!end subroutine SR_gam_mutFH


!-------------------------------------------------------------------------------
subroutine SR_lin_mutFH(gene,EGG,interval,allmin,allmax,mthresh,MUT,test99)
!-------------------------------------------------------------------------------
!mutations in genes in affect and hedonic functions, if linear function
implicit none

!variables from main program
integer MUT(10),test99
real gene(10),EGG(10),interval,allmin,allmax,mthresh
!local variables
integer dirsign,g,steps
real random,rand2,dummy,step
if (test99 == 1) write(10,*) "SR-lin_mutFH"

         !Amax : g1
         !Pmin : g2
         !Pmax : g3

! the 3 next genes are controlling the affects in real function linear
do g = 1,3
   MUT(g) = 0
   call random_number (random)
   if (random < mthresh) then !mutation occurs
      MUT(g) = 1
      if (random > mthresh*0.1) then  !90% of mutations are small and local
         !find direction of mutation (incresasing or decreasing allele value)
         call random_number (rand2)
         if (rand2 > 0.5) then
           dirsign = 1
          else
           dirsign = -1
         end if
         !Now as dirsign is found, find allele value of the three genes
         EGG(g) = gene(g) + interval*dirsign
         if (EGG(g) < allmin) EGG(g) = allmin
         if (EGG(g) > allmax) EGG(g) = allmax
        else ! random mutation within (allmin,allmax)
         steps = (allmax-allmin)/interval
         call random_number (rand2)
         step = nint(steps*rand2)
         EGG(g) = interval*step
      end if
    else
    !no mutation, allele value preserved
     EGG(g) = gene(g)
  end if
end do

!make sure Pmax > Pmin
if (EGG(3) < EGG(2)) then
    dummy = EGG(2)
    EGG(2) = EGG(3)
    EGG(3) = dummy
end if


end
!end subroutine SR_lin_mutFH



!-------------------------------------------------------------------------------
subroutine SR_sig_mutFH(gene,EGG,interval,allmin,allmax,mthresh,MUT,test99)
!-------------------------------------------------------------------------------
!mutations in genes in affect and hedonic functions, if sigmoid function
implicit none

!variables from main program
real gene(10),EGG(10),interval,allmin,allmax,mthresh
integer MUT(10),test99
!local variables
integer dirsign,g
real random,rand2
if (test99 == 1) write(10,*) "SR-sig_mutFH"

! the 3 next genes are controlling the affects in real function sigmoid
do g = 1,3
   MUT(g) = 0 !ni mutation so far
   call random_number (random)
   if (random < mthresh) then !mutation occurs
      MUT(g) = 1
      !find direction of mutation (incresasing or decreasing allele value)
      call random_number (rand2)
      if (rand2 > 0.5) then
         dirsign = 1
        else
         dirsign = -1
      end if
     !Now as dirsign is found, find allele value of the three genes
     !The K and r-genes move only one interval per mutation, while
     EGG(g) = gene(g) + interval*dirsign
     if (EGG(g) < allmin) EGG(g) = allmin
     if (EGG(g) > allmax) EGG(g) = allmax
    else
    !no mutation, allele value preserved
     EGG(g) = gene(g)
  end if
end do

end
!end subroutine SR_sig_mutFH


!-------------------------------------------------------------------------------
subroutine SRfeelings(runtag,time,fpop,curve,gABM,expmt,swHsize,swHstomach, &
                      swHfood,swFsize,swFlight,swFothers,swFmort,alleleconv, &
                      hungersensitivity,errorP,test99)
!-------------------------------------------------------------------------------
!calculate frequencies of individuals in the population who are afraid, in a range of conditions
implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer    fpop,curve,gABM,expmt,time,swHsize,swHstomach
integer    swHfood,swFsize,swFlight,swFothers,swFmort,test99
character (6) runtag
real hungersensitivity,errorP

!local variables
integer x,y,i,xvar,yvar,ind,numafraid,filen,affect,alleleconv
real numx,numy,fearfraction
real hunger,Hsize,Hstomach,Hfood,fear,Fsize,Flight,Fothers,Fmort
character (1) tag          !letter for dominant affect (F or H)
character(55) string
character(5) text1
character(6) text2
character(10) text3
character (10) comingfrom  !name of this SR to be sent to SRaffect to know where call came from

if (test99 == 1) write(10,*) "SRfeelings"
!TIME specifies whether SR-call was made at 10, 25, 50, 75 or 100 % of GENERATIONS.
!"text3" is name of directory for output files.
if (time == 1) then
    text3 = "fearplot10"
  else if (time == 2) then
    text3 = "fearplot25"
  else if (time == 3) then
    text3 = "fearplot50"
  else if (time == 4) then
    text3 = "fearplot75"
  else
    text3 = "fearplt100"
end if

filen = 70 !initiate output file number one lower than first file
!then set the value of the two perceptions that shall be used in the fear plot
do x = 1,5  !principal axix in 3D plot

   if (x == 1) then ! x axis is light
      text1 = "light"
     else if (x == 2) then ! x axis is other agents
      text1 = "stoma"
     else if (x == 3) then !and x-axis is food concentration
      text1 = "bmass"
     else if (x == 4) then ! x axis is stomach
      text1 = "other"
     else ! x = 5 and x-axis is body mass
      text1 = "foodc"
   end if

   do y = 1, 6-x  !second axis in 3D plot
      filen = filen + 1 !increase number of output file by 1 for each new plot
      if (y == 1) then !give label in file name for second axis variable
         text2 = "&prisk"
        else if (y == 2) then
         text2 = "&foodc"
        else if (y == 3) then
         text2 = "&other"
        else if (y == 4) then
         text2 = "&bmass"
        else
         text2 = "&stoma"
      end if

       if (expmt < 10) then
         write(string,1000)"HED18-",runtag,"-E0",expmt,"-o0",filen,"-fearplot-",text1,text2,".txt"
        else
         write(string,1001)"HED18-",runtag,"-E",expmt,"-o0",filen,"-fearplot-",text1,text2,".txt"
      end if
      open(filen, file = text3//"/"//string)
1000 format(2A6,A3,I1,A3,I2,A10,A5,A6,A4)
1001 format(2A6,A2,I2,A3,I2,A10,A5,A6,A4)

      do xvar = 1,50
         numx = 0.02*xvar

         do yvar = 1,50
            numy = 0.02*yvar

            !first set all finfo to 0.5 (half of maximum value)
            do i = 1,19
              finfo(i) = 0.5
            end do

            if (x == 1) then ! x axis is light
               finfo(4) = numx
              else if (x == 2) then ! x axis is stomach
               finfo(1) = numx
              else if (x == 3) then ! x axis is body mass
               finfo(12) = numx
              else if (x == 4) then ! x axis is other agents
               finfo(9) = numx
              else ! x = 5 and x-axis is food concentration
               finfo(15) = numx
            end if

            if (y == 1) then ! y axis is predation risk
               finfo(19) = numy
              else if (y == 2) then ! y axis is food
               finfo(15) = numy
              else if (y == 3) then ! y axis other agents
               finfo(9) = numy
              else if (y == 4) then ! y axis body mass
               finfo(12) = numy
              else ! (y == 5) and y axis is stomach
               finfo(1) = numy
            end if

           !call SRaffect and make statistics for plot
            numafraid = 0
            comingfrom = "SRfeelings"

            do ind = 1,fpop
               call SRaffect(ind,curve,alleleconv,gABM,affect,tag,hungersensitivity,  &
                    errorP,swHsize,swHstomach,swHfood,swFsize,swFlight,swFothers,  &
                    swFmort,hunger,Hstomach,Hsize,Hfood,fear,Fsize,Flight,Fothers,  &
                    Fmort,comingfrom,test99)
               if (affect == 1) numafraid = numafraid + 1
            end do !ind over fpop
            fearfraction = numafraid/(1.*fpop)
            write(filen,1101) numx,numy,fearfraction

         end do ! yvar
       end do ! xvar
       close(filen)
  end do ! y
end do ! x
1101 format(2F7.3,F8.4)


end
!end subroutine SRfeelings


!-------------------------------------------------------------------------------
subroutine SRaffect(ind,curve,alleleconv,gABM,affect,tag,hungersensitivity,errorP,  &
                    swHsize,swHstomach,swHfood,swFsize,swFlight,swFothers,swFmort,  &
                    hunger,Hstomach,Hsize,Hfood,fear,Fsize,Flight,Fothers,Fmort,    &
                    comingfrom,test99)
!-------------------------------------------------------------------------------
!calculate frequencies individuals in last generation who are afraid in a range of conditions
implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer    ind,curve,alleleconv,gABM,affect,test99
integer swHsize,swHstomach,swHfood,swFsize,swFlight,swFothers,swFmort
character (1) tag          !letter for dominant affect (f, F or H)
character (10) comingfrom  !name of the SR where call came from
real hunger,Hstomach,Hsize,Hfood             !hunger affect and its components
real fear,Fsize,Flight,Fothers,Fmort         !fear affect and its components
real hungersensitivity,errorP

!local variables
integer g
real zero,a1,a2,a3,a4
!up to 10 genes deciding how fear or hunger are controlled by age or body mass:
real ABMgenes(10)
!general functions that gives genetically modified affect from perception of stimulus:
real sigmoid,linear,gamma2gene,gamma1gene,linearABM

if (test99 == 1 .and. ind < 10) write(10,*) "SRaffect for ind ",ind

!reset all affect components of individual
hunger = -0.1
fear = -0.1
Fsize = 0.
Flight = 0.
Fothers = 0.
Fmort = 0.
Hsize = 0.
Hstomach = 0.
Hfood = 0.

!new for 3 July 2009
!A problem with the sigmoid is that it can have a strong function value in absence of environmental signal.
!If e.g. a = 1 and r = 0, then half of a fear is present in absence of perception.
!To avoid this, the fear and hunger are scaled against this absence-value in the following calculations.
!The absence value ("zero") is for 0 perception at INCREASING feeling and for max (=1) at DECREASING feeling.


!SUMMING UP THE COMPONENTS OF FEAR
!body size influence any component of fear
if (curve == 1) then ! sigmoid relationship
    a1 = geneAHsize(ind,1)
    a2 = geneAHsize(ind,2)
    a3 = geneAHsize(ind,3)
    a4 = finfo(12)
    Hsize = sigmoid(a1,a2,a3,a4)
    zero = sigmoid(a1,a2,a3,0.)
    Hsize = Hsize-zero
    Fsize = 1. - Hsize  !never tested!!!!!
  else !linear or gamma
    do g = 1,10
        ABMgenes(g) = geneAHsize(ind,g)
    end do
    !interpolate between nearest two values to find F&Hsize at current BM
    Hsize = linearABM(ABMgenes,finfo(12),gABM,errorP)
    Fsize = 1. - Hsize
end if
if (swFsize == 0) Fsize = 1. !no size-effect on fear, fear = Flight + Fmort - Fothers
if (swHsize == 0) Hsize = 1. !no size-effect on hunger

!fear due to light at depth
if (swFlight == 1) then
     a1 = geneAFlight(ind,1)
     a2 = geneAFlight(ind,2)
     a3 = geneAFlight(ind,3)
     if (alleleconv == 2) then !0.1 -> 0.1; 0.3 -> 0.9;  0.7 -> 4.9;  1 -> 10;
          a1 = 10*a1*abs(a1)
          a2 = 10*a2*abs(a2)
          a3 = 10*a3*abs(a3)
      else if (alleleconv == 3) then
         a1 = 10*a1
         a2 = 10*a2
         a3 = 10*a3
      end if
      a4 = finfo(4)
      if (curve == 1) then ! sigmoid relationship
         Flight = sigmoid(a1,a2,a3,a4)
         zero = sigmoid(a1,a2,a3,0.)
         Flight = Flight-zero
        else if (curve == 2) then ! linear relationship
         Flight = linear(a1,a2,a3,a4)
        else if (curve == 3) then ! gamma distribution relationship
         Flight = gamma2gene(a1,a2,a4,errorP)
        else if (curve == 4) then ! predfined gamma distribution relationship
         Flight = gamma1gene(a1,a2,a4,errorP)
      end if
  else
      Flight = 0.
end if

!fear adjustment by presence of conspecifics
!rewritten 10 April 2010. Now Fothers INCREASE, but is SUBTRACTED in fear function
!Hence, seeing others (=crowding) gives a positive feeling ("relief") which reduces fear
if (swFothers == 1) then
     a1 = geneAFother(ind,1)
     a2 = geneAFother(ind,2)
     a3 = geneAFother(ind,3)
     if (alleleconv == 2) then
          a1 = 10*a1*abs(a1)
          a2 = 10*a2*abs(a2)
          a3 = 10*a3*abs(a3)
        else if (alleleconv == 3) then
         a1 = 10*a1
         a2 = 10*a2
         a3 = 10*a3
      end if
      a4 = finfo(9)
      if (curve == 1) then ! sigmoid relationship
         Fothers = sigmoid(a1,a2,a3,a4)
         zero = sigmoid(a1,a2,a3,0.)
         Fothers = Fothers-zero
        else if (curve == 2) then ! linear relationship
         Fothers = linear(a1,a2,a3,a4)
        else if (curve == 3) then ! gamma distribution relationship
         Fothers = gamma2gene(a1,a2,a4,errorP)
        else if (curve == 4) then ! predfined gamma distribution relationship
         Fothers = gamma1gene(a1,a2,a4,errorP)
      end if
  else
      Fothers = 0.
end if

!fear MUST INCREASE due to increased predator presence
if (swFmort == 1) then
     a1 = geneAFmort(ind,1)
     a2 = geneAFmort(ind,2)
     a3 = geneAFmort(ind,3)
     if (alleleconv == 2) then
          a1 = 10*a1*abs(a1)
          a2 = 10*a2*abs(a2)
          a3 = 10*a3*abs(a3)
        else if (alleleconv == 3) then
         a1 = 10*a1
         a2 = 10*a2
         a3 = 10*a3
      end if
      a4 = finfo(19)
      if (curve == 1) then ! sigmoid relationship
         Fmort = sigmoid(a1,a2,a3,a4)
         zero = sigmoid(a1,a2,a3,0.)
         Fmort = Fmort-zero
        else if (curve == 2) then ! linear relationship
         Fmort = linear(a1,a2,a3,a4)
        else if (curve == 3) then ! gamma distribution relationship
         Fmort = gamma2gene(a1,a2,a4,errorP)
        else if (curve == 4) then ! predfined gamma distribution relationship
         Fmort = gamma1gene(a1,a2,a4,errorP)
      end if
  else
      Fmort = 0.
end if


fear = Fsize * (Flight - Fothers + Fmort)  !before 21.09.09 Fsize + Flight ...


!SUMMING UP THE COMPONENTS OF HUNGER

!appetite from available stomach capacity
if (swHstomach == 1) then
     a1 = geneAHstom(ind,1)
     a2 = geneAHstom(ind,2)
     a3 = geneAHstom(ind,3)
     if (alleleconv == 2) then
          a1 = 10*a1*abs(a1)
          a2 = 10*a2*abs(a2)
          a3 = 10*a3*abs(a3)
        else if (alleleconv == 3) then
         a1 = 10*a1
         a2 = 10*a2
         a3 = 10*a3
      end if
      a4 = finfo(1)
      if (curve == 1) then ! sigmoid relationship
         Hstomach = sigmoid(a1,a2,a3,a4)
         zero = sigmoid(a1,a2,a3,0.)
         Hstomach = Hstomach-zero
        else if (curve == 2) then ! linear relationship
         Hstomach = linear(a1,a2,a3,a4)
        else if (curve == 3) then ! gamma distribution relationship
         Hstomach = gamma2gene(a1,a2,a4,errorP)
        else if (curve == 4) then ! predfined gamma distribution relationship
         Hstomach = gamma1gene(a1,a2,a4,errorP)
      end if
  else
      Hstomach = 0.
end if

!appetite due to seeing food
if (swHfood == 1) then
     a1 = geneAHfood(ind,1)
     a2 = geneAHfood(ind,2)
     a3 = geneAHfood(ind,3)
     if (alleleconv == 2) then
          a1 = 10*a1*abs(a1)
          a2 = 10*a2*abs(a2)
          a3 = 10*a3*abs(a3)
        else if (alleleconv == 3) then
         a1 = 10*a1
         a2 = 10*a2
         a3 = 10*a3
      end if
      a4 = finfo(15)
      if (curve == 1) then ! sigmoid relationship
         Hfood = sigmoid(a1,a2,a3,a4)
         zero = sigmoid(a1,a2,a3,0.)
         Hfood = Hfood-zero
        else if (curve == 2) then ! linear relationship
         Hfood = linear(a1,a2,a3,a4)
        else if (curve == 3) then ! gamma distribution relationship
         Hfood = gamma2gene(a1,a2,a4,errorP)
        else if (curve == 4) then ! predfined gamma distribution relationship
         Hfood = gamma1gene(a1,a2,a4,errorP)
      end if
  else
      Hfood = 0.
end if

hunger = Hsize * (Hstomach + Hfood)


!adjust fear and hunger for short-term memory. This is new after HED14
fear = fear + fmemory(ind,1)*geneMemo(ind,1)
hunger = hunger + fmemory(ind,2)*geneMemo(ind,2)
!adjust memory for next time step
fmemory(ind,1) = fear
fmemory(ind,2) = hunger


!Find dominant affect
g = fgender(ind)
comingfrom = comingfrom
if (hunger < hungersensitivity) then !demand hunger larger than this to be effective
     affect = 1
     tag = "f"  !"f" means fear due to lack of hunger
     fnumberafraid(ind) = fnumberafraid(ind) + 1
     fneverafraid(ind) = 0
     numHF(2,g) = numHF(2,g) + 1
   else if (fear .ge. hunger) then !hunger will increase later if not eating, but fear will probably fall soon
     affect = 1
     tag = "F" !capital "F" means fear due to fear affect
     fnumberafraid(ind) = fnumberafraid(ind) + 1
     fneverafraid(ind) = 0
     numHF(2,g) = numHF(2,g) + 1
   else
     affect = 0
     tag = "H"  !capital "H" means hunger
     numHF(1,g) = numHF(1,g) + 1
endif
findfeel(ind) = affect

end
!end SRaffect


!-------------------------------------------------------------------------------
subroutine SRfileheadings(filen,curve,heading,test99)
!-------------------------------------------------------------------------------
!make common headings for many output files
implicit none
!variables from main program
integer filen,curve,heading,test99
if (test99 == 1) write(10,*) "SRfileheadings for filen",filen


 if (heading == 2) then
      write(filen,5005) "ind","bmass","% stom","depth","bmass","% stom","depth","bmass",    &
                       "% stom","depth",            &
                         "gene1","gene2","gene3","gene4","gene5","gene6","gene7","gene8",  &
                       "gene9","gene10","gene11","gene12","gene13","gene14","gene15",    &
                       "gene16","gene17","gene18","gene19","gene20","gene21","gene22",   &
                       "gene23","gene24","gene25","gene26","gene27","gene28","gene29",   &
                       "gene30","gene31","gene32","gene33","gene34","gene35","gene36",   &
                       "gene37","gene38","gene39"

      else if (heading == 3) then
        if (curve == 1) then !sigmoid affect functions
             write(filen,5005) "prev","mid-","night","last","mid-","day","last","mid-", &
                            "night","k-Hsiz","a-Hsiz","r-Hsiz", &
                            "k-Flig","a-Flig","r-Flig","void","void", &
                            "void","void","void","void","void",  &
                              "k-Foth","a-Foth","r-Foth", &
                              "k-Fmrt","a-Fmrt","r-Fmrt", &
                            "kHstom","aHstom","rHstom", &
                            "kHfood","aHfood","rHfood", &
                            "k-FdL","a-FdL","r-FdL", &
                            "k-FdO","a-FdO","r-FdO", &
                            "k-hdf","a-HdF","r-HdF", &
                            "k-HdO","a-HdO","r-HdO", &
                            "f-mem","h-mem"
      else  if (curve == 2) then ! linear affect functions
             write(filen,5005) "prev","mid-","night","last","mid-","day","last","mid-", &
                            "night","P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "P6Hsiz","P7Hsiz","P8Hsiz","P9Hsiz","10Hsiz", &
                            "A-Flig","P0Flig","P1Flig", &
                              "A-Foth","P0Foth","P1Foth", &
                              "A-Fmrt","P0Fmrt","P1Fmrt", &
                            "AHstom","P0Hstm","P1Hstm", &
                            "AHfood","P0Hfod","P1Hfod", &
                            "A-FdL","P0FdL","P1FdL", &
                            "A-FdO","P0FdO","P1FdO", &
                            "A-HdF","P0HdF","P1HdF", &
                            "A-HdO","P0HdO","P1HdO", &
                            "f-mem","h-mem"
      else  if (curve == 3) then ! gamma distribution affect functions
             write(filen,5005) "prev","mid-","night","last","mid-","day","last","mid-", &
                            "night","P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "P6Hsiz","P7Hsiz","P8Hsiz","P9Hsiz","10Hsiz", &
                            "g-Flig","P0Flig","void", &
                              "g-Foth","P0Foth","void", &
                              "g-Fmrt","P0Fmrt","void", &
                            "gHstom","P0Hstm","void", &
                            "gHfood","P0Hfod","void", &
                            "g-FdL","P0FdL","void", &
                            "g-FdO","P0FdO","void", &
                            "g-HdF","P0HdF","void", &
                            "g-HdO","P0HdO","void", &
                            "f-mem","h-mem"
      else  if (curve == 4) then ! predefined gamma distribution affect functions
             write(filen,5005) "prev","mid-","night","last","mid-","day","last","mid-", &
                            "night","P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "P6Hsiz","P7Hsiz","P8Hsiz","P9Hsiz","10Hsiz", &
                            "g-Flig","k-Flig","void", &
                              "g-Foth","k-Foth","void", &
                              "g-Fmrt","k-Fmrt","void", &
                            "gHstom","k-Hstm","void", &
                            "gHfood","k-Hfod","void", &
                            "g-FdL","k-FdL","void", &
                            "g-FdO","k-FdO","void", &
                            "g-HdF","k-HdF","void", &
                            "g-HdO","k-HdO","void", &
                            "f-mem","h-mem"
   end if !curve

  else if (heading == 4) then
     if (curve == 1) then !sigmoid affect functions
          write(filen,3005) "ind","k-Fsiz","a-Fsiz","r-Fsiz","void","void","void", &
                            "void","void", "void","void", &
                            "k-Flig","a-Flig","r-Flig", &
                              "k-Foth","a-Foth","r-Foth", &
                              "k-Fmrt","a-Fmrt","r-Fmrt", &
                            "kHstom","aHstom","rHstom", &
                            "kHfood","aHfood","rHfood", &
                            "k-FdL","a-FdL","r-FdL", &
                            "k-FdO","a-FdO","r-FdO", &
                            "k-HdF","a-HdF","r-HdF", &
                            "k-HdO","a-HdO","r-HdO", &
                            "f-mem","h-mem","sex","b-dep","r-dep","move","d-dep","d-age","d-aff","d-BM","status",    &
                            "NumAf","B-max","str-BM","bmass","gonad","M-kids","F-kids","m-pop","cre-m","cre-g","cre-a","cre-i", &
                            "m1","m2","m3","m4","m5","m6","m7","m8","m9","m10"
       else if (curve == 2) then ! linear affect functions
           write(filen,3005) "ind","P1Fsiz","P2Fsiz","P3Fsiz","P4Fsiz","P5Fsiz", &
                            "P6Fsiz","P7Fsiz","P8Fsiz","P9Fsiz","P10Fsz", &
                            "A-Flig","P0Flig","P1Flig", &
                              "A-Foth","P0Foth","P1Foth", &
                              "A-Fmrt","P0Fmrt","P1Fmrt", &
                            "AHstom","P0Hstm","P1Hstm", &
                            "AHfood","P0Hfod","P1Hfod", &
                            "A-FdL","P0FdL","P1FdL", &
                            "A-FdO","P0FdO","P1FdO", &
                            "A-HdF","P0HdF","P1HdF", &
                            "A-HdO","P0HdO","P1HdO", &
                            "f-mem","h-mem","sex","b-dep","r-dep","move","d-dep","d-age","d-aff","d-BM","status",    &
                            "NumAf","B-max","str-BM","bmass","gonad","M-kids","F-kids","m-pop","cre-m","cre-g","cre-a","cre-i", &
                            "m1","m2","m3","m4","m5","m6","m7","m8","m9","m10"
       else if (curve == 3) then ! gamma affect functions
           write(filen,3005) "ind","P1Fsiz","P2Fsiz","P3Fsiz","P4Fsiz","P5Fsiz", &
                            "P6Fsiz","P7Fsiz","P8Fsiz","P9Fsiz","P10Fsz", &
                            "g-Flig","P0Flig","void", &
                              "g-Foth","P0Foth","void", &
                              "g-Fmrt","P0Fmrt","void", &
                            "gHstom","P0Hstm","void", &
                            "gHfood","P0Hfod","void", &
                            "g-FdL","P0FdL","void", &
                            "g-FdO","P0FdO","void", &
                            "g-HdF","P0HdF","void", &
                            "g-HdO","P0HdO","void", &
                            "f-mem","h-mem","sex","b-dep","r-dep","move","d-dep","d-age","d-aff","d-BM","status",    &
                            "NumAf","B-max","str-BM","bmass","gonad","M-kids","F-kids","m-pop","cre-m","cre-g","cre-a","cre-i", &
                            "m1","m2","m3","m4","m5","m6","m7","m8","m9","m10"
       else if (curve == 4) then ! predefined gamma affect functions
           write(filen,3005) "ind","P1Fsiz","P2Fsiz","P3Fsiz","P4Fsiz","P5Fsiz", &
                            "P6Fsiz","P7Fsiz","P8Fsiz","P9Fsiz","P10Fsz", &
                            "g-Flig","k-Flig","void", &
                              "g-Foth","k-Foth","void", &
                              "g-Fmrt","k-Fmrt","void", &
                            "gHstom","k-Hstm","void", &
                            "gHfood","k-Hfod","void", &
                            "g-FdL","k-FdL","void", &
                            "g-FdO","k-FdO","void", &
                            "g-HdF","k-HdF","void", &
                            "g-HdO","k-HdO","void", &
                            "f-mem","h-mem","sex","b-dep","r-dep","move","d-dep","d-age","d-aff","d-BM","status",    &
                            "NumAf","B-max","str-BM","bmass","gonad","M-kids","F-kids","m-pop","cre-m","cre-g","cre-a","cre-i", &
                            "m1","m2","m3","m4","m5","m6","m7","m8","m9","m10"
   end if

  else if (heading == 5) then
      if (curve == 1) then !sigmoid affect functions
        write(filen,2402) "allele","k-Hsiz","a-Hsiz","r-Hsiz", &
                            "k-Flig","a-Flig","r-Flig","void","void",  &
                            "void","void","void","void","void", &
                              "k-Foth","a-Foth","r-Foth", &
                              "k-Fmrt","a-Fmrt","r-Fmrt", &
                            "kHstom","aHstom","rHstom", &
                            "kHfood","aHfood","rHfood", &
                            "k-FdL","a-FdL","r-FdL", &
                            "k-FdO","a-FdO","r-FdO", &
                            "k-HdF","a-HdF","r-HdF", &
                            "k-HdO","a-HdO","r-HdO", &
                            "f-mem","h-mem"
      else if (curve == 2) then ! linear affect functions
        write(filen,2402) "allele","P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "P6Hsiz","P7Hsiz","P8Hsiz","P9Hsiz","P10Hsz", &
                            "A-Flig","P0Flig","P1Flig", &
                              "A-Foth","P0Foth","P1Foth", &
                              "A-Fmrt","P0Fmrt","P1Fmrt", &
                            "AHstom","P0Hstm","P1Hstm", &
                            "AHfood","P0Hfod","P1Hfod", &
                            "A-FdL","P0FdL","P1FdL", &
                            "A-FdO","P0FdO","P1FdO", &
                            "A-HdF","P0HdF","P1HdF", &
                            "A-HdO","P0HdO","P1HdO", &
                            "f-mem","h-mem"
      else if (curve == 3) then ! gamma affect functions
        write(filen,2402) "allele","P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "P6Hsiz","P7Hsiz","P8Hsiz","P9Hsiz","P10Hsz", &
                            "g-Flig","P0Flig","void", &
                              "g-Foth","P0Foth","void", &
                              "g-Fmrt","P0Fmrt","void", &
                            "gHstom","P0Hstm","void", &
                            "gHfood","P0Hfod","void", &
                            "g-FdL","P0FdL","void", &
                            "g-FdO","P0FdO","void", &
                            "g-HdF","P0HdF","void", &
                            "g-HdO","P0HdO","void", &
                            "f-mem","h-mem"
      else if (curve == 4) then ! predefined gamma affect functions
        write(filen,2402) "allele","P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "P6Hsiz","P7Hsiz","P8Hsiz","P9Hsiz","P10Hsz", &
                            "g-Flig","k-Flig","void", &
                              "g-Foth","k-Foth","void", &
                              "g-Fmrt","k-Fmrt","void", &
                            "gHstom","k-Hstm","void", &
                            "gHfood","k-Hfod","void", &
                            "g-FdL","k-FdL","void", &
                            "g-FdO","k-FdO","void", &
                            "g-HdF","k-HdF","void", &
                            "g-HdO","k-HdO","void", &
                            "f-mem","h-mem"
    end if


  else if (heading == 6) then
    if (curve == 1) then !sigmoid affect functions
             write(filen,2415) "allele","inds","k-Hsiz","a-Hsiz","r-Hsiz", &
                            "k-Flig","a-Flig","r-Flig","void","void", &
                            "void","void","void","void","void",  &
                              "k-Foth","a-Foth","r-Foth", &
                              "k-Fmrt","a-Fmrt","r-Fmrt", &
                            "kHstom","aHstom","rHstom", &
                            "kHfood","aHfood","rHfood", &
                            "k-FdL","a-FdL","r-FdL", &
                            "k-FdO","a-FdO","r-FdO", &
                            "k-HdF","a-HdF","r-HdF", &
                            "k-HdO","a-HdO","r-HdO", &
                            "f-mem","h-mem"
       else if (curve == 2) then ! linear affect functions
            write(filen,2415) "allele","inds","P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "PHFsiz","P7Hsiz","P8Hsiz","P9Hsiz","10Hsiz", &
                            "A-Flig","P0Flig","P1Flig", &
                              "A-Foth","P0Foth","P1Foth", &
                              "A-Fmrt","P0Fmrt","P1Fmrt", &
                            "AHstom","P0Hstm","P1Hstm", &
                            "AHfood","P0Hfod","P1Hfod", &
                            "A-FdL","P0FdL","P1FdL", &
                            "A-FdO","P0FdO","P1FdO", &
                            "A-HdF","P0HdF","P1HdF", &
                            "A-HdO","P0HdO","P1HdO", &
                            "f-mem","h-mem"
       else if (curve == 3) then ! gamma affect functions
            write(filen,2415) "allele","inds","P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "PHFsiz","P7Hsiz","P8Hsiz","P9Hsiz","10Hsiz", &
                            "g-Flig","P0Flig","void", &
                              "g-Foth","P0Foth","void", &
                              "g-Fmrt","P0Fmrt","void", &
                            "gHstom","P0Hstm","void", &
                            "gHfood","P0Hfod","void", &
                            "g-FdL","P0FdL","void", &
                            "g-FdO","P0FdO","void", &
                            "g-HdF","P0HdF","void", &
                            "g-HdO","P0HdO","void", &
                            "f-mem","h-mem"
       else if (curve == 4) then ! predefined gamma affect functions
            write(filen,2415) "allele","inds","P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "PHFsiz","P7Hsiz","P8Hsiz","P9Hsiz","10Hsiz", &
                            "g-Flig","k-Flig","void", &
                              "g-Foth","k-Foth","void", &
                              "g-Fmrt","k-Fmrt","void", &
                            "gHstom","k-Hstm","void", &
                            "gHfood","k-Hfod","void", &
                            "g-FdL","k-FdL","void", &
                            "g-FdO","k-FdO","void", &
                            "g-HdF","k-HdF","void", &
                            "g-HdO","k-HdO","void", &
                            "f-mem","h-mem"
     end if

  else if (heading == 7) then

      write(filen,6007)"gene1","gene2","gene3","gene4","gene5","gene6","gene7","gene8",  &
                       "gene9","gene10","gene11","gene12","gene13","gene14","gene15",    &
                       "gene16","gene17","gene18","gene19","gene20","gene21","gene22",   &
                       "gene23","gene24","gene25","gene26","gene27","gene28","gene29",   &
                       "gene30","gene31","gene32","gene33","gene34","gene35","gene36",   &
                       "gene37","gene38","gene39"


  else if (heading == 8) then
        if (curve == 1) then !sigmoid affect functions
             write(filen,6007)"k-Hsiz","a-Hsiz","r-Hsiz","void","void",  &
                            "void","void","void","void","void", &
                            "k-Flig","a-Flig","r-Flig", &
                              "k-Foth","a-Foth","r-Foth", &
                              "k-Fmrt","a-Fmrt","r-Fmrt", &
                            "kHstom","aHstom","rHstom", &
                            "kHfood","aHfood","rHfood", &
                            "k-FdL","a-FdL","r-FdL", &
                            "k-FdO","a-FdO","r-FdO", &
                            "k-HdF","a-HdF","r-HdF", &
                            "k-HdO","a-HdO","r-HdO", &
                            "f-mem","h-mem"
          else if (curve == 2) then ! linear affect functions
           write(filen,6007)"P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "P6Hsiz","P7Hsiz","P8Hsiz","P9Hsiz","P10Hsz", &
                            "A-Flig","P0Flig","P1Flig", &
                              "A-Foth","P0Foth","P1Foth", &
                              "A-Fmrt","P0Fmrt","P1Fmrt", &
                            "AHstom","P0Hstm","P1Hstm", &
                            "AHfood","P0Hfod","P1Hfod", &
                            "A-FdL","P0FdL","P1FdL", &
                            "A-FdO","P0FdO","P1FdO", &
                            "A-HdF","P0HdF","P1HdF", &
                            "A-HdO","P0HdO","P1HdO", &
                            "f-mem","h-mem"
          else if (curve == 3) then ! gamma affect functions
           write(filen,6007)"P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "P6Hsiz","P7Hsiz","P8Hsiz","P9Hsiz","P10Hsz", &
                            "g-Flig","P0Flig","void", &
                              "g-Foth","P0Foth","void", &
                              "g-Fmrt","P0Fmrt","void", &
                            "gHstom","P0Hstm","void", &
                            "gHfood","P0Hfod","void", &
                            "g-FdL","P0FdL","void", &
                            "g-FdO","P0FdO","void", &
                            "g-HdF","P0HdF","void", &
                            "g-HdO","P0HdO","void", &
                            "f-mem","h-mem"
          else if (curve == 4) then ! predefined gamma affect functions
           write(filen,6007)"P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "P6Hsiz","P7Hsiz","P8Hsiz","P9Hsiz","P10Hsz", &
                            "g-Flig","k-Flig","void", &
                              "g-Foth","k-Foth","void", &
                              "g-Fmrt","k-Fmrt","void", &
                            "gHstom","k-Hstm","void", &
                            "gHfood","k-Hfod","void", &
                            "g-FdL","k-FdL","void", &
                            "g-FdO","k-FdO","void", &
                            "g-HdF","k-HdF","void", &
                            "g-HdO","k-HdO","void", &
                            "f-mem","h-mem"
      end if

  else if (heading == 9) then
      if (curve == 1) then !sigmoid
         write(filen,1802)  "ex","gen","avgF1","avgF2","avgF3","void","void","void","void","void","void","void",    &
         "gen","avgM1","avgM2","avgM3","void","void","void","void","void","void","void",    &
         "gen","av(11)","av(12)","av(13)","gen","av(14)","av(15)","av(16)","gen","av(17)","av(18)","av(19)",  &
         "gen","av(20)","av(21)","av(22)","gen","av(23)","av(24)","av(25)",  &
         "gen","av(26)","av(27)","av(28)","gen","av(29)","av(30)","av(31)","gen","av(32)","av(33)","av(34)",  &
         "gen","av(35)","av(36)","av(37)","gen","av(38)","av(39)"
      else if (curve == 2) then !linear
        write(filen,1802)  "ex","gen","avgF1","avgF2","avgF3","avgF4","avgF5","avgF6","avgF7","avgF8","avgF9","avF10",   &
         "gen","avgM1","avgM2","avgM3","avgM4","avgM5","avgM6","avgM7","avgM8","avgM9","avM10",   &
         "gen","av(11)","av(12)","av(13)","gen","av(14)","av(15)","av(16)","gen","av(17)","av(18)","av(19)",  &
         "gen","av(20)","av(21)","av(22)","gen","av(23)","av(24)","av(25)",  &
         "gen","av(26)","av(27)","av(28)","gen","av(29)","av(30)","av(31)","gen","av(32)","av(33)","av(34)",  &
         "gen","av(35)","av(36)","av(37)","gen","av(38)","av(39)"
      else if (curve == 3) then !gamma
        write(filen,1802)  "ex","gen","avgF1","avgF2","avgF3","avgF4","avgF5","avgF6","avgF7","avgF8","avgF9","avF10",   &
         "gen","avgM1","avgM2","avgM3","avgM4","avgM5","avgM6","avgM7","avgM8","avgM9","avM10",   &
         "gen","av(11)","av(12)","void","gen","av(14)","av(15)","void","gen","av(17)","av(18)","void",  &
         "gen","av(20)","av(21)","void","gen","av(23)","av(24)","void",  &
         "gen","av(26)","av(27)","void","gen","av(29)","av(30)","void","gen","av(32)","av(33)","void",  &
         "gen","av(35)","av(36)","void","gen","av(38)","av(39)"
     end if

  else if (heading == 10) then
     if (curve == 1) then !sigmoid
      write(filen,1802)  "ex","gen","sdF1","sdF2","sdF3","void","void","void","void","void","void","void",    &
         "gen","sdM1","sdM2","sdM3","void","void","void","void","void","void","void",    &
         "gen","sd(11)","sd(12)","sd(13)","gen","sd(14)","sd(15)","sd(16)","gen","sd(17)","sd(18)","sd(19)",  &
         "gen","sd(20)","sd(21)","sd(22)","gen","sd(23)","sd(24)","sd(25)",  &
         "gen","sd(26)","sd(27)","sd(28)","gen","sd(29)","sd(30)","sd(31)","gen","sd(32)","sd(33)","sd(34)",  &
         "gen","sd(35)","sd(36)","sd(37)","gen","sd(38)","sd(39)"
     else if (curve == 2) then !linear
      write(filen,1802)  "ex","gen","sdF1","sdF2","sdF3","sdF4","sdF5","sdF6","sdF7","sdF8","sdF9","sdF10",   &
         "gen","sdM1","sdM2","sdM3","sdM4","sdM5","sdM6","sdM7","sdM8","sdM9","sdM10",   &
         "gen","sd(11)","sd(12)","sd(13)","gen","sd(14)","sd(15)","sd(16)","gen","sd(17)","sd(18)","sd(19)",  &
         "gen","sd(20)","sd(21)","sd(22)","gen","sd(23)","sd(24)","sd(25)",  &
         "gen","sd(26)","sd(27)","sd(28)","gen","sd(29)","sd(30)","sd(31)","gen","sd(32)","sd(33)","sd(34)",  &
         "gen","sd(35)","sd(36)","sd(37)","gen","sd(38)","sd(39)"
      else if (curve == 3) then !gamma
      write(filen,1802)  "ex","gen","sdF1","sdF2","sdF3","sdF4","sdF5","sdF6","sdF7","sdF8","sdF9","sdF10",   &
         "gen","sdM1","sdM2","sdM3","sdM4","sdM5","sdM6","sdM7","sdM8","sdM9","sdM10",   &
         "gen","sd(11)","sd(12)","void","gen","sd(14)","sd(15)","void","gen","sd(17)","sd(18)","void",  &
         "gen","sd(20)","sd(21)","void","gen","sd(23)","sd(24)","void",  &
         "gen","sd(26)","sd(27)","void","gen","sd(29)","sd(30)","void","gen","sd(32)","sd(33)","void",  &
         "gen","sd(35)","sd(36)","void","gen","sd(38)","sd(39)"
     end if

  else if (heading == 11) then
      if (curve == 1) then !sigmoid affect functions
        write(filen,2402) "ind","k-Hsiz","a-Hsiz","r-Hsiz", &
                            "k-Flig","a-Flig","r-Flig","void","void",  &
                            "void","void","void","void","void", &
                              "k-Foth","a-Foth","r-Foth", &
                              "k-Fmrt","a-Fmrt","r-Fmrt", &
                            "kHstom","aHstom","rHstom", &
                            "kHfood","aHfood","rHfood", &
                            "k-FdL","a-FdL","r-FdL", &
                            "k-FdO","a-FdO","r-FdO", &
                            "k-HdF","a-HdF","r-HdF", &
                            "k-HdO","a-HdO","r-HdO", &
                            "f-mem","h-mem"
      else if (curve == 2) then ! linear affect functions
        write(filen,2402) "ind","P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "P6Hsiz","P7Hsiz","P8Hsiz","P9Hsiz","P10Hsz", &
                            "A-Flig","P0Flig","P1Flig", &
                              "A-Foth","P0Foth","P1Foth", &
                              "A-Fmrt","P0Fmrt","P1Fmrt", &
                            "AHstom","P0Hstm","P1Hstm", &
                            "AHfood","P0Hfod","P1Hfod", &
                            "A-FdL","P0FdL","P1FdL", &
                            "A-FdO","P0FdO","P1FdO", &
                            "A-HdF","P0HdF","P1HdF", &
                            "A-HdO","P0HdO","P1HdO", &
                            "f-mem","h-mem"
      else if (curve == 3) then ! gamma affect functions
        write(filen,2402) "ind","P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "P6Hsiz","P7Hsiz","P8Hsiz","P9Hsiz","P10Hsz", &
                            "g-Flig","P0Flig","void", &
                              "g-Foth","P0Foth","void", &
                              "g-Fmrt","P0Fmrt","void", &
                            "gHstom","P0Hstm","void", &
                            "gHfood","P0Hfod","void", &
                            "g-FdL","P0FdL","void", &
                            "g-FdO","P0FdO","void", &
                            "g-HdF","P0HdF","void", &
                            "g-HdO","P0HdO","void", &
                            "f-mem","h-mem"
      else if (curve == 4) then ! predefined gamma affect functions
        write(filen,2402) "allele","P1Hsiz","P2Hsiz","P3Hsiz","P4Hsiz","P5Hsiz", &
                            "P6Hsiz","P7Hsiz","P8Hsiz","P9Hsiz","P10Hsz", &
                            "g-Flig","k-Flig","void", &
                              "g-Foth","k-Foth","void", &
                              "g-Fmrt","k-Fmrt","void", &
                            "gHstom","k-Hstm","void", &
                            "gHfood","k-Hfod","void", &
                            "g-FdL","k-FdL","void", &
                            "g-FdO","k-FdO","void", &
                            "g-HdF","k-HdF","void", &
                            "g-HdO","k-HdO","void", &
                            "f-mem","h-mem"
     end if

  else if (heading == 12) then
     if (curve == 1) then !sigmoid affect functions
          write(filen,3006) "gen","ind","k-Fsiz","a-Fsiz","r-Fsiz","void","void","void", &
                            "void","void", "void","void", &
                            "k-Flig","a-Flig","r-Flig", &
                              "k-Foth","a-Foth","r-Foth", &
                              "k-Fmrt","a-Fmrt","r-Fmrt", &
                            "kHstom","aHstom","rHstom", &
                            "kHfood","aHfood","rHfood", &
                            "k-FdL","a-FdL","r-FdL", &
                            "k-FdO","a-FdO","r-FdO", &
                            "k-HdF","a-HdF","r-HdF", &
                            "k-HdO","a-HdO","r-HdO", &
                            "f-mem","h-mem","sex","b-dep","r-dep","move","d-dep","d-age","d-aff","d-BM","status",    &
                            "NumAf","B-max","str-BM","bmass","gonad","M-kids","F-kids","m-pop","cre-m","cre-g","cre-a","cre-i", &
                            "m1","m2","m3","m4","m5","m6","m7","m8","m9","m10"
       else if (curve == 2) then ! linear affect functions
           write(filen,3006) "gen","ind","P1Fsiz","P2Fsiz","P3Fsiz","P4Fsiz","P5Fsiz", &
                            "P6Fsiz","P7Fsiz","P8Fsiz","P9Fsiz","P10Fsz", &
                            "A-Flig","P0Flig","P1Flig", &
                              "A-Foth","P0Foth","P1Foth", &
                              "A-Fmrt","P0Fmrt","P1Fmrt", &
                            "AHstom","P0Hstm","P1Hstm", &
                            "AHfood","P0Hfod","P1Hfod", &
                            "A-FdL","P0FdL","P1FdL", &
                            "A-FdO","P0FdO","P1FdO", &
                            "A-HdF","P0HdF","P1HdF", &
                            "A-HdO","P0HdO","P1HdO", &
                            "f-mem","h-mem","sex","b-dep","r-dep","move","d-dep","d-age","d-aff","d-BM","status",    &
                            "NumAf","B-max","str-BM","bmass","gonad","M-kids","F-kids","m-pop","cre-m","cre-g","cre-a","cre-i", &
                            "m1","m2","m3","m4","m5","m6","m7","m8","m9","m10"
       else if (curve == 3) then ! gamma affect functions
           write(filen,3006) "gen","ind","P1Fsiz","P2Fsiz","P3Fsiz","P4Fsiz","P5Fsiz", &
                            "P6Fsiz","P7Fsiz","P8Fsiz","P9Fsiz","P10Fsz", &
                            "g-Flig","P0Flig","void", &
                              "g-Foth","P0Foth","void", &
                              "g-Fmrt","P0Fmrt","void", &
                            "gHstom","P0Hstm","void", &
                            "gHfood","P0Hfod","void", &
                            "g-FdL","P0FdL","void", &
                            "g-FdO","P0FdO","void", &
                            "g-HdF","P0HdF","void", &
                            "g-HdO","P0HdO","void", &
                            "f-mem","h-mem","sex","b-dep","r-dep","move","d-dep","d-age","d-aff","d-BM","status",    &
                            "NumAf","B-max","str-BM","bmass","gonad","M-kids","F-kids","m-pop","cre-m","cre-g","cre-a","cre-i", &
                            "m1","m2","m3","m4","m5","m6","m7","m8","m9","m10"
       else if (curve == 4) then ! predefined gamma affect functions
           write(filen,3006) "gen","ind","P1Fsiz","P2Fsiz","P3Fsiz","P4Fsiz","P5Fsiz", &
                            "P6Fsiz","P7Fsiz","P8Fsiz","P9Fsiz","P10Fsz", &
                            "g-Flig","k-Flig","void", &
                              "g-Foth","k-Foth","void", &
                              "g-Fmrt","k-Fmrt","void", &
                            "gHstom","k-Hstm","void", &
                            "gHfood","k-Hfod","void", &
                            "g-FdL","k-FdL","void", &
                            "g-FdO","k-FdO","void", &
                            "g-HdF","k-HdF","void", &
                            "g-HdO","k-HdO","void", &
                            "f-mem","h-mem","sex","b-dep","r-dep","move","d-dep","d-age","d-aff","d-BM","status",    &
                            "NumAf","B-max","str-BM","bmass","gonad","M-kids","F-kids","m-pop","cre-m","cre-g","cre-a","cre-i", &
                            "m1","m2","m3","m4","m5","m6","m7","m8","m9","m10"
    end if
end if   !heading

1802 format(63A9)
2402 format(40A8)
2415 format(41A8)
3005 format(72A8)
3006 format(73A8)
5005 format(49A8)
5103 format(66A8)
6007 format(49A12)

end
!end SRfileheadings

!-------------------------------------------------------------------------------
subroutine SRgainplot(runtag,attackfactor,beamatt,BMscale,contrast,     &
                densdep,density,depth,deviation,expmt,eyesat,flifespan,   &
                flivingcost,fmultgain,fmultrisk,fpop,realgen, &
                gen,generations,lightdecay,maxstomcap,preyarea,restattention, &
                srerr,viscap,screenplot,test99,during)
!-------------------------------------------------------------------------------
!make plots of fgain and frisk: (density-dependent but not individual-based) feeding and mortality risk
implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer    srerr,expmt,realgen,gen,fpop,generations,densdep,depth,flifespan
integer screenplot,test99,during
real attackfactor,beamatt,BMscale,contrast,density,eyesat,flivingcost
real fmultgain,fmultrisk,lightdecay,maxstomcap,preyarea,restattention,viscap
character(8) deviation
character(6) runtag

!local variables
real copmult4,risk4,gain4,eyesat4
integer a,i,ind4,dep,selection
integer age4,predatorattack4,attackdepth4,fpop4,gainfunction4,riskfunction4
integer test4,time,midnight,midday,notfoundday,plotfunction,trial
character(78) string1,string2
character(95) string3
character(7) text1
character(8) text2
character(16) text3,text4

if (test99 == 1) write(10,*) "SRgainplot"
test4 = 100 ! message to SRinitage and SRhabitat that the call comes from SRgainplot (cf test99)
!softM4 = 0
!softG4 = 0
predatorattack4 = 0
attackdepth4 = 0
copmult4 = 1.
fpop4 = fpop
deviation = "GainPlot"

notfoundday = 1


!Find surface lght midday and midnight
do a = 1, flifespan
    !midday comes before midnight
    if (notfoundday == 1 .and. surlig(a) < surlig(a-1)) then ! passed midday
        midday = a-1
        notfoundday = 0
    end if
    if (notfoundday == 0 .and. surlig(a) > surlig(a-1)) then ! passed midnight
        midnight = a-1
        goto 1111  !finished searching
    end if
end do
1111 continue




do plotfunction = 1,3
   gainfunction4 = plotfunction
   riskfunction4 = plotfunction
   do trial = 1,5

      eyesat4 = eyesat
      gain4 = fmultgain
      risk4 = fmultrisk
      !make plots around the chosen values of fmultgain and fmultrisk
      if (trial == 1) then
          eyesat4 = 5.
!          gain4 = 0.25*fmultgain
!          risk4 = 0.25*fmultrisk
        elseif (trial == 2) then
          eyesat4 = 50.
!          gain4 = 0.5*fmultgain
!          risk4 = 0.5*fmultrisk
        elseif (trial == 3) then
          eyesat4 = 500.
!          gain4 = fmultgain
!          risk4 = fmultrisk
        elseif (trial == 4) then
          eyesat4 = 5000.
!          gain4 = 2.*fmultgain
!          risk4 = 2.*fmultrisk
        else
          eyesat4 = 50000.
!          gain4 = 4.*fmultgain
!          risk4 = 4.*fmultrisk
      end if

    selection = 2
    if (selection == 1) then
      if (gain4 > 9999.99) then
          write(text3,1008)"fmultgain=",gain4
        else if (gain4 > 999.99) then
          write(text3,1007)"fmultgain=",gain4
        else if (gain4 > 99.99) then
          write(text3,1004)"fmultgain=",gain4
        else if (gain4 > 9.99) then
          write(text3,1005)"fmultgain=",gain4
        else
          write(text3,1006)"fmultgain=",gain4
      end if
      if (risk4 > 9999.99) then
          write(text4,1008)"fmultrisk=",risk4
        else if (risk4 > 999.99) then
          write(text4,1007)"fmultrisk=",risk4
        else if (risk4 > 99.99) then
          write(text4,1004)"fmultrisk=",risk4
        else if (risk4 > 9.99) then
          write(text4,1005)"fmultrisk=",risk4
        else
          write(text4,1006)"fmultrisk=",risk4
      end if
    else if (selection == 2) then
      if (eyesat4 > 9999.99) then
          write(text3,1008)"eyesatura=",int(eyesat4)
          write(text4,1008)"eyesatura=",int(eyesat4)
        else if (eyesat4 > 999.99) then
          write(text3,1007)"eyesatura=",eyesat4
          write(text4,1007)"eyesatura=",eyesat4
        else if (eyesat4 > 99.99) then
          write(text3,1004)"eyesatura=",eyesat4
          write(text4,1004)"eyesatura=",eyesat4
        else if (eyesat4 > 9.99) then
          write(text3,1005)"eyesatura=",eyesat4
          write(text4,1005)"eyesatura=",eyesat4
        else
          write(text3,1006)"eyesatura=",eyesat4
          write(text4,1006)"eyesatura=",eyesat4
      end if
    end if


1004 format(A10,F6.1)
1005 format(A10,F6.2)
1006 format(A10,F6.3)
1007 format(A10,F6.0)
1008 format(A10,I6)


      if (plotfunction == 1) then
             text1 = "-linear"
            else if (plotfunction == 2) then
             text1 = "-sqroot"
            else
             text1 = "-square"
      end if
      do time = 1,2
         if (time == 1) then
             age4 = midday
             text2 = "-midday-"
            else
             age4 = midnight
             text2 = "-mnight-"
         end if
        !open files for print
         if (expmt < 10) then
           write(string1,1000)"HED18-",runtag,"-E0",expmt,"-o043-gainplot",text1,text2,text3,".txt"
           write(string2,1000)"HED18-",runtag,"-E0",expmt,"-o044-riskplot",text1,text2,text4,".txt"
           write(string3,1002)"HED18-",runtag,"-E0",expmt,"-o045-gainrisk",text1,text2,text3,text4,".txt"
          else
           write(string1,1001)"HED18-",runtag,"-E",expmt,"-o043-gainplot",text1,text2,text3,".txt"
           write(string2,1001)"HED18-",runtag,"-E",expmt,"-o044-riskplot",text1,text2,text4,".txt"
           write(string3,1003)"HED18-",runtag,"-E",expmt,"-o045-gainrisk",text1,text2,text3,text4,".txt"
        end if
        open(43, file = string1)
        open(44, file = string2)
!        open(45, file = string3)
        1000 format(2A6,A3,I1,A14,A7,A8,A16,A4)
        1001 format(2A6,A2,I2,A14,A7,A8,A16,A4)
        1002 format(2A6,A3,I1,A14,A7,A8,2A16,A4)
        1003 format(2A6,A2,I2,A14,A7,A8,2A16,A4)

        do i = 1,107
           if (i == 1) then
               ind4 = i
              else if (i < 8) then
               ind4 = 2*ind4
              else if (i == 8) then
               ind4 = 100
              else
               ind4 = ind4 + 100
            end if

            !initiate environmental information
            call SRinitage(srerr,test4,gen,age4,depth,fpop,flifespan,risk4,gain4,   &
               predatorattack4,attackdepth4,attackfactor,beamatt,contrast,preyarea,viscap,  &
               eyesat4,lightdecay)
            call SRhabitat(srerr,test4,expmt,generations,densdep,density,    &
               gen,age4,depth,ind4,fpop4,flifespan,gainfunction4,riskfunction4,  &
               restattention,BMscale,flivingcost,maxstomcap,predatorattack4,attackdepth4,   &
               lightdecay,copmult4,risk4,risk4,deviation,screenplot,realgen,1,during)

            do dep = 1,depth
               write(43,1101) ind4,dep,fgain(dep)
               write(44,1101) ind4,dep,frisk(dep)
!               write(45,1101) ind4,dep,fgain(dep)/max(frisk(dep),1.e-12)
            end do  !dep
            1101 format(2I8,F20.10)

          end do !i, density
          close(43)
          close(44)
!          close(45)

       end do !time
    end do  ! trial (variation in risk and gain)
end do     ! plotfunction

end
!end SRgainplot


!-------------------------------------------------------------------------------
subroutine SRallelefrequency(runtag,srerr,test99,expmt,gen,generations,fnewpop,   &
                             realgen,allmin,allmax,interval,curve,gABM,stepABM,   &
                             minABM,maxABM)
!-------------------------------------------------------------------------------
!calculate allele frequencies of the newly formed eggs for next generation
implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
character(6) runtag
integer    srerr,test99,expmt,step,gen,realgen,fnewpop,generations,curve,gABM
real allmin,allmax,interval,stepABM,minABM,maxABM
!local variables
integer ind,i,c,g,a,b,steps,ABMsteps,num,filen,g2,error99,totalgenes
integer nr(0:201),numAHsize(10,0:201),numAFlight(3,0:201),numAFother(3,0:201)
integer numMemo(2,0:201)
integer numAFmort(3,0:201),numAHstom(3,0:201),numAHfood(3,0:201)
integer numHFlight(3,0:201),numHFother(3,0:201),numHHfood(3,0:201),numHHother(3,0:201)
integer totAFlight(0:201,0:201),totAFother(0:201,0:201)
integer totAFmort(0:201,0:201),totAHstom(0:201,0:201),totAHfood(0:201,0:201)
integer totHFlight(0:201,0:201),totHFother(0:201,0:201),totHHfood(0:201,0:201),totHHother(0:201,0:201)
real freqAHsize(10,0:201),freqAFlight(3,0:201),freqAFother(3,0:201),freqAFmort(3,0:201)
real freqAHstom(3,0:201),freqAHfood(3,0:201)
real freqHFlight(3,0:201),freqHFother(3,0:201),freqHHfood(3,0:201),freqHHother(3,0:201)
real freqMemo(2,0:201),genome(15000,39),avg(0:201,39)
real value,arange,frommax,frommin,rpop
character(52) string
character(6) genename
character(18) stringEX
character(55) stringST1,stringST2,stringST3

! number of alleles is (allmax-allmin)/interval,
!  e.g. (5--5)/0.05 = 200
!  e.g. (15--15)/0.05 = 600
!  e.g. (25--25)/0.01 = 5000

if (test99 == 1) write(10,*) "SRallelefrequency for generation ",gen
error99 = 0
if (srerr == 16) error99 = 1
!if (gen == 2) write(6,*) "SRallelefrequency"

    totalgenes = 39
    steps = 1 + int((allmax-allmin)/interval)
    ABMsteps = 1 + int((maxABM-minABM)/stepABM)

    !reset arrays
    do g = 1,gABM
     do a = 0,steps
      freqAHsize(g,a) = 0.
      numAHsize(g,a) = 0
     end do
    end do

    do g = 1,3
     do a = 0,steps
      freqAFlight(g,a) = 0.
      freqAFother(g,a) = 0.
      freqAFmort(g,a) = 0.
      freqAHstom(g,a) = 0.
      freqAHfood(g,a) = 0.
      freqHFlight(g,a) = 0.
      freqHFother(g,a) = 0.
      freqHHfood(g,a) = 0.
      freqHHother(g,a) = 0.
      numAFlight(g,a) = 0
      numAFother(g,a) = 0
      numAFmort(g,a) = 0
      numAHstom(g,a) = 0
      numAHfood(g,a) = 0
      numHFlight(g,a) = 0
      numHFother(g,a) = 0
      numHHfood(g,a) = 0
      numHHother(g,a) = 0
     end do
    end do

    do g = 1,2
     do a = 0,steps
        freqMemo(g,a) = 0.
        numMemo(g,a) = 0
     end do
    end do

    do a = 1,7    !reset statistics of number of alleles in affect and gABM genes
       Aalleles(a) = 0
    end do

    do a = 1,12    !reset statistics of hedonic genes
       Halleles(a) = 0
       Hdominant(a) = 0.
       Hdomfreq(a) = 0.
    end do

    !find number of copies of all possible alleles
     do i = 1, fnewpop

       arange = allmax-min(allmin,minABM)
       do g = 1,gABM
          frommax = allmax - geneAHsize(i,g)                 !hunger due to size
          frommin = arange - frommax
          step = int(steps*frommin/arange)
          numAHsize(g,step) = numAHsize(g,step) + 1
       end do !g

       arange = allmax-min(allmin,minABM)

       do g = 1,3
          frommax = allmax - geneAFlight(i,g)                 !fear due to light
          frommin = arange - frommax
          step = int(steps*frommin/arange)
          numAFlight(g,step) = numAFlight(g,step) + 1

          frommax = allmax - geneAFother(i,g)                 !fear due to others
          frommin = arange - frommax
          step = int(steps*frommin/arange)
          numAFother(g,step) = numAFother(g,step) + 1

          frommax = allmax - geneAFmort(i,g)                 !fear due to predators
          frommin = arange - frommax
          step = int(steps*frommin/arange)
          numAFmort(g,step) = numAFmort(g,step) + 1

          frommax = allmax - geneAHstom(i,g)                 !hunger due to stomach
          frommin = arange - frommax
          step = int(steps*frommin/arange)
          numAHstom(g,step) = numAHstom(g,step) + 1

          frommax = allmax - geneAHfood(i,g)                 !hunger due to food
          frommin = arange - frommax
          step = int(steps*frommin/arange)
          numAHfood(g,step) = numAHfood(g,step) + 1

          frommax = allmax - geneHFlight(i,g)                 !hedonic avoidance of light
          frommin = arange - frommax
          step = int(steps*frommin/arange)
          numHFlight(g,step) = numHFlight(g,step) + 1

          frommax = allmax - geneHFother(i,g)                 !hedonic pleasure in others
          frommin = arange - frommax
          step = int(steps*frommin/arange)
          numHFother(g,step) = numHFother(g,step) + 1

          frommax = allmax - geneHHfood(i,g)                 !hedonic attractioon to food
          frommin = arange - frommax
          step = int(steps*frommin/arange)
          numHHfood(g,step) = numHHfood(g,step) + 1

          frommax = allmax - geneHHother(i,g)                 !hedonic avoidance of others
          frommin = arange - frommax
          step = int(steps*frommin/arange)
          numHHother(g,step) = numHHother(g,step) + 1
       end do !g

       do g = 1,2                                             !memory genes
          frommax = allmax - genememo(i,g)
          frommin = arange - frommax
          step = int(steps*frommin/arange)
          nummemo(g,step) = nummemo(g,step) + 1
       end do

     end do !i

    !find frequencies of alleles

    do g = 1,gABM
       do a = 0,steps
         freqAHsize(g,a) = numAHsize(g,a)/(fnewpop*1.)
       end do
    end do

   do g = 1,3
       do a = 0,steps
         freqAFlight(g,a) = numAFlight(g,a)/(fnewpop*1.)
         freqAFother(g,a) = numAFother(g,a)/(fnewpop*1.)
         freqAFmort(g,a) = numAFmort(g,a)/(fnewpop*1.)
         freqAHstom(g,a) = numAHstom(g,a)/(fnewpop*1.)
         freqAHfood(g,a) = numAHfood(g,a)/(fnewpop*1.)
         freqHFlight(g,a) = numHFlight(g,a)/(fnewpop*1.)
         freqHFother(g,a) = numHFother(g,a)/(fnewpop*1.)
         freqHHfood(g,a) = numHHfood(g,a)/(fnewpop*1.)
         freqHHother(g,a) = numHHother(g,a)/(fnewpop*1.)
       end do
    end do
    do g = 1,2
       do a = 0,steps
         freqMemo(g,a) = numMemo(g,a)/(fnewpop*1.)
       end do
    end do


!   print frequency of alleles in the gABM genes

    do g = 1, gABM
       write(149+g,1300)realgen,(freqAHsize(g,a),a=1,ABMsteps)
    end do
!   print frequency of alleles in the affect genes
    write(160,1300)realgen,(freqAFlight(1,a),a=0,steps)
    write(161,1300)realgen,(freqAFlight(2,a),a=0,steps)
    write(162,1300)realgen,(freqAFother(1,a),a=0,steps)
    write(163,1300)realgen,(freqAFother(2,a),a=0,steps)
    write(164,1300)realgen,(freqAFmort(1,a),a=0,steps)
    write(165,1300)realgen,(freqAFmort(2,a),a=0,steps)
    write(166,1300)realgen,(freqAHstom(1,a),a=0,steps)
    write(167,1300)realgen,(freqAHstom(2,a),a=0,steps)
    write(168,1300)realgen,(freqAHfood(1,a),a=0,steps)
    write(169,1300)realgen,(freqAHFood(2,a),a=0,steps)
!   print frequency of alleles in the hedonic genes
    write(130,1300)realgen,(freqHFlight(1,a),a=0,steps)
    write(131,1300)realgen,(freqHFlight(2,a),a=0,steps)
    write(132,1300)realgen,(freqHFother(1,a),a=0,steps)
    write(133,1300)realgen,(freqHFother(2,a),a=0,steps)
    write(134,1300)realgen,(freqHHfood(1,a),a=0,steps)
    write(135,1300)realgen,(freqHHfood(2,a),a=0,steps)
    write(136,1300)realgen,(freqHHother(1,a),a=0,steps)
    write(137,1300)realgen,(freqHHother(2,a),a=0,steps)
    if (gen == generations) then
       do a = 0,7
          close(130+a)
       end do
       do a = 1,gABM
          close(149+a)
       end do
       do a = 0,9
          close(160+a)
       end do
    end if

1300 format(I6,102F8.4)


    !find number of alleles in use, and dominant allele of hedonic genes
    do g = 1,gABM
       do a = 0,steps
         if(numAHsize(g,a) > 0) Aalleles(1) = Aalleles(1) + 1
       end do
    end do
    do a = 0,steps
       if(numAFlight(1,a) > 0) Aalleles(2) = Aalleles(2) + 1
       if(numAFlight(2,a) > 0) Aalleles(3) = Aalleles(3) + 1
       if(numAFlight(3,a) > 0) Aalleles(4) = Aalleles(4) + 1
       if(numAFother(1,a) > 0) Aalleles(2) = Aalleles(2) + 1
       if(numAFother(2,a) > 0) Aalleles(3) = Aalleles(3) + 1
       if(numAFother(3,a) > 0) Aalleles(4) = Aalleles(4) + 1
       if(numAFmort(1,a) > 0) Aalleles(2) = Aalleles(2) + 1
       if(numAFmort(2,a) > 0) Aalleles(3) = Aalleles(3) + 1
       if(numAFmort(3,a) > 0) Aalleles(4) = Aalleles(4) + 1
       if(numAHstom(1,a) > 0) Aalleles(5) = Aalleles(5) + 1
       if(numAHstom(2,a) > 0) Aalleles(6) = Aalleles(6) + 1
       if(numAHstom(3,a) > 0) Aalleles(7) = Aalleles(7) + 1
       if(numAHfood(1,a) > 0) Aalleles(5) = Aalleles(5) + 1
       if(numAHfood(2,a) > 0) Aalleles(6) = Aalleles(6) + 1
       if(numAHfood(3,a) > 0) Aalleles(7) = Aalleles(7) + 1
      end do

    do g = 1,3
       do a = 0,steps
         if (numHFlight(g,a) > 0) then
            Halleles(g) = Halleles(g) + 1
            if (freqHFlight(g,a) > Hdomfreq(g)) then
                Hdomfreq(g) = freqHFlight(g,a)
                Hdominant(g) = allmin+a*interval
            end if
         end if
         if (numHFother(g,a) > 0) then
            Halleles(g+3) = Halleles(g+3) + 1
            if (freqHFother(g,a) > Hdomfreq(g+3)) then
                Hdomfreq(g+3) = freqHFother(g,a)
                Hdominant(g+3) = allmin+a*interval
            end if
         end if
         if (numHHfood(g,a) > 0) then
            Halleles(g+6) = Halleles(g+6) + 1
            if (freqHHfood(g,a) > Hdomfreq(g+6)) then
                Hdomfreq(g+6) = freqHHfood(g,a)
                Hdominant(g+6) = allmin+a*interval
            end if
         end if
         if (numHHother(g,a) > 0) then
            Halleles(g+9) = Halleles(g+9) + 1
            if (freqHHother(g,a) > Hdomfreq(g+9)) then
                Hdomfreq(g+9) = freqHHother(g,a)
                Hdominant(g+9) = allmin+a*interval
            end if
         end if
       end do
    end do

!print to files every 10 or 100 generations
if (generations < 2001) then  !print every 10 generations
     if (gen < 101 .or. gen/10 == gen/10. .or. gen > generations-10)  then
       write(24,2401) "expmt, next generation and pop size: ", expmt, realgen+1, fnewpop
       call SRfileheadings(24,curve,5,test99)
       do a = 0,steps
         value = allmin+a*interval
         write(24,2416) value,(freqAHsize(g,a),g = 1,10),(freqAFlight(g,a),g = 1,3), &
         (freqAFother(g,a),g = 1,3),(freqAFmort(g,a),g = 1,3),   &
            (freqAHstom(g,a),g = 1,3),     &
         (freqAHfood(g,a),g = 1,3),(freqHFlight(g,a),g = 1,3),   &
         (freqHFother(g,a),g = 1,3),(freqHHfood(g,a),g = 1,3),   &
         (freqHHother(g,a),g = 1,3),(freqMemo(g,a),g = 1,2)
       end do
     end if
    else                      !print every 100 generations
     if (gen < 101 .or. gen/100 == gen/100. .or. gen > generations-10)  then
       write(24,2401) "expmt, next generation and pop size: ", expmt, realgen+1, fnewpop
       call SRfileheadings(24,curve,5,test99)
       do a = 0,steps
         value = allmin+a*interval
         write(24,2416) value,(freqAHsize(g,a),g = 1,10),(freqAFlight(g,a),g = 1,3), &
         (freqAFother(g,a),g = 1,3),(freqAFmort(g,a),g = 1,3),   &
            (freqAHstom(g,a),g = 1,3),     &
         (freqAHfood(g,a),g = 1,3),(freqHFlight(g,a),g = 1,3),   &
         (freqHFother(g,a),g = 1,3),(freqHHfood(g,a),g = 1,3),   &
         (freqHHother(g,a),g = 1,3),(freqMemo(g,a),g = 1,2)
       end do
     end if
end if
2401 format(A56,3I8)
2416 format(F8.2,37F8.3,2F8.4)


!make printout of allele combinations of hedonic genes in final generation
if (gen == generations) then
    num = expmt
    filen = 55
    !load hedonic genome

    do ind = 1,fnewpop
       do g = 1,10
          genome(ind,g) = geneAHsize(ind,g)
       end do
       do g = 1,3
          genome(ind,g+10) = geneAFlight(ind,g)
          genome(ind,g+13) = geneAFother(ind,g)
          genome(ind,g+16) = geneAFmort(ind,g)
          genome(ind,g+19) = geneAHstom(ind,g)
          genome(ind,g+22) = geneAHfood(ind,g)
          genome(ind,g+25) = geneHFlight(ind,g)
          genome(ind,g+28) = geneHFother(ind,g)
          genome(ind,g+31) = geneHHfood(ind,g)
          genome(ind,g+34) = geneHHother(ind,g)
       end do
    end do

    if (num < 10) then
      write(string,2413)"HED18-",runtag,"-E0",num,"-o055-affect-allele-combinations.txt"
     else
      write(string,2417)"HED18-",runtag,"-E",num,"-o055-affect-allele-combinations.txt"
    end if

    open(filen, file = string)
    do g = 1,totalgenes
       if (curve == 1) then !sigmoid functions
          if (g == 1) then
                genename = "k-Hsiz"
             goto 2222
          end if
          if (g == 2) then
                genename = "a-Hsiz"
             goto 2222
          end if
          if (g == 3) then
                genename = "r-Hsiz"
             goto 2222
          end if
          if (g < 11) then
                genename = "void"
             goto 2222
          end if
          if (g == 11) then
                genename = "k-Flig"
             goto 2222
          end if
          if (g == 12) then
                genename = "a-Flig"
             goto 2222
          end if
          if (g == 13) then
                genename = "r-Flig"
             goto 2222
          end if
          if (g == 14) then
                genename = "k-Foth"
             goto 2222
          end if
          if (g == 15) then
                genename = "a-Foth"
             goto 2222
          end if
          if (g == 16) then
                genename = "r-Foth"
             goto 2222
          end if
          if (g == 17) then
                genename = "k-Fmrt"
             goto 2222
          end if
          if (g == 18) then
                genename = "a-Fmrt"
             goto 2222
          end if
          if (g == 19) then
                genename = "r-Fmrt"
             goto 2222
          end if
          if (g == 20) then
                genename = "k-Hstm"
             goto 2222
          end if
          if (g == 21) then
                genename = "a-Hstm"
             goto 2222
          end if
          if (g == 22) then
                genename = "r-Hstm"
             goto 2222
          end if
          if (g == 23) then
                genename = "k-Hfod"
             goto 2222
          end if
          if (g == 24) then
                genename = "a-Hfod"
             goto 2222
          end if
          if (g == 25) then
                genename = "r-Hfod"
             goto 2222
          end if
          if (g == 26) then
                genename = "k-FdL"
             goto 2222
          end if
          if (g == 27) then
                genename = "a-FdL"
             goto 2222
          end if
          if (g == 28) then
                genename = "r-FdL"
             goto 2222
          end if
          if (g == 29) then
                genename = "k-FdO"
             goto 2222
          end if
          if (g == 30) then
                genename = "a-FdO"
             goto 2222
          end if
          if (g == 31) then
                genename = "r-FdO"
             goto 2222
          end if
          if (g == 32) then
                genename = "k-HdF"
             goto 2222
          end if
          if (g == 33) then
                genename = "a-HdF"
             goto 2222
          end if
          if (g == 34) then
                genename = "r-HdF"
             goto 2222
          end if
          if (g == 35) then
                genename = "k-HdO"
             goto 2222
          end if
          if (g == 36) then
                genename = "a-HdO"
             goto 2222
          end if
          if (g == 37) then
                genename = "r-HdO"
             goto 2222
          end if

         else if (curve == 2) then !linear functions
          if (g == 1) then
                genename = "P1Hsiz"
             goto 2222
          end if
          if (g == 2) then
                genename = "P2Hsiz"
             goto 2222
          end if
          if (g == 3) then
                genename = "P3Hsiz"
             goto 2222
          end if
          if (g == 4) then
                genename = "P4Hsiz"
             goto 2222
          end if
          if (g == 5) then
                genename = "P5Hsiz"
             goto 2222
          end if
          if (g == 6) then
                genename = "P6Hsiz"
             goto 2222
          end if
          if (g == 7) then
                genename = "P7Hsiz"
             goto 2222
          end if
          if (g == 8) then
                genename = "P8Hsiz"
             goto 2222
          end if
          if (g == 9) then
                genename = "P9Hsiz"
             goto 2222
          end if
          if (g == 10) then
                genename = "P10Hsz"
             goto 2222
          end if
          if (g == 11) then
                genename = "A-Flig"
             goto 2222
          end if
          if (g == 12) then
                genename = "P0Flig"
             goto 2222
          end if
          if (g == 13) then
                genename = "P1Flig"
             goto 2222
          end if
          if (g == 14) then
                genename = "A-Foth"
             goto 2222
          end if
          if (g == 15) then
                genename = "P0Foth"
             goto 2222
          end if
          if (g == 16) then
                genename = "P1Foth"
             goto 2222
          end if
          if (g == 17) then
                genename = "A-Fmrt"
             goto 2222
          end if
          if (g == 18) then
                genename = "P0Fmrt"
             goto 2222
          end if
          if (g == 19) then
                genename = "P1Fmrt"
             goto 2222
          end if
          if (g == 20) then
                genename = "A-Hstm"
             goto 2222
          end if
          if (g == 21) then
                genename = "P0Hstm"
             goto 2222
          end if
          if (g == 22) then
                genename = "P1Hstm"
             goto 2222
          end if
          if (g == 23) then
                genename = "A-Hfod"
             goto 2222
          end if
          if (g == 24) then
                genename = "P0Hfod"
             goto 2222
          end if
          if (g == 25) then
                genename = "P1Hfod"
             goto 2222
          end if
          if (g == 26) then
                genename = "A-FdL"
             goto 2222
          end if
          if (g == 27) then
                genename = "P0FdL"
             goto 2222
          end if
          if (g == 28) then
                genename = "P1FdL"
             goto 2222
          end if
          if (g == 29) then
                genename = "A-FdO"
             goto 2222
          end if
          if (g == 30) then
                genename = "P0FdO"
             goto 2222
          end if
          if (g == 31) then
                genename = "P1FdO"
             goto 2222
          end if
          if (g == 32) then
                genename = "A-HdF"
             goto 2222
          end if
          if (g == 33) then
                genename = "P0HdF"
             goto 2222
          end if
          if (g == 34) then
                genename = "P1HdF"
             goto 2222
          end if
          if (g == 35) then
                genename = "A-HdO"
             goto 2222
          end if
          if (g == 36) then
                genename = "P0HdO"
             goto 2222
          end if
          if (g == 37) then
                genename = "P1HdO"
             goto 2222
          end if

         else if (curve == 3) then !gamma functions
          if (g == 1) then
                genename = "P1Hsiz"
             goto 2222
          end if
          if (g == 2) then
                genename = "P2Hsiz"
             goto 2222
          end if
          if (g == 3) then
                genename = "P3Hsiz"
             goto 2222
          end if
          if (g == 4) then
                genename = "P4Hsiz"
             goto 2222
          end if
          if (g == 5) then
                genename = "P5Hsiz"
             goto 2222
          end if
          if (g == 6) then
                genename = "P6Hsiz"
             goto 2222
          end if
          if (g == 7) then
                genename = "P7Hsiz"
             goto 2222
          end if
          if (g == 8) then
                genename = "P8Hsiz"
             goto 2222
          end if
          if (g == 9) then
                genename = "P9Hsiz"
             goto 2222
          end if
          if (g == 10) then
                genename = "P10Hsz"
             goto 2222
          end if
          if (g == 11) then
                genename = "g-Flig"
             goto 2222
          end if
          if (g == 12) then
                genename = "P0Flig"
             goto 2222
          end if
          if (g == 13) then
                genename = "void"
             goto 2222
          end if
          if (g == 14) then
                genename = "g-Foth"
             goto 2222
          end if
          if (g == 15) then
                genename = "P0Foth"
             goto 2222
          end if
          if (g == 16) then
                genename = "void"
             goto 2222
          end if
          if (g == 17) then
                genename = "g-Fmrt"
             goto 2222
          end if
          if (g == 18) then
                genename = "P0Fmrt"
             goto 2222
          end if
          if (g == 19) then
                genename = "void"
             goto 2222
          end if
          if (g == 20) then
                genename = "g-Hstm"
             goto 2222
          end if
          if (g == 21) then
                genename = "P0Hstm"
             goto 2222
          end if
          if (g == 22) then
                genename = "void"
             goto 2222
          end if
          if (g == 23) then
                genename = "g-Hfod"
             goto 2222
          end if
          if (g == 24) then
                genename = "P0Hfod"
             goto 2222
          end if
          if (g == 25) then
                genename = "void"
             goto 2222
          end if
          if (g == 26) then
                genename = "g-FdL"
             goto 2222
          end if
          if (g == 27) then
                genename = "P0FdL"
             goto 2222
          end if
          if (g == 28) then
                genename = "void"
             goto 2222
          end if
          if (g == 29) then
                genename = "g-FdO"
             goto 2222
          end if
          if (g == 30) then
                genename = "P0FdO"
             goto 2222
          end if
          if (g == 31) then
                genename = "void"
             goto 2222
          end if
          if (g == 32) then
                genename = "g-HdF"
             goto 2222
          end if
          if (g == 33) then
                genename = "P0HdF"
             goto 2222
          end if
          if (g == 34) then
                genename = "void"
             goto 2222
          end if
          if (g == 35) then
                genename = "g-HdO"
             goto 2222
          end if
          if (g == 36) then
                genename = "P0HdO"
             goto 2222
          end if
          if (g == 37) then
                genename = "void"
             goto 2222
          end if


         else if (curve == 4) then !predefined gamma functions
          if (g == 1) then
                genename = "P1Hsiz"
             goto 2222
          end if
          if (g == 2) then
                genename = "P2Hsiz"
             goto 2222
          end if
          if (g == 3) then
                genename = "P3Hsiz"
             goto 2222
          end if
          if (g == 4) then
                genename = "P4Hsiz"
             goto 2222
          end if
          if (g == 5) then
                genename = "P5Hsiz"
             goto 2222
          end if
          if (g == 6) then
                genename = "P6Hsiz"
             goto 2222
          end if
          if (g == 7) then
                genename = "P7Hsiz"
             goto 2222
          end if
          if (g == 8) then
                genename = "P8Hsiz"
             goto 2222
          end if
          if (g == 9) then
                genename = "P9Hsiz"
             goto 2222
          end if
          if (g == 10) then
                genename = "P10Hsz"
             goto 2222
          end if
          if (g == 11) then
                genename = "g-Flig"
             goto 2222
          end if
          if (g == 12) then
                genename = "k-Flig"
             goto 2222
          end if
          if (g == 13) then
                genename = "void"
             goto 2222
          end if
          if (g == 14) then
                genename = "g-Foth"
             goto 2222
          end if
          if (g == 15) then
                genename = "k-Foth"
             goto 2222
          end if
          if (g == 16) then
                genename = "void"
             goto 2222
          end if
          if (g == 17) then
                genename = "g-Fmrt"
             goto 2222
          end if
          if (g == 18) then
                genename = "k-Fmrt"
             goto 2222
          end if
          if (g == 19) then
                genename = "void"
             goto 2222
          end if
          if (g == 20) then
                genename = "g-Hstm"
             goto 2222
          end if
          if (g == 21) then
                genename = "k-Hstm"
             goto 2222
          end if
          if (g == 22) then
                genename = "void"
             goto 2222
          end if
          if (g == 23) then
                genename = "g-Hfod"
             goto 2222
          end if
          if (g == 24) then
                genename = "k-Hfod"
             goto 2222
          end if
          if (g == 25) then
                genename = "void"
             goto 2222
          end if
          if (g == 26) then
                genename = "g-FdL"
             goto 2222
          end if
          if (g == 27) then
                genename = "k-FdL"
             goto 2222
          end if
          if (g == 28) then
                genename = "void"
             goto 2222
          end if
          if (g == 29) then
                genename = "g-FdO"
             goto 2222
          end if
          if (g == 30) then
                genename = "k-FdO"
             goto 2222
          end if
          if (g == 31) then
                genename = "void"
             goto 2222
          end if
          if (g == 32) then
                genename = "g-HdF"
             goto 2222
          end if
          if (g == 33) then
                genename = "k-HdF"
             goto 2222
          end if
          if (g == 34) then
                genename = "void"
             goto 2222
          end if
          if (g == 35) then
                genename = "g-HdO"
             goto 2222
          end if
          if (g == 36) then
                genename = "k-HdO"
             goto 2222
          end if
          if (g == 37) then
                genename = "void"
             goto 2222
          end if


       end if
       2222 continue

       !no prints for genes not in use
       if (genename == "void") goto 3333 ! and next gene


!       reset array between each table to be made

       do a = 0,steps
          nr(a) = 0
          do g2 = 1,totalgenes
             avg(a,g2) = 0.
          end do
       end do
       !make one table for each gene in use
       write(filen,*) "  "
       write(filen,*) "  "
       write(filen,2414) "allele", "crosses", "for", "gene",g,genename

!      write(filen,2401) "expmt, next generation and pop size: ", expmt, gen+1, fnewpop
       call SRfileheadings(filen,curve,6,test99)


        !find and print average allele values of all genes, for each allele of present gene

       do i = 1, fnewpop
          do a = 0,steps
            !make a threshold midways between two allowed allele values
            value = allmin + a*interval + interval/2.
            !find average allele values
             if (genome(i,g) < value) then
               nr(a) = nr(a) + 1
               do g2 = 1,totalgenes
                  !treat alleles that code for "not interested" as zero values
                  !(count individuals, but do not add allele values)
                  if (abs(genome(i,g2)) .le. allmax) avg(a,g2) = avg(a,g2) + genome(i,g2)
               end do
               goto 124
            end if
          end do
          124 continue
       end do
       !.. and now do the printing
       do a = 0,steps
          if (nr(a) > 0) then  !skip all unused allele frequencies
                 do g2 = 1,totalgenes
                 avg(a,g2) = avg(a,g2)/(1.*nr(a))
              end do
              value = a*interval
              write(filen,2412)value,nr(a),(avg(a,g2),g2 = 1, totalgenes)
          end if
       end do
       3333 continue !if genename was "void"
!       close(filen)
    end do !g to totalgenes

    !make plots of pairwise affect and hedonic genes
    !Only the first third of the array is needed
    do c = 0,steps
       do b = 0, steps
         totAFlight(b,c) = 0
         totAFother(b,c) = 0
         totAFmort(b,c) = 0
            totAHstom(b,c) = 0
           totAHfood(b,c) = 0
         totHFlight(b,c) = 0
         totHFother(b,c) = 0
         totHHfood(b,c) = 0
         totHHother(b,c) = 0
       end do
    end do
    arange = allmax-allmin

    do i = 1,fnewpop
          frommax = allmax - geneAFlight(i,1)                 !fear due to light
          frommin = arange - frommax
          b = int(steps*frommin/arange)
          frommax = allmax - geneAFlight(i,2)
          frommin = arange - frommax
          c = int(steps*frommin/arange)
          totAFlight(b,c) = totAFlight(b,c) + 1

          frommax = allmax - geneAFother(i,1)                 !fear due to others
          frommin = arange - frommax
          b = int(steps*frommin/arange)
          frommax = allmax - geneAFother(i,2)
          frommin = arange - frommax
          c = int(steps*frommin/arange)
          totAFother(b,c) = totAFother(b,c) + 1

          frommax = allmax - geneAFmort(i,1)                 !fear due to predators
          frommin = arange - frommax
          b = int(steps*frommin/arange)
          frommax = allmax - geneAFmort(i,2)
          frommin = arange - frommax
          c = int(steps*frommin/arange)
          totAFmort(b,c) = totAFmort(b,c) + 1


          frommax = allmax - geneAHstom(i,1)                 !hunger due to stomach
          frommin = arange - frommax
          b = int(steps*frommin/arange)
          frommax = allmax - geneAHstom(i,2)
          frommin = arange - frommax
          c = int(steps*frommin/arange)
          totAHstom(b,c) = totAHstom(b,c) + 1

          frommax = allmax - geneAHfood(i,1)                 !hunger due to food
          frommin = arange - frommax
          b = int(steps*frommin/arange)
          frommax = allmax - geneAHfood(i,2)
          frommin = arange - frommax
          c = int(steps*frommin/arange)
          totAHfood(b,c) = totAHfood(b,c) + 1

          frommax = allmax - geneHFlight(i,1)                 !avoidance of light
          frommin = arange - frommax
          b = int(steps*frommin/arange)
          frommax = allmax - geneHFlight(i,2)
          frommin = arange - frommax
          c = int(steps*frommin/arange)
          totHFlight(b,c) = totHFlight(b,c) + 1

          frommax = allmax - geneHFother(i,1)                 !attraction to others
          frommin = arange - frommax
          b = int(steps*frommin/arange)
          frommax = allmax - geneHFother(i,2)
          frommin = arange - frommax
          c = int(steps*frommin/arange)
          totHFother(b,c) = totHFother(b,c) + 1

          frommax = allmax - geneHHfood(i,1)                  !attraction to food
          frommin = arange - frommax
          b = int(steps*frommin/arange)
          frommax = allmax - geneHHfood(i,2)
          frommin = arange - frommax
          c = int(steps*frommin/arange)
          totHHfood(b,c) = totHHfood(b,c) + 1

          frommax = allmax - geneHHother(i,1)                 !avoidance of competitors
          frommin = arange - frommax
          b = int(steps*frommin/arange)
          frommax = allmax - geneHHother(i,2)
          frommin = arange - frommax
          c = int(steps*frommin/arange)
          totHHother(b,c) = totHHother(b,c) + 1
    end do

!5500 format(41A12)
5520 format(2A6,A3,I1,A2)
5521 format(2A6,A2,I2,A2)
5522 format(A18,A25)
5523 format(A18,A32)
5524 format(A18,A33)
1801 format(F9.2,F9.5,102I6)
1802 format(2A9,102F6.2)
1803 format(2F8.3,F10.5)

    if (expmt < 10) then
       write(stringEX,5520)"HED18-",runtag,"-E0",expmt,"-o"
     else
       write(stringEX,5521)"HED18-",runtag,"-E",expmt,"-o"
    end if
    write(stringST1,5523)stringEX,"107-3D-paired-alleles-Affect.txt"
    write(stringST3,5524)stringEX,"107-3D-paired-alleles-Hedonic.txt"
    write(stringST2,5522)stringEX,"108-TABpaired-alleles.txt"
    open(106, file = stringST1)
    open(107, file = stringST3)
    open(108, file = stringST2)

    rpop = 1./(1.*fnewpop)
    write(106,*)"alleles of P0 or k in Fear of light, as function of g-allele"
    write(108,*)"P0AFL or k-AFL"
    write(108,1802) "g-allele","g-freq",(interval*b, b = 1,steps)
    do c = 0,steps                 !fear du to light
      if (freqAFlight(1,c) > 0.) write(108,1801) allmin+interval*c,freqAFlight(1,c),(totAFlight(c,b), b=1,steps)
      do b = 0,steps
         write(106,1803)allmin+c*interval,allmin+b*interval,totAFlight(c,b)*rpop
      end do
    end do

    write(106,*)" "
    write(106,*)"alleles of P0 or k in Attraction to others, as function of g-allele"
    write(108,*)" "
    write(108,*)"P0AFO or k-AFO"
    write(108,1802) "g-allele","g-freq",(interval*b, b = 1,steps)
    do c = 0,steps                 !fear du to others
      if (freqAFother(1,c) > 0.) write(108,1801) allmin+interval*c,freqAFother(1,c),(totAFother(c,b), b=1,steps)
      do b = 0,steps
         write(106,1803)allmin+c*interval,allmin+b*interval,totAFother(c,b)*rpop
      end do
    end do

    write(106,*)" "
    write(106,*)"alleles of P0 or k in Fear of predators, as function of g-allele"
    write(108,*)" "
    write(108,*)"P0AFP or k-AFP"
    write(108,1802) "g-allele","g-freq",(interval*b, b = 1,steps)
    do c = 0,steps                  !fear due to predators
      if (freqAFmort(1,c) > 0.) write(108,1801) allmin+interval*c,freqAFmort(1,c),(totAFmort(c,b), b=1,steps)
      do b = 0,steps
         write(106,1803)allmin+c*interval,allmin+b*interval,totAFmort(c,b)*rpop
      end do
    end do

    write(106,*)" "
    write(106,*)"alleles of P0 or k in Hunger due to stomach, as function of g-allele"
    write(108,*)" "
    write(108,*)"P0AHS or k-AHS"
    write(108,1802) "g-allele","g-freq",(interval*b, b = 1,steps)
    do c = 0,steps                 !hunger due to stomach
      if (freqAHstom(1,c) > 0.) write(108,1801) allmin+interval*c,freqAHstom(1,c),(totAHstom(c,b), b=1,steps)
      do b = 0,steps
         write(106,1803)allmin+c*interval,allmin+b*interval,totAHstom(c,b)*rpop
      end do
    end do

    write(106,*)" "
    write(106,*)"alleles of P0 or k in Hunger from food, as function of g-allele"
    write(108,*)" "
    write(108,*)"P0AHF or k-AHF"
    write(108,1802) "g-allele","g-freq",(interval*b, b = 1,steps)
    do c = 0,steps                 !hunger due to food
      if (freqAHfood(1,c) > 0.) write(108,1801) allmin+interval*c,freqAHfood(1,c),(totAHfood(c,b), b=1,steps)
      do b = 0,steps
         write(106,1803)allmin+c*interval,allmin+b*interval,totAHfood(c,b)*rpop
      end do
    end do

    write(107,*)"alleles of P0 or k in Hedonic avoidance of light, as function of g-allele"
    write(108,*)" "
    write(108,*)"P0HFL or k-HFL"
    write(108,1802) "g-allele","g-freq",(interval*b, b = 1,steps)
    do c = 0,steps                 !hedonic fear of light
      if (freqHFlight(1,c) > 0.) write(108,1801) allmin+interval*c,freqHFlight(1,c),(totHFlight(c,b), b=1,steps)
      do b = 0,steps
         write(107,1803)allmin+c*interval,allmin+b*interval,totHFlight(c,b)*rpop
      end do
    end do

    write(107,*)" "
    write(107,*)"alleles of P0 or k in Hedonic attraction to others, as function of g-allele"
    write(108,*)" "
    write(108,*)"P0HFO or k-HFO"
    write(108,1802) "g-allele","g-freq",(interval*b, b = 1,steps)
    do c = 0,steps                !hedonic pleasure in others
      if (freqHFother(1,c) > 0.) write(108,1801) allmin+interval*c,freqHFother(1,c),(totHFother(c,b), b=1,steps)
      do b = 0,steps
         write(107,1803)allmin+c*interval,allmin+b*interval,totHFother(c,b)*rpop
      end do
    end do

    write(107,*)" "
    write(107,*)"alleles of P0 or k in Hedonic attraction to food, as function of g-allele"
    write(108,*)" "
    write(108,*)"P0HHF or k-HHF"
    write(108,1802) "g-allele","g-freq",(interval*b, b = 1,steps)
    do c = 0,steps                 !hedonic pleasure in food
      if (freqHHfood(1,c) > 0.) write(108,1801) allmin+interval*c,freqHHfood(1,c),(totHHfood(c,b), b=1,steps)
      do b = 0,steps
         write(107,1803)allmin+c*interval,allmin+b*interval,totHHfood(c,b)*rpop
      end do
    end do

    write(107,*)" "
    write(107,*)"alleles of P0 or k in Hedonic avoidance of others, as function of g-allele"
    write(108,*)" "
    write(108,*)"P0HHO or k-HHO"
    write(108,1802) "g-allele","g-freq",(interval*b, b = 1,steps)
    do c = 0,steps                 !hedonic hunger avoidance of competitors
      if (freqHHother(1,c) > 0.) write(108,1801) allmin+interval*c,freqHHother(1,c),(totHHother(c,b), b=1,steps)
      do b = 0,steps
         write(107,1803)allmin+c*interval,allmin+b*interval,totHHother(c,b)*rpop
      end do
    end do

    close(106)
    close(107)
    close(108)

    close(filen)

end if !gen == generations

2412 format(F8.2,I8,39F8.2)
2413 format(2A6,A3,I1,A36)
2417 format(2A6,A2,I2,A36)
2414 format(4A8,I8,A8)

end
!end subroutine SRallelefrequency


!-----------------------------------------------------------------------
subroutine SRcorrplotsHED(file,fpop,fmaxweight,realgen,dev,flifespan,allmin,test99)
!----------------------------------------------------------------------
!write plots of correlation between genes and traits in the last few generations
!genes: the 8 gamma function genes in the hedonic functions
!traits: birth depth, final body mass, gonad size, fatherly offspring, survival

implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer    file,fpop,fmaxweight,realgen,flifespan,test99
real allmin
character(8) dev
!local variables
integer filen,i,step,class1,c,gene,ind(0:201)
real inds,sur,bm(0:201),bd(0:201),gon(0:201),fkids(0:201),Nafraid(0:201),surv(0:201),lspan(0:201)
real gFL(0:201),PFL(0:201),gFO(0:201),PFO(0:201),gHF(0:201),PHF(0:201),gHO(0:201),PHO(0:201)

if (test99 == 1) write(10,*) "SRcorrplotsHED"

!body mass
filen = file
step = fmaxweight/100
!reset arrays
do class1 = 0,201
       ind(class1) = 0
       bd(class1) = 0.
       gon(class1) = 0.
       fkids(class1) = 0.
       Nafraid(class1) = 0.
       gFL(class1) = 0.
       PFL(class1) = 0.
       gFO(class1) = 0.
       PFO(class1) = 0.
       gHF(class1) = 0.
       PHF(class1) = 0.
       gHO(class1) = 0.
       PHO(class1) = 0.
end do
!get info of individuals
do i = 1,fpop
    if(fstatus(i) == 1) then !ind survived and final body mass exists
       class1 = 1 + fbodymass(i)/step
       ind(class1) = ind(class1) + 1
       bd(class1) = bd(class1) + fbirthdepth(i)
       gon(class1) = gon(class1) + fgonad(i)
       fkids(class1) = fkids(class1) + father(i)
       Nafraid(class1) = Nafraid(class1) + fnumberafraid(i)
       gFL(class1) = gFL(class1) + geneHFlight(i,1)
       PFL(class1) = PFL(class1) + geneHFlight(i,2)
       gFO(class1) = gFO(class1) + geneHFother(i,1)
       PFO(class1) = PFO(class1) + geneHFother(i,2)
       gHF(class1) = gHF(class1) + geneHHfood(i,1)
       PHF(class1) = PHF(class1) + geneHHfood(i,2)
       gHO(class1) = gHO(class1) + geneHHother(i,1)
       PHO(class1) = PHO(class1) + geneHHother(i,2)
    end if
end do

!find average values
do class1 = 0,201
   if (ind(class1) == 0) then
       bd(class1) = 0.
       gon(class1) = 0.
       fkids(class1) = 0.
       Nafraid(class1) = 0.
       gFL(class1) = 0.
       PFL(class1) = 0.
       gFO(class1) = 0.
       PFO(class1) = 0.
       gHF(class1) = 0.
       PHF(class1) = 0.
       gHO(class1) = 0.
       PHO(class1) = 0.
     else
       inds = 1./1.*ind(class1)
       bm(class1) = bm(class1)*inds
       bd(class1) = bd(class1)*inds
       gon(class1) = gon(class1)*inds
       fkids(class1) = fkids(class1)*inds
       Nafraid(class1) = Nafraid(class1)*inds
       gFL(class1) = gFL(class1)*inds
       PFL(class1) = PFL(class1)*inds
       gFO(class1) = gFO(class1)*inds
       PFO(class1) = PFO(class1)*inds
       gHF(class1) = gHF(class1)*inds
       PHF(class1) = PHF(class1)*inds
       gHO(class1) = gHO(class1)*inds
       PHO(class1) = PHO(class1)*inds

       c = class1
       write(filen,6101)realgen,dev,c*step,ind(c),bd(c),gon(c),fkids(c),Nafraid(c),  &
                        gFL(c),PFL(c),gFO(c),PFO(c),gHF(c),PHF(c),gHO(c),PHO(c)

    end if
end do
6101 format(I12,A12,2I12,12F12.3)


!birth depth
filen = filen+1

!reset arrays
do class1 = 1,30
       ind(class1) = 0
       bm(class1) = 0.
       bd(class1) = 0.
       gon(class1) = 0.
       fkids(class1) = 0.
       Nafraid(class1) = 0.
       surv(class1) = 0.
       lspan(class1) = 0.
       gFL(class1) = 0.
       PFL(class1) = 0.
       gFO(class1) = 0.
       PFO(class1) = 0.
       gHF(class1) = 0.
       PHF(class1) = 0.
       gHO(class1) = 0.
       PHO(class1) = 0.
end do
!get info of individuals
do i = 1,fpop
    class1 = fbirthdepth(i)
!    lspan(class1) = lspan(class1) + fdeathage(i)
    lspan(class1) = lspan(class1) + min(flifespan,fdeathage(i))
    ind(class1) = ind(class1) + 1
    bd(class1) = bd(class1) + fbirthdepth(i)
    Nafraid(class1) = Nafraid(class1) + fnumberafraid(i)
    gFL(class1) = gFL(class1) + geneHFlight(i,1)
    PFL(class1) = PFL(class1) + geneHFlight(i,2)
    gFO(class1) = gFO(class1) + geneHFother(i,1)
    PFO(class1) = PFO(class1) + geneHFother(i,2)
    gHF(class1) = gHF(class1) + geneHHfood(i,1)
    PHF(class1) = PHF(class1) + geneHHfood(i,2)
    gHO(class1) = gHO(class1) + geneHHother(i,1)
    PHO(class1) = PHO(class1) + geneHHother(i,2)
    if(fstatus(i) == 1) then !ind survived and final body mass exists
       bm(class1) = bm(class1) + fbodymass(i)
       gon(class1) = gon(class1) + fgonad(i)
       fkids(class1) = fkids(class1) + father(i)
       surv(class1) = surv(class1) + 1.
    end if
end do

!find average values
do class1 = 1,30
   if (ind(class1) == 0) then
       bm(class1) = 0.
       bd(class1) = 0.
       gon(class1) = 0.
       fkids(class1) = 0.
       Nafraid(class1) = 0.
       surv(class1) = 0.
       lspan(class1) = 0.
       gFL(class1) = 0.
       PFL(class1) = 0.
       gFO(class1) = 0.
       PFO(class1) = 0.
       gHF(class1) = 0.
       PHF(class1) = 0.
       gHO(class1) = 0.
       PHO(class1) = 0.
     else
       c = class1
       inds = 1./max(1.,1.*ind(c))
       sur = 1./max(1.,1.*surv(c))
       bm(c) = bm(c)*sur
       bd(c) = bd(c)*inds
       gon(c) = gon(c)*sur
       fkids(c) = fkids(c)*sur
       Nafraid(c) = Nafraid(c)*inds
       surv(c) = surv(c)*inds
       lspan(c) = lspan(c)*inds
       gFL(c) = gFL(c)*inds
       PFL(c) = PFL(c)*inds
       gFO(c) = gFO(c)*inds
       PFO(c) = PFO(c)*inds
       gHF(c) = gHF(c)*inds
       PHF(c) = PHF(c)*inds
       gHO(c) = gHO(c)*inds
       PHO(c) = PHO(c)*inds
       write(filen,6102)realgen,dev,c*1.0,ind(c),bm(c),bd(c),gon(c),fkids(c),Nafraid(c), &
                        surv(c),lspan(c),gFL(c),PFL(c),gFO(c),PFO(c),gHF(c),PHF(c),  &
                        gHO(c),PHO(c)
    end if
end do
6102 format(I12,A12,F12.3,I12,15F12.3)


!cycle over all 8 hedonic genes
do gene = 1,8
  filen = filen+1

  !reset arrays
  do class1 = 0,201
       ind(class1) = 0
       bm(class1) = 0.
       bd(class1) = 0.
       gon(class1) = 0.
       fkids(class1) = 0.
       Nafraid(class1) = 0.
       surv(class1) = 0.
       lspan(class1) = 0.
       gFL(class1) = 0.
       PFL(class1) = 0.
       gFO(class1) = 0.
       PFO(class1) = 0.
       gHF(class1) = 0.
       PHF(class1) = 0.
       gHO(class1) = 0.
       PHO(class1) = 0.
  end do


  !get info of individuals
  do i = 1,fpop

   !determine selection criterion for each gene
    if (gene == 1) class1 = nint(geneHFlight(i,1)*100.) !gamma,hedonic fear for light
    if (gene == 2) class1 = nint(geneHFlight(i,2)*100.) !P0 -  hedonic fear for light
    if (gene == 3) class1 = nint(geneHFother(i,1)*100.) !gamma,hedonic attraction to others
    if (gene == 4) class1 = nint(geneHFother(i,2)*100.) !P0 -  hedonic attraction to others
    if (gene == 5) class1 = nint(geneHHfood(i,1)*100.)  !gamma,hedonic attraction to food
    if (gene == 6) class1 = nint(geneHHfood(i,2)*100.)  !P0 -  hedonic attraction to food
    if (gene == 7) class1 = nint(geneHHother(i,1)*100.) !gamma,hedonic avoidance of others
    if (gene == 8) class1 = nint(geneHHother(i,2)*100.) !P0 -  hedonic avoidance of others

    !add effect of placement of allmin (e.g. at -1 or 0)
    if (allmin < 0.) class1 = class1 - nint(100*allmin)  !array made to accept 201 entries, e.g. allmin = -1

    if (class1 > 201) then
       write(10,*) "class1 =",class1, "for ind ",i
       write(10,*) "geneHFlight(i,1) = ",geneHFlight(i,1)
       write(10,*) "geneHFlight(i,2) = ",geneHFlight(i,2)
       write(10,*) "geneHFother(i,1) = ",geneHFother(i,1)
       write(10,*) "geneHFother(i,2) = ",geneHFother(i,2)
       write(10,*) "geneHHfood(i,1) = ",geneHHfood(i,1)
       write(10,*) "geneHHfood(i,2) = ",geneHHfood(i,2)
       write(10,*) "geneHHother(i,1) = ",geneHHother(i,1)
       write(10,*) "geneHHother(i,2) = ",geneHHother(i,2)
       class1 = 201
      else if (class1 < 0) then
       write(10,*) "class1 =",class1, "for ind ",i
       write(10,*) "geneHFlight(i,1) = ",geneHFlight(i,1)
       write(10,*) "geneHFlight(i,2) = ",geneHFlight(i,2)
       write(10,*) "geneHFother(i,1) = ",geneHFother(i,1)
       write(10,*) "geneHFother(i,2) = ",geneHFother(i,2)
       write(10,*) "geneHHfood(i,1) = ",geneHHfood(i,1)
       write(10,*) "geneHHfood(i,2) = ",geneHHfood(i,2)
       write(10,*) "geneHHother(i,1) = ",geneHHother(i,1)
       write(10,*) "geneHHother(i,2) = ",geneHHother(i,2)
       class1 = 0
    end if

    lspan(class1) = lspan(class1) + min(flifespan,fdeathage(i))
    bd(class1) = bd(class1) + fbirthdepth(i)
    ind(class1) = ind(class1) + 1
    Nafraid(class1) = Nafraid(class1) + fnumberafraid(i)
    gFL(class1) = gFL(class1) + geneHFlight(i,1)
    PFL(class1) = PFL(class1) + geneHFlight(i,2)
    gFO(class1) = gFO(class1) + geneHFother(i,1)
    PFO(class1) = PFO(class1) + geneHFother(i,2)
    gHF(class1) = gHF(class1) + geneHHfood(i,1)
    PHF(class1) = PHF(class1) + geneHHfood(i,2)
    gHO(class1) = gHO(class1) + geneHHother(i,1)
    PHO(class1) = PHO(class1) + geneHHother(i,2)
    if(fstatus(i) == 1) then !ind survived and final body mass exists
       bm(class1) = bm(class1) + fbodymass(i)
       gon(class1) = gon(class1) + fgonad(i)
       fkids(class1) = fkids(class1) + father(i)
       surv(class1) = surv(class1) + 1.
    end if
  end do

  !find average values
  do class1 = 0,201
    if (ind(class1) == 0) then
       bm(class1) = 0.
       bd(class1) = 0.
       gon(class1) = 0.
       fkids(class1) = 0.
       Nafraid(class1) = 0.
       surv(class1) = 0.
       lspan(class1) = 0.
       gFL(class1) = 0.
       PFL(class1) = 0.
       gFO(class1) = 0.
       PFO(class1) = 0.
       gHF(class1) = 0.
       PHF(class1) = 0.
       gHO(class1) = 0.
       PHO(class1) = 0.
     else
       c = class1
       inds = 1./max(1.,1.*ind(c))
       sur = 1./max(1.,1.*surv(c))
       bm(c) = bm(c)*sur
       gon(c) = gon(c)*sur
       fkids(c) = fkids(c)*sur
       surv(c) = surv(c)*inds
       bd(c) = bd(c)*inds
       Nafraid(c) = Nafraid(c)*inds
       lspan(c) = lspan(c)*inds
       gFL(c) = gFL(c)*inds
       PFL(c) = PFL(c)*inds
       gFO(c) = gFO(c)*inds
       PFO(c) = PFO(c)*inds
       gHF(c) = gHF(c)*inds
       PHF(c) = PHF(c)*inds
       gHO(c) = gHO(c)*inds
       PHO(c) = PHO(c)*inds
       write(filen,6102)realgen,dev,c*0.01,ind(c),bm(c),bd(c),gon(c),fkids(c),Nafraid(c),  &
                        surv(c),lspan(c),gFL(c),PFL(c),gFO(c),PFO(c),gHF(c),PHF(c),  &
                        gHO(c),PHO(c)
    end if !individuals in class1
  end do !class1es within gene
end do !cycle over all 8 hedonic genes


end
!end subroutine SRcorrplotsHED

!-----------------------------------------------------------------------
subroutine SRcorrplotsAFF(file,fpop,realgen,dev,flifespan,allmin,test99)
!----------------------------------------------------------------------
!write plots of correlation between genes and traits in the last few generations
!genes: the 10 gamma function genes in the 10 affect functions
!traits: birth depth, final body mass, gonad size, fatherly offspring, survival

implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer    file,fpop,realgen,flifespan,test99
real allmin
character(8) dev
!local variables
integer filen,i,class1,c,gene,ind(0:201)
real inds,sur,bm(0:201),bd(0:201),gon(0:201),fkids(0:201),Nafraid(0:201),surv(0:201),lspan(0:201)
real gFL(0:201),PFL(0:201),gFO(0:201),PFO(0:201),gFP(0:201),PFP(0:201)
real gHF(0:201),PHF(0:201),gHS(0:201),PHS(0:201)

if (test99 == 1) write(10,*) "SRcorrplotsAFF"
filen = file-1 !allow file number to incease within the g-loop

!cycle over all 10 affect genes
do gene = 1,10
  filen = filen+1

  !reset arrays
  do class1 = 0,201
       ind(class1) = 0
       bm(class1) = 0.
       bd(class1) = 0.
       gon(class1) = 0.
       fkids(class1) = 0.
       Nafraid(class1) = 0.
       surv(class1) = 0.
       lspan(class1) = 0.
       gFL(class1) = 0.
       PFL(class1) = 0.
       gFO(class1) = 0.
       PFO(class1) = 0.
       gFP(class1) = 0.
       PFP(class1) = 0.
       gHS(class1) = 0.
       PHS(class1) = 0.
       gHF(class1) = 0.
       PHF(class1) = 0.
  end do


  !get info of individuals
  do i = 1,fpop

   !determine selection criterion for each gene
    if (gene == 1) class1 = nint(geneAFlight(i,1)*100.) !gamma,affective fear for light
    if (gene == 2) class1 = nint(geneAFlight(i,2)*100.) !P0 -  affective fear for light
    if (gene == 3) class1 = nint(geneAFother(i,1)*100.) !gamma,affective attraction to others
    if (gene == 4) class1 = nint(geneAFother(i,2)*100.) !P0 -  affective attraction to others
    if (gene == 5) class1 = nint(geneAFmort(i,1)*100.)  !gamma,affective attraction to others
    if (gene == 6) class1 = nint(geneAFmort(i,2)*100.)  !P0 -  affective attraction to others
    if (gene == 7) class1 = nint(geneAHstom(i,1)*100.)  !gamma,affective avoidance of others
    if (gene == 8) class1 = nint(geneAHstom(i,2)*100.)  !P0 -  affective avoidance of others
    if (gene == 9) class1 = nint(geneAHfood(i,1)*100.)  !gamma,affective attraction to food
    if (gene == 10) class1= nint(geneAHfood(i,2)*100.)  !P0 -  affective attraction to food

    !add effect of placement of allmin (e.g. at -1 or 0)
    if (allmin < 0.) class1 = class1 - nint(100*allmin)  !array made to accept 201 entries, e.g. allmin = -1


    if (class1 > 201) then
       write(10,*) "class1 = ",class1, "for ind ",i,"in gene ",gene,"in SRcorrplotsAFF in generation ",realgen
       class1 = 201
      else if (class1 < 0) then
       write(10,*) "class1 = ",class1, "for ind ",i,"in gene ",gene,"in SRcorrplotsAFF in generation ",realgen
       class1 = 0
    end if

    lspan(class1) = lspan(class1) + min(flifespan,fdeathage(i))
    bd(class1) = bd(class1) + fbirthdepth(i)
    ind(class1) = ind(class1) + 1
    Nafraid(class1) = Nafraid(class1) + fnumberafraid(i)
    gFL(class1) = gFL(class1) + geneAFlight(i,1)
    PFL(class1) = PFL(class1) + geneAFlight(i,2)
    gFO(class1) = gFO(class1) + geneAFother(i,1)
    PFO(class1) = PFO(class1) + geneAFother(i,2)
    gFP(class1) = gFP(class1) + geneAFmort(i,1)
    PFP(class1) = PFP(class1) + geneAFmort(i,2)
    gHS(class1) = gHS(class1) + geneAHstom(i,1)
    PHS(class1) = PHS(class1) + geneAHstom(i,2)
    gHF(class1) = gHF(class1) + geneAHfood(i,1)
    PHF(class1) = PHF(class1) + geneAHfood(i,2)
    if(fstatus(i) == 1) then !ind survived and final body mass exists
       bm(class1) = bm(class1) + fbodymass(i)
       gon(class1) = gon(class1) + fgonad(i)
       fkids(class1) = fkids(class1) + father(i)
       surv(class1) = surv(class1) + 1.
    end if
  end do

  !find average values
  do class1 = 0,201
    if (ind(class1) > 0) then
       c = class1
       inds = 1./max(1.,1.*ind(c))
       sur = 1./max(1.,1.*surv(c))
       bm(c) = bm(c)*sur
       gon(c) = gon(c)*sur
       fkids(c) = fkids(c)*sur
       surv(c) = surv(c)*inds
       bd(c) = bd(c)*inds
       Nafraid(c) = Nafraid(c)*inds
       lspan(c) = lspan(c)*inds
       gFL(c) = gFL(c)*inds
       PFL(c) = PFL(c)*inds
       gFO(c) = gFO(c)*inds
       PFO(c) = PFO(c)*inds
       gFP(c) = gFP(c)*inds
       PFP(c) = PFP(c)*inds
       gHS(c) = gHS(c)*inds
       PHS(c) = PHS(c)*inds
       gHF(c) = gHF(c)*inds
       PHF(c) = PHF(c)*inds
       write(filen,6103)realgen,dev,c*0.01,ind(c),bm(c),bd(c),gon(c),fkids(c),Nafraid(c),  &
                        surv(c),lspan(c),gFL(c),PFL(c),gFO(c),PFO(c),gFP(c),PFP(c), &
                        gHS(c),PHS(c),gHF(c),PHF(c)
    end if !individuals in class1
  end do !class1es within gene
end do !cycle over all 8 hedonic genes
6103 format(I12,A12,F12.3,I12,17F12.3)


end
!end subroutine SRcorrplotsAFF

!-----------------------------------------------------------------------
subroutine SRcorrplotsABM(file,fpop,realgen,dev,flifespan,gABM,test99)
!----------------------------------------------------------------------
!write plots of correlation between genes and traits in the last few generations
!genes: the 10 or less ABM-genes in the 10 affect functions
!traits: birth depth, final body mass, gonad size, fatherly offspring, survival

implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer    file,fpop,realgen,flifespan,gABM,test99
character(8) dev
!local variables
integer filen,i,class1,c,g,gene,ind(-101:101)
real inds,sur,bm(-101:101),bd(-101:101),gon(-101:101),fkids(-101:101),Nafraid(-101:101),surv(-101:101),lspan(-101:101)
real gFL(-101:101),PFL(-101:101),gFO(-101:101),PFO(-101:101),gFP(-101:101),PFP(-101:101)
real gHF(-101:101),PHF(-101:101),gHS(-101:101),PHS(-101:101),ABM(10,-101:101)

if (test99 == 1) write(10,*) "SRcorrplotsABM"
filen = file-1 !allow file number to incease within the g-loop

!cycle over all gABM affect genes
do gene = 1,gABM
   filen = filen+1

   !reset arrays
   do class1 = -101,101
       ind(class1) = 0
       bm(class1) = 0.
       bd(class1) = 0.
       gon(class1) = 0.
       fkids(class1) = 0.
       Nafraid(class1) = 0.
       surv(class1) = 0.
       lspan(class1) = 0.
       do g = 1,gABM
          ABM(g,class1) = 0.
       end do
       gFL(class1) = 0.
       PFL(class1) = 0.
       gFO(class1) = 0.
       PFO(class1) = 0.
       gFP(class1) = 0.
       PFP(class1) = 0.
       gHS(class1) = 0.
       PHS(class1) = 0.
       gHF(class1) = 0.
       PHF(class1) = 0.
   end do


  !get info of individuals
  do i = 1,fpop

   !determine selection criterion for each gene
    class1 = nint(geneAHsize(i,gene)*100.)
    if (class1 > 101) then
       write(10,*) "class1 = ",class1, "for ind ",i,"in gene ",gene,"in SRcorrplotsABM in generation ",realgen
       class1 = 101
      else if (class1 < -101) then
       write(10,*) "class1 = ",class1, "for ind ",i,"in gene ",gene,"in SRcorrplotsAFF in generation ",realgen
       class1 = -101
    end if

    lspan(class1) = lspan(class1) + min(flifespan,fdeathage(i))
    bd(class1) = bd(class1) + fbirthdepth(i)
    ind(class1) = ind(class1) + 1
    Nafraid(class1) = Nafraid(class1) + fnumberafraid(i)
    do g = 1,gABM
       ABM(g,class1) = ABM(g,class1) + geneAHsize(i,g)
    end do
    gFL(class1) = gFL(class1) + geneAFlight(i,1)
    PFL(class1) = PFL(class1) + geneAFlight(i,2)
    gFO(class1) = gFO(class1) + geneAFother(i,1)
    PFO(class1) = PFO(class1) + geneAFother(i,2)
    gFP(class1) = gFP(class1) + geneAFmort(i,1)
    PFP(class1) = PFP(class1) + geneAFmort(i,2)
    gHS(class1) = gHS(class1) + geneAHstom(i,1)
    PHS(class1) = PHS(class1) + geneAHstom(i,2)
    gHF(class1) = gHF(class1) + geneAHfood(i,1)
    PHF(class1) = PHF(class1) + geneAHfood(i,2)
    if(fstatus(i) == 1) then !ind survived and final body mass exists
       bm(class1) = bm(class1) + fbodymass(i)
       gon(class1) = gon(class1) + fgonad(i)
       fkids(class1) = fkids(class1) + father(i)
       surv(class1) = surv(class1) + 1.
    end if
  end do

  !find average values
  do class1 = -101,101
    if (ind(class1) > 0) then
       c = class1
       inds = 1./max(1.,1.*ind(c))
       sur = 1./max(1.,1.*surv(c))
       bm(c) = bm(c)*sur
       gon(c) = gon(c)*sur
       fkids(c) = fkids(c)*sur
       surv(c) = surv(c)*inds
       bd(c) = bd(c)*inds
       Nafraid(c) = Nafraid(c)*inds
       do g = 1,gABM
          ABM(g,class1) = ABM(g,class1)*inds
       end do
       lspan(c) = lspan(c)*inds
       gFL(c) = gFL(c)*inds
       PFL(c) = PFL(c)*inds
       gFO(c) = gFO(c)*inds
       PFO(c) = PFO(c)*inds
       gFP(c) = gFP(c)*inds
       PFP(c) = PFP(c)*inds
       gHS(c) = gHS(c)*inds
       PHS(c) = PHS(c)*inds
       gHF(c) = gHF(c)*inds
       PHF(c) = PHF(c)*inds
       write(filen,6104)realgen,dev,c*0.01,ind(c),bm(c),bd(c),gon(c),fkids(c),Nafraid(c),  &
                        surv(c),lspan(c),gFL(c),PFL(c),gFO(c),PFO(c),gFP(c),PFP(c), &
                        gHS(c),PHS(c),gHF(c),PHF(c),(ABM(g,c),g=1,gABM)
    end if !individuals in class1
  end do !class1es within gene
end do !cycle over all 8 hedonic genes
6104 format(I12,A12,F12.3,I12,17F12.3,10F12.3)


end
!end subroutine SRcorrplotsABM


!--------------------------------------------------------------------------
subroutine SRvertical(srerr,test99,expmt,screenplot,verticalfile,file1,  &
                      realgen,age,fsurvivors,flifespan,depth,autorisk)
!--------------------------------------------------------------------------
implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer expmt,file1,realgen,age,fsurvivors,flifespan,depth
integer screenplot,verticalfile,srerr,test99
real autorisk
!local variables
integer nf(10),error99,dep
real decimalage

if (test99 == 1) write(10,*) "SRvertical"
error99 = 0
if (srerr == 13) error99 = 1
!if (gen == 2) write(6,*) "SRvertical"

if (verticalfile == 1 .or. screenplot == 2) then
  !convert generation and age to a growing decimal number
  decimalage = realgen*1.0 + (age-1.)/(1.0*flifespan)

  !make printout-file of vertical distribution
  if (screenplot == 2) then !prepare for plot to monitor
     nf(1) = numfish(1,age)+numfish(2,age)+numfish(3,age)
     nf(2) = numfish(4,age)+numfish(5,age)+numfish(6,age)
     nf(3) = numfish(7,age)+numfish(8,age)+numfish(9,age)
     nf(4) = numfish(10,age)+numfish(11,age)+numfish(12,age)
     nf(5) = numfish(13,age)+numfish(14,age)+numfish(15,age)
     nf(6) = numfish(16,age)+numfish(17,age)+numfish(18,age)
     nf(7) = numfish(19,age)+numfish(20,age)+numfish(21,age)
     nf(8) = numfish(22,age)+numfish(23,age)+numfish(24,age)
     nf(9) = numfish(25,age)+numfish(26,age)+numfish(27,age)
     nf(10) = numfish(28,age)+numfish(29,age)+numfish(30,age)
  end if !screenplot == 2
end if

if (age > 0) then
   !make prints at 20 first generations and late in each gen/10 cycle, assuming 30 depth cells
   if (verticalfile == 1) write(file1,3901) decimalage,fsurvivors,(numfish(dep,age), dep = depth,1,-1)
   if (screenplot == 2) then
      write(6,666) expmt,realgen,age,int(autorisk),fsurvivors,(nf(dep), dep = 10,1,-1)
     elseif (screenplot == 1 .and. age == 1) then
      write(6,666) expmt,realgen
   end if
end if

666 format(I4,I7,3I6,10I5)
3901 format(F14.4,31I7)
!4101 format(30I1)

end
!end subroutine SRvertical


!---------------------------------------------------------------------------
subroutine SRgenestat(srerr,test99,expmt,gen,generations,realgen,   &
                     fpop,flifespan,totalsenses,deviation,gABM,       &
                     sdgABM,sdAfear,sdAhunger,sdHfear,sdHhunger, &
                     sdAHunused)
!---------------------------------------------------------------------------
implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer expmt,gen,generations,realgen,fpop,flifespan,srerr,test99,totalsenses,gABM
real sdgABM,sdAfear,sdAhunger,sdHfear,sdHhunger,sdAHunused
character(8) deviation
!local variables
integer ind,gn,error99,inp,age,i,n,g,sumtrans,totalgenes,males
real favg(39),fsd(39),diff,genome(15000,39),favgFabm(10),favgMabm(10),fsdFabm(10),fsdMabm(10)
real iavg(19),rtrans(19,21)  !frequency of perception values in each of 20 range values (step 0.1) of each sense in a generation
!real rmax(48),rmin(48),ravg(48),rsd(48)
!real rest,fmaxr,fminr,favgr,fsdr,rmaxr,rminr,ravgr,rsdr
!  remember to adjust vectors if totalgenes is increased


if (test99 == 1) write(10,*) "SRgenestat"
error99 = 0
if (srerr == 14) error99 = 1

!do not perfrom subroutine if population is extinct
if (fpop < 1) then
   write(6,*) "SRgenestat found npop = 0"
   goto 999
end if

totalgenes = 39
!load hedonic genome
do ind = 1,fpop
       do g = 1,10
          genome(ind,g) = geneAHsize(ind,g)
       end do
       do g = 1,3
          genome(ind,g+10) = geneAFlight(ind,g)
          genome(ind,g+13) = geneAFother(ind,g)
          genome(ind,g+16) = geneAFmort(ind,g)
          genome(ind,g+19) = geneAHstom(ind,g)
          genome(ind,g+22) = geneAHfood(ind,g)
          genome(ind,g+25) = geneHFlight(ind,g)
          genome(ind,g+28) = geneHFother(ind,g)
          genome(ind,g+31) = geneHHfood(ind,g)
          genome(ind,g+34) = geneHHother(ind,g)
       end do
       do g = 1,2
          genome(ind,g+37) = geneMemo(ind,g)
       end do
end do

!find allele average, standard deviation for each gene in all fish
do gn = 1,totalgenes
   favg(gn) = 0.
   fsd(gn) = 0.
   do ind = 1,fpop
     favg(gn) = favg(gn) + genome(ind,gn)
   end do
   favg(gn) = favg(gn)/(1.*fpop)
   do ind = 1,fpop
     diff = genome(ind,gn) - favg(gn)
     fsd(gn) = fsd(gn) + diff*diff
   end do
   fsd(gn) = sqrt(fsd(gn)/(1.*fpop))
end do

!sex-specific ABM-genes
males = 0
do gn = 1,10
   favgFabm(gn) = 0.
   favgMabm(gn) = 0.
   fsdFabm(gn) = 0.
   fsdMabm(gn) = 0.
   do ind = 1,fpop
     if (fgender(ind) == 1) then
        favgFabm(gn) = favgFabm(gn) + genome(ind,gn)
       else if (fgender(ind) == 2) then
        favgMabm(gn) = favgMabm(gn) + genome(ind,gn)
        if(gn == 1) males = males + 1
     end if
   end do
   favgFabm(gn) = favgFabm(gn)/(1.*(fpop-males))
   favgMabm(gn) = favgMabm(gn)/(1.*males)
   do ind = 1,fpop
     if (fgender(ind) == 1) then
        diff = genome(ind,gn) - favgFabm(gn)
        fsdFabm(gn) = fsdFabm(gn) + diff*diff
       else if (fgender(ind) == 2) then
        diff = genome(ind,gn) - favgMabm(gn)
        fsdMabm(gn) = fsdMabm(gn) + diff*diff
     end if
   end do
   fsdFabm(gn) = sqrt(fsdFabm(gn)/(1.*(fpop-males)))
   fsdMabm(gn) = sqrt(fsdMabm(gn)/(1.*males))
end do


!calculate statistics of information sensors
do inp = 1, 19
   iavg(inp) = 0.
   do age = 1, flifespan
      iavg(inp) = iavg(inp) + finavg(inp,age)
   end do
   iavg(inp) = iavg(inp)/(1.*flifespan)
end do


!calculate frequency of occurrence of perception values
sumtrans = 0
do age = 1,flifespan
   sumtrans = sumtrans + finobs(age)
end do
do inp = 1, totalsenses
   do i = 1,21
      rtrans(inp,i) = itrans(inp,i)/(1.*sumtrans)
      itrans(inp,i) = 0
   end do
end do


if (gen < 21 .or. gen/100 == gen/100. .or. gen > generations-10) then
   if (gen == 1) then
       write(32,*)"VALUES OF TRANSFORMED INFORMATION"
       write(33,*)"VALUES OF TRANSFORMED INFORMATION"
   end if
   write(32,*) "   "
   write(32,3212)" ","Generation:",realgen,"  ",deviation, "fpop:",fpop
   write(32,*)" information                       minimum  average  maximum"
   write(32,3211)" 1 remaining stomach capacity   ",finmin(1),iavg(1),finmax(1)
   write(32,3211)" 2 light at 2 depths above      ",finmin(2),iavg(2),finmax(2)
   write(32,3211)" 3 light at 1 depth above       ",finmin(3),iavg(3),finmax(3)
   write(32,3211)" 4 light at depth               ",finmin(4),iavg(4),finmax(4)
   write(32,3211)" 5 light at depth below         ",finmin(5),iavg(5),finmax(5)
   write(32,3211)" 6 light at 2 depths below      ",finmin(6),iavg(6),finmax(6)
   write(32,3211)" 7 number of others 2 above     ",finmin(7),iavg(7),finmax(7)
   write(32,3211)" 8 number of conspecifics above ",finmin(8),iavg(8),finmax(8)
   write(32,3211)" 9 number of conspecifics       ",finmin(9),iavg(9),finmax(9)
   write(32,3211)"10 number of others below       ",finmin(10),iavg(10),finmax(10)
   write(32,3211)"11 number of others 2 below     ",finmin(11),iavg(11),finmax(11)
   write(32,3211)"12 body mass                    ",finmin(12),iavg(12),finmax(12)
   write(32,3211)"13 prey encounter rate 2 above  ",finmin(13),iavg(13),finmax(13)
   write(32,3211)"14 prey encounter rate above    ",finmin(14),iavg(14),finmax(14)
   write(32,3211)"15 prey encounter rate          ",finmin(15),iavg(15),finmax(15)
   write(32,3211)"16 prey encounter rate below    ",finmin(16),iavg(16),finmax(16)
   write(32,3211)"17 prey encounter rate 2 below  ",finmin(17),iavg(17),finmax(17)
   write(32,3211)"18 age                          ",finmin(18),iavg(18),finmax(18)
   write(32,3211)"19 predation risk               ",finmin(19),iavg(19),finmax(19)

   write(33,*) "   "
   write(33,3212)" ","Generation:",realgen,"  ",deviation, "fpop:",fpop
   write(33,3312)" information","0.0-0.1","0.1-0.2","0.2-0.3","0.3-0.4","0.4-0.5", &
                "0.5-0.6","0.6-0.7","0.7-0.8","0.8-0.9","0.9-1.0","1.0-1.1",        &
                "1.1-1.2","1.2-1.3","1.3-1.4","1.4-1.5","1.5-1.6","1.6-1.7",        &
                "1.7-1.8","1.8-1.9","1.9-2.0",">2.0"
   write(33,3311)" 1 remaining stomach capacity   ",(rtrans(1,n),n = 1,21)
   write(33,3311)" 2 light at 2 depths above      ",(rtrans(2,n),n = 1,21)
   write(33,3311)" 3 light at 1 depth above       ",(rtrans(3,n),n = 1,21)
   write(33,3311)" 4 light at depth               ",(rtrans(4,n),n = 1,21)
   write(33,3311)" 5 light at depth below         ",(rtrans(5,n),n = 1,21)
   write(33,3311)" 6 light at 2 depths below      ",(rtrans(6,n),n = 1,21)
   write(33,3311)" 7 number of others 2 above     ",(rtrans(7,n),n = 1,21)
   write(33,3311)" 8 number of conspecifics above ",(rtrans(8,n),n = 1,21)
   write(33,3311)" 9 number of conspecifics       ",(rtrans(9,n),n = 1,21)
   write(33,3311)"10 number of others below       ",(rtrans(10,n),n = 1,21)
   write(33,3311)"11 number of others 2 below     ",(rtrans(11,n),n = 1,21)
   write(33,3311)"12 body mass                    ",(rtrans(12,n),n = 1,21)
   write(33,3311)"13 prey encounter rate 2 above  ",(rtrans(13,n),n = 1,21)
   write(33,3311)"14 prey encounter rate above    ",(rtrans(14,n),n = 1,21)
   write(33,3311)"15 prey encounter rate          ",(rtrans(15,n),n = 1,21)
   write(33,3311)"16 prey encounter rate below    ",(rtrans(16,n),n = 1,21)
   write(33,3311)"17 prey encounter rate 2 below  ",(rtrans(17,n),n = 1,21)
   write(33,3311)"18 age                          ",(rtrans(18,n),n = 1,21)
   write(33,3311)"19 predation risk               ",(rtrans(19,n),n = 1,21)
end if

!print average and sd of allele values of all genes
g = realgen
write(18,1802) expmt,g,(favgFabm(n),n = 1,10),g,(favgMabm(n),n = 1,10),g,  &
    (favg(n),n = 11,13),g,(favg(n),n = 14,16),g,(favg(n),n = 17,19),g,(favg(n),n = 20,22), &
     g,(favg(n),n = 23,25),g,(favg(n),n = 26,28),g,(favg(n),n = 29,31),   &
     g,(favg(n),n = 32,34),g,(favg(n),n = 35,37),g,(favg(n),n = 38,39)
write(19,1802) expmt,g,(fsdFabm(n),n = 1,10),g,(fsdMabm(n),n = 1,10),g,  &
    (fsd(n),n = 11,13),g,(fsd(n),n = 14,16),g,(fsd(n),n = 17,19),g,(fsd(n),n = 20,22),  &
     g,(fsd(n),n = 23,25),g,(fsd(n),n = 26,28),g,(fsd(n),n = 29,31),   &
     g,(fsd(n),n = 32,34),g,(fsd(n),n = 35,37),g,(fsd(n),n = 38,39)


!calculate average variation in hedonic genes
!OBS: assuming GAMMA distribution
sdgABM = 0.
do g = 1,gABM
  sdgABM = sdgABM + fsd(g)
end do
sdgABM = sdgABM/(1.*gABM)
sdAfear = (fsd(11)+fsd(12)+fsd(14)+fsd(15)+fsd(17)+fsd(18))/6.
sdAhunger = (fsd(20)+fsd(21)+fsd(23)+fsd(24))/4.
sdHfear = (fsd(26)+fsd(27)+fsd(29)+fsd(30))/4.
sdHhunger = (fsd(32)+fsd(33)+fsd(35)+fsd(36))/4.
sdAHunused = (fsd(13)+fsd(16)+fsd(19)+fsd(22)+fsd(25)+fsd(28)+fsd(31)+fsd(34)+fsd(37))/9.


1802 format(I9,2(I9,10F9.3),3(I9,3F9.3),   &
            6(I9,3F9.3),I9,2F9.5)
3211 format(A32,3F9.3)
3212 format(A3,A12,I7,A14,A8,A6,I9)
3311 format(A32,21F11.7)
3312 format(A32,21A11)

999 continue !skipped SR if fpop = 0
end



!-------------------------------------------------------------------------------
subroutine SRageneurons(srerr,test99,realgen,age,totalsenses,fpop,MaxPercept)
!-------------------------------------------------------------------------------
implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer    realgen,age,totalsenses,fpop,srerr,test99
real MaxPercept(7)   !scaling of present perception to value of 1 as maximum, averaging 100 generations

!local variables
integer inp,i,a,alive,ind,error99
real rawdata(19),avgbodymass

if (test99 == 1) write(10,*) "SRageneurons"
error99 = 0
if (srerr == 15) error99 = 1

!write print heading for each generation
if (age == 1) then
   write(22,*) " "
   write(22,*) "average value of transformed information "
   write(22,*) "generation = ", realgen
   write(22,2202)"age","observ","stom","light-2","light-1","light","light+1",    &
                 "light+1","fish-2","fish-1","fish","fish+1","fish+2","bodyz",   &
                 "p_enc-2","p_enc-1","p_enc","p_enc+1","p_enc+2","age","risk"
   write(23,*) " "
   write(23,*) "average value of back-transformed information "
   write(23,*) "generation = ", realgen
   write(23,2302)"age","observ","stom","light-2","light-1","light","light+1",    &
                 "light+1","fish-2","fish-1","fish","fish+1","fish+2","bodyz",   &
                 "p_enc-2","p_enc-1","p_enc","p_enc+1","p_enc+2","age","risk"
end if

a = age
do inp = 1, totalsenses
   finavg(inp,a) = finsum(inp,a)/(1.*finobs(a))
end do


!convert back from transformed input-neuron data to original ecological/physiological data
alive = 0
avgbodymass = 0.0
do ind = 1,fpop
  if (fstatus(ind) == 1) then
    alive = alive + 1
    avgbodymass = avgbodymass + fbodymass(ind)
  end if
end do
avgbodymass = avgbodymass/(1.*alive)


!procedure from SRhedonic, to be reversed here:
!   do inp = 1,totalsenses
!      !ensure non-zero values of input neurons
!      finfo(inp) = finfo(inp) + 0.000001
!       !final rescaling to 0-1:
!      finfo(inp) = finfo(inp)/prevmaxinfo(inp)
!   end do

rawdata(1) = finavg(1,a)*MaxPercept(1)
rawdata(12) = finavg(12,a)*MaxPercept(4)
rawdata(18) = finavg(18,a)*MaxPercept(6)
rawdata(19) = finavg(19,a)*MaxPercept(7)
do i = 1,5
   rawdata(1+i) = finavg(1+i,a)*MaxPercept(2)
   rawdata(6+i) = finavg(6+i,a)*MaxPercept(3)
   rawdata(12+i) = finavg(12+i,a)*MaxPercept(5)
end do

write(22,2201)a,finobs(a),(finavg(i,a), i = 1,19)
write(23,2301)a,finobs(a),(rawdata(i), i = 1,19)
2201 format(I4,I9,19F9.3)
2202 format(A4,A9,19A9)
2301 format(I6,I12,6F12.2,5F12.0,F12.0,5F12.0,2F12.3)
2302 format(A6,20A12)

end


!-------------------------------------------------------------------------------
subroutine SRbehavfiles(runtag,expmt,realgen,part,behavstep,behavmax,curve,test99)
!-------------------------------------------------------------------------------
!open files to print individual states and decisions for select individuals (100, 200,...)
implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer    expmt,realgen,part,behavmax,behavstep,curve,test99
character(6) runtag
!local variables
integer ind,filen,num,g
character(46) string
character(5) itext

if (test99 == 1) write(10,*) "SRbehavfiles"
num = expmt
!write(6,*)"num = ",num
if (part == 1) then

   do ind = behavstep,behavmax,behavstep
      filen = 199 + int(ind/behavstep)
!      if (filen < 205) write(6,*)"filen = ",filen
      if (ind >9999) then
         write(itext,6002)ind
        elseif (ind > 999) then
         write(itext,6003)"i",ind
        else
         write(itext,6004)"i0",ind
      end if
   if (num < 10) then
      write(string,6000)"HED18-",runtag,"-E0",num,"-o",filen,"-behaviour-for-",itext,".txt"
     else
      write(string,6001)"HED18-",runtag,"-E",num,"-o",filen,"-behaviour-for-",itext,".txt"
   end if
      open(filen, file = "behaviour/"//string)
   end do

   do ind = behavstep,behavmax,behavstep
      filen = 199 + int(ind/behavstep)
      write(filen,*)" "
      write(filen,6008)"genes of"," ind.",ind
      write(filen,6009)"after",realgen, "gener."
!      write(filen,6009)string," after ",generations, " generations"
      call SRfileheadings(filen,curve,7,test99)
      call SRfileheadings(filen,curve,8,test99)

      write(filen,6011) (geneAHsize(ind,g),g  = 1,10),(geneAFlight(ind,g),g = 1,3),   &
                 (geneAFother(ind,g),g = 1,3),(geneAFmort(ind,g),g = 1,3),   &
                    (geneAHstom(ind,g),g = 1,3),     &
                  (geneAHfood(ind,g),g = 1,3),(geneHFlight(ind,g),g = 1,3),   &
                 (geneHFother(ind,g),g = 1,3),(geneHHfood(ind,g),g = 1,3),   &
                 (geneHHother(ind,g),g = 1,3),(geneMemo(ind,g),g = 1,2)
      write(filen,*)" "
      write(filen,*)" "
   end do
  else if (part == 2) then

  do ind = behavstep,behavmax,behavstep
      filen = 199 + int(ind/behavstep)
      close(filen)
   end do
end if
6000 format(2A6,A3,I1,A2,I3,A15,A5,A4)
6001 format(2A6,A2,I2,A2,I3,A15,A5,A4)
6002 format(I5)
6003 format(A1,I4)
6004 format(A2,I3)
6008 format(2A12,I12)
6009 format(A12,I12,A12)
6011 format(9F12.2,9F12.2,19F12.2,2F12.5)
end

!-------------------------------------------------------------------------------
subroutine SRbehaviour(tag,ind,age,behavstep,habval,fear,hunger,Fsize,   &
                       Flight,Fothers,Fmort,Hstomach,Hsize,Hfood,HdomF,  &
                       FdomL,FdomO,HdomO,foodmax,test99)
!-------------------------------------------------------------------------------
!Print invididual states and decisions for either
! - selected individuals (500, 1000, 1500,..., 10000) or
! - experimentally added individuals (fpop-expmntfish+1,fpop)
!in one file per individual
implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
character (1) tag        !letter for dominant affect (F or H)
character (2) aff        !letter for dominant affect (F or H) (local variable)
integer    ind,age,behavstep,foodmax,test99
real habval(5),HdomF(5),FdomL(5),FdomO(5),HdomO(5)
real fear,hunger,Fsize,Flight,Fothers,Fmort,Hstomach,Hsize,Hfood
!local variables
integer filen,i,a,z
real fullness

if (test99 == 1) write(10,*) "SRbehaviour"
i = ind
a = age
!z dummy for current depth

if (tag == "F") then
   aff = "F "
  else
   aff = " H"
end if

!file number depends on sheme (ordinary or experimental inds) and ind number

filen = 199 + ind/behavstep


!print heading if at the first age
if (age == 1) then
    write(filen,*) "Individual ",ind
    write(filen, 5008)"info1","info4","info9","info12","info15","info18","info19"
    write(filen, 5007)"age","b-mass","stomach","fullness","age","depth",       &
                      "foodmax","affect","age","fear","hunger","Fsize",        &
                      "Flight","Fothers","Fmort","Hsize","Hstomach","Hfood",    &
                      "age","avail_stcap","light","others","b-mass","food","age","risk",  &
                       "age","habv2U","FdmL2U","FdmO2U","HdmF2U","HdmO2U",      &
                       "age","habv1U","FdmL1U","FdmO1U","HdmF1U","HdmO1U",      &
                       "age","habv-z","FdmL-z","FdmO-z","HdmF-z","HdmO-z",      &
                       "age","habv1D","FdmL1D","FdmO1D","HdmF1D","HdmO1D",      &
                       "age","habv2D","FdmL2D","FdmO2D","HdmF2D","HdmO2D"
end if
fullness = 4.*fstomach(i,0)/fbodymass(i)
write(filen, 5006)a,fbodymass(i),fstomach(i,0),fullness,a,fdepthchoice(i,1),   &
                  foodmax,aff,a,fear,hunger,Fsize,Flight,Fothers,              &
                  Fmort,Hsize,Hstomach,Hfood,a,finfo(1),finfo(4),finfo(9),      &
                  finfo(12),finfo(15),finfo(18),finfo(19),                     &
                  (age,habval(z),FdomL(z),FdomO(z),HdomF(z),HdomO(z), z = 1,5)

5006 format(I12,F12.1,F12.2,F12.3,3I12,A12,I12,9E12.3,I12,7F12.5,5(I12,5F12.5))
5007 format(56A12)
5008 format(228x,7A12)

end !of SRbehaviour



!-------------------------------------------------------------------------------
subroutine SRgeneread(sets,fish,fnewpop,gen,age,motherlength,cop2egg,realgen,test99)
!-------------------------------------------------------------------------------
!read genes from one or several previous simulations to evolve in the genetic algorithm
implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program

integer sets        !number of previous populations to extract data from ("readfiles")
integer fish        !maximum population size of the population to be created
!integer gABM        !number of genes controlling Body mass influence on fear and hunger
integer fnewpop     !number of individuals created in this process
integer realgen     !last generation of previous simulation
integer gen,age     !current generation and time step (0 for both)
!integer flifespan   !number of time steps in a generation
integer motherlength,cop2egg
!local variables
integer sim         !current population to read from
integer Npop(10)    !number of individuals in each of the populations to be read from
integer nr1,nr3     !file number dummy
integer sumpop      !sum of individuals in the population to extract genes from
integer jumpover    !number of genomes not to read, between each to read
integer lastread    !last genome to be read in a gene pool
integer leastread   !lastread from the smallest population, willl be the last read in all populations
integer readfromeach ! number of genes to read from each population to get close to FISH in new pop
character(15)name   !name of each file that gene data shall be read from
character(30)tag
integer i,g,d,ind,m,test99
integer dummy,dummy1,dummy3,hundred
real random

if (test99 == 1) write(10,*) "SRgeneread"
hundred = 700 !basis for file numbering

!reset genes

do i = 1,fish
  do g = 1,10
      geneAHsize(i,g) = -10.
  end do
   do g = 1,3
      geneAFlight(i,g) = -10.
      geneAFother(i,g) = -10.
      geneAFmort(i,g) = -10.
      geneAHstom(i,g) = -10.
      geneAHfood(i,g) = -10.
      geneHFlight(i,g) = -10.
      geneHFother(i,g) = -10.
      geneHHfood(i,g) = -10.
      geneHHother(i,g) = -10.
  end do
end do


!read heading of all input files
3001 format(A10,I1,A4)
sumpop = 0
do sim = 1,sets
   nr1 = hundred+sim
   write(name,3001) "readgenes-",sim,".txt"
   open(nr1, file = name)

   read(nr1,*)
   read(nr1,3004) tag
   read(nr1,3005) dummy1
   read(nr1,3005) realgen
   read(nr1,3005) dummy3
   read(nr1,3005) Npop(sim)
   read(nr1,*)
   write(6,*) tag
   write(6,*) dummy1
   write(6,*) realgen
   write(6,*) dummy3
   write(6,*) Npop(sim)
   sumpop = sumpop + Npop(sim)
end do


!determine how to read genes from the files (individuals are often closely related to their neighbours)
readfromeach = min(fish,sumpop)/sets
leastread = readfromeach
do sim = 1,sets
   if(readfromeach < Npop(sim)) then
      jumpover = Npop(sim)/readfromeach - 1
     else
      jumpover = 0
   end if
   lastread = min(readfromeach,Npop(sim)/(jumpover+1))
   if(lastread < leastread) leastread = lastread
end do
readfromeach = leastread

write(6,*) "readfromeach :", readfromeach

i = 0
do sim = 1,sets
   nr1 = hundred+sim
   if(readfromeach < Npop(sim)) then
      jumpover = Npop(sim)/readfromeach - 1
     else
      jumpover = 0
   end if
   write(6,*) "jumpover :", jumpover
   if (jumpover == 0) then !read data from all individuals
      do ind = 1, Npop(sim)
         i = i+1
         motherpop(i) = sim
         read(nr1,3006) dummy,(geneAHsize(i,g),g = 1,10),(geneAFlight(i,g),g = 1,3),      &
         (geneAFother(i,g),g = 1,3),(geneAFmort(i,g),g = 1,3),(geneAHstom(i,g),g = 1,3),  &
         (geneAHfood(i,g),g = 1,3),(geneHFlight(i,g),g = 1,3),(geneHFother(i,g),g = 1,3), &
         (geneHHfood(i,g),g = 1,3),(geneHHother(i,g),g = 1,3),(geneMemo(i,g),g = 1,2),fbirthdepth(i)
         !test for overflow
         if (i == fish) goto 1111
      end do
     else
!      lastread = min(readfromeach,Npop(sim)/(jumpover+1))
      do ind = 1, leastread
         do d = 1,jumpover
            read(nr1,*)   !skip the genes of these individuals
            end do
         i = i+1
         motherpop(i) = sim
         read(nr1,3006) dummy,(geneAHsize(i,g),g = 1,10),(geneAFlight(i,g),g = 1,3),      &
         (geneAFother(i,g),g = 1,3),(geneAFmort(i,g),g = 1,3),(geneAHstom(i,g),g = 1,3),  &
         (geneAHfood(i,g),g = 1,3),(geneHFlight(i,g),g = 1,3),(geneHFother(i,g),g = 1,3), &
         (geneHHfood(i,g),g = 1,3),(geneHHother(i,g),g = 1,3),(geneMemo(i,g),g = 1,2),fbirthdepth(i)
         !test for overflow
         if (i == fish) goto 1111
      end do
   end if
end do
1111 continue !if FISH individuals have been read already
fnewpop = i

!give other traits to these new creations

do ind = 1,fnewpop
   fstatus(ind) = 1
   fbodymax(ind) = cop2egg
   fbodymass(ind) = cop2egg
   fgonad(ind) = 0.
   fstomach(ind,0) = 0.0
   fstomach(ind,1) = 0.0
   fdepthchoice(ind,1) = fbirthdepth(ind)
   fdepthchoice(ind,0) = fdepthchoice(ind,1)
   fdeathage(ind) = 0
   fdeathdepth(ind) = 0
   call random_number (random)
   findfeel(ind) = nint(random)
   foffspring(ind) = 0
   do m = 1, motherlength
      motherstring(ind,m) = 0
   end do
   gcreation(ind) = gen
   acreation(ind) = age
   icreation(ind) = ind
   mcreation(ind) = 4
   do m = 1,2
      fmemory(ind,m) = 0.
   end do
end do

if (fnewpop < fish) then

 do ind = fnewpop+1,fish
   fstatus(ind) = 0
   fbodymass(ind) = 0.
   fgonad(ind) = 0.
   fstomach(ind,0) = 0.0
   fstomach(ind,1) = 0.0
   fdepthchoice(ind,1) = 0
   fdepthchoice(ind,0) = 0
   fdeathage(ind) = 0
   fdeathdepth(ind) = 0
   findfeel(ind) = -1
   foffspring(ind) = 0
   motherpop(ind) = 0
   do m = 1, motherlength
      motherstring(ind,m) = 0
   end do
   gcreation(ind) = 0
   acreation(ind) = 0
   icreation(ind) = 0
   mcreation(ind) = 0
   do m = 1,2
      fmemory(ind,m) = 0.
   end do
 end do
end if

nr3 = hundred + sets + 1
open(nr3, file = 'writegenes.txt')
do i = 1, fnewpop
   write(nr3,3007) i,motherpop(i),(geneAHsize(i,g),g = 1,10),(geneAFlight(i,g),g = 1,3), &
         (geneAFother(i,g),g = 1,3),(geneAFmort(i,g),g = 1,3),   &
            (geneAHstom(i,g),g = 1,3),    &
         (geneAHfood(i,g),g = 1,3),(geneHFlight(i,g),g = 1,3),   &
         (geneHFother(i,g),g = 1,3),(geneHHfood(i,g),g = 1,3),   &
         (geneHHother(i,g),g = 1,3),(geneMemo(i,g),g = 1,2),fbirthdepth(i)
end do

3004 format(A30)
3005 format(I8)
3006 format(I8,37F8.2,2F8.5,I8)
3007 format(2I8,37F8.2,2F8.5,I8)


do sim = 1,sets
   nr1 = hundred+sim
   close(nr1)
end do
close(nr3)

end !of SRgeneread



!-------------------------------------------------------------------------------
subroutine SRgenecreate(state,fpop,allmax,allmin,interval,depth,gen,age,  &
                 flifespan,cop2egg,fsurvivors,motherlength,memory,curve,  &
                 gABM,stepABM,minABM,maxABM,fish,test99,copies,new,itag,brain)
!-------------------------------------------------------------------------------
!create genes to evolve in the genetic algorithm

!state = 0: create a population from scratch by random genes
!state = 1: create extra individuals in a population near extinction
!(state = 2: immigration of individiuals from outside, this option is removed)
!(state = 3: addition of a few individiuals as an experiment, this option is removed)
implicit none
!include commonblock of fish arrays
include 'Commonfish.txt'
!variables from main program
integer fpop,state,depth,gen,age,flifespan,cop2egg,gABM
integer fsurvivors,motherlength,memory,curve
integer fish,test99,copyfrom,copyto,brain
real    interval,allmax,allmin,stepABM,minABM,maxABM
character(8) itag(15000)
!local variables
integer ind,i,j,k,g,copies,new,last,dead,copygoal,popgoal
real    r,random,igene(10)
!integer immigrants

if (test99 == 1) write(10,*) "SRgenecreate gen,age,state: ",gen,age,state
copies = 0 ! number of individuals created from copying existing
new = 0  ! number of individuals created from random

!New individuals shall be created either from existing (copy, PART2) or random (PART 3).
!If STATE = 0, all are created by random (at the start of a simulation)
!If STATE = 1, 3/4 are made by COPY and 1/4 from random

!store tag of creation of this ind
do ind = 1,fpop
   if (fstatus(ind) == 1) then
      itag(ind) = "S" !survivor
     else
      itag(ind) = "DD" !dead
   end if
end do
do ind = fpop+1,fish
   itag(ind) = "Above-f" !above fpop
end do

if (state == 1) then !copy living individuals in an attempt to rescue a population
  !(PART 1: sort all so that the living have lower ind-numbers than the dead
  !--> this part is removed, so no sorting takes place)

  !PART 2: rescue dying population by copying living individuals
  !Search from below (ind 1, 2, ..) and replace dead by living
   copies = 0
   if (fpop < fish/3) then !if a population is very small also at age 1, then boost population size
       if (brain == 0) then  !using SR2003hed for hedonic tones in brain
          popgoal = 3*fpop
         else                !using SRhedonic for mental states in brain and attention selection
          popgoal = 2*fpop
       end if
     else
      popgoal = min(fish-1,fpop+2) !Re-establish a living population slightly larger than in age 1, to make sure that
                                   !the new estimate of fpop will contain living inds all the way from 1 to the new fpop
   end if
   dead = popgoal - fsurvivors
   copygoal = dead*3/4
   do k = 1,100  ! make up to 100 copies of all individuals alive, i.e of ind (1..fsurvivors)
      last = fpop  !highest possible ind-number of live indivudal to copy from
      do copyto = 1,fish
         if (copies .ge. copygoal) goto 9004 !jump out of loop when number alive high enough
         if (fstatus(copyto) == 1) goto 9002 !skip living individuals
            j = copyto
            do copyfrom = last,1,-1
               if (fstatus(copyfrom) < 1) goto 9001 !skip dead individuals
               i = copyfrom
               goto 9000 !both COPYFROM and COPYTO found
               9001 continue !this ind was dead, try next
            end do !copyto
            9000 continue !both COPYFROM and COPYTO found, now make the new ind

            !make a new individual from COPYFROM
            fstatus(j) = 1
            itag(j) = "COPY"
            copies = copies + 1 !count indivuduals made by copying
            last = copyfrom - 1 !next seach for COPYFROM-ind starts here
            if(last < 1) last = fpop !start again when all living have been copied
            fsurvivors = fsurvivors + 1 !a new living ind is about to be made
            numfish(fdepthchoice(i,1),age) = numfish(fdepthchoice(i,1),age) + 1
            !give traits (genes and state) of COPYFROM to COPYTO
            !(Make a new living ind identical to COPYFROM)
            do g = 1,10
                geneAHsize(j,g) = geneAHsize(i,g)
                mutAHsize(j,g) = mutAHsize(i,g)
            end do
            do g = 1,3
                geneAFlight(j,g) = geneAFlight(i,g)
                geneAFother(j,g) = geneAFother(i,g)
                geneAFmort(j,g) = geneAFmort(i,g)
                geneAHstom(j,g) = geneAHstom(i,g)
                geneAHfood(j,g) = geneAHfood(i,g)
                geneHFlight(j,g) = geneHFlight(i,g)
                geneHFother(j,g) = geneHFother(i,g)
                geneHHfood(j,g) = geneHHfood(i,g)
                geneHHother(j,g) = geneHHother(i,g)

                mutAFlight(j,g) = mutAFlight(i,g)
                mutAFother(j,g) = mutAFother(i,g)
                mutAFmort(j,g) = mutAFmort(i,g)
                mutAHstom(j,g) = mutAHstom(i,g)
                mutAHfood(j,g) = mutAHfood(i,g)
                mutHFlight(j,g) = mutHFlight(i,g)
                mutHFother(j,g) = mutHFother(i,g)
                mutHHfood(j,g) = mutHHfood(i,g)
                mutHHother(j,g) = mutHHother(i,g)
            end do
            do g = 1,2
                geneMemo(j,g) = geneMemo(i,g)
                mutMemo(j,g) = mutMemo(i,g)
            end do
            fgender(j) = fgender(i)

            fbodymass(j) = fbodymass(i)
            fgonad(j) = fgonad(i)
            fstomach(j,0) = fstomach(i,0)
            fstomach(j,1) = fstomach(i,1)
            fdepthchoice(j,0) = fdepthchoice(i,0)
            fdepthchoice(j,1) = fdepthchoice(i,1)
            fdeathage(j) = 0
            fdeathdepth(j) = 0
            fbirthdepth(j) = fbirthdepth(i)
            findfeel(j) = findfeel(i)
            foffspring(j) = foffspring(i)
            motherpop(j) = motherpop(i)
            do g = 1, motherlength
               motherstring(j,g) = motherstring(i,g)
            end do

            mcreation(j) = 1
            gcreation(j) = gen
            acreation(j) = age
            icreation(j) = i
            do g = 1,2
               fmemory(j,g) = fmemory(i,g)
            end do !g
            9002 continue !this ind was dead and not used in COPYFROM
      end do !copyfrom
      9003 continue !copyto hits copyfrom, next k needed for more individuals
   end do ! k
   9004 continue !rescued pop big enough
!1012 format(2I7,F7.1,I7,F7.2,F7.2)

end if !state = 1


!PART 3: make random individuals
!Search from below (ind 1, 2, ..) and replace dead by living

!either only random individuals (state = 0) or a mix of copied and random (state = 1)
if (state == 0) then
   last = fish-1-fsurvivors !fsurvivors shall be 0 at this time
  else  !the program can only come to here if state = 1 and PART 2 is done
   last = popgoal - fsurvivors
end if

new = 0
do ind = 1, fish
   if (new .ge. last) goto 9005  !stop this when population close to max
   if (fstatus(ind) < 1) then  !create a new individual from a dead one, with random genes
     fstatus(ind) = 1
     itag(ind) = "RANDOM"
     fsurvivors = fsurvivors + 1
     new = new + 1

     !genes are initiated to a random x.y5 or x.y0 number within predefined boundaries
     !  interpretation of genes differ among simulations with LINEAR and SIGMOID functions
     !
     !                                                  k-gene
     !           affect = [switch 0/1] * ------------------------------------
     !                                           [a-gene (r-gene - perception)]
     !                                    1 + exp

     !sigmoid functions
     !k-gene : igene(1)
     !a-gene : igene(2)
     !r-gene : igene(3)

     !linear functions
     !Amax : igene(1)
     !Pmin : igene(2)
     !Pmax : igene(3)

     !affect functions
     !x-gene    : igene(1)
     !p-gene    : igene(2)
     !not in use: igene(3)

     !gender
     call random_number (random)
     if (random > 0.5) then
       fgender(ind) = 2  !a male is born
      else
       fgender(ind) = 1  !a female is born
     end if

     !geneAHsize
        if (curve == 1) then !sigmoid functions
           call SR_sig_createFH(igene,stepABM,minABM,maxABM,test99)
           do g = 4,10
              igene(g) = 0.
           end do
          else  !linear or gamma
           call SR_createABM(igene,stepABM,minABM,maxABM,gABM,test99)
        end if
        do g = 1,gABM
              geneAHsize(ind,g) = igene(g)
              mutAHsize(ind,g) = 0
        end do
        if (gABM < 10) then
            do g = gABM+1,10
               geneAHsize(ind,g) = 0.
               mutAHsize(ind,g) = 0
            end do
        end if

      !geneAFlight
        if (curve == 1) then !sigmoid functions
           call SR_sig_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 2) then !linear functions
           call SR_lin_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 3 .or. curve == 4) then !gamma functions
           call SR_gam_createFH(igene,interval,allmin,allmax,test99)
        end if
        do g = 1,3
              geneAFlight(ind,g) = igene(g)
              mutAFlight(ind,g) = 0
        end do

       !geneAFother
        if (curve == 1) then !sigmoid functions
           call SR_sig_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 2) then !linear functions
           call SR_lin_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 3 .or. curve == 4) then !gamma functions
           call SR_gam_createFH(igene,interval,allmin,allmax,test99)
        end if
        do g = 1,3
              geneAFother(ind,g) = igene(g)
              mutAFother(ind,g) = 0
        end do

      !geneAFmort
        if (curve == 1) then !sigmoid functions
           call SR_sig_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 2) then !linear functions
           call SR_lin_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 3 .or. curve == 4) then !gamma functions
           call SR_gam_createFH(igene,interval,allmin,allmax,test99)
        end if
        do g = 1,3
              geneAFmort(ind,g) = igene(g)
              mutAFmort(ind,g) = 0
        end do

      !geneAHstom
        if (curve == 1) then !sigmoid functions
           call SR_sig_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 2) then !linear functions
           call SR_lin_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 3 .or. curve == 4) then !gamma functions
           call SR_gam_createFH(igene,interval,allmin,allmax,test99)
        end if
        do g = 1,3
              geneAHstom(ind,g) = igene(g)
              mutAHstom(ind,g) = 0
        end do

      !geneAHfood
        if (curve == 1) then !sigmoid functions
           call SR_sig_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 2) then !linear functions
           call SR_lin_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 3 .or. curve == 4) then !gamma functions
           call SR_gam_createFH(igene,interval,allmin,allmax,test99)
        end if
        do g = 1,3
              geneAHfood(ind,g) = igene(g)
              mutAHfood(ind,g) = 0
        end do

      !geneHFlight
        if (curve == 1) then !sigmoid functions
           call SR_sig_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 2) then !linear functions
           call SR_lin_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 3 .or. curve == 4) then !gamma functions
           call SR_gam_createFH(igene,interval,allmin,allmax,test99)
        end if
        do g = 1,3
              geneHFlight(ind,g) = igene(g)
              mutHFlight(ind,g) = 0
        end do

      !geneHFother
        if (curve == 1) then !sigmoid functions
           call SR_sig_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 2) then !linear functions
           call SR_lin_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 3 .or. curve == 4) then !gamma functions
           call SR_gam_createFH(igene,interval,allmin,allmax,test99)
        end if
        do g = 1,3
              geneHFother(ind,g) = igene(g)
              mutHFother(ind,g) = 0
        end do

      !geneHHfood
        if (curve == 1) then !sigmoid functions
           call SR_sig_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 2) then !linear functions
           call SR_lin_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 3 .or. curve == 4) then !gamma functions
           call SR_gam_createFH(igene,interval,allmin,allmax,test99)
        end if
        do g = 1,3
              geneHHfood(ind,g) = igene(g)
              mutHHfood(ind,g) = 0
        end do

      !geneHHother
        if (curve == 1) then !sigmoid functions
           call SR_sig_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 2) then !linear functions
           call SR_lin_createFH(igene,interval,allmin,allmax,test99)
          else if (curve == 3 .or. curve == 4) then !gamma functions
           call SR_gam_createFH(igene,interval,allmin,allmax,test99)
        end if
        do g = 1,3
              geneHHother(ind,g) = igene(g)
              mutHHother(ind,g) = 0
        end do

      !The 2 memory genes specify how much of memory of fear and hunger is retained between time steps
      !Allowed values are 0.00001 -- 0.9999, but initiated 0.001-0.2
      if (memory == 0) then !agents carry no memory of past feelings
        geneMemo(ind,1) = 0. !fear
        geneMemo(ind,2) = 0. !hunger
        else !adaptive and individual memory ability
         call random_number (random)
         r = int(random*200)*0.001 + 0.001
         geneMemo(ind,1) = r   !fear
         mutMemo(ind,1) = 0
         call random_number (random)
         r = int(random*200)*0.001 + 0.001
         geneMemo(ind,2) = r   !hunger
         mutMemo(ind,2) = 0
       end if

       if (state > 0) then ! we must also give these organisms some traits
          fbodymass(ind) = max(2.*cop2egg, 3000.*(age/(1.*flifespan))) + 100.
          fgonad(ind) = 1.
          fstomach(ind,0) = 0.5
          fstomach(ind,1) = 0.5
          mcreation(ind) = 2
          call random_number (random)
          fdepthchoice(ind,1) = int(1 + (depth-2)*random)
          fdepthchoice(ind,0) = fdepthchoice(ind,1)
          numfish(fdepthchoice(ind,1),age) = numfish(fdepthchoice(ind,1),age) + 1
          fbirthdepth(ind) = 50
          fdeathage(ind) = 0
          fdeathdepth(ind) = 0
          findfeel(ind) = nint(random)
          foffspring(ind) = 0
          motherpop(ind) = 0
          do g = 1, motherlength
            motherstring(ind,g) = 0
          end do
          gcreation(ind) = gen
          acreation(ind) = age
          icreation(ind) = ind
          do g = 1,2
            fmemory(ind,g) = 0.
          end do
       end if !state > 0
   end if !fstatus(ind = 0)
end do !ind first, last
9005 continue

end

!-------------------------------------------------------------------------------
subroutine SR_sig_createFH(gene,interval,allmin,allmax,test99)
!-------------------------------------------------------------------------------
!create genes in one affective or hedonic function with sigmoid curve
implicit none
!variables from main program
integer test99
real gene(10),interval,allmin,allmax
!local variable
real random

if (test99 == 1) write(10,*) "SR_sig_createFH"
call random_number (random)
gene(1) = allmin + INT(random/interval)*interval   !K scales to max = 1 (29.1.10)

call random_number (random)
!now in all cases: positive correlation between perception and affect: a>0
gene(2) = INT(random*allmax/interval)*interval + allmin

!The r-gene defines the values of the perception that gives maximum change in affect,
!   and this must be within the range of possible perception values (0-1)
call random_number (random)
gene(3) = NINT(random*(allmax-allmin)/interval)*interval + allmin


end


!-------------------------------------------------------------------------------
subroutine SR_lin_createFH(gene,interval,allmin,allmax,test99)
!-------------------------------------------------------------------------------
!create genes in one affective or hedonic function with linear curve
implicit none
!variables from main program
integer test99
real gene(10),interval,allmin,allmax

!local variable
real dummy,random

if (test99 == 1) write(10,*) "SR_lin_createFH"
!Amax in the full range
call random_number (random)
gene(1) = allmin + INT(allmax*random/interval)*interval   !corrected 1 Oct 2010
   !all genes scales to max = 1 (29.1.10)

!Pmin in the 0-0.5 range
call random_number (random)
gene(2) = allmin + INT(0.5*allmax*random/interval)*interval   !corrected 1 Oct 2010

!Pmax larger than Pmin, but within allmax
call random_number (random)
gene(3) = min(allmax, gene(2) + interval + INT(0.5*allmax*random/interval)*interval)   !corrected 1 Oct 2010

!make sure Pmax > Pmin
if (gene(2) > gene(3)) then
   dummy = gene(3)
   gene(3) = gene(2)
   gene(2) = dummy
end if


end


!-------------------------------------------------------------------------------
subroutine SR_gam_createFH(gene,interval,allmin,allmax,test99)
!-------------------------------------------------------------------------------
!create genes in one affective or hedonic function with gamma distribution
implicit none
!variables from main program
integer test99
real gene(10),interval,allmin,allmax
!local variable
integer g
real    random

if (test99 == 1) write(10,*) "SR_gam_createFH"
!all the genes in the full range
do g = 1,2
   call random_number (random)
   gene(g) = allmin + INT(allmax*random/interval)*interval   !corrected 1 Oct 2010
end do

!if allmin < 0, this should only appply to gamma, not P0
if (allmin < 0.) gene(2) = abs(gene(2))

gene(3) = allmin !this gene not in use


end


!-------------------------------------------------------------------------------
subroutine SR_createABM(gene,stepABM,minABM,maxABM,gABM,test99)
!-------------------------------------------------------------------------------
!create genes in an affective function involving age or body mass, with linear or gamma curve
implicit none
!variables from main program
integer gABM,test99
real gene(10),stepABM,minABM,maxABM

!local variable
real random
integer g

if (test99 == 1) write(10,*) "SR_createABM"
!all the genes in the full range
do g = 1,gABM
   call random_number (random)
   gene(g) = minABM + INT(maxABM*random/stepABM)*stepABM   !corrected 1 Oct 2010
end do


end



!-------------------------------------------------------------------------------
!subroutine SRcopepod(srerr,test99,flifespan,realgen,cop)
!-------------------------------------------------------------------------------
!print vertical distribution of copepods, in generation 2
!implicit none
!variables from main program
!integer flifespan,realgen,srerr,test99,cop(30,0:5000)
!local variables
!integer age,dep,error99

!if (test99 == 1) write(10,*) "SRcopepod"
!error99 = 0
!if (srerr == 12) error99 = 1

!do age = 1, flifespan
!   write(13,1301) realgen,age,(cop(dep,age), dep = 30,1,-1)
!end do
!1301 format(2I8,30I8)

!end


!------------------------------------------------------------------
!visual range subroutines start here
!------------------------------------------------------------------
!

!    -------------------------------------------------------------
      SUBROUTINE SRGETR(r,c,C0,Ap,Vc,Ke,Eb,test99)
!   -------------------------------------------------------------

!     Obtain visual range by solving the non-linear equation
!     by means of Newton-Raphson iteration and derivation in
!     subroutine DERIV. Initial value is calculated in EASYR.
!      The calculation is based on the model described in Aksnes &
!      Utne (1997) Sarsia 83:137-147.
!
!      Programmed and tested 29 January 2001 Dag L Aksnes
!
      implicit none
      REAL       r,c,C0,Vc,Ap,Ke,Eb
      REAL       EPS,RST,TOL,TOLF,F1,FDER,DX
      INTEGER    IEND,AS,I,test99
!      INTEGER IER
!     Input parameters
!        RST       : start value of r calculated by EASYR
!        c         : beam attenuation coefficient (m-1)
!        C0        : prey inherent contrast
!        Ap        : prey area (m^2)
!        Vc        : parameter characterising visual capacity (d.l.)
!                    this parameter is denoted E' in Aksnes & Utne
!        Ke        : saturation parameter (uE m-2 s-1)
!        Eb        : background irradiance at depth DEPTH

!    Output parameters
!        F1     : function value of equation in DERIV
!        FDER   : the derivative of the function
!        r      : the predator's visual range (when F1=0)
!        IER    : = 1, No convergence after IEND steps. Error return.
!                  = 2, Return in case of zero divisor.
!                  = 3, r out of allowed range (negative)
!                  = 0, valid r returned
if (test99 == 1) write(10,*) "SRgetr"


!     Initial guess of visual range (RST)
      CALL EASYR(RST,C0,Ap,Vc,Ke,Eb)


!     Upper boundary of allowed error of visual range
      EPS = .0000001
!     Maximum number of iteration steps
      IEND = 100

!     Prepare iteration
      r = RST
      TOL = r
      CALL DERIV(r,F1,FDER,c,C0,Ap,Vc,Ke,Eb)
      TOLF = 100. * EPS

!     Start iteration expmt
      DO 6 I = 1, IEND
         IF (F1 == 0) goto 7

!        Equation is not satisfied by r
         IF (FDER == 0) goto 8

!        Iteration is possible
         DX = F1/FDER
         r = r-DX

!        Test on allowed range
         IF (r .LT. 0.) GOTO 9

         TOL = r
         CALL DERIV(r,F1,FDER,c,C0,Ap,Vc,Ke,Eb)

!        Test on satisfactory accuracy
         TOL = EPS
         AS = ABS(r)
         IF ((AS-1) > 0) TOL = TOL*AS
         TOL = TOL*AS
         IF ((ABS(DX)-TOL) > 0.) GOTO 6
         IF ((ABS(F1)-TOLF) .LE. 0.) GOTO 7
6     CONTINUE

!     No convergence after IEND steps. Error return.
!      IER = 1
7     RETURN
!     Return in case of zero divisor
!     IER = 2
8      RETURN
!     r out of allowed range (negative)
!     IER = 3
9      RETURN

      END

!     -------------------------------------------------------------
      SUBROUTINE EASYR(r,C0,Ap,Vc,Ke,Eb)
!     -------------------------------------------------------------
!     Obtain a first estimate of visual range by using a simplified
!     expression of visual range

      REAL       r,C0,Ap,Vc,Ke,Eb
      REAL       R2
!
!     Se calling routine for explanation of parameters
!
      R2= ABS(C0)*Ap*Vc*Eb/(Ke+Eb)
      r = SQRT(R2)
      RETURN
      END


!     -----------------------------------------------------
      SUBROUTINE DERIV(r,F1,FDER,c,C0,Ap,Vc,Ke,Eb)
!     -----------------------------------------------------
!
!     Derivation of equation for visual range of a predator

      REAL    r,c,C0,Ap,Vc,Ke,Eb
      REAL    FR1,FR2,F1,FDER

!     Input parameters
!        Se explanation in calling routine
!
!    Output parameters
!        F1     : function value of equation in DERIV
!        FDER   : the derivative of the function
!        r      : the predator's visual range (when F1=0)
!
!       The function and the derivative is calculated on the basis of the
!       log-transformed expression


       FR2=ALOG(ABS(C0)*Ap*Vc)
       FR1=ALOG(((Ke+Eb)/Eb)*r*r*EXP(c*r))
       F1 = FR1-FR2
       FDER = c + 2./r

      RETURN
      END


!---------------------------------------------------------------------------
subroutine SRopenclose(runtag,srerr,test99,expmt,gen,generations,during)
!---------------------------------------------------------------------------
implicit none
!variables from main program
character(6) runtag
integer expmt,gen,generations,srerr,test99,during
!local variables
character(16) string1
character(11) string2
character(41) stringhab
character(41) stringmut
character(42) stringIH
!character(41) string5g
character(41) stringdep
integer gg,num,i


gg = generations
num = expmt

if (test99 == 1) write(10,*) "SRopenclose"
if (test99 == 1 .and. srerr == 4) write(10,*) "SRopenclose # 1"


if ((gen == gg/10) .or. (gen == 2*gg/10) .or. (gen == 3*gg/10) &
    .or. (gen == 4*gg/10) .or. (gen == 5*gg/10) .or. (gen == 6*gg/10)     &
    .or. (gen == 7*gg/10)                                                 &
    .or. (gen == 8*gg/10) .or. (gen == 9*gg/10) .or. (gen == gg)) then
    close(11)
    close(20)
    close(30)
    close(39)
end if

if (test99 == 1 .and. srerr == 4) write(10,*) "SRopenclose # 2"

if (num < 10) then
   write(string1, 5001)"HED18-",runtag,"-E0",num
  else
   write(string1, 5002)"HED18-",runtag,"-E",num
end if

if (test99 == 1 .and. srerr == 4) write(10,*) "SRopenclose # 3"

if (gen == generations/10) then
    string2 ="-part02.txt"
  elseif (gen == 2*generations/10) then
    string2 ="-part03.txt"
  elseif (gen == 3*generations/10) then
    string2 ="-part04.txt"
  elseif (gen == 4*generations/10) then
    string2 ="-part05.txt"
  elseif (gen == 5*generations/10) then
    string2 ="-part06.txt"
  elseif (gen == 6*generations/10) then
    string2 ="-part07.txt"
  elseif (gen == 7*generations/10) then
    string2 ="-part08.txt"
  elseif (gen == 8*generations/10) then
    string2 ="-part09.txt"
  elseif (gen == 9*generations/10) then
    string2 ="-part10.txt"
end if

if (test99 == 1 .and. srerr == 4) write(10,*) "SRopenclose # 4"

write(stringhab, 5003)string1,"-o011-habitat",string2
write(stringmut, 5003)string1,"-o020-MutHist",string2
write(stringIH, 5004)string1,"-o030-HEDgene",string2
!write(string5g, 5004)string1,"-o031-5Ggenes",string2
write(stringdep, 5003)string1,"-o039-30depth",string2

if (test99 == 1 .and. srerr == 4) write(10,*) "SRopenclose # 5"

if ((during == 1 .and. gen .le. 8*gg/10) .or. &
      (gen > 8*gg/10 .and. gen .le. 9*gg/10)) then
   open(30, file = stringIH)
   open(11, file = stringhab)
   open(20, file = stringmut)
   open(39, file = stringdep)
   write(39,3902) 0.0,(i, i = 30,1,-1)
end if

if (test99 == 1 .and. srerr == 4) write(10,*) "SRopenclose # 6"

3902 format(F14.4,30I7)
5001 format(2A6,A3,I1)
5002 format(2A6,A2,I2)
5003 format(A17,A13,A11)
5004 format(A17,A13,A11)
end
!end SRopenclose



!---------------------------------------------------------------------------
subroutine SRendopenclose(expmt,realgen,runtag,test99)
!---------------------------------------------------------------------------
implicit none
!variables from main program
integer expmt,realgen,test99
character(6) runtag

!local variables
character(16) string1
character(41) stringFATHER
character(41) stringPREY
character(41) stringAFFECT
character(41) stringAGENT
character(52) string150bm1
character(52) string150age1
character(52) string150aff1
character(52) string150sto1

if (test99 == 1) write(10,*) "SRendopenclose"
close(31)
close(34)
close(35)
close(36)

close(60)
close(61)
close(62)
close(63)

if (expmt < 10) then
   write(string1, 5001)"HED18-",runtag,"-E0",expmt
  else
   write(string1, 5002)"HED18-",runtag,"-E",expmt
end if

if (realgen < 10) then
   write(stringFATHER, 5003)string1,"-o031-fathers-gener-",realgen,".txt"
   write(stringPREY, 5003)string1,"-o034-BM_at_z-gener-",realgen,".txt"
   write(stringAFFECT, 5003)string1,"-o035-3Daffect-gener",realgen,".txt"
   write(stringAGENT, 5003)string1,"-o036-3Daff@w-gener-",realgen,".txt"
   write(string150bm1, 5013)string1,"-o061-bmass-all-ages-genertion-",realgen,".txt"
   write(string150age1,5013)string1,"-o060-depth-all-ages-genertion-",realgen,".txt"
   write(string150aff1,5013)string1,"-o062-affect-all-ages-generatn-",realgen,".txt"
   write(string150sto1,5013)string1,"-o063-stcap_rst-all-ages-gener-",realgen,".txt"
  else if (realgen < 100) then
   write(stringFATHER, 5004)string1,"-o031-fathers-genr-",realgen,".txt"
   write(stringPREY, 5004)string1,"-o034-BM_at_z-gener",realgen,".txt"
   write(stringAFFECT, 5004)string1,"-o035-3Daffect-genr-",realgen,".txt"
   write(stringAGENT, 5004)string1,"-o036-3Daff@w-gener-",realgen,".txt"
   write(string150bm1, 5012)string1,"-o061-bmass-all-ages-generatn-",realgen,".txt"
   write(string150age1,5012)string1,"-o060-depth-all-ages-generatn-",realgen,".txt"
   write(string150aff1,5012)string1,"-o062-affect-all-ages-generat-",realgen,".txt"
   write(string150sto1,5012)string1,"-o063-stcap_rst-all-ages-genr-",realgen,".txt"
  else if (realgen < 1000) then
   write(stringFATHER, 5005)string1,"-o031-fathers-gen-",realgen,".txt"
   write(stringPREY, 5005)string1,"-o034-BM_at_z-genr",realgen,".txt"
   write(stringAFFECT, 5005)string1,"-o035-3Daffect-gen",realgen,".txt"
   write(stringAGENT, 5005)string1,"-o036-3Daff@w-genr",realgen,".txt"
   write(string150bm1, 5011)string1,"-o061-bmass-all-ages-generat-",realgen,".txt"
   write(string150age1,5011)string1,"-o060-depth-all-ages-generat-",realgen,".txt"
   write(string150aff1,5011)string1,"-o062-affect-all-ages-genera-",realgen,".txt"
   write(string150sto1,5011)string1,"-o063-stcap_rst-all-ages-gnr-",realgen,".txt"
  else if (realgen < 10000) then
   write(stringFATHER, 5006)string1,"-o031-father-gen-",realgen,".txt"
   write(stringPREY, 5006)string1,"-o034-BM_at_z-gen",realgen,".txt"
   write(stringAFFECT, 5006)string1,"-o035-3Daffect-gn",realgen,".txt"
   write(stringAGENT, 5006)string1,"-o036-3Daff@w-gen",realgen,".txt"
   write(string150bm1, 5010)string1,"-o061-bmass-all-ages-genera-",realgen,".txt"
   write(string150age1,5010)string1,"-o060-depth-all-ages-genera-",realgen,".txt"
   write(string150aff1,5010)string1,"-o062-affect-all-ages-gener-",realgen,".txt"
   write(string150sto1,5010)string1,"-o063-stcap_rst-all-ages-gn-",realgen,".txt"
  else if (realgen < 100000) then
   write(stringFATHER, 5007)string1,"-o031-father-gn-",realgen,".txt"
   write(stringPREY, 5007)string1,"-o034-BM_at_z-gn",realgen,".txt"
   write(stringAFFECT, 5007)string1,"-o035-3Daffect-g",realgen,".txt"
   write(stringAGENT, 5007)string1,"-o036-3Daff@w-gn",realgen,".txt"
   write(string150bm1, 5009)string1,"-o061-bmass-all-ages-gener-",realgen,".txt"
   write(string150age1,5009)string1,"-o060-depth-all-ages-gener-",realgen,".txt"
   write(string150aff1,5009)string1,"-o062-affect-all-ages-genr-",realgen,".txt"
   write(string150sto1,5009)string1,"-o063-stcap_rst-all-ages-g-",realgen,".txt"
  else if (realgen < 1000000) then
   write(stringFATHER, 5007)string1,"-o031-father-g-",realgen,".txt"
   write(stringPREY, 5007)string1,"-o034-BM_at_z-g",realgen,".txt"
   write(stringAFFECT, 5007)string1,"-o035-3Dafect-g",realgen,".txt"
   write(stringAGENT, 5007)string1,"-o036-3Daff@w-g",realgen,".txt"
   write(string150bm1, 5009)string1,"-o061-bmass-all-ages-genr-",realgen,".txt"
   write(string150age1,5009)string1,"-o060-depth-all-ages-genr-",realgen,".txt"
   write(string150aff1,5009)string1,"-o062-affect-all-ages-gen-",realgen,".txt"
   write(string150sto1,5009)string1,"-o063-stcap_rst-allages-g-",realgen,".txt"
end if

open(31,file=stringFATHER) !file for individual fatherhoods in last few generations
open(34,file=stringPREY) !file for 3D prey vertical distribution
open(35,file=stringAFFECT) !file for 3D vertical distribution of affect
open(36,file=stringAGENT) !file for 3D vertical distribution of agents

open(60, file = string150age1) !individual depth choice each age
open(61, file = string150bm1)  !individual body mass each age
open(62, file = string150aff1) !indvidual dominant affect each age
open(63, file = string150sto1) !individual stomach capacity each age

write(35,3570) "Age","Depth","Prey","Agents","Hungry","Afraid","Affect","Females","Afraid","Males","Afraid"
3570 format(2A7,4A9,A7,4A9)

5001 format(2A6,A3,I1)
5002 format(2A6,A2,I2)
5003 format(A16,A20,I1,A4)
5004 format(A16,A19,I2,A4)
5005 format(A16,A18,I3,A4)
5006 format(A16,A17,I4,A4)
5007 format(A16,A16,I5,A4)

5013 format(A16,A31,I1,A4)
5012 format(A16,A30,I2,A4)
5011 format(A16,A29,I3,A4)
5010 format(A16,A28,I4,A4)
5009 format(A16,A27,I5,A4)

end
!end SRendopenclose



!-------------------------------------------------------------------------
subroutine SRfileopen(expmt,file,runtag,gABM,test99,during)
!-------------------------------------------------------------------------
implicit none
!variables from main program
character(6) runtag
integer expmt,file,gABM,test99,during
!local variables
integer g
character(16) string1
!character(33) string2
character(33) stringpop
character(35) stringerr
!character(35) stringhed
character(37) stringhed3D
!character(37) stringFHd
character(39) stringdvm
character(39) stringgav
character(39) stringgsd
character(39) stringinp
character(39) stringraw
character(40) stringdaf
character(41) stringinf
character(41) stringfrq
character(41) stringper
!character(33) string5g   !ikke funnet
character(41) stringmidgen
character(41) stringdep
character(41) stringmut
character(41) stringhab
character(41) stringIH
character(42) string100
character(42) stringday
character(42) stringgenD
character(44) stringgen1N
character(44) stringgen2N
character(44) stringnight
character(44) stringcop
character(45) string2night
character(45) stringact
character(45) stringalf
character(46) stringavA
character(46) stringsdA
character(47) stringLNGf
character(51) stringLNG
character(50) stringopen

character(41) stringgHFL
character(38) stringPHFL
character(41) stringgHFO
character(39) stringPHFO
character(40) stringgHHF
character(37) stringPHHF
character(41) stringgHHO
character(39) stringPHHO

character(8) string3
character(33) stringABM

character(50) stringgAFP
character(50) stringPAFP
character(50) stringgAHF
character(50) stringPAHF
character(50) stringgAFL
character(50) stringPAFL
character(50) stringgAFO
character(50) stringPAFO
character(50) stringgAHS
character(50) stringPAHS

integer num,i

num = expmt
if (num < 10) then
   write(string1, 5001)"HED18-",runtag,"-E0",num
  else
   write(string1, 5002)"HED18-",runtag,"-E",num
end if


!competition between subpopulations if FILE = 1
if (file == 1) then
  write(stringopen,5015)string1,"-o016-motherpopulation-success.txt"
  open(16, file = stringopen)
  write(16,1615)"gen","pop0","pop1","pop2","pop3","pop4","pop5","pop6"
1615 format(11A8)
endif

write(stringpop,    5006)string1,"-o012-popdyn.txt"
!write(stringhed,    5011)string1,"-o025-avgF&H@z.txt"
write(stringerr,    5011)string1,"-o010-warnings.txt"
write(stringhed3D,  5012)string1,"-o035-avgF&H@z3D.txt"
write(stringPHHF,   5024)string1,'-o135-P0-HH-food.txt'
write(stringPHFL,   5014)string1,'-o131-P0-HF-light.txt'
write(stringdvm,    5008)string1,"-o028-DVM-200-inds.txt"
write(stringgav,    5008)string1,"-o018-geneplot-avg.txt"
write(stringgsd,    5008)string1,"-o019-geneplot-std.txt"
write(stringinp,    5008)string1,"-o022-iperceptSTAT.txt"
write(stringraw,    5008)string1,"-o023-irawdataSTAT.txt"
write(stringdaf,    5005)string1,"-o015-depth&feeling.txt"
write(stringPHFO,   5008)string1,'-o133-P0-HF-others.txt'
write(stringPHHO,   5008)string1,'-o137-P0-HH-others.txt'
write(stringgHHF,   5005)string1,'-o134-gamma-HH-food.txt'
write(stringinf,    5003)string1,"-o032-finfo_0-1_STAT.txt"
write(stringper,    5003)string1,"-o014-max-perception.txt"
write(stringfrq,    5003)string1,"-o033-finfo_0-1_FREQ.txt"
if (during == 1) then !make files many times during evolution
   write(stringhab,    5003)string1,"-o011-habitat-part01.txt"
   write(stringmut,    5003)string1,"-o020-MutHist-part01.txt"
   write(stringdep,    5003)string1,"-o039-30depth-part01.txt"
   write(stringIH,     5003)string1,"-o030-HEDgene-part01.txt"
 else !only make print at end
   write(stringhab,    5003)string1,"-o011-habitat-part10.txt"
   write(stringmut,    5003)string1,"-o020-MutHist-part10.txt"
   write(stringdep,    5003)string1,"-o039-30depth-part10.txt"
   write(stringIH,     5003)string1,"-o030-HEDgene-part10.txt"
end if
write(stringmidgen, 5003)string1,"-o043-genome-halfway.txt"
write(string100,    5004)string1,"-o050-200_individuals.txt"
write(stringday,    5004)string1,"-o052-200_inds_midday.txt"
write(stringgenD,   5004)string1,"-o041-genome-last-day.txt"
write(stringgHFL,   5003)string1,'-o130-gamma-HF-light.txt'
write(stringgHFO,   5003)string1,'-o132-gamma-HF-other.txt'
write(stringgHHO,   5003)string1,'-o136-gamma-HH-other.txt'
write(stringnight,  5007)string1,"-o053-200_inds_midnight.txt"
write(stringgen1N,  5007)string1,"-o040-genome-prev-night.txt"
write(stringgen2N,  5007)string1,"-o042-genome-last-night.txt"
write(stringcop,    5007)string1,"-o013-copepod-vert-dist.txt"
write(stringalf,    5010)string1,"-o024-allele_frequencies.txt"
write(string2night, 5010)string1,"-o054-200_inds_prevnight.txt"
write(stringact,    5010)string1,"-o026-active_allele-freq.txt"
write(stringavA,    5013)string1,"-o020-geneplot-active-avg.txt"
write(stringsdA,    5013)string1,"-o021-geneplot-active-std.txt"
write(stringLNGf,   5013)string1,"-o026-late-normal-fathers.txt"
write(stringLNG,    5025)string1,"-o027-late-normal-generations.txt"
write(stringgAFL,   5020)string1,'-o160-g1-AF-light.txt'
write(stringPAFL,   5020)string1,'-o161-g2-AF-light.txt'
write(stringgAFO,   5021)string1,'-o162-g1-AF-others.txt'
write(stringPAFO,   5021)string1,'-o163-g2-AF-others.txt'
write(stringgAFP,   5023)string1,'-o164-g1-AF-predators.txt'
write(stringPAFP,   5023)string1,'-o165-g2-AF-predators.txt'
write(stringgAHS,   5022)string1,'-o166-g1-AH-stomach.txt'
write(stringPAHS,   5022)string1,'-o167-g2-AH-stomach.txt'
write(stringgAHF,   5019)string1,'-o168-g1-AH-food.txt'
write(stringPAHF,   5019)string1,'-o169-g2-AH-food.txt'


open(10, file = stringerr)
if (test99 == 1) write(10,*) "SRfileopen"

open(11, file = stringhab)
open(12, file = stringpop)
open(13, file = stringcop)
open(14, file = stringper)
open(15, file = stringdaf)
!open(16, file = stringrnk)
open(18, file = stringgav)
open(19, file = stringgsd)
open(20, file = stringmut)
!open(21, file = stringsdA)
open(22, file = stringinp)
open(23, file = stringraw)
open(24, file = stringalf)
!open(25, file = stringhed)
open(26, file = stringLNGf)
open(27, file = stringLNG)
open(28, file = stringdvm)
open(30, file = stringIH)
!open(31, file = stringfat)
open(32, file = stringinf)
open(33, file = stringfrq)
!open(34, file = stringprey3D)
open(35, file = stringhed3D)
!open(36, file = stringagent3D)
open(39, file = stringdep)
open(40, file = stringgen1N)
open(41, file = stringgenD)
open(42, file = stringgen2N)
!open(43, file = stringmidgen)
!open(50, file = string100)
open(52, file = stringday)
open(53, file = stringnight)
open(54, file = string2night)


open(130, file = stringgHFL)
open(131, file = stringPHFL)
open(132, file = stringgHFO)
open(133, file = stringPHFO)
open(134, file = stringgHHF)
open(135, file = stringPHHF)
open(136, file = stringgHHO)
open(137, file = stringPHHO)

open(160, file = stringgAFL)
open(161, file = stringPAFL)
open(162, file = stringgAFO)
open(163, file = stringPAFO)
open(164, file = stringgAFP)
open(165, file = stringPAFP)
open(166, file = stringgAHS)
open(167, file = stringPAHS)
open(168, file = stringgAHF)
open(169, file = stringPAHF)

do g = 1,gABM
   if (g<10) then
      write(string3, 5016)"-gABM-0",g
     else
      write(string3, 5017)"-gABM-",g
   end if
   write(stringABM,5018)string1,'-o',149+g,string3,'.txt'
   open(149+g,file = stringABM)
end do

write(39,3902) 0.0, (i, i = 30,1,-1)
3902 format(F14.4,30I7)
5001 format(2A6,A3,I1)
5002 format(2A6,A2,I2)
5003 format(A17,A24)
5004 format(A17,A25)
5005 format(A17,A23)
5006 format(A17,A16)
5007 format(A17,A27)
5008 format(A17,A22)
5010 format(A17,A28)
5011 format(A17,A18)
5012 format(A17,A20)
5013 format(A17,A29)
5014 format(A17,A21)
5015 format(A16,A34)
5016 format(A7,I1)
5017 format(A6,I2)
5018 format(A16,A2,I3,A8,A4)
5019 format(A16,A20)
5020 format(A16,A21)
5021 format(A16,A22)
5022 format(A16,A23)
5023 format(A16,A25)
5024 format(A17,A20)
5025 format(A17,A33)

end
! end of subroutine SRfileopen


!---------------------------------------------------------------------------
!            FUNCTIONS RELATED TO HEDONIC AFFECTS
!---------------------------------------------------------------------------
! REAL FUNCTION sigmoid (a, b, signal)
! REAL FUNCTION sigmoid (param,k,a,r,signal)
  REAL FUNCTION sigmoid(k,a,r,signal)

 !14.08.02: Function in general expression of hedonic feelings: 1/1+exp(a(-signalinp+b))
 !20.03.08: Modified from Sigrunn Eliassen's 3-param function by JG: k/[1+exp(a(-signal+r))]


 implicit none
 real, intent(in) :: k        ! gene or constant that determines maximum of affect
 real, intent(in) :: r        ! gene or constant that determines area for maximum sensitivity to signal; when
                              ! signal is r, the function yields k/2
 real, intent(in) :: a        ! gene or constant that determines shape (growth rate) of curve for signal near r
 real, intent(in) :: signal   ! input value of (external or internal) stimulus
! integer, intent(in) :: param ! determination of number of genes in sigmoid function


 !local variable:
 real comp,a2

! if (param == 2) then
!      a2 = 5.
!   else
!      a2 = (5.*a)**3
!      a2 = NINT((0.75 + a)**6)
a2 = 10.*(0.5 + a)**4
!   end if

 comp = a2*(r-signal)

 !prevention of MATH overflow for exp(88) or larger
 if (comp < 80.) then
     sigmoid = k/(1. + exp(comp))
   else
     sigmoid = 0.
 end if



 END FUNCTION sigmoid


!---------------------------------------------------------------------------
 REAL FUNCTION linear(Amax,P1,P2,signal)


 !10.05.10: Linear function for general expression of affect
 !A window of sensitivity is assumed, in which a linear response between perception and affect
 !Weaker signals are neglected, stronger signals cannot yield more than maximum affect


 implicit none
 real, intent(in) :: Amax    ! gene or constant that determines maximum of affect

 real, intent(in) :: P1      ! P1 and P2 are genes or constants that deternimes the
                             ! minimum perception strength to avoid neglect and the
 real, intent(in) :: P2      ! minimum perception strength to yield Amax

 real, intent(in) :: signal  ! input value of (external or internal) perception stimulus


 !local variable:
 real Pmin,Pmax,slope

 !make sure the smallest really is the smallest
 Pmin = min(P1,P2)
 Pmax = max(P1,P2)

!find slope: delta y/delta x
 if (Pmax == Pmin) then !avoid division by zero
     slope = Amax/0.01
   else
     slope = Amax/(Pmax-Pmin)
 end if

!find affect strength
 if (signal < Pmin) then
    linear = 0.
   else if (signal > Pmax) then
    linear = Amax
   else
    linear = slope*(signal-Pmin)
 end if

 END FUNCTION linear


!---------------------------------------------------------------------------
 REAL FUNCTION gamma2gene(g,P0,signal,errorP)

 !06.08.10: Gamma function for general expression of affect
 !A 2-gene function which can take both sigmoid and linear shape, with or without a
 !window of neglect. Maximum affect is 1, minimum is 0.
 !Included after suggestion from Marc Mangel

 implicit none
 real, intent(in) :: g       ! gene or constant that determines the shape of the gamma function
 real, intent(in) :: P0      ! gene or constant for the signal strength giving half max affect
 real, intent(in) :: signal  ! input value of (external or internal) perception stimulus
 real, intent(in) :: errorP  ! inherent uncertainty in measurement of strength of perception stimulus

 !local variables:
 real P10,d1,s,perception,random
!real perceptionerror


!Adjust measurement for error in observation (include error..)
call random_number (random)
perception = signal * (1.+ 2*(random-0.5)*errorP)

 !the allele range is (0,1], while the function value range is (0.1, 10]
 P10 = max(P0, 0.0001) !prevent division by zero, assuming alleles of genes should be minimum 0.01
 s = min(1.0,perception) !no affect stronger than at signal 1.0
 s = max(0.0,s) !no negative signals

 d1 = (s/P10)**g
 gamma2gene = d1/(1.+d1)

 END FUNCTION gamma2gene


!---------------------------------------------------------------------------
 REAL FUNCTION gamma1gene(number,strength,signal,errorP)

 !In gamma1gene there is a set of predefined gamma functions (predefined gamma, P0),
 !and the single gene (g) gives the number for the coresponding two-gene function
 !The next gene "strength" scales the strength of the affect


 implicit none
 real, intent(in) :: number    ! gene giving the number of the gamma function to be used
 real, intent(in) :: strength  ! gene giving the indivudual weighting og the affect
 real, intent(in) :: signal    ! input value of (external or internal) perception stimulus
 real, intent(in) :: errorP    ! inherent uncertainty in measurement of strength of perception stimulus

 !local variables:
 real g,P0,d1,s,perception,random
 !real perceptionerror

 P0 = 1. !for all functions
 !now assuming 10 predefined functions, given by g
 if (number < 0.55) then !lower half
      if (number < 0.15) then         !function 1
         g = 0.1
        else if (number < 0.25) then  !function 2
         g = 0.3
        else if (number < 0.35) then  !function 3
         g = 0.6
        else if (number < 0.45) then  !function 4
         g = 1.0
        else                          !function 5
         g = 1.5
     end if
   else
      if (number < 0.65) then         !function 6
         g = 2.
        else if (number < 0.75) then  !function 7
         g = 3.
        else if (number < 0.85) then  !function 8
         g = 5.
        else if (number < 0.95) then  !function 9
         g = 10.0
        else                          !function 10
         g = 20.0
      end if
end if



!Adjust measurement for error in observation (include error..)
call random_number (random)
perception = signal * (1.+ 2*(random-0.5)*errorP)

 !the allele range is (0,1], while the function value range is (0.1, 20]
 s = min(1.0,perception) !no affect stronger than at signal 1.0
 s = max(0.0,s) !no negative signals
 d1 = (s/P0)**g

 gamma1gene = strength*d1/(1.+d1)

 END FUNCTION gamma1gene

!---------------------------------------------------------------------------
 REAL FUNCTION linearABM(P,signal,gABM,errorP)


! 9 June 2010
! The affect multiplication factors for AGE or BODY MASS in affect equations must have a value for any signal.
! Therefore, the stepwise linear function for all other perceptions cannot be used here.
! Rather, gABM affect values are all evolved, and affect at each possible SIGNAL is interpolated.

!The individual is unable to precicely measure its body mass (or age),
! bound by the error magnitude of parameter errorP

 implicit none
 real, intent(in) :: P(10)    ! gene or constant that determines affect at perception intervals
 real, intent(in) :: signal   ! input value of (external or internal) perception stimulus
 real, intent(in) :: errorP   ! inherent uncertainty in measurement of strength of perception stimulus
 integer gABM

 !local variable:
 real step,steps,lower,higher,perception,random
 !real perceptionerror
 integer g

steps = (gABM-1)*1. !1 step fewer than Px-es
step = 1./steps


!Adjust measurement for error in observation (include error..)
call random_number (random)
perception = signal * (1.+ 2*(random-0.5)*errorP)

if (perception .le. 0.) then
    linearABM = P(1)
   else if (perception > 1.) then
    linearABM = P(gABM)
   else
    do g = 1,gABM-1
      if (perception > (g-1)*step .and. perception .le. g*step) then
         lower = (perception-(g-1)*step)*P(g+1)
         higher = (g*step-perception)*P(g)
         linearABM = (lower+higher)*steps !average weighted against distance to P(g) and P(g+1)
         goto 1111
      end if
    end do
1111 continue
end if


END FUNCTION linearABM
