!Commonfish.txt for HED18 (used in simulations in Giske et al 2014 Proc R Soc B)
integer, parameter:: Cdepth = 30, Cfish = 10500, Cfinput = 19, Cfmothers = 10,  Cmaxlife = 5000, CgABM = 5
!the "C"-parameters above correspond to parameters without "C" in the main program

integer acreation(Cfish)      !creation age of first foremother of ind
integer gcreation(Cfish)      !creation generation of first foremother of ind
integer icreation(Cfish)      !individual number of first foremother of ind
integer mcreation(Cfish)      !creation method of first foremother of ind
integer motherpop(Cfish)      !population from which first foremother of ind was read from
integer motherstring(Cfish,Cfmothers) !array of individual-number for n of previous mothers
integer eggstring(Cfish,Cfmothers)    !array of individual-number for n of previous mothers at the egg-stage
integer foffspring(Cfish)     !the final number of offspring produced by a fish
integer fstatus(Cfish)        !mortal status of individual (1 = alive, 0 = dead)
integer father(Cfish)         !number of fatherly offspring a fish produces
integer fbirthdepth(Cfish)    !the depth where the fish was born, the 2 depth choices of its parent
integer fdeathaffect(Cfish)   !dominant affect at death of an individual (0: hunger 1: fear)
integer fdeathage(Cfish)      !age of death of an individual
integer fdeathBM(Cfish)       !body mass of an individual when dying
integer fdeathdepth(Cfish)    !depth of an individual when dying
integer fdepthchoice(Cfish,0:1)   !the previous (0) and present (1) depth the fish chose to move to
integer fgender(Cfish)        !the sex of an individual (1: female, 2: male)
integer findfeel(Cfish)       !the current feeling of an individual (0: hunger 1: fear)
integer fnumberafraid(Cfish)  !number of time steps an agent is afraid during life
integer fneverafraid(Cfish)   !1 for an agent who is never afraid during life, 0 otherwise
integer mutAFlight(Cfish,3)   !number of mutations in genes in fear of light affect component
integer mutAFmort(Cfish,3)    !number of mutations in genes in fear of predator presence affect component
integer mutAFother(Cfish,3)   !number of mutations in genes in fear due to lack of conspecific affect component
integer mutAHfood(Cfish,3)    !number of mutations in genes in hunger from seeing food affect component
integer mutAHsize(Cfish,CgABM)  !number of mutations in genes in fear due to size affect component
integer mutAHstom(Cfish,3)    !number of mutations in genes in hunger from stomach capacity affect component
integer mutHFlight(Cfish,3)   !number of mutations in genes in fear of light hedonic component
integer mutHFother(Cfish,3)   !number of mutations in genes in fear due to lack of conspecific hedonic component
integer mutHHfood(Cfish,3)    !number of mutations in genes in hunger from seeing food hedonic component
integer mutHHother(Cfish,3)   !number of mutations in genes in hunger stress from seeing competitors hedonic component
integer mutMemo(Cfish,2)      !number of mutations in genes for memory of fear (1) and hunger (2)

real fbodymass(Cfish)         !current body weight of ind
real fbodymax(Cfish)          !historic maximum body weight of ind
real fgonad(Cfish)            !preliminary estimate of number of offspring produced by each fish
real fstomach(Cfish,0:1)      !stomach contents of individual after feeding at previous (0) and current (1) age
real fmemory(Cfish,2)         !memory (brain chemical level) of individual for each feeling at beginning of age
real geneAFlight(Cfish,3)     !linear or sigmoid genes in fear of light affect component
real geneAFmort(Cfish,3)      !linear or sigmoid genes in fear of predator presence affect component
real geneAFother(Cfish,3)     !linear or sigmoid genes in fear due to lack of conspecific affect component
real geneAHfood(Cfish,3)      !linear or sigmoid genes in hunger from seeing food affect component
real geneAHsize(Cfish,CgABM)  !linear or sigmoid genes in fear due to size affect component
real geneAHstom(Cfish,3)      !linear or sigmoid genes in hunger from stomach capacity affect component
real geneHFlight(Cfish,3)     !linear or sigmoid genes in fear of light hedonic component
real geneHFother(Cfish,3)     !linear or sigmoid genes in fear due to lack of conspecific hedonic component
real geneHHfood(Cfish,3)      !linear or sigmoid genes in hunger from seeing food hedonic component
real geneHHother(Cfish,3)     !linear or sigmoid genes in hunger stress from seeing competitors hedonic component
real geneMemo(Cfish,2)        !genes for memory of fear (1) and hunger (2)

integer favgkids(500)         !number of kids produced in 500 last generations
integer favgstart(500)        !number of individuals at start of 500 last generations
integer favgsurv(500)         !# of individuals at end of 500 last generations
integer favgNeverA(500,2)     !# of individuals never afraid in 500 last gener for each sex
integer finobs(Cmaxlife)      !number of the value of an input neuron each age in a generation
integer itrans(Cfinput,21)    !# of perception values in each of 20 range values (step 0.05) of each sense in a gener
integer zavgNeverA(2)         !avg # of inds never afraid in 500 last generations for each sex
integer autocop(Cdepth,0:Cmaxlife)      !autocorrelated number of copepods in each depth
integer numcop(Cdepth,0:Cmaxlife)       !number of copepods in each depth
integer numcopBAS(Cdepth,0:Cmaxlife)    !number of copepods in each depth in a stardard generation
integer numfish(Cdepth,0:Cmaxlife)      !number of fish in each depth day and night
integer numHF(2,2)            !number of occasions of an hungry and an afraid agent of each sex
integer Aalleles(7)           !current number of alleles in all affect genes,gABM,gFear,Pfear,gHunger,PHunger
integer Halleles(12)          !current number of alleles in each of 12 hedonic genes
integer zgAalleles(500,7)     !# of alleles in 500 last gener in all affect genes,gABM,gFear,Pfear,gHunger,PHunger
integer zgHalleles(500,12)    !number of alleles in 500 last generations in each of 12 hedonic genes

real zeAalleles(7)            !avg # alleles in 500 last gener in all affect genes,gABM,gFear,Pfear,gHunger,PHunger
real zeHalleles(12)           !average number of alleles in 500 last generations in each of 12 hedonic genes
real Hdominant(12)            !most common allele in each of 12 hedonic genes
real Hdomfreq(12)             !frequency of most common allele in each of 12 hedonic genes
real autosurlig(0:Cmaxlife)   !autocorrelated light intensity just beneath surface during a time step
real autotemp(0:Cdepth)       !autocorelated temperature (centigrades) in each depth, constant over time
real didgain(Cdepth)          !density-independent feeding gain at each depth during a time step
real didrisk(Cdepth)          !density-independent mortality risk at each depth during a time step
real favgpctA(500,2)          !percentage of fear in each of 500 last generations for each sex
real fzEggDepth(500)          !average birth depth in 500 last generations
real fzAVGlocal(6,500,2)  !avg # other agents in same cell for each sex first and last midday and midnight in 500 last gener
real fzAVGBM(6,500,2)     !average body mass of each sex first and last midday and midnight in 500 last generations
real fzAVGdepth(6,500,2)  !average depth for each sex first and last midday and midnight in 500 last generations
real ffeeling(Cdepth,Cmaxlife)  !average of hedonic feeling of inhabitants of a depth at a time
real fgain(0:Cdepth+1)        !contribution to body mass by staying in depth i in current time step
real finavg(Cfinput,Cmaxlife) !average value of an input neuron during a generation
real finfo(Cfinput)           !value of input information for a fish (information at a sensory organ)
real finmax(Cfinput)          !maximum value of an input neuron during a generation
real finmin(Cfinput)          !minimum value of an input neuron during a generation
real finsum(Cfinput,Cmaxlife) !sum of an input neuron during a generation
real frisk(0:Cdepth+1)        !mortality risk in depth in current time step
real fzavgR0(500)             !R0 in 500 last generations
real surlig(0:Cmaxlife)       !light intensity just beneath surface during a time step
real temperature(0:Cdepth)    !temperature (centigrades) in each depth, constant over time
real visran(0:Cdepth+1,0:1)   !visual range at each depth at past and current time step
real zavgpctA(2)              !average frequency of fear in 500 last generations for each sex
real zavgDepth(6,2)           !average depth at 6 times in 500 last generations for each sex
real zavgBM(6,2)              !average body mass at 6 times in 500 last generations for each sex
real zavgLocal(6,2)           !average number of neithbours at 6 times in 500 last generations for each sex

common //numfish,fbirthdepth,fdepthchoice,foffspring,findfeel,finobs,fdeathage,     &
         fdeathdepth,fdeathBM,fdeathaffect,favgstart,favgkids,motherstring,   &
         eggstring,numcopBAS,itrans,fstatus,numcop,autocop,father,acreation,icreation,     &
         mcreation,gcreation,numHF,fneverafraid,fnumberafraid,motherpop,zavgNeverA,  &
         favgNeverA,favgsurv,Aalleles,Halleles,zgAalleles,zgHalleles,mutAHsize,       &
         mutAFlight,mutAFother,mutAFmort,mutAHstom,mutAHfood,mutHFlight,mutHFother, &
         mutHHfood,mutHHother,mutMemo,fgender,fbodymass,fstomach,surlig,autosurlig,      &
         temperature,autotemp,ffeeling,frisk,fzavgR0,finfo,finmin,finmax,finavg,  &
         finsum,fmemory,geneAHsize,geneAFlight,geneAFother,geneAFmort,geneAHstom,     &
         geneAHfood,geneHFlight,geneHFother,geneHHfood,geneHHother,geneMemo,   &
         fgonad,fgain,didgain,didrisk,visran,fbodymax,zavgpctA,favgpctA,       &
         fzEggDepth,fzAVGdepth,fzAVGBM,fzAVGlocal,zavgDepth,zavgBM,    &
         zavgLocal,Hdominant,Hdomfreq,zeAalleles,zeHalleles
