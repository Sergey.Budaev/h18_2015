# The emotion system promotes diversity and evolvability

Giske, Jarl, University of Bergen
Eliassen, Sigrunn, University of Bergen
Fiksen, Øyvind, University of Bergen
Jakobsen, Per J., University of Bergen
Aksnes, Dag L., University of Bergen
Mangel, Marc, University of California System
Jørgensen, Christian, Hjort Centre for Marine Ecosystem Dynamics, Bergen, Norway
Fiksen, O., University of Bergen
Publication date: July 22, 2014
Publisher: Dryad

[https://doi.org/10.5061/dryad.m6k1r](https://doi.org/10.5061/dryad.m6k1r)

Git: [https://git.app.uib.no/Sergey.Budaev/h18_2015](https://git.app.uib.no/Sergey.Budaev/h18_2015)

## Citation

Giske, Jarl et al. (2014), Data from: The emotion system promotes diversity
and evolvability, Dryad, Dataset,
[https://doi.org/10.5061/dryad.m6k1r](https://doi.org/10.5061/dryad.m6k1r)

## Abstract

Studies on the relationship between the optimal phenotype and its environment
have had limited focus on genotype-to-phenotype pathways and their evolutionary
consequences. Here, we study how multi-layered trait architecture and its
associated constraints prescribe diversity. Using an idealized model of
the emotion system in fish, we find that trait architecture yields genetic
and phenotypic diversity even in absence of frequency-dependent selection or
environmental variation. That is, for a given environment, phenotype frequency
distributions are predictable while gene pools are not. The conservation of
phenotypic traits among these genetically different populations is due to
the multi-layered trait architecture, in which one adaptation at a higher
architectural level can be achieved by several different adaptations at a
lower level. Our results emphasize the role of convergent evolution and the
organismal level of selection. While trait architecture makes individuals more
constrained than what has been assumed in optimization theory, the resulting
populations are genetically more diverse and adaptable. The emotion system in
animals may thus have evolved by natural selection because it simultaneously
enhances three important functions, the behavioural robustness of individuals,
the evolvability of gene pools and the rate of evolutionary innovation at
several architectural levels.

## Usage Notes

**H18 FORTRAN code**
H18 is the FORTRAN program for evolving behaviour based on emotions in a
fish population. `H18.f90`

**Commonfish Common block for FORTRAN program**
This file contains a commonblock which will be read by and included in many
subroutines of the H18.f90 FORTRAN program. `Commonfish.txt`

**i101-parameters parameter file**
This file contains the parameter values the researcher may change for the
H18.f90 FORTRAN program for fish behaviour by emotion. The file will be read
by the FORTRAN program. `i101-parameters.txt`
